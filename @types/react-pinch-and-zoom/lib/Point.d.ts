import { Rect } from './Rect';
import { Size } from './Size';
interface Point {
    x: number;
    y: number;
}
declare function newOriginPoint(): Point;
declare function distance(p1: Point, p2: Point): number;
declare function midpoint(p1: Point, p2: Point): Point;
declare function offset(p: Point, origin: Point): Point;
declare function map(p: Point, f: (n: number) => number): Point;
declare function scale(p: Point, s: number): Point;
declare function sum(p1: Point, p2: Point): Point;
declare function isEqual(p1: Point, p2: Point): boolean;
declare function boundWithin(min: Point, current: Point, max: Point): Point;
declare function normalizePointInRect(point: Point, rect: Rect): {
    x: number;
    y: number;
};
declare function toSize(p: Point): Size;
export { Point, newOriginPoint, distance, midpoint, offset, sum, map, scale, isEqual, boundWithin, toSize, normalizePointInRect, };
