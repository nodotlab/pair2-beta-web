import { Point } from './Point';
import { Size } from './Size';
interface Rect {
    origin: Point;
    size: Size;
}
declare function isEqual(m: Rect, n: Rect): boolean;
export { Rect, isEqual };
