"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var PropTypes = require("prop-types");
var React = require("react");
var Point = require("./Point");
var Size = require("./Size");
var truncRound = function (num) { return Math.trunc(num * 10000) / 10000; };
var GUESTURE_TYPE;
(function (GUESTURE_TYPE) {
    GUESTURE_TYPE["UNSET"] = "GUESTURE_TYPE_UNSET";
    GUESTURE_TYPE["PAN"] = "GUESTURE_TYPE_PAN";
    GUESTURE_TYPE["PINCH"] = "GUESTURE_TYPE_PINCH";
})(GUESTURE_TYPE || (GUESTURE_TYPE = {}));
var PinchToZoom = (function (_super) {
    __extends(PinchToZoom, _super);
    function PinchToZoom(props) {
        var _this = _super.call(this, props) || this;
        _this.transform = {
            zoomFactor: 1.0,
            translate: Point.newOriginPoint(),
        };
        _this.currentGesture = GUESTURE_TYPE.UNSET;
        _this.pinchStartZoomFactor = 1.0;
        _this.pinchStartTouchMidpoint = Point.newOriginPoint();
        _this.pinchStartTranslate = Point.newOriginPoint();
        _this.pinchStartTouchPointDist = 0;
        _this.panStartPoint = Point.newOriginPoint();
        _this.panStartTranslate = Point.newOriginPoint();
        _this.state = {
            lastSingleTouchPoint: Point.newOriginPoint(),
        };
        return _this;
    }
    PinchToZoom.getTouchesCoordinate = function (syntheticEvent) {
        var parentNode = syntheticEvent.currentTarget.parentNode, nativeEvent = syntheticEvent.nativeEvent;
        if (!(parentNode instanceof HTMLElement) ||
            !(nativeEvent instanceof TouchEvent)) {
            return [];
        }
        var containerRect = parentNode.getBoundingClientRect();
        var rect = {
            origin: { x: containerRect.left, y: containerRect.top },
            size: {
                width: containerRect.width,
                height: containerRect.height,
            },
        };
        var touchList = nativeEvent.touches;
        var coordinates = [];
        for (var i = 0; i < touchList.length; i += 1) {
            var touch = touchList.item(i);
            if (touch) {
                var touchPoint = {
                    x: touch.clientX,
                    y: touch.clientY,
                };
                var p = Point.normalizePointInRect(touchPoint, rect);
                coordinates.push(p);
            }
        }
        return coordinates;
    };
    PinchToZoom.prototype.componentDidUpdate = function (prevProps) {
        if (prevProps.minZoomScale !== this.props.minZoomScale ||
            prevProps.boundSize.height !== this.props.boundSize.height) {
            this.zoomContentArea(this.props.minZoomScale);
            this.guardZoomAreaTranslate();
        }
    };
    PinchToZoom.prototype.componentDidMount = function () {
        var _this = this;
        setTimeout(function () {
            var startEvent = new TouchEvent('touchstart', {
                cancelable: true,
                bubbles: true,
            });
            var endEvent = new TouchEvent('touchend', {
                cancelable: true,
                bubbles: true,
            });
            if (_this.zoomAreaContainer) {
                _this.zoomAreaContainer.dispatchEvent(startEvent);
                _this.zoomAreaContainer.dispatchEvent(endEvent);
            }
        }, 0);
    };
    PinchToZoom.prototype.onPinchStart = function (syntheticEvent) {
        var _a = PinchToZoom.getTouchesCoordinate(syntheticEvent), p1 = _a[0], p2 = _a[1];
        this.pinchStartTouchMidpoint = Point.midpoint(p1, p2);
        this.pinchStartTouchPointDist = Point.distance(p1, p2);
        var _b = this.getTransform(), currentZoomFactor = _b.currentZoomFactor, currentTranslate = _b.currentTranslate;
        this.pinchStartZoomFactor = currentZoomFactor;
        this.pinchStartTranslate = currentTranslate;
    };
    PinchToZoom.prototype.onPinchMove = function (syntheticEvent) {
        var _a = PinchToZoom.getTouchesCoordinate(syntheticEvent), p1 = _a[0], p2 = _a[1];
        var pinchCurrentTouchPointDist = Point.distance(p1, p2);
        var deltaTouchPointDist = pinchCurrentTouchPointDist - this.pinchStartTouchPointDist;
        var newZoomFactor = this.pinchStartZoomFactor + deltaTouchPointDist * 0.01;
        this.zoomContentArea(newZoomFactor);
    };
    PinchToZoom.prototype.onPinchEnd = function () {
        this.guardZoomAreaScale();
        this.guardZoomAreaTranslate();
    };
    PinchToZoom.prototype.onPanStart = function (syntheticEvent) {
        var p1 = PinchToZoom.getTouchesCoordinate(syntheticEvent)[0];
        var currentTranslate = this.getTransform().currentTranslate;
        this.panStartPoint = p1;
        this.panStartTranslate = currentTranslate;
    };
    PinchToZoom.prototype.onPanMove = function (syntheticEvent) {
        var dragPoint = PinchToZoom.getTouchesCoordinate(syntheticEvent)[0];
        var currentZoomFactor = this.getTransform().currentZoomFactor;
        var origin = this.panStartPoint;
        var prevTranslate = this.panStartTranslate;
        var dragOffset = Point.offset(dragPoint, origin);
        var adjustedZoomOffset = Point.scale(dragOffset, 1 / currentZoomFactor);
        var nextTranslate = Point.sum(adjustedZoomOffset, prevTranslate);
        this.panContentArea(nextTranslate);
    };
    PinchToZoom.prototype.onPanEnd = function () {
        this.guardZoomAreaTranslate();
    };
    PinchToZoom.prototype.guardZoomAreaScale = function () {
        var currentZoomFactor = this.getTransform().currentZoomFactor;
        var _a = this.props, minZoomScale = _a.minZoomScale, maxZoomScale = _a.maxZoomScale;
        if (currentZoomFactor > maxZoomScale) {
            this.zoomContentArea(maxZoomScale);
        }
        else if (currentZoomFactor < minZoomScale) {
            this.zoomContentArea(minZoomScale);
        }
    };
    PinchToZoom.prototype.guardZoomAreaTranslate = function () {
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        var _a = this.getTransform(), currentZoomFactor = _a.currentZoomFactor, currentTranslate = _a.currentTranslate;
        var minZoomScale = this.props.minZoomScale;
        var _b = this.zoomAreaContainer, containerW = _b.clientWidth, containerH = _b.clientHeight;
        var _c = this.zoomArea, contentW = _c.clientWidth, contentH = _c.clientHeight;
        if (currentZoomFactor < minZoomScale) {
            return;
        }
        var boundSize = {
            width: containerW,
            height: containerH,
        };
        var contentSize = Size.scale({
            width: contentW,
            height: contentH,
        }, currentZoomFactor);
        var diff = Size.diff(boundSize, contentSize);
        var diffInPoint = Size.toPoint(diff);
        var unitScaleLeftTopPoint = Point.scale(diffInPoint, 1 / (2 * currentZoomFactor));
        var maxLeftTopPoint = Point.boundWithin(Point.newOriginPoint(), unitScaleLeftTopPoint, Point.map(unitScaleLeftTopPoint, truncRound));
        var unitScaleRightBottomPoint = Point.scale(diffInPoint, 1 / currentZoomFactor);
        var maxRightBottomPoint = {
            x: Math.min(unitScaleRightBottomPoint.x, maxLeftTopPoint.x),
            y: Math.min(unitScaleRightBottomPoint.y, maxLeftTopPoint.y),
        };
        var validatePos = Point.boundWithin(maxRightBottomPoint, currentTranslate, maxLeftTopPoint);
        if (!Point.isEqual(validatePos, currentTranslate)) {
            this.panContentArea(validatePos);
        }
    };
    PinchToZoom.prototype.panContentArea = function (pos) {
        this.setTransform({
            translate: pos,
        });
    };
    PinchToZoom.prototype.zoomContentArea = function (zoomFactor) {
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        var prevZoomFactor = this.pinchStartZoomFactor;
        var prevTranslate = this.pinchStartTranslate;
        var _a = this.zoomAreaContainer, containerW = _a.clientWidth, containerH = _a.clientHeight;
        var boundSize = {
            width: containerW,
            height: containerH,
        };
        var prevZoomSize = Size.scale(boundSize, prevZoomFactor);
        var nextZoomSize = Size.scale(boundSize, zoomFactor);
        var prevRectCenterPoint = {
            x: prevZoomSize.width / 2,
            y: prevZoomSize.height / 2,
        };
        var nextRectCenterPoint = {
            x: nextZoomSize.width / 2,
            y: nextZoomSize.height / 2,
        };
        var deltaTranslate = Point.scale(Point.offset(prevRectCenterPoint, nextRectCenterPoint), 1 / (zoomFactor * prevZoomFactor));
        var accumulateTranslate = Point.sum(deltaTranslate, prevTranslate);
        this.setTransform({
            zoomFactor: truncRound(zoomFactor),
            translate: accumulateTranslate,
        });
    };
    PinchToZoom.prototype.handleTouchStart = function (syntheticEvent) {
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        var nativeEvent = syntheticEvent.nativeEvent;
        if (!(nativeEvent instanceof TouchEvent)) {
            return;
        }
        this.zoomArea.style.transitionDuration = '0.0s';
        switch (nativeEvent.touches.length) {
            case 2:
                this.currentGesture = GUESTURE_TYPE.PINCH;
                this.onPinchStart(syntheticEvent);
                break;
            default: {
                var p1 = PinchToZoom.getTouchesCoordinate(syntheticEvent)[0];
                this.setState({ lastSingleTouchPoint: p1 });
                this.currentGesture = GUESTURE_TYPE.PAN;
                this.onPanStart(syntheticEvent);
            }
        }
    };
    PinchToZoom.prototype.handleTouchMove = function (syntheticEvent) {
        var nativeEvent = syntheticEvent.nativeEvent;
        if (!(nativeEvent instanceof TouchEvent)) {
            return;
        }
        switch (nativeEvent.touches.length) {
            case 2:
                if (this.currentGesture === GUESTURE_TYPE.PINCH) {
                    this.onPinchMove(syntheticEvent);
                }
                break;
            default:
                if (this.currentGesture === GUESTURE_TYPE.PAN) {
                    this.onPanMove(syntheticEvent);
                }
        }
    };
    PinchToZoom.prototype.handleTouchEnd = function (syntheticEvent) {
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        this.zoomArea.style.transitionDuration = '0.3s';
        if (this.currentGesture === GUESTURE_TYPE.PINCH) {
            this.onPinchEnd();
        }
        if (this.currentGesture === GUESTURE_TYPE.PAN) {
            this.onPanEnd();
        }
        this.currentGesture = GUESTURE_TYPE.UNSET;
    };
    PinchToZoom.prototype.autoZoomToLastTouchPoint = function () {
        var lastSingleTouchPoint = this.state.lastSingleTouchPoint;
        if (lastSingleTouchPoint.x === 0 && lastSingleTouchPoint.y === 0) {
            return;
        }
        this.autoZoomToPosition(lastSingleTouchPoint);
    };
    PinchToZoom.prototype.autoZoomToPosition = function (pos) {
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        var autoZoomFactor = 2.0;
        var _a = this.getTransform(), currentZoomFactor = _a.currentZoomFactor, currentTranslate = _a.currentTranslate;
        var zoomAreaContainerW = this.zoomAreaContainer.clientWidth;
        var zoomAreaContainerH = this.zoomAreaContainer.clientHeight;
        var zoomAreaX = (pos.x / currentZoomFactor - currentTranslate.x) * autoZoomFactor;
        var zoomAreaY = (pos.y / currentZoomFactor - currentTranslate.y) * autoZoomFactor;
        var deltaX = zoomAreaContainerW / 2 - zoomAreaX;
        var deltaY = zoomAreaContainerH / 2 - zoomAreaY;
        var inScaleTranslate = {
            x: deltaX / autoZoomFactor,
            y: deltaY / autoZoomFactor,
        };
        this.zoomArea.style.transitionDuration = '0.3s';
        this.setTransform({
            zoomFactor: autoZoomFactor,
            translate: {
                x: inScaleTranslate.x,
                y: inScaleTranslate.y,
            },
        });
        this.guardZoomAreaTranslate();
    };
    PinchToZoom.prototype.setTransform = function (_a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.zoomFactor, zoomFactor = _c === void 0 ? this.transform.zoomFactor : _c, _d = _b.translate, translate = _d === void 0 ? {
            x: this.transform.translate.x,
            y: this.transform.translate.y,
        } : _d;
        var _e = this.props, onTransform = _e.onTransform, minZoomScale = _e.minZoomScale;
        if (!this.zoomAreaContainer || !this.zoomArea) {
            return;
        }
        var roundTransalteX = Math.round(translate.x * 1000) / 1000;
        var roundTransalteY = Math.round(translate.y * 1000) / 1000;
        if (zoomFactor < minZoomScale * 0.8) {
            return;
        }
        this.transform.zoomFactor = zoomFactor;
        this.transform.translate.x = roundTransalteX;
        this.transform.translate.y = roundTransalteY;
        if (onTransform) {
            onTransform(this.transform);
        }
        var styleString = "\n        scale(" + zoomFactor + ")\n        translate(" + roundTransalteX + "px, " + roundTransalteY + "px)\n        translateZ(" + 0 + ")\n      ";
        this.zoomArea.style.transform = styleString;
        this.zoomArea.style.webkitTransform = styleString;
    };
    PinchToZoom.prototype.getTransform = function () {
        var _a = this.transform, zoomFactor = _a.zoomFactor, translate = _a.translate;
        return {
            currentZoomFactor: zoomFactor,
            currentTranslate: {
                x: translate.x,
                y: translate.y,
            },
        };
    };
    PinchToZoom.prototype.render = function () {
        var _this = this;
        var _a = this.props, debug = _a.debug, fillContainer = _a.fillContainer, className = _a.className, children = _a.children;
        var classNameList = ['', 'pinch-to-zoom-container'];
        var containerInlineStyle = {
            display: 'inline-block',
            overflow: 'hidden',
            backgroundColor: 'inherit',
            width: fillContainer ? '100%' : undefined,
            height: fillContainer ? '100%' : undefined,
        };
        var zoomAreaInlineStyle = {
            display: 'inline-block',
            willChange: 'transform',
            transformOrigin: '0px 0px 0px',
            transition: 'transform 0ms ease',
            transitionTimingFunction: 'cubic-bezier(0.1, 0.57, 0.1, 1)',
            transitionDuration: '0ms',
            perspective: 1000,
            width: '100%',
        };
        if (debug) {
            classNameList.push('debug');
            containerInlineStyle.backgroundColor = 'red';
        }
        return (React.createElement("div", { className: className.concat(classNameList.join(' ')), style: containerInlineStyle, onTouchStart: function (e) { return _this.handleTouchStart(e); }, onTouchMove: function (e) { return _this.handleTouchMove(e); }, onTouchEnd: function (e) { return _this.handleTouchEnd(e); }, ref: function (c) {
                _this.zoomAreaContainer = c || undefined;
            } },
            React.createElement("div", { className: "pinch-to-zoom-area", style: zoomAreaInlineStyle, ref: function (c) {
                    _this.zoomArea = c || undefined;
                } }, children)));
    };
    return PinchToZoom;
}(React.Component));
PinchToZoom.defaultProps = {
    debug: false,
    className: '',
    minZoomScale: 1.0,
    maxZoomScale: 4.0,
    boundSize: {
        width: 100,
        height: 100,
    },
    contentSize: {
        width: 100,
        height: 100,
    },
    fillContainer: false,
};
PinchToZoom.propTypes = {
    debug: PropTypes.bool,
    className: PropTypes.string,
    minZoomScale: PropTypes.number,
    maxZoomScale: PropTypes.number,
    boundSize: PropTypes.shape({
        width: PropTypes.number,
        height: PropTypes.number,
    }),
    contentSize: PropTypes.shape({
        width: PropTypes.number,
        height: PropTypes.number,
    }),
    fillContainer: PropTypes.bool,
    children: PropTypes.node,
};
exports.default = PinchToZoom;
//# sourceMappingURL=index.js.map