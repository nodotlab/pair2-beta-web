import { Point } from './Point';
interface Size {
    width: number;
    height: number;
}
declare function scale(k: Size, s: number): Size;
declare function diff(m: Size, n: Size): Size;
declare function isEqual(m: Size, n: Size): boolean;
declare function toPoint(s: Size): Point;
export { Size, scale, diff, isEqual, toPoint };
