"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point_1 = require("./Point");
var Size_1 = require("./Size");
function isEqual(m, n) {
    return Point_1.isEqual(m.origin, n.origin) && Size_1.isEqual(m.size, n.size);
}
exports.isEqual = isEqual;
//# sourceMappingURL=Rect.js.map