// import * as React from 'react';
// import * as Point from './Point';
// import * as Size from './Size';
// declare enum GUESTURE_TYPE {
//     UNSET = "GUESTURE_TYPE_UNSET",
//     PAN = "GUESTURE_TYPE_PAN",
//     PINCH = "GUESTURE_TYPE_PINCH"
// }
// interface PinchToZoomProps {
//     debug: boolean;
//     className: string;
//     minZoomScale: number;
//     maxZoomScale: number;
//     boundSize: Size.Size;
//     contentSize: Size.Size;
//     fillContainer?: boolean;
//     onTransform?: (transform: {
//         zoomFactor: number;
//         translate: Point.Point;
//     }) => void;
// }
// interface PinchToZoomState {
//     lastSingleTouchPoint: Point.Point;
// }
// declare class PinchToZoom extends React.Component<PinchToZoomProps, PinchToZoomState> {
//     static defaultProps: {};
//     static propTypes: {};
//     static getTouchesCoordinate(syntheticEvent: React.SyntheticEvent): Point.Point[];
//     transform: {
//         zoomFactor: number;
//         translate: Point.Point;
//     };
//     currentGesture: GUESTURE_TYPE;
//     pinchStartZoomFactor: number;
//     pinchStartTouchMidpoint: Point.Point;
//     pinchStartTranslate: Point.Point;
//     pinchStartTouchPointDist: number;
//     panStartPoint: Point.Point;
//     panStartTranslate: Point.Point;
//     zoomAreaContainer?: HTMLDivElement;
//     zoomArea?: HTMLDivElement;
//     constructor(props: PinchToZoomProps);
//     componentDidUpdate(prevProps: PinchToZoomProps): void;
//     componentDidMount(): void;
//     onPinchStart(syntheticEvent: React.SyntheticEvent): void;
//     onPinchMove(syntheticEvent: React.SyntheticEvent): void;
//     onPinchEnd(): void;
//     onPanStart(syntheticEvent: React.SyntheticEvent): void;
//     onPanMove(syntheticEvent: React.SyntheticEvent): void;
//     onPanEnd(): void;
//     guardZoomAreaScale(): void;
//     guardZoomAreaTranslate(): void;
//     panContentArea(pos: Point.Point): void;
//     zoomContentArea(zoomFactor: number): void;
//     handleTouchStart(syntheticEvent: React.SyntheticEvent): void;
//     handleTouchMove(syntheticEvent: React.SyntheticEvent): void;
//     handleTouchEnd(syntheticEvent: React.SyntheticEvent): void;
//     autoZoomToLastTouchPoint(): void;
//     autoZoomToPosition(pos: Point.Point): void;
//     setTransform({ zoomFactor, translate, }?: {
//         zoomFactor?: number | undefined;
//         translate?: {
//             x: number;
//             y: number;
//         } | undefined;
//     }): void;
//     getTransform(): {
//         currentZoomFactor: number;
//         currentTranslate: {
//             x: number;
//             y: number;
//         };
//     };
//     render(): JSX.Element;
// }
// export default PinchToZoom;


declare module 'react-pinch-and-zoom';