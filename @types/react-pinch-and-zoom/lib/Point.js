"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function newOriginPoint() {
    return {
        x: 0,
        y: 0
    };
}
exports.newOriginPoint = newOriginPoint;
function distance(p1, p2) {
    var x1 = p1.x, y1 = p1.y;
    var x2 = p2.x, y2 = p2.y;
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}
exports.distance = distance;
function midpoint(p1, p2) {
    return {
        x: (p1.x + p2.x) / 2,
        y: (p1.y + p2.y) / 2,
    };
}
exports.midpoint = midpoint;
function offset(p, origin) {
    return {
        x: p.x - origin.x,
        y: p.y - origin.y,
    };
}
exports.offset = offset;
function map(p, f) {
    return {
        x: f(p.x),
        y: f(p.y),
    };
}
exports.map = map;
function scale(p, s) {
    return map(p, function (v) { return v * s; });
}
exports.scale = scale;
function sum(p1, p2) {
    return {
        x: p1.x + p2.x,
        y: p1.y + p2.y,
    };
}
exports.sum = sum;
function isEqual(p1, p2) {
    return p1.x === p2.x && p1.y === p2.y;
}
exports.isEqual = isEqual;
function boundWithin(min, current, max) {
    var numberWithin = function (lower, num, upper) {
        return num > upper ? upper : lower > num ? lower : num;
    };
    return {
        x: numberWithin(min.x, current.x, max.x),
        y: numberWithin(min.y, current.y, max.y),
    };
}
exports.boundWithin = boundWithin;
function normalizePointInRect(point, rect) {
    return {
        x: point.x - rect.origin.x,
        y: point.y - rect.origin.y,
    };
}
exports.normalizePointInRect = normalizePointInRect;
function toSize(p) {
    return {
        width: p.x,
        height: p.y
    };
}
exports.toSize = toSize;
//# sourceMappingURL=Point.js.map