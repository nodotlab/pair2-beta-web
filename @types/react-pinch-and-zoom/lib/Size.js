"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function map(k, f) {
    return {
        width: f(k.width),
        height: f(k.height),
    };
}
function scale(k, s) {
    return map(k, function (v) { return v * s; });
}
exports.scale = scale;
function diff(m, n) {
    return {
        width: m.width - n.width,
        height: m.height - n.height,
    };
}
exports.diff = diff;
function isEqual(m, n) {
    return m.width === n.width && m.height === n.height;
}
exports.isEqual = isEqual;
function toPoint(s) {
    return {
        x: s.width,
        y: s.height
    };
}
exports.toPoint = toPoint;
//# sourceMappingURL=Size.js.map