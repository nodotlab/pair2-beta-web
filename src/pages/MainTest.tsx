import React from "react";

class MainTest extends React.Component<any> {
  status = {
    status: "200",
    message: "Heath check",
  };

  public render() {
    return <p>{JSON.stringify(this.status)}</p>;
  }
}

export default MainTest;
