import React from "react";
import { currency, timeFormat2, timeFormat6, timeFormat7 } from "../Method";
import { getMain } from "../db/Actions";
import imgSneaker from "../images/Thumbnail_Game_01.png";
import imgSneaker2 from "../images/Thumbnail_Game_02.png";
import imgSneaker3 from "../images/Thumbnail_Game_03.png";
import imgSneaker4 from "../images/Thumbnail_Game_04.png";
import imgSneaker5 from "../images/Thumbnail_Game_05.png";
import videoSneaker from "../images/Thumbnail_Game_06.png";
import imgAlso from "../images/Thumbnail_Product_03.png";
import imgBg from "../images/Game_Detail_BG.png";
import imgBgIn from "../images/Game_03.png";
import imgBullet from "../images/bullet.svg";


class Sneaker {
  public brand: string;
  public name: string;
  public name2: string;
  public size: string;
  public price: number;
  public imageUrl: string;
  public imageUrl2: string;
  public imageUrl3: string;
  public imageUrl4: string;
  public imageUrl5: string;
  public videoUrl: string;
  public releaseDate: number;
  public releasePrice: number;
  public serialNumber: string;
  public endTime: number;
  public presentation: string;

  constructor(
    _brand: string,
    _name: string,
    _name2: string,
    _size: string,
    _price: number,
    _imageUrl: string,
    _imageUrl2: string,
    _imageUrl3: string,
    _imageUrl4: string,
    _imageUrl5: string,
    _videoUrl: string,
    _releaseDate: number,
    _releasePrice: number,
    _serialNumber: string,
    _endTime: number,
    _presentation: string,
  ) {
    this.brand = _brand;
    this.name = _name;
    this.name2 = _name2;
    this.size = _size;
    this.price = _price;
    this.imageUrl = _imageUrl;
    this.imageUrl2 = _imageUrl2;
    this.imageUrl3 = _imageUrl3;
    this.imageUrl4 = _imageUrl4;
    this.imageUrl5 = _imageUrl5;
    this.videoUrl = _videoUrl;
    this.releaseDate = _releaseDate;
    this.releasePrice = _releasePrice;
    this.serialNumber = _serialNumber;
    this.endTime = _endTime;
    this.presentation = _presentation;
  }
}

class Size {
  public size: string;
  public active: boolean;
  public quantity: number;

  constructor(_size: string, _active: boolean, _quantity: number) {
    this.size = _size;
    this.active = _active;
    this.quantity = _quantity;
  }
}

class RecommendSneaker {
  public brand: string;
  public name: string;
  public size: string;
  public lowPrice: number;
  public imageUrl: string;
  public entered: boolean;
  public tooltip: boolean = false;

  constructor(_brand: string, _name: string, _size: string, _lowPrice: number, _imageUrl: string, _entered: boolean) {
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.lowPrice = _lowPrice;
    this.imageUrl = _imageUrl;
    this.entered = _entered;
  }
}

interface SneakersState {
  sneaker: Sneaker;
  sneakerSelectImage: string;
  sizes: Size[];
  recommendSneakers: RecommendSneaker[];
  selectSize: string;
  enteredMain: boolean;
}

class Sneakers extends React.Component<any, SneakersState> {
  constructor(props: any) {
    super(props);
    this.state = {
      sneaker: new Sneaker("", "", "", "", 0, "", "", "", "", "", "", 0, 0, "", 0, ""),
      sneakerSelectImage: imgSneaker,
      sizes: this.initSizes(),
      recommendSneakers: this.initRecommendSneakers(),
      selectSize: "",
      enteredMain: false,
    };
  }

  public componentDidMount() {
    // need main entered value
    this.initData();
  }

  private async initData() {
    const currentSneaker = new Sneaker(
      "NIKE",
      "Nike Air Force 1 Shadow Mystic Navy",
      "나이키 에어 포스 1 쉐도우 미스틱 네이비",
      "250",
      100000,
      imgSneaker,
      imgSneaker2,
      imgSneaker3,
      imgSneaker4,
      imgSneaker5,
      videoSneaker,
      Date.now(),
      100000,
      "AJ6844-005",
      Date.now(),
      "11월 23일 오전 10시 이후"
    );

    const result: any = await getMain();

    const tempArrary: RecommendSneaker[] = [];
    for (let i = 0; i < 4; ++i) {
      tempArrary.push(new RecommendSneaker(result[i].brand, result[i].name, result[i].releasePrice, result[i].releasePrice, result[i].thumbUrl, false));
    }

    this.setState({
      sneaker: currentSneaker,
      recommendSneakers: tempArrary
    });
  }

  private initSizes(): Size[] {
    const tempSizes = [];

    for (let i = 220; i < 315; i += 5) {
      if (i < 270) {
        tempSizes.push(new Size(i.toString(), false, 0));
      } else {
        tempSizes.push(new Size(i.toString(), false, 1));
      }
    }

    return tempSizes;
  }

  private initRecommendSneakers(): RecommendSneaker[] {
    const sneakers: RecommendSneaker[] = [];
    for (let i = 0; i < 4; ++i) {
      sneakers.push(new RecommendSneaker("Adidas", "Jordan 1 Retro High Obsidian UNC", "250", 210000, imgAlso, false));
    }

    return sneakers;
  }

  private handleSelectImage = (imageUrl: string): void => {
    this.setState({ sneakerSelectImage: imageUrl });
  }

  private handleSizeActive = (size: string): void => {
    const tempSizes: Size[] = this.state.sizes;

    for (let i = 0; i < tempSizes.length; ++i) {
      tempSizes[i].active = false;
      if (size === tempSizes[i].size && 0 !== tempSizes[i].quantity) {
        tempSizes[i].active = true;
      }
    }

    this.setState({ sizes: tempSizes, selectSize: size });
  }

  private handleTooltip = (index: number): void => {
    const tempData: RecommendSneaker[] = this.state.recommendSneakers;
    for (let i = 0; i < tempData.length; ++i) {
      if (i === index && !tempData[i].tooltip) {
        tempData[i].tooltip = true;
      } else {
        tempData[i].tooltip = false;
      }
    }

    this.setState({ recommendSneakers: tempData });
  }

  private handleEnter = (index: number): void => {
    const tempData: RecommendSneaker[] = this.state.recommendSneakers;
    for (let i = 0; i < tempData.length; ++i) {
      if (i === index) {
        tempData[i].entered = true;
      }
    }

    this.setState({ recommendSneakers: tempData });
  }

  public render() {
    return (
      <div>
      <div className="box-detail-first pb-0">
      <div className="container">
      <div className="row">
      <div className="col-md-8">
        <p className="p-18 roboto bold black-2 o-60 letter-space-4 mb-2 hidden-m">{this.state.sneaker.brand.toUpperCase()}</p>
        <p className="p-40 roboto black-2 line-48 hidden-m">{this.state.sneaker.name.toUpperCase()}</p>
        <p className="p-13 roboto bold black-2 o-60 letter-space-3 mb-2 pl-3 hidden-p">{this.state.sneaker.brand.toUpperCase()}</p>
        <p className="p-24 roboto black-2 hidden-p pl-3 pr-3 line-31 hidden-p">{this.state.sneaker.name.toUpperCase()}</p>
        <p className="p-13 bold roboto orange mt-2 pl-3 pr-3 hidden-p">{this.state.sneaker.name2}</p>

        <div className="box-detail-first-in" style={{backgroundImage: `url(${this.state.sneakerSelectImage})`}}>
        <p className="p-16 bold roboto orange mt-3 hidden-m">{this.state.sneaker.name2}</p>
        </div>

        <div className="col-md-8 offset-md-2 mt-4">
        <div className="row horizontal-detail mb-5">
        <div onClick={() => this.handleSelectImage(this.state.sneaker.imageUrl)}
          className={`detail-bar cursor ${this.state.sneaker.imageUrl === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.imageUrl})`}} />
        <div onClick={() => this.handleSelectImage(this.state.sneaker.imageUrl2)}
          className={`detail-bar cursor ${this.state.sneaker.imageUrl2 === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.imageUrl2})`}} />
        <div onClick={() => this.handleSelectImage(this.state.sneaker.imageUrl3)}
          className={`detail-bar cursor ${this.state.sneaker.imageUrl3 === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.imageUrl3})`}} />
        <div onClick={() => this.handleSelectImage(this.state.sneaker.imageUrl4)}
          className={`detail-bar cursor ${this.state.sneaker.imageUrl4 === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.imageUrl4})`}} />
        <div onClick={() => this.handleSelectImage(this.state.sneaker.imageUrl5)}
          className={`detail-bar cursor ${this.state.sneaker.imageUrl5 === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.imageUrl5})`}} />
        {this.state.sneaker.videoUrl !== "" &&
        <div onClick={() => this.handleSelectImage(this.state.sneaker.videoUrl)}
          className={`detail-bar cursor ${this.state.sneaker.videoUrl === this.state.sneakerSelectImage && "detail-bar-active"}`}
          style={{backgroundImage: `url(${this.state.sneaker.videoUrl})`}} />}
        </div>
        </div>
      </div>

      <div className="col-md-4">
      <div className="hidden-m mt-5">
      <div className="d-block">
        <div className="detail-left-inline">
        <p className="p-14 bold gray-2">발매일</p>
        </div>
        <div className="detail-right-inline">
        <p className="p-14 gray-3">{timeFormat2(this.state.sneaker.releaseDate)}</p>
        </div>
      </div>
      <div className="d-block mt-1">
        <div className="detail-left-inline">
        <p className="p-14 bold gray-2">발매가</p>
        </div>
        <div className="detail-right-inline">
        <p className="p-14 gray-3">{currency(this.state.sneaker.releasePrice)}원</p>
        </div>
      </div>
      <div className="d-block mt-1">
        <div className="detail-left-inline">
        <p className="p-14 bold gray-2">품번</p>
        </div>
        <div className="detail-right-inline">
        <p className="p-14 gray-3">{this.state.sneaker.serialNumber}</p>
        </div>
      </div>
      </div>

      <div className="container hidden-p">
      <div className="row detail-thirdbox">
      <div className="col-4 thirdbox-border">
        <p className="p-14 bold gray-2">발매일</p>
        <p className="p-14 gray-3">{timeFormat2(this.state.sneaker.releaseDate)}</p>
      </div>
      <div className="col-4 thirdbox-border">
        <p className="p-14 bold gray-2">발매가</p>
        <p className="p-14 gray-3">{currency(this.state.sneaker.releasePrice)}원</p>
      </div>
      <div className="col-4 thirdbox-border thirdbox-noneborder">
        <p className="p-14 bold gray-2">품번</p>
        <p className="p-14 gray-3">{this.state.sneaker.serialNumber}</p>
      </div>
      </div>
      </div>

      <p className="p-16 black-2 bold mt-4 mb-2">사이즈</p>
      {this.state.sizes.map((size: Size, i: number) =>
      <button onClick={() => this.handleSizeActive(size.size)} key={i}
        className={`btn btn-filter w-64 ${size.active && "btn-filter-active"} ${size.quantity === 0 && "btn-filter-disable"}`}>
        {size.size}
      </button>)}

      <div className="row mt-3">
      <div className="col-6">
        <p className="p-16 black-2 bold pt-3">판매가</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-32 bold orange">{currency(this.state.sneaker.price)}<span className="p-16">원</span></p>
      </div>
      </div>
      <div className="mt-5 hidden-p"></div>

      <div className="box-game-enter hidden-m">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">선택 사이즈 <span className="p-18 orange roboto ml-2">{this.state.selectSize}</span></p>
      </div>
      <div className="col-6 text-right">
        <p onClick={() => window.scrollTo(0, 0)} className="p-13 gray-2 underline pt-1">변경</p>
      </div>
      </div>
      <div className="row mt-3">
      <div className="col-12">
      {!this.state.enteredMain ?
      <button onClick={() => this.setState({ enteredMain: true })} className="btn btn-custom-active w-100">ENTER THE SHOE GAME</button> :
      <button className="btn btn-custom-disable w-100">ENTERED</button>}
      </div>
      </div>
      </div>

      <div className="bottom-box row hidden-p pt-3 pb-3">
      <div className="col-6">
        <p className="p-16 bold black-2">선택 사이즈 <span className="p-18 orange roboto ml-2">{this.state.selectSize}</span></p>
      </div>
      <div className="col-6 text-right">
        <p onClick={() => window.scrollTo(0, 650)} className="p-13 gray-2 underline pt-1">변경</p>
      </div>
      <div className="col-12 mt-2">
      {!this.state.enteredMain ?
      <button onClick={() => this.setState({ enteredMain: true })} className="btn btn-custom-active w-100">ENTER THE SHOE GAME</button> :
      <button className="btn btn-custom-disable w-100">ENTERED</button>}
      </div>
      </div>

      </div>
      </div>
      </div>
      </div>

      <div className="box-noti hidden-m" style={{backgroundImage: `url(${imgBg})`}}>
      <div className="container">
      <div className="row no-gutters">
      <div className="col-md-6 box-noti-left">
        <p className="p-18 bold roboto orange letter-sapce-5">~ &nbsp;{timeFormat6(this.state.sneaker.endTime)}&nbsp;&nbsp;&nbsp;{timeFormat7(this.state.sneaker.endTime)}</p>
        <p className="p-30 black-2 bold mt-1">정가 게임</p>
        <p className="p-14 gray-3 mt-3">
          발매 이후 다시 한번 정가로 구매할 수 있는 기회! <br />
          지금 PAIR2에서 참여하세요. <b>ENTER THE SHOE GAME</b>
        </p>
        <p className="p-14 black-2 mt-5">결과발표</p>
        <p className="p-13 gray-2 mt-2"><img src={imgBullet} /> {this.state.sneaker.presentation}</p>
        <p className="p-14 black-2 mt-3">유의사항</p>
        <p className="p-13 gray-2 mt-2"><img src={imgBullet} /> PAIR2 회원만이 슈게임에 참여할 수 있습니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 정가게임은 추첨방식으로 기간내 여유롭게 참여하세요.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 당첨자에게만 PAIR2 앱의 푸시알림으로 결과를 알려드립니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 당첨자는 'MYPAGE > 구매내역'에서 확인 및 구매할 수 있습니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> MM월 dd일(결과발표 일자 + 1일 ) 23시 59분까지 미 결제 시, 당첨은 취소됩니다.</p>
      </div>
      <div className="col-md-6">
        <div className="box-noti-in" style={{backgroundImage: `url(${imgBgIn})`}} />
      </div>
      </div>
      </div>
      </div>

      <div className="row hidden-p">
      <div className="box-noti-left">
        <p className="p-18 bold roboto orange letter-sapce-5">~ &nbsp;{timeFormat6(this.state.sneaker.endTime)}&nbsp;&nbsp;&nbsp;{timeFormat7(this.state.sneaker.endTime)}</p>
        <p className="p-30 black-2 bold mt-1">정가 게임</p>
        <p className="p-14 gray-3 mt-3">
          발매 이후 다시 한번 정가로 구매할 수 있는 기회! <br />
          지금 PAIR2에서 참여하세요. <b>ENTER THE SHOE GAME</b>
        </p>
        <p className="p-14 black-2 mt-5">결과발표</p>
        <p className="p-13 gray-2 mt-2"><img src={imgBullet} /> {this.state.sneaker.presentation}</p>
        <p className="p-14 black-2 mt-3">유의사항</p>
        <p className="p-13 gray-2 mt-2"><img src={imgBullet} /> PAIR2 회원만이 슈게임에 참여할 수 있습니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 정가게임은 추첨방식으로 기간내 여유롭게 참여하세요.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 당첨자에게만 PAIR2 앱의 푸시알림으로 결과를 알려드립니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> 당첨자는 'MYPAGE > 구매내역'에서 확인 및 구매할 수 있습니다.</p>
        <p className="p-13 gray-2 mt-1"><img src={imgBullet} /> MM월 dd일(결과발표 일자 + 1일 ) 23시 59분까지 미 결제 시, 당첨은 취소됩니다.</p>
      </div>
      <div className="col-md-12 pt-5">
        <img src={imgBgIn} className="img-fluid" />
      </div>
      </div>

      <div className="box-game-alsolike hidden-m">
      <div className="container">
      <p className="p-18 roboto bold letter-space-5 black-2">YOU ALSO LIKE</p>

      <div className="row pt-4">
      {this.state.recommendSneakers.map((sneaker: RecommendSneaker, i: number) =>
      <div key={i} className="col-md-3">
      <div onClick={() => this.handleTooltip(i)} className="main-release-in" style={{backgroundImage: `url(${sneaker.imageUrl})`}} />
      <p onClick={() => this.handleTooltip(i)} className="p-14 roboto bold gray-2 mt-3">{sneaker.brand}</p>
      <p onClick={() => this.handleTooltip(i)} className="sneaker-name p-16 roboto back-2 mt-1">{sneaker.name}</p>
      <p onClick={() => this.handleTooltip(i)} className="float-left p-14 gray-2 mt-4">최저판매가</p>
      <p onClick={() => this.handleTooltip(i)} className="float-right roboto p-18 bold orange mt-3 pt-1">{currency(sneaker.lowPrice)}
        <span className="p-14 noto normal">원</span>
      </p>
      </div>)}
      </div>
      </div>
      </div>

      <div className="box-alsolike hidden-p mt-0">
      <div className="container">
      <p className="p-13 roboto bold letter-space-3 black-2">YOU ALSO LIKE</p>
      <div className="horizontal-wrapper">
      {this.state.recommendSneakers.map((sneaker: RecommendSneaker, i: number) =>
      <a href={`/sneaker/${sneaker.name}`} key={i} className="horizontal-card">
      <div className="main-release-in" style={{backgroundImage: `url(${sneaker.imageUrl})`}} />
      <p className="p-14 roboto bold gray-2 mt-3">{sneaker.brand}</p>
      <p className="sneaker-name p-16 roboto back-2 mt-1">{sneaker.name.substr(0, 28)}</p>
      <p className="text-right p-14 gray-2">최저판매가</p>
      <p className="text-right roboto p-18 bold orange">{currency(sneaker.lowPrice)}
        <span className="p-14 noto normal">원</span>
      </p>
      </a>)}
      </div>
      </div>
      </div>
      </div>
    );
  }
}


export default Sneakers;