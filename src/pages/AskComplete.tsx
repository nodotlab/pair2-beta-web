import React from "react";
import complete from "../images/Completed.svg";
import { Link } from "react-router-dom";

class BuyComplete extends React.Component<any> {
  public render() {
    return (
      <div className="container">
        <div className="mt-180 mb-180">
          <div className="text-center">
            <img src={complete} alt="" />
          </div>

          <p className="text-center black-2 p-22 mt-3">
            구매요청이 완료되었습니다.
          </p>
          <p className="text-center p-14 gray-2 mt-2">
            판매자가 거래요청을 수락하면 등록하신 카드로 자동결제가
            이루어집니다.
            <br />
          </p>

          <div className="text-center mt-4">
            <Link to="/mypage/buys">
              <button className="btn-custom-sm w-160">
                구매내역 으로 이동
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default BuyComplete;
