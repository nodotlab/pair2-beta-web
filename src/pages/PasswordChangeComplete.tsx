import React from "react";
import { Link } from "react-router-dom";

class PasswordChangeComplete extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  public render() {
    return (
      <div className="container">
        <div className="col-md-8 offset-md-2 pt-5 pb-5 text-center">
          <p className="p-36">비밀번호 재설정 완료</p>

          <div className="bg-gray mt-5 pt-5 pb-5">
            <p className="p-15 mb-5">
              새로운 비밀번호로 변경이 완료되었습니다. <br />
              변경된 비밀번호로 서비스를 이용해 주시기 바랍니다.
            </p>
            <Link to="/signin">
              <button className="btn btn-dark no-radius pt-2 pb-2 pl-5 pr-5">
                로그인하기
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default PasswordChangeComplete;
