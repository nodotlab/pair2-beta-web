import React from "react";
import { Link } from "react-router-dom";
import { currency } from "../Method";
import { search } from "../db/Actions";
import Sneaker from "../reduce/Sneaker";
import thumb from "../images/thumb.png";
import imgDown from "../images/tab_open.svg";
import SneakerDetail from "../pages/SneakerDetailModal";

interface SearchResultState {
  searchText: string;
  sneakers: Sneaker[];
  modalDetail: boolean;
  detailSneaker: string;
}

class Search extends React.Component<any, SearchResultState> {
  constructor(props: any) {
    super(props);
    this.state = {
      searchText: "",
      sneakers: [],
      modalDetail: false,
      detailSneaker: "",
    };
  }

  public componentDidMount() {
    this.initSneakers();
  }

  private async initSneakers() {
    this.setState({ searchText: this.props.match.params.search });
    const result = await search(this.props.match.params.search);
    const tempSneakers: Sneaker[] = [];

    for (let i = 0; i < result.length; ++i) {
      tempSneakers.push(
        new Sneaker(
          result[i].brand,
          result[i].name,
          "",
          isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
          result[i].thumbUrl ? result[i].thumbUrl : thumb
        )
      );
    }

    this.setState({ sneakers: tempSneakers });
  }

  private handleMore = (): void => {};

  private handleModalWithSneakerName = (name: string): void => {
    this.setState({
      modalDetail: true,
      detailSneaker: name,
    });
  };

  private handleModalDetail = (): void =>
    this.setState({ modalDetail: !this.state.modalDetail });

  public render() {
    return (
      <div>
        <div className="box-info">
          <p className="p-32 roboto bold hidden-m">
            {this.state.searchText}
            <span className="search-count">{this.state.sneakers.length}</span>
          </p>
          <p className="p-20 roboto bold hidden-p">
            {this.state.searchText}
            <span className="search-count">{this.state.sneakers.length}</span>
          </p>
        </div>

        <div className="container mb-5">
          <div className="row mb-3 mt-5">
            <div className="col-6">
              <p className="p-14 black-2">
                총 <span className="bold">{this.state.sneakers.length}</span>건
              </p>
            </div>
            <div className="col-6 text-right">
              <p className="p-14 black-2">
                최신 발매일순 &nbsp;
                <img src={imgDown} alt="" />
              </p>
            </div>
          </div>

          <div className="row hidden-m">
            {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
              <div key={i} className="col-md-3 mb-5">
                <Link to={`/sneaker/${sneaker.name}`}>
                  <div
                    className="box-sneaker"
                    style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                  />
                  <p className="p-14 roboto bold gray-2 mt-3">
                    {sneaker.brand}
                  </p>
                  <p className="sneaker-name p-16 roboto black-2 mt-1">
                    {sneaker.name}
                  </p>
                  <p className="float-left p-14 gray-2 mt-2">최저판매가</p>
                  <p className="float-right roboto p-18 bold orange sneaker-price">
                    {sneaker.lowPrice === 0 ? "—" : currency(sneaker.lowPrice)}
                    {sneaker.lowPrice > 0 && (
                      <span className="p-14 noto normal">원</span>
                    )}
                  </p>
                </Link>
              </div>
            ))}
          </div>

          <div className="row sneaker-mobile-row hidden-p">
            {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
              <div key={i} className="col-md-4 col-6 mb-5 sneaker-mobile">
                <div
                  onClick={() => this.handleModalWithSneakerName(sneaker.name)}
                >
                  <div
                    className="box-sneaker"
                    style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                  />
                  <p className="p-14 roboto bold gray-2 mt-3">
                    {sneaker.brand.toUpperCase()}
                  </p>
                  <p className="sneaker-name p-16 roboto black-2 mt-1 hidden-m">
                    {sneaker.name.toUpperCase()}
                  </p>
                  <p className="sneaker-name p-16 roboto black-2 mt-1 hidden-p">
                    {sneaker.name.toUpperCase().substr(0, 34)}
                    {sneaker.name.length > 34 && "..."}
                  </p>
                  <p className="float-left p-14 gray-2 mt-2 hidden-m">
                    최저판매가
                  </p>
                  <p className="sneaker-price float-right roboto p-18 bold orange hidden-m">
                    {sneaker.lowPrice === 0 ? "—" : currency(sneaker.lowPrice)}
                    <span className="p-14 noto normal">원</span>
                  </p>
                  <p className="text-right p-14 gray-2 mt-2 hidden-p">
                    최저판매가
                  </p>
                  <p className="sneaker-price text-right roboto p-18 bold orange hidden-p">
                    {sneaker.lowPrice === 0 ? "—" : currency(sneaker.lowPrice)}
                    {sneaker.lowPrice > 0 && (
                      <span className="p-14 noto normal">원</span>
                    )}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>

        {this.state.modalDetail && <this.detailFullScreen />}
      </div>
    );
  }

  private detailFullScreen = () => {
    return (
      <div className="modal-fullscreen">
        <SneakerDetail
          name={this.state.detailSneaker}
          handleModal={this.handleModalDetail}
        />
      </div>
    );
  };
}

export default Search;
