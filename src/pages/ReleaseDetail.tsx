import React from "react";
import { releaseDetail, getStore } from "../db/Actions";
import { currency, timeFormat2, dDay, timeFormat10, currencyPrice } from "../Method";
import parse from "html-react-parser";
import imgLink from "../images/store-link.png";
import kream from "../images/kream-logo.jpg";
import stockx from "../images/stockx-logo.jpg";
import MetaData from "../components/MetaGenerator";


interface Release {
  releaseTimestamp: number;
  brand: string;
  name: string;
  nameKr: string;
  imageUrl: string;
  unknownRelease: boolean;
  type: number;
  index: number;
  body: string;
  price: any;
  color: string;
  serial: string;
  marketPriceUrlKream: string;
  marketPriceUrlStockx: string;
}

interface ReleaseDetailState {
  data: Release;
  store: any[];
}

class ReleaseDetail extends React.Component<any, ReleaseDetailState> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: {
        releaseTimestamp: 0,
        brand: "",
        name: "",
        nameKr: "",
        imageUrl: "",
        unknownRelease: false,
        type: 0,
        index: 0,
        body: "",
        price: 0,
        color: "",
        serial: "",
        marketPriceUrlKream: "",
        marketPriceUrlStockx: ""
      },
      store: [],
    };
  }

  public componentDidMount() {
    this.initData();
  }

  private async initData() {
    const result: any = await releaseDetail(this.props.match.params.id);
    const data: Release = {
      releaseTimestamp: result.releaseDate,
      brand: result.brand,
      name: result.name.toUpperCase(),
      nameKr: result.nameKr ? result.nameKr : result.name,
      imageUrl: result.imageUrlLarge ? result.imageUrlLarge : result.imageUrl,
      unknownRelease: result.unknownRelease,
      type: 0,
      index: result.index,
      body: result.body,
      price: result.price,
      color: result.color,
      serial: result.serial,
      marketPriceUrlKream: result.marketPriceUrlKream,
      marketPriceUrlStockx: result.marketPriceUrlStockx
    };

    const store = await getStore(result.index);
    this.setState({ data: data, store: store });
  }

  private releaseType(type: string, start: number, end: number): string {
    if (type === "추첨") {
      let value: string = "";
      if (Date.now() < start) {
        value = `${timeFormat10(start)} 시작`;
      } else {
        value = `${timeFormat10(end)} 마감`;
      }

      return value;
    } else {
      let value: string = "";
      // if (Date.now() < start) {
      //   value = `${timeFormat10(start)} 발매 예정`;
      // } else {
      value = `${timeFormat10(start)} 발매`;
      // }

      return value;
    }
  }

  private isEnd(type: string, end: number): boolean {
    if (type === "선착순") {
      return false;
    } else {
      if (Date.now() < end) {
        return false;
      } else {
        return true;
      }
    }
  }

  private isStart(type: string, start: number): boolean {
    if (type === "선착순") {
      return false;
    } else {
      if (Date.now() > start) {
        return true;
      } else {
        return false;
      }
    }
  }

  private releaseTypeTag(type: string, start: number, end: number): string {
    let currentTime = Date.now();
    if (type === "추첨") {
      let value: string = "";
      if (currentTime < start) {
        value = `응모예정`;
      } else if (currentTime >= start && currentTime < end) {
        value = `응모중`;
      } else {
        value = `응모종료`;
      }
      return value;
    } else {
      let value: string = "";
      if (Date.now() < start) {
        value = `선착순`;
      } else {
        value = `선착순`;
      }

      return value;
    }
  }

  public render() {

    const metaData = {
      title: this.state.data.nameKr + " (" + this.state.data.serial + ") 발매 정보 | PAIR2(페어투)",
      desc: `발매처 ${this.state.store.length}곳, ${this.state.data.nameKr} (${this.state.data.serial}) 의 발매처 정보를 확인해 보세요.`,
      url: window.location.pathname,
      image: this.state.data.imageUrl
    };

    return (
      <div>
        <MetaData data={metaData} />
        <div className="release-detail-box">
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <p className="p-18 bold black-2 o-60 letter-space-4 mb-2 hidden-m">
                  {this.state.data.brand.toUpperCase()}
                </p>
                <p className="p-40 black-2 line-48 hidden-m">
                  {this.state.data.name}
                </p>
                <p className="p-13 bold black-2 o-60 letter-space-2 mb-2 pl-2 pr-2 hidden-p">
                  {this.state.data.brand.toUpperCase()}
                </p>
                <p className="p-24 black-2 hidden-p pl-2 pr-2 line-31 hidden-p">
                  {this.state.data.name}
                </p>
                <p className="p-13 bold orange mt-2 pl-2 pr-2 hidden-p">
                  {this.state.data.nameKr}
                </p>
                <p className="p-16 bold orange mt-3 hidden-m">
                  {this.state.data.nameKr}
                </p>
                <div className="box-detail-first-in">
                  <img className="today" src={this.state.data.imageUrl} alt={`${this.state.data.nameKr} (${this.state.data.serial})`} />
                </div>
              </div>

              <div className="col-md-4">
                <div className="release-detail-top">
                  {this.state.data.unknownRelease ? (
                    <>
                      <p className="p-13 gray-4 hidden-m">
                        출시일이 곧 공개됩니다.
                      </p>
                      <p className="p-22 bold black hidden-m">COMING SOON</p>
                      <p className="p-13 gray-4 hidden-p text-center">
                        출시일이 곧 공개됩니다.
                      </p>
                      <p className="p-22 bold black hidden-p text-center">
                        COMING SOON
                      </p>
                    </>
                  ) : (
                    <>
                      <p className="p-13 gray-4 hidden-m">발매일</p>
                      <p className="p-22 bold black hidden-m">
                        {timeFormat2(this.state.data.releaseTimestamp)}
                      </p>
                      <p className="p-13 gray-4 hidden-p text-center">발매일</p>
                      <p className="p-22 bold black hidden-p text-center">
                        {timeFormat2(this.state.data.releaseTimestamp)}
                      </p>
                    </>
                  )}

                  <div className="flex bg-white mt-4 p-3">
                    <div className="col-2 pr-0 pl-1">
                      <p className="p-13 bold gray-2 mt-3">발매가</p>
                      <p className="p-13 bold gray-2 mt-2">색상</p>
                      <p className="p-13 bold gray-2 mt-2 mb-3">품번</p>
                    </div>
                    <div className="col-10 pr-1 pl-0 text-right ">
                      <p className="p-13 gray-3 mt-3">
                        {currencyPrice(this.state.data.price)}
                      </p>
                      <p className="p-13 gray-3 mt-2">
                        {this.state.data.color}
                      </p>
                      <p className="p-13 gray-3 mt-2 mb-3">
                        {this.state.data.serial}
                      </p>
                    </div>
                  </div>
                  <div>
                    {this.state.data.marketPriceUrlKream ? (
                      <>
                        <div className="price-box">
                          <div className="col-3">
                            <img src={kream} alt="" />
                          </div>
                          <div className="col-5">
                            <p className="bold">
                              KREAM
                            </p>
                            <p className="gray-2">
                              대한민국
                            </p>
                          </div>
                          <div className="col-4">
                            <a href={this.state.data.marketPriceUrlKream} target="_new">
                              <div className="market-link">
                                <p className="p-12 black mt-1">시세보기</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </>
                    ) : (<></>)}
                    {this.state.data.marketPriceUrlStockx ? (
                      <>
                        <div className="price-box">
                          <div className="col-3">
                            <img src={stockx} alt="" />
                          </div>
                          <div className="col-5">
                            <p className="bold">
                              STOCKX
                            </p>
                            <p className="gray-2">
                              해외
                            </p>
                          </div>
                          <div className="col-4">
                            <a href={this.state.data.marketPriceUrlStockx} target="_new">
                              <div className="market-link">
                                <p className="p-12 black mt-1">시세보기</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </>
                    ) : (<></>)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="bg-white release-detail-middle pt-5-5 pb-5-5">
          <div className="container release-detail-body">
            <p>{parse(this.state.data.body ? this.state.data.body : "")}</p>
          </div>
        </div>

        <div className="release-detail-bottom bg-gray pt-4 pb-4">
          <div className="container">
            <div className="p-14 mt-1 mb-3 ml-1 gray-2"><p>발매 정보 <span className="bold">{this.state.store.length}</span>건</p></div>
            <div className="row">
              {this.state.store.map((data: any, i: number) => (
                <div className="col-md-4" key={i}>
                  <div className={`${this.isEnd(data.storesaleType, data.releaseDateEnd) && "box-store-end" || "box-store"}`}>
                    <div className="row">
                      <div className="col-4">
                        <p
                          className={`p-12 medium ${this.isEnd(
                            data.storesaleType,
                            data.releaseDateEnd,
                          ) && "color-disable" || this.isStart(data.storesaleType, data.releaseDate) && "color-raffle-on"
                            }`}
                        >
                          { /**응모 상태 태그 */
                            this.releaseTypeTag(data.storesaleType, data.releaseDate, data.releaseDateEnd)
                          }
                        </p>
                      </div>
                      <div className="col-8 text-right">
                        <p
                          className={`p-12 medium ${this.isEnd(
                            data.storesaleType,
                            data.releaseDateEnd,
                          ) && "color-disable"
                            }`}
                        >
                          {this.releaseType(
                            data.storesaleType,
                            data.releaseDate,
                            data.releaseDateEnd,
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="row p-3 border-top3">
                      <div className="col-3">
                        <div
                          className="img-store"
                          style={{
                            backgroundImage: `url(${data.imageUrl})`,
                          }}
                        />
                      </div>
                      <div className="col-6 pl-0 pr-0">
                        <p
                          className={`p-12 bold ${this.isEnd(
                            data.storesaleType,
                            data.releaseDateEnd,
                          ) && "color-disable"
                            }`}
                        >
                          {data.storename}
                        </p>
                        <p
                          className={`p-14 ${this.isEnd(
                            data.storesaleType,
                            data.releaseDateEnd,
                          ) && "color-disable"
                            }`}
                        >
                          {currencyPrice(data.price)}
                          {/* {data.price} */}
                        </p>
                        <p className="p-12 gray-2 mt-1">
                          {data.storeCountry} | {data.online ? "온라인 구매" : "오프라인 구매"}
                        </p>
                      </div>
                      <div className="col-3 pl-0">
                        {this.isEnd(data.storesaleType, data.releaseDateEnd) ? (
                          <a href={data.storeUrl} target="_new">
                            <div className="box-store-link">
                              <img src={imgLink} />
                              <p className="p-12">바로가기</p>
                            </div>
                          </a>
                        ) : (
                          <a href={data.storeUrl} target="_new">
                            <div className="box-store-link">
                              <img src={imgLink} />
                              <p className="p-12 black mt-1">바로가기</p>
                            </div>
                          </a>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ReleaseDetail;
