import React from "react";
import { smsSend, smsVerify, signupByEmail } from "../db/Actions";
import bullet from "../images/bullet.svg";
import radio from "../images/Radio_Default.svg";
import radioChecked from "../images/Radio_Checked.svg";
import agreeDisable from "../images/Agree_Disabled.svg";
import agreeActive from "../images/Agree_Active.svg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import warining from "../images/Input_Warning.svg";
import inputDelete from "../images/Input_Delete_LightBG.svg";
import modalClose from "../images/Popup_Delete.svg";

interface SignUpState {
  name: string;
  phone: string;
  smsText: string;
  smsReady: boolean;
  smsConfirm: boolean;
  secretNumber: string;
  password: string;
  passwordConfirm: string;
  email: string;
  nickname: string;
  age: string;
  ageList: string[];
  month: string;
  monthList: string[];
  date: string;
  dateList: string[];
  sex: string;
  size: string;
  sizeList: string[];
  smsAgree: boolean;
  emailAgree: boolean;
  agree1: boolean;
  agree2: boolean;
  agree3: boolean;
  modal: boolean;
  modalValue: string;
}

class SignUp extends React.Component<any, SignUpState> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      phone: "",
      smsText: "",
      smsReady: false,
      secretNumber: "",
      smsConfirm: false,
      password: "",
      passwordConfirm: "",
      email: "",
      nickname: "",
      age: "",
      ageList: [],
      month: "",
      monthList: [],
      date: "",
      dateList: [],
      sex: "",
      size: "",
      sizeList: [],
      smsAgree: true,
      emailAgree: true,
      agree1: false,
      agree2: false,
      agree3: false,
      modal: false,
      modalValue: "",
    };
  }

  public componentDidMount() {
    if (localStorage.getItem("pair2signin") === "true")
      this.props.history.push("/");
    this.createList();
  }

  private createList() {
    const tmpAgeList: string[] = [];
    const tmpMonthList: string[] = [];
    const tmpDateList: string[] = [];
    const tempSizeList: string[] = [];
    tmpAgeList.push("10대");
    tmpAgeList.push("20대");
    tmpAgeList.push("30대");
    tmpAgeList.push("40대");
    tmpAgeList.push("50대");
    tmpAgeList.push("60대");
    tmpAgeList.push("70대");
    tmpAgeList.push("80대");
    tmpAgeList.push("90대");

    for (let i = 1; i < 13; ++i) {
      tmpMonthList.push(i.toString());
    }

    for (let i = 1; i < 32; ++i) {
      tmpDateList.push(i.toString());
    }

    for (let i = 220; i < 320; i += 5) {
      tempSizeList.push(i.toString());
    }

    this.setState({
      ageList: tmpAgeList,
      monthList: tmpMonthList,
      dateList: tmpDateList,
      sizeList: tempSizeList,
    });
  }

  private handleText = (e: React.ChangeEvent<any>) => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value } as {
      [i in keyof SignUpState]: any;
    });
  };

  private handlePhone = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleInputClear = (formName: string): any => {
    this.setState({ [formName]: "" } as any);
  };

  private handleSmsSend = async () => {
    if (this.state.smsConfirm) return;

    const result = await smsSend(this.state.phone);

    if (result === "success") {
      this.setState({
        smsReady: true,
      });
    } else {
      alert("이미 가입 된 번호입니다.");
    }
  };

  private handleSmsConfirm = async () => {
    const result: string = await smsVerify(
      this.state.phone,
      this.state.smsText
    );

    if (result === "success") {
      this.setState({
        smsReady: false,
        smsConfirm: true,
      });
      alert("인증이 완료되었습니다.");
    } else {
      alert("인증번호를 확인해주세요.");
    }
  };

  private handleAgree = (name: string) => {
    if (name === "agree1") this.setState({ agree1: !this.state.agree1 });
    if (name === "agree2") this.setState({ agree2: !this.state.agree2 });
    if (name === "agree3") this.setState({ agree3: !this.state.agree3 });
    if (name === "all")
      this.setState({ agree1: true, agree2: true, agree3: true });
  };

  private agreeAll(): boolean {
    if (this.state.agree1 && this.state.agree2 && this.state.agree3) {
      return true;
    } else {
      return false;
    }
  }

  private verifyPhone = (): boolean => {
    const regex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/;

    if (regex.test(this.state.phone)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPassword = (): boolean => {
    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=_]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.password)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPasswordConfirm = (): boolean => {
    if (this.state.password === this.state.passwordConfirm) {
      return true;
    } else {
      return false;
    }
  };

  private verifyEmail = (): boolean => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(this.state.email.toLowerCase());
  };

  private verifyAll = (): boolean => {
    // if (this.state.name.length === 0) return false;
    // if (!this.verifyPhone() || this.state.phone.length === 0) return false;
    if (!this.verifyPassword() || this.state.password.length === 0) return false;
    if (!this.verifyPasswordConfirm()) return false;
    if (this.state.nickname.length === 0) return false;
    if (!this.verifyEmail() || this.state.email.length === 0) return false;
    if (!this.state.agree1) return false;
    if (!this.state.agree2) return false;

    return true;
  };

  private handleSubmit = async () => {
    if (!this.verifyEmail() || this.state.email.length === 0) return;
    if (!this.verifyPassword() || this.state.password.length === 0) return;
    if (!this.verifyPasswordConfirm()) return;
    if (this.state.nickname.length === 0) return;
    if (!this.state.agree1) return;
    if (!this.state.agree2) return;
    // if (this.state.name.length === 0) return;
    // if (!this.verifyPhone() || this.state.phone.length === 0) return;
    // if (!this.state.smsConfirm) return;

    const result = await signupByEmail(
      this.state.password,
      this.state.nickname,
      this.state.email,
      this.state.agree1,
      this.state.agree2
    );

    if (result === "Email existed") {
      alert("이미 가입된 이메일 입니다.");
    } else {
      localStorage.setItem("pair2token", result);
      localStorage.setItem("pair2signin", "true");
      this.props.history.push("/");
    }
  };

  private handleModal = () => this.setState({ modal: !this.state.modal });

  public render() {
    return (
      <div className="container">
        <p className="text-center black-2 p-22 mt-100 hidden-m">회원가입</p>
        {/* <p className="text-center p-14 gray-2 mt-2 hidden-m">
          PAIR2는 거래간에 안정성과 신뢰도 향상을 위해 휴대폰 번호로 가입합니다.{" "}
          <br />
          수집한 번호는 외부로 노출되지 않도록 안전하게 보관하겠습니다.
        </p>
        <p className="text-center black-2 p-22 mt-5 hidden-p">회원가입</p>
        <p className="text-center p-14 gray-2 mt-2 pl-3 pr-3 hidden-p">
          PAIR2는 거래간에 안정성과 신뢰도 향상을 위해 휴대폰 번호로 가입합니다.
          수집한 번호는 외부로 노출되지 않도록 안전하게 보관하겠습니다.
        </p> */}

        {/* <div className="col-lg-6 offset-lg-3 pr-0">
          <p className="text-right mt-4 p-13 red">*필수입력</p>
        </div> */}

        <div className="col-lg-6 offset-lg-3 box-signup mt-3">
          {/* <p className="p-14 black-2 mb-2">
            이름<span className="red">*</span>
          </p>
          <input
            onChange={this.handleText}
            value={this.state.name}
            name="name"
            type="text"
            placeholder="이름을 입력해주세요"
            className={`form-control form-custom`}
          />

          <p className="p-14 black-2 mt-4 mb-2">
            휴대폰 번호<span className="red">*</span>
          </p> */}

          {/* <div className="flex">
            <div className="col-8 pl-0 pr-1">
              {this.state.smsConfirm ? (
                <div className="form-disable">{this.state.phone}</div>
              ) : (
                <input
                  onChange={this.handlePhone}
                  value={this.state.phone}
                  name="phone"
                  type="number"
                  placeholder="숫자만 입력해주세요"
                  className={`form-control form-custom ${
                    !this.verifyPhone() &&
                    this.state.phone.length > 0 &&
                    "form-warning"
                  }`}
                />
              )}
              {!this.verifyPhone() && this.state.phone.length > 0 && (
                <img
                  src={inputDelete}
                  onClick={() => this.handleInputClear("phone")}
                  className="placeholder-right cursor"
                />
              )}
            </div>
            <div className="col-4 pl-1 pr-0">
              {this.verifyPhone() ? (
                <button
                  onClick={this.handleSmsSend}
                  className={`w-100 h-100 ${
                    this.state.smsConfirm
                      ? "btn-custom-sm-disable"
                      : "btn-second-sm"
                  }`}
                >
                  {this.state.smsConfirm ? "인증완료" : "인증번호 받기"}
                </button>
              ) : (
                <button className="btn-custom-sm-disable w-100 h-100">
                  인증번호 받기
                </button>
              )}
            </div>
          </div> */}
          {/* {!this.verifyPhone() && this.state.phone.length > 0 && (
            <p className="p-13 red mt-1 mb-3">
              <img src={warining} className="mr-2" />
              올바른 휴대폰 번호를 입력해주세요
            </p>
          )}

          {this.state.smsReady && (
            <div>
              <input
                onChange={this.handleText}
                value={this.state.smsText}
                name="smsText"
                type="text"
                placeholder="인증번호를 입력해주세요"
                className="form-control form-custom mt-2"
              />
              {this.state.smsText.length > 0 && (
                <img
                  src={inputDelete}
                  onClick={() => this.handleInputClear("smsText")}
                  className="placeholder-right cursor"
                />
              )}

              <div className="mt-2">
                <p className="p-13 gray-2 float-left">
                  <img src={bullet} className="mr-2" />
                  휴대폰인증은 1일 5회까지 가능합니다.
                </p>
                <p
                  onClick={this.handleSmsSend}
                  className="p-13 gray-2 underline cursor float-right"
                >
                  인증번호 재전송
                </p>
              </div>
              <button
                onClick={this.handleSmsConfirm}
                className={`w-100 mt-4 ${
                  this.state.smsText.length > 0
                    ? "btn-custom-sm-active"
                    : "btn-custom-sm-disable"
                }`}
              >
                인증번호 확인
              </button>
            </div>
          )} */}

          <p className="p-14 black-2 mt-4 mb-2">
            이메일
          </p>
          <input
            onChange={this.handleText}
            value={this.state.email}
            name="email"
            type="email"
            placeholder="이메일주소를 입력해주세요"
            className={`form-control form-custom ${
              !this.verifyEmail() &&
              this.state.email.length > 0 &&
              "form-warning"
            }`}
          />

          {this.state.email.length > 0 && (
            <img
              src={inputDelete}
              onClick={() => this.handleInputClear("email")}
              className="placeholder-right cursor"
            />
          )}
          
          {!this.verifyEmail() && this.state.email.length > 0 && (
            <p className="p-13 red mt-1 mb-2">
              <img src={warining} className="mr-2" />
              이메일 형식이 잘못되었습니다.
            </p>
          )}

          <p className="p-14 black-2 mt-4 mb-2">
            닉네임
          </p>
          <input
            onChange={this.handleText}
            value={this.state.nickname}
            name="nickname"
            type="text"
            maxLength={19}
            placeholder="닉네임을 입력해주세요"
            className={`form-control form-custom`}
          />
          <p className="p-13 gray-4 placeholder-right">
            {this.state.nickname.length}/20
          </p>

          <p className="p-14 black-2 mt-4 mb-2">
            비밀번호
          </p>
          <input
            onChange={this.handleText}
            value={this.state.password}
            name="password"
            maxLength={20}
            type="password"
            placeholder="8-20자리의 영문, 숫자, 특수문자"
            className={`form-control form-custom ${
              !this.verifyPassword() &&
              this.state.password.length > 0 &&
              "form-warning"
            }`}
          />
          {!this.verifyPassword() && this.state.password.length > 0 && (
            <p className="p-13 red mt-1 mb-3">
              <img src={warining} className="mr-2" />
              8~20자 영문, 숫자, 특수문자를 사용하세요.
            </p>
          )}

          <input
            onChange={this.handleText}
            value={this.state.passwordConfirm}
            maxLength={20}
            name="passwordConfirm"
            type="password"
            placeholder="비밀번호를 재입력해주세요"
            className={`form-control form-custom mt-2 ${
              !this.verifyPasswordConfirm() &&
              this.state.passwordConfirm.length > 0 &&
              "form-warning"
            }`}
          />
          {!this.verifyPasswordConfirm() &&
            this.state.passwordConfirm.length > 0 && (
              <p className="p-13 red mt-1 mb-3">
                <img src={warining} className="mr-2" />
                비밀번호가 잘못되었습니다.
              </p>
            )}
          

          {/* <hr className="br2 mt-5" /> */}

          {/* <div className="row mt-5">
            <div className="col-6 pr-1">
              <p className="p-14 black-2 mb-2">연령대</p>
              <select
                onChange={this.handleText}
                defaultValue={"default"}
                className="form-control form-select no-radius"
                name="age"
              >
                <option value="default" disabled>
                  연령대 선택
                </option>
                {this.state.ageList.map((age: string, i: number) => (
                  <option key={i}>{age}</option>
                ))}
              </select>
            </div>

            <div className="col-6 pl-1">
              <p className="p-14 black-2 mb-2">생일</p>
              <div className="flex">
                <select
                  onChange={this.handleText}
                  defaultValue={"default"}
                  className="form-control form-select mr-1"
                  name="month"
                >
                  <option value="default" disabled>
                    월
                  </option>
                  {this.state.monthList.map((month: string, i: number) => (
                    <option key={i}>{month}</option>
                  ))}
                </select>
                <select
                  onChange={this.handleText}
                  defaultValue={"default"}
                  className="form-control form-select ml-1"
                  name="date"
                >
                  <option value="default" disabled>
                    일
                  </option>
                  {this.state.dateList.map((date: string, i: number) => (
                    <option key={i}>{date}</option>
                  ))}
                </select>
              </div>
            </div>
          </div> */}

          {/* <div className="row mt-4">
            <div className="col-6 pr-1">
              <p className="p-14 black-2 mb-2">성별</p>
              <select
                onChange={this.handleText}
                defaultValue={"default"}
                className="form-control form-select no-radius"
                name="sex"
              >
                <option value="default" disabled>
                  성별 선택
                </option>
                <option>여자</option>
                <option>남자</option>
              </select>
            </div>

            <div className="col-6 pl-1">
              <p className="p-14 black-2 mb-2">신발 사이즈</p>
              <select
                onChange={this.handleText}
                defaultValue={"default"}
                className="form-control form-select mr-1"
                name="size"
              >
                <option value="default" disabled>
                  사이즈 선택
                </option>
                {this.state.sizeList.map((size: string, i: number) => (
                  <option key={i}>{size}</option>
                ))}
              </select>
            </div>
          </div> */}

          {/* <p className="black-2 p-14 mt-4">SMS/이메일 수신여부 (선택)</p>
          <p className="gray-2 p-13 mt-2 float-left">
            <img src={bullet} /> PAIR2의 다양한 정보를 받아보시겠습니까?
          </p>
          <a href="/tos5.html" target="_new">
            <p className="gray-2 p-13 underline cursor float-right mt-2 hidden-m">
              보기
            </p>
          </a>
          <p
            onClick={() => this.setState({ modal: true, modalValue: "tos5" })}
            className="gray-2 p-13 underline cursor float-right mt-2 hidden-p"
          >
            보기
          </p>

          <div className="bg-gray mt-5 p-4">
            <div className="row">
              <div className="col-3">
                <p className="black-2 bold p-13">SMS</p>
                <p className="black-2 bold p-13 mt-3">이메일</p>
              </div>
              <div className="col-4">
                <p
                  onClick={() => this.setState({ smsAgree: true })}
                  className={`p-14 cursor ${
                    this.state.smsAgree ? "gray-3" : "gray-6"
                  }`}
                >
                  <img
                    src={this.state.smsAgree ? radioChecked : radio}
                    className="mr-2"
                  />
                  동의함
                </p>
                <p
                  onClick={() => this.setState({ emailAgree: true })}
                  className={`p-14 cursor mt-3 ${
                    this.state.emailAgree ? "gray-3" : "gray-6"
                  }`}
                >
                  <img
                    src={this.state.emailAgree ? radioChecked : radio}
                    className="mr-2"
                  />
                  동의함
                </p>
              </div>
              <div className="col-5">
                <p
                  onClick={() => this.setState({ smsAgree: false })}
                  className={`p-14 cursor ${
                    this.state.smsAgree ? "gray-6" : "gray-3"
                  }`}
                >
                  <img
                    src={this.state.smsAgree ? radio : radioChecked}
                    className="mr-2"
                  />
                  동의안함
                </p>
                <p
                  onClick={() => this.setState({ emailAgree: false })}
                  className={`p-14 cursor mt-3 ${
                    this.state.emailAgree ? "gray-6" : "gray-3"
                  }`}
                >
                  <img
                    src={this.state.emailAgree ? radio : radioChecked}
                    className="mr-2"
                  />
                  동의안함
                </p>
              </div>
            </div>
          </div> */}

          <button
            onClick={() => this.handleAgree("all")}
            className={`w-100 mt-4 ${
              this.agreeAll() ? "btn-second" : "btn-custom-disable"
            }`}
          >
            전체 약관에 동의{" "}
            <img
              src={this.agreeAll() ? agreeActive : agreeDisable}
              className="ml-2"
            />
          </button>

          <div className="mt-3 p-2 w-100">
            <img
              onClick={() => this.handleAgree("agree1")}
              src={this.state.agree1 ? checked : check}
              className="cursor float-left"
            />
            <p
              onClick={() => this.handleAgree("agree1")}
              className="gray-6 p-14 float-left cursor ml-2"
            >
              PAIR2 이용약관 동의 (필수)
            </p>
            <a href="/tos/page1" target="_new">
              <p className="gray-2 p-13 underline cursor float-right hidden-m">
                보기
              </p>
            </a>
            <p
              onClick={() => this.setState({ modal: true, modalValue: "tos1" })}
              className="gray-2 p-13 underline cursor float-right hidden-p"
            >
              보기
            </p>
          </div>
          <div className="mt-4 p-2 w-100">
            <img
              onClick={() => this.handleAgree("agree2")}
              src={this.state.agree2 ? checked : check}
              className="cursor float-left"
            />
            <p
              onClick={() => this.handleAgree("agree2")}
              className="gray-6 p-14 float-left cursor ml-2"
            >
              개인정보 수집 및 이용에 대한 안내 (필수)
            </p>
            <a href="/tos3.html" target="_new">
              <p className="gray-2 p-13 underline cursor float-right hidden-m">
                보기
              </p>
            </a>
            <p
              onClick={() => this.setState({ modal: true, modalValue: "tos3" })}
              className="gray-2 p-13 underline cursor float-right hidden-p"
            >
              보기
            </p>
          </div>
          {/* <div className="mt-4 p-2 w-100">
            <img
              onClick={() => this.handleAgree("agree3")}
              src={this.state.agree3 ? checked : check}
              className="cursor float-left"
            />
            <p
              onClick={() => this.handleAgree("agree3")}
              className="gray-6 p-14 float-left cursor ml-2"
            >
              개인정보 수집 및 이용에 대한 안내 (선택)
            </p>
            <a href="/tos4.html" target="_new">
              <p className="gray-2 p-13 underline cursor float-right hidden-m">
                보기
              </p>
            </a>
            <p
              onClick={() => this.setState({ modal: true, modalValue: "tos4" })}
              className="gray-2 p-13 underline cursor float-right hidden-p"
            >
              보기
            </p>
          </div> */}
        </div>

        <div className="mt-6 mb-100 text-center hidden-m">
          <button
            onClick={() => this.props.history.goBack()}
            className="btn-custom mr-2"
          >
            취소
          </button>
          <button
            onClick={this.handleSubmit}
            className={
              this.verifyAll() ? "btn-custom-active" : "btn-custom-disable"
            }
          >
            가입하기
          </button>
        </div>
        <div className="mt-3 mb-5 text-center flex hidden-p">
          <button
            onClick={() => this.props.history.goBack()}
            className="btn-custom mr-1"
          >
            취소
          </button>
          <button
            onClick={this.handleSubmit}
            className={
              this.verifyAll()
                ? "ml-1 btn-custom-active"
                : "ml-1 btn-custom-disable"
            }
          >
            가입하기
          </button>
        </div>

        <this.componentModal />
      </div>
    );
  }

  private componentModal = () => {
    return (
      <div
        className="modal-full"
        style={{ display: this.state.modal ? "block" : "none" }}
      >
        <div className="container">
          <div className="row pt-2 pb-2">
            <div className="col-6">
              <p className="p-16 bold black-2"></p>
            </div>
            <div className="col-6 text-right">
              <img
                onClick={this.handleModal}
                src={modalClose}
                alt=""
                className="cursor"
              />
            </div>
          </div>
        </div>
        <div className="col-12">
          <div className="detail-border-top" />
        </div>
        {this.state.modalValue === "tos1" && (
          <iframe src="/tos/page1" className="iframe" />
        )}
        {this.state.modalValue === "tos2" && (
          <iframe src="/tos2.html" className="iframe" />
        )}
        {this.state.modalValue === "tos3" && (
          <iframe src="/tos3.html" className="iframe" />
        )}
        {this.state.modalValue === "tos4" && (
          <iframe src="/tos4.html" className="iframe" />
        )}
        {this.state.modalValue === "tos5" && (
          <iframe src="/tos5.html" className="iframe" />
        )}
        <div className="container"></div>
      </div>
    );
  };
}

export default SignUp;
