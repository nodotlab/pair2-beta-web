import React from "react";
import { randomNumber } from "../Method";
import { Link } from "react-router-dom";
import bullet from "../images/bullet.svg";
import warining from "../images/Input_Warning.svg";
import complete from "../images/Completed.svg";
import { smsSendFind, smsVerifyFind, editPassword } from "../db/Actions";

interface FindPasswordState {
  name: string;
  phone: string;
  smsText: string;
  smsReady: boolean;
  currentComponent: string;
  secretNumber: string;
  password: string;
  passwordConfirm: string;
  warning: boolean;
}

class FindPassword extends React.Component<any, FindPasswordState> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      phone: "",
      smsText: "",
      smsReady: false,
      currentComponent: "verify",
      secretNumber: "",
      password: "",
      passwordConfirm: "",
      warning: false,
    };
  }

  public componentDidMount() {}

  private handleText = (e: React.ChangeEvent<any>) => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value } as {
      [i in keyof FindPasswordState]: any;
    });
  };

  private verifyPhone = (): boolean => {
    const regex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/;

    if (regex.test(this.state.phone)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPassword = (): boolean => {
    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.password)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPasswordConfirm = (): boolean => {
    if (this.state.password === this.state.passwordConfirm) {
      return true;
    } else {
      return false;
    }
  };

  private verifyAll = (): boolean => {
    if (this.state.password.length === 0) return false;
    if (!this.verifyPassword()) return false;
    if (!this.verifyPasswordConfirm()) return false;

    return true;
  };

  private handleSmsSend = async () => {
    if (this.state.phone.length === 0) return;
    if (this.state.name.length === 0) return alert("이름을 입력해주세요.");

    const result = await smsSendFind(this.state.phone, this.state.name);

    if (result === "success") {
      this.setState({
        smsReady: true,
      });
    } else {
      alert("이름과 휴대폰 번호를 확인해주세요.");
    }
  };

  private handleSmsConfirm = async () => {
    const result = await smsVerifyFind(this.state.phone, this.state.smsText);

    if (result === "success") {
      this.setState({
        smsReady: false,
        currentComponent: "change",
      });
    } else {
      alert("인증번호를 확인해주세요.");
    }
  };

  private handleChangeSubmit = async () => {
    const result = await editPassword(
      this.state.phone,
      this.state.smsText,
      this.state.password
    );

    if (result === "success") {
      this.setState({ currentComponent: "complete" });
    }
  };

  public render() {
    return (
      <React.Fragment>
        {this.state.currentComponent === "verify" && <this.componentVerify />}
        {this.state.currentComponent === "change" && <this.componentChange />}
        {this.state.currentComponent === "complete" && (
          <this.componentComplete />
        )}
      </React.Fragment>
    );
  }

  private componentVerify = () => {
    return (
      <div className="container">
        <div className="mt-100 hidden-m">
          <p className="text-center black-2 p-22">비밀번호 찾기</p>
          <p className="text-center p-14 gray-2 mt-2">
            PAIR2 가입 시 사용한 이름과 휴대폰 번호를 통해 비밀번호를
            재설정해주세요.
          </p>
        </div>
        <div className="mt-60 hidden-p">
          <p className="text-center black-2 p-22">비밀번호 찾기</p>
          <p className="text-center p-14 gray-2 mt-2">
            PAIR2 가입 시 사용한 이름과 휴대폰 번호를 통해
            <br /> 비밀번호를 재설정해주세요.
          </p>
        </div>

        <div className="col-lg-6 offset-lg-3 mt-5 box-findpassword">
          <p className="p-14 black-2 mb-2">이름</p>
          <input
            onChange={this.handleText}
            value={this.state.name}
            name="name"
            className={`form-control form-custom`}
            type="text"
            placeholder="이름을 입력해주세요"
          />

          <p className="p-14 black-2 mt-4 mb-2">휴대폰 번호</p>
          <div className="row">
            <div className="col-8 pr-1">
              <input
                onChange={this.handleText}
                value={this.state.phone}
                name="phone"
                className={`form-control form-custom`}
                type="number"
                placeholder="숫자만 입력해주세요"
              />
            </div>
            <div className="col-4 pl-1">
              {this.verifyPhone() ? (
                <button
                  onClick={this.handleSmsSend}
                  className="btn-second-sm btn-verify-phone w-100 h-100"
                >
                  인증번호 받기
                </button>
              ) : (
                <button className="btn-custom-sm-disable btn-verify-phone w-100 h-100">
                  인증번호 받기
                </button>
              )}
            </div>
          </div>

          {this.state.smsReady && (
            <div>
              <input
                onChange={this.handleText}
                value={this.state.smsText}
                name="smsText"
                type="text"
                placeholder="인증번호를 입력해주세요"
                className="form-control form-custom mt-2"
              />
              <div className="mt-2">
                <p className="p-13 gray-2 float-left">
                  <img src={bullet} alt="" className="mr-2" />
                  휴대폰인증은 1일 5회까지 가능합니다.
                </p>
                <p
                  onClick={this.handleSmsSend}
                  className="p-13 gray-2 underline cursor float-right"
                >
                  인증번호 재전송
                </p>
              </div>
              <button
                onClick={this.handleSmsConfirm}
                className={`w-100 mt-4 ${
                  this.state.smsText.length > 0
                    ? "btn-custom-sm-active"
                    : "btn-custom-sm-disable"
                }`}
              >
                인증번호 확인
              </button>
            </div>
          )}
        </div>
        <div className="text-center mt-5 mb-100">
          <button
            onClick={() => this.props.history.goBack()}
            className="btn-custom"
          >
            취소
          </button>
        </div>
      </div>
    );
  };

  private componentChange = () => {
    return (
      <div className="container">
        <div className="mt-100 hidden-m">
          <p className="text-center black-2 p-22">비밀번호 재설정</p>
          <p className="text-center p-14 gray-2 mt-2">
            사용할 새 비밀번호를 입력 후 저장해주세요.
          </p>
        </div>
        <div className="mt-60">
          <p className="text-center black-2 p-22">비밀번호 재설정</p>
          <p className="text-center p-14 gray-2 mt-2">
            사용할 새 비밀번호를 입력 후 저장해주세요.
          </p>
        </div>

        <div className="col-lg-6 offset-lg-3 mt-5 box-findpassword">
          <p className="p-14 black-2 mb-2">새 비밀번호</p>
          <input
            onChange={this.handleText}
            value={this.state.password}
            name="password"
            type="password"
            placeholder="8-20자리의 영문, 숫자, 특수문자"
            className={`form-control form-custom ${
              !this.verifyPassword() &&
              this.state.password.length > 0 &&
              "form-warning"
            }`}
          />
          {!this.verifyPassword() && this.state.password.length > 0 && (
            <p className="p-13 red mt-1 mb-2">
              <img src={warining} alt="" className="mr-2" />
              8~20자 영문, 숫자, 특수문자를 사용하세요.
            </p>
          )}

          <input
            onChange={this.handleText}
            value={this.state.passwordConfirm}
            name="passwordConfirm"
            type="password"
            placeholder="비밀번호를 재입력해주세요"
            className={`form-control form-custom mt-2 ${
              !this.verifyPasswordConfirm() &&
              this.state.passwordConfirm.length > 0 &&
              "form-warning"
            }`}
          />
          {!this.verifyPasswordConfirm() &&
            this.state.passwordConfirm.length > 0 && (
              <p className="p-13 red mt-1 mb-2">
                <img src={warining} alt="" className="mr-2" />
                비밀번호가 잘못되었습니다.
              </p>
            )}
        </div>
        <div className="text-center mt-5 mb-100 hidden-m">
          <button
            onClick={() => this.props.history.goBack()}
            className="btn-custom mr-2"
          >
            취소
          </button>
          {!this.verifyAll() && (
            <button className="btn-custom-disable">저장</button>
          )}
          {this.verifyAll() && (
            <button
              onClick={this.handleChangeSubmit}
              className="btn-custom-active"
            >
              저장
            </button>
          )}
        </div>
        <div className="text-center mt-5 mb-100 flex hidden-p">
          <button
            onClick={() => this.props.history.goBack()}
            className="btn-custom mr-2"
          >
            취소
          </button>
          {!this.verifyAll() && (
            <button className="btn-custom-disable">저장</button>
          )}
          {this.verifyAll() && (
            <button
              onClick={this.handleChangeSubmit}
              className="btn-custom-active"
            >
              저장
            </button>
          )}
        </div>
      </div>
    );
  };

  private componentComplete = () => {
    return (
      <div className="container">
        <div className="mt-180 mb-180">
          <div className="text-center hidden-m">
            <img src={complete} alt="" />
          </div>
          <div className="mt-180 hidden-p" />

          <p className="text-center black-2 p-22 mt-3 hidden-m">
            비밀번호 재설정이 완료되었습니다
          </p>
          <p className="text-center black-2 p-22 mt-3 hidden-p">
            비밀번호 재설정이
            <br /> 완료되었습니다
          </p>
          <p className="text-center p-14 gray-2 mt-2">
            새로운 비밀번호로 변경이 완료되었습니다.
            <br />
            변경된 비밀번호로 서비스를 이용해 주시기 바랍니다.
          </p>

          <div className="text-center mt-6 hidden-m">
            <Link to="/">
              <button className="btn-custom mr-2">홈 바로가기</button>
            </Link>
            <Link to="/login/">
              <button className="btn-custom-active">로그인하기</button>
            </Link>
          </div>
          <div className="text-center mt-5 hidden-p">
            <Link to="/">
              <button className="btn-custom-sm mr-1">홈 바로가기</button>
            </Link>
            <Link to="/login/">
              <button className="btn-custom-sm-active pl-4 pr-4 ml-1">
                로그인하기
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  };
}

export default FindPassword;
