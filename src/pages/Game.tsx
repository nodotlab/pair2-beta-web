import React from "react";
import { Link } from "react-router-dom";
import image1 from "../images/Game_01_PC.png";
import image2 from "../images/Game_02_PC.png";
import imgPlus from "../images/Icon_Plus.svg";

interface GameState {
  sneakers: Sneaker[];
}

class Sneaker {
  public index: string = "";
  public name: string = "";
  public subName: string = "";
  public favorite: boolean = false;
  public imageUrl: any;
  public theme: string;

  constructor(
    _index: string,
    _name: string,
    _subName: string,
    _imageUrl: any,
    _theme: string
  ) {
    this.index = _index;
    this.name = _name;
    this.subName = _subName;
    this.imageUrl = _imageUrl;
    this.theme = _theme;
  }
}

class Game extends React.Component<any, GameState> {
  constructor(props: any) {
    super(props);
    this.state = {
      sneakers: this.initSneakers(),
    };
  }

  private initSneakers(): any {
    const tmpArray: Sneaker[] = [];
    tmpArray.push(
      new Sneaker(
        Date.now().toString(),
        "sacai x Nike LDWaffle Summit White",
        "사카이 x 나이키 LD와플 화이트",
        image1,
        "white"
      )
    );
    tmpArray.push(
      new Sneaker(
        Date.now().toString(),
        "Air Jordan XXXIV Blue Void",
        "에어조던 34 블루 보이드",
        image2,
        "black"
      )
    );
    return tmpArray;
  }

  public render() {
    return (
      <div className="mb-5">
        <div className="container pt-4">
          {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
            <Link to={`/game/${sneaker.index}`} key={i}>
              <div
                className={`game-mobilebox hidden-p ${
                  sneaker.theme !== "white" && "game-mobilegray"
                }`}
              >
                <p
                  className={`p-24 roboto ${
                    sneaker.theme === "white" ? "white" : "black-2"
                  }`}
                >
                  {sneaker.name}
                </p>
                <p className="p-13 orange-3 mt-1 bold">{sneaker.subName}</p>
              </div>

              <div
                style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                className="img-game-back"
              >
                <div className="box-game-in">
                  <p
                    className={`p-42 roboto hidden-m ${
                      sneaker.theme === "white" ? "white" : "black-2"
                    }`}
                  >
                    {sneaker.name}
                  </p>
                  <p className="p-16 hidden-m bold orange-3 mt-3">
                    {sneaker.subName}
                  </p>

                  <Link to={`/game/${sneaker.index}`}>
                    <div
                      className={`hidden-m box-plus mt-5 ${
                        sneaker.theme === "white"
                          ? "box-plus-orange"
                          : "box-plus-black"
                      }`}
                    >
                      <span className="box-plus-icon">
                        <img src={imgPlus} alt="" />
                      </span>
                      <p className="mt-2">자세히보기</p>
                    </div>
                  </Link>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

export default Game;
