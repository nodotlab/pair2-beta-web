import React from "react";
import { timeFormat4, timeFormat5 } from "../Method";
import { releaseToday } from "../db/Actions";
import { filters } from "../db/Actions";
import { Link } from "react-router-dom";
import Release from "../reduce/Release";
import imgPrev from "../images/Arrow_Prev.svg";
import imgNext from "../images/Arrow_Next.svg";
import imgClose from "../images/GNB_Close.svg";
import imgReset from "../images/reset.svg";
import imgDown from "../images/tab_open.svg";
import imgFOpen from "../images/Filter_Open.svg";
import imgFClose from "../images/Filter_Close.svg";
import MetaData from "../components/MetaGenerator";
import 'moment/locale/ko';
import moment from 'moment-timezone';
import Clock from 'react-live-clock';

interface RelaseState {
  initData: Release[];
  data: Release[];
  year: number; // filter year
  month: number; // filter month
  filters: string[];
  otherBrandOpen: boolean;
  filterBrand: string[];
  filterOtherBrand: string[];
  openFilter: boolean;
}

class ReleaseTodayPage extends React.Component<any, RelaseState> {
  constructor(props: any) {
    super(props);
    this.state = {
      initData: [],
      data: [],
      year: this.getThisYear(),
      month: this.getThisMonth(),
      filters: [],
      otherBrandOpen: false,
      filterBrand: [],
      filterOtherBrand: [],
      openFilter: false,
    };
  }

  // private scrollRef: any = React.createRef();

  public componentDidMount() {
    // this.scrollRef.current.scrollLeft = 55 * new Date().getMonth();
    this.initBrands();
  }

  private async initBrands() {
  
    moment.locale('ko'); //현재시간 실시간 카운트, 지역설정
    
    const _filters: any = await filters();
    const brands: string[] = [];
    const otherBrands: string[] = [];

    for (let i = 0; i < _filters.length; ++i) {
      if (!_filters[i].other) {
        brands.push(_filters[i].name.toUpperCase());
      } else if (_filters[i].name.toUpperCase() !== "OTHER") {
        otherBrands.push(_filters[i].name.toUpperCase());
      }
    }

    brands.sort((a: any, b: any) => {
      if (a > b) {
        return 1;
      } else {
        return -1;
      }
    });

    otherBrands.sort((a: any, b: any) => {
      if (a > b) {
        return 1;
      } else {
        return -1;
      }
    });

    otherBrands.push("OTHER");

    this.setState({
      filterBrand: brands,
      filterOtherBrand: otherBrands,
    });

    const tmpRelease: Release[] = [];

    const _release = await releaseToday();

    for(const store of _release){
      tmpRelease.push(
        new Release(
          store.snkrs.releaseDate,
          store.snkrs.brand.toUpperCase(),
          store.snkrs.nameKr,
          store.snkrs.imageUrl,
          store.snkrs.unknownRelease,
          store.snkrs.type,
          store.snkrs.index,
        )
      );
    }

    // for (let i = 0; i < _release.length; ++i) {
    //   tmpRelease.push(
    //     new Release(
    //       _release[i].releaseDate,
    //       _release[i].brand.toUpperCase(),
    //       _release[i].name,
    //       _release[i].imageUrl,
    //       _release[i].unknownRelease,
    //       _release[i].type,
    //       _release[i].index,
    //     ),
    //   );
    // }

    tmpRelease.sort((a: Release, b: Release) => {
      if (a.name > b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    tmpRelease.sort((a: Release, b: Release) => {
      if (a.releaseTimestamp > b.releaseTimestamp) return -1;
      if (a.releaseTimestamp > b.releaseTimestamp) return 1;
      return 0;
    });

    this.setState({ initData: tmpRelease }, () => this.currentFilter());
  }

  private getThisYear = (): number => {
    return new Date().getFullYear();
  };

  private getThisMonth = (): number => {
    return new Date().getMonth() + 1;
    // return new Date().getMonth();
  }; // test mode

  private handleYear = (operator: string): void => {
    if (operator === "-") {
      const prevYear: number = this.state.year - 1;
      this.setState(
        {
          year: prevYear,
        },
        () => this.currentFilter(),
      );
    }
    if (operator === "+") {
      const nextYear: number = this.state.year + 1;
      this.setState(
        {
          year: nextYear,
        },
        () => this.currentFilter(),
      );
    }
  };

  private handleMonth = (_month: number): void => {
    this.setState({ month: _month }, () => this.currentFilter());
  };

  private calenderStyle = (month: number): string => {
    if (month === this.state.month) {
      return "calendar-month-active";
    } else if (month > this.state.month) {
      return "calendar-month-future";
    } else {
      return "calendar-month";
    }
  };

  private currentFilter(): void {
    const data = [];
    for (let i in this.state.initData) {
      const year = new Date(
        this.state.initData[i].releaseTimestamp,
      ).getFullYear();
      const month =
        new Date(this.state.initData[i].releaseTimestamp).getMonth() + 1;

      if (this.state.year === year && this.state.month === month) {
        data.push(this.state.initData[i]);
      }
    }

    let data2 = [];
    if (this.state.filters.length > 0) {
      for (let i in data) {
        if (this.state.filters[0] === data[i].brand) {
          data2.push(data[i]);
        }
      }
    } else {
      data2 = data;
    }

    this.setState({ data: data2 });
  }

  private handleFilterAdd = (brand: string): void => {
    this.setState({ filters: [brand] }, () => this.currentFilter());
  };

  private handleFilterPop = (brand: string): void => {
    const tmpArray: string[] = [];
    for (let i = 0; i < this.state.filters.length; ++i) {
      if (this.state.filters[i] !== brand) {
        tmpArray.push(this.state.filters[i]);
      }
    }

    this.setState({ filters: tmpArray }, () => this.currentFilter());
  };

  private existFilter = (filter: string): boolean => {
    let exist: boolean = false;
    for (let i = 0; i < this.state.filters.length; ++i) {
      if (filter === this.state.filters[i]) exist = true;
    }

    return exist;
  };

  private handleReset = (): void =>
    this.setState({ filters: [] }, () => this.currentFilter());

  private handleFilterDelete = (name: string): void => {
    const tempFilters: string[] = [];

    for (let i = 0; i < this.state.filters.length; ++i) {
      if (name !== this.state.filters[i]) {
        tempFilters.push(this.state.filters[i]);
      }
    }

    this.setState({ filters: tempFilters }, () => this.currentFilter());
  };

  private handleOtherBrand = (): void =>
    this.setState({ otherBrandOpen: !this.state.otherBrandOpen });

  private handleFilterOpen = (): void =>
    this.setState({ openFilter: !this.state.openFilter });

  private dateDisable = (releaseTimestamp: number): boolean => {
    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    if (date.getTime() < releaseTimestamp + 1000) {
      return false;
    } else {
      return true;
    }
  };

  private handleMore = (): void => {};

  

  public render() {

    const metaData = {
      title: "런칭캘린더 - 한정판 신발 스니커즈 실시간 발매 일정 | PAIR2(페어투)",
      description : "실시간으로 업데이트 되는 나이키, 아디다스, 조던 등의 한정판 신발 스니커즈 발매 일정을 페어투 확인해 보세요."      
    }

    return (
      <div className="ReleasePage">        
      <MetaData data={metaData}/>
        {/* <div className="box-calendar-today hidden-m">
          <div className="container">
            <div className="box-calendar-years">
              <span onClick={() => this.handleYear("-")}>
                <img src={imgPrev} className="mr-3 pb-1 cursor" />
              </span>
              {this.state.year}
              <span onClick={() => this.handleYear("+")}>
                <img src={imgNext} className="ml-3 pb-1 cursor" />
              </span>
            </div>
            <div
              onClick={() => this.handleMonth(1)}
              className={`${this.calenderStyle(1)}`}
            >
              1월
            </div>
            <div
              onClick={() => this.handleMonth(2)}
              className={`${this.calenderStyle(2)}`}
            >
              2월
            </div>
            <div
              onClick={() => this.handleMonth(3)}
              className={`${this.calenderStyle(3)}`}
            >
              3월
            </div>
            <div
              onClick={() => this.handleMonth(4)}
              className={`${this.calenderStyle(4)}`}
            >
              4월
            </div>
            <div
              onClick={() => this.handleMonth(5)}
              className={`${this.calenderStyle(5)}`}
            >
              5월
            </div>
            <div
              onClick={() => this.handleMonth(6)}
              className={`${this.calenderStyle(6)}`}
            >
              6월
            </div>
            <div
              onClick={() => this.handleMonth(7)}
              className={`${this.calenderStyle(7)}`}
            >
              7월
            </div>
            <div
              onClick={() => this.handleMonth(8)}
              className={`${this.calenderStyle(8)}`}
            >
              8월
            </div>
            <div
              onClick={() => this.handleMonth(9)}
              className={`${this.calenderStyle(9)}`}
            >
              9월
            </div>
            <div
              onClick={() => this.handleMonth(10)}
              className={`${this.calenderStyle(10)}`}
            >
              10월
            </div>
            <div
              onClick={() => this.handleMonth(11)}
              className={`${this.calenderStyle(11)}`}
            >
              11월
            </div>
            <div
              onClick={() => this.handleMonth(12)}
              className={`${this.calenderStyle(12)}`}
            >
              12월
            </div>
          </div>
        </div> */}

        {/* <div className="calendar-yearbox border-bottom hidden-p">
          <div className="calendar-left p-18 bold">
            <span onClick={() => this.handleYear("-")}>
              <img src={imgPrev} className="mr-4 pb-1 cursor" />
            </span>
            {this.state.year}
            <span onClick={() => this.handleYear("+")}>
              <img src={imgNext} className="ml-4 pb-1 cursor" />
            </span>
          </div>
          <div ref={this.scrollRef} className="horizontal-years">
            <div
              onClick={() => this.handleMonth(1)}
              className={`calendar-box ${this.calenderStyle(1)}`}
            >
              1월
            </div>
            <div
              onClick={() => this.handleMonth(2)}
              className={`calendar-box ${this.calenderStyle(2)}`}
            >
              2월
            </div>
            <div
              onClick={() => this.handleMonth(3)}
              className={`calendar-box ${this.calenderStyle(3)}`}
            >
              3월
            </div>
            <div
              onClick={() => this.handleMonth(4)}
              className={`calendar-box ${this.calenderStyle(4)}`}
            >
              4월
            </div>
            <div
              onClick={() => this.handleMonth(5)}
              className={`calendar-box ${this.calenderStyle(5)}`}
            >
              5월
            </div>
            <div
              onClick={() => this.handleMonth(6)}
              className={`calendar-box ${this.calenderStyle(6)}`}
            >
              6월
            </div>
            <div
              onClick={() => this.handleMonth(7)}
              className={`calendar-box ${this.calenderStyle(7)}`}
            >
              7월
            </div>
            <div
              onClick={() => this.handleMonth(8)}
              className={`calendar-box ${this.calenderStyle(8)}`}
            >
              8월
            </div>
            <div
              onClick={() => this.handleMonth(9)}
              className={`calendar-box ${this.calenderStyle(9)}`}
            >
              9월
            </div>
            <div
              onClick={() => this.handleMonth(10)}
              className={`calendar-box ${this.calenderStyle(10)}`}
            >
              10월
            </div>
            <div
              onClick={() => this.handleMonth(11)}
              className={`calendar-box ${this.calenderStyle(11)}`}
            >
              11월
            </div>
            <div
              onClick={() => this.handleMonth(12)}
              className={`calendar-box ${this.calenderStyle(12)}`}
            >
              12월
            </div>
          </div>
        </div> */}

        <div className="today-realtiem-margin hidden-m">
          
        </div>

        <div className="bg-gray border-bottom pt-2 hidden-p" />
        <div
          onClick={this.handleFilterOpen}
          className="box-mobile-filter text-center hidden-p mt-1"
        >
          <p className="p-13 black-2 roboto bold letter-space-4">
            FILTERS
            <img
              src={this.state.openFilter ? imgFOpen : imgFClose}
              alt=""
              className="ml-1"
            />
          </p>
        </div>
        {this.state.openFilter && (
          <div className="horizontal-filter hidden-p">
            <div className="mobile-reset">
              <div className="mobile-reset-left"></div>
              <div className="mobile-reset-right">
                <p
                  onClick={this.handleReset}
                  className="p-14 roboto gray-2 cursor"
                >
                  Reset <img src={imgReset} />
                </p>
              </div>
            </div>
            {this.state.filters.map((filter: string, i: number) => (
              <button
                onClick={() => this.handleFilterDelete(filter)}
                key={i}
                className="btn btn-filtered horizontal-filter-btn"
              >
                {filter} <img src={imgClose} className="icon-close" />
              </button>
            ))}
          </div>
        )}
        {this.state.openFilter && (
          <div className="hidden-p mt-5">
            <div className="flex mt-4">
              <div className="col-3 pl-0 pr-0 text-center">
                <p className="p-16 bold mt-1">브랜드</p>
              </div>
              <div className="col-9 pl-0 pr-0">
                {this.state.filterBrand.map((brand: string, i: number) => (
                  <button
                    onClick={() => this.handleFilterAdd(brand)}
                    key={i}
                    className={`btn btn-brand ${
                      this.existFilter(brand) && "btn btn-brand-active"
                    }`}
                  >
                    {brand}
                  </button>
                ))}
                <button
                  onClick={this.handleOtherBrand}
                  className={`btn btn-brand ${
                    this.state.otherBrandOpen && "btn-brand"
                  }`}
                >
                  {"Other Brands".toUpperCase()}
                </button>
                {this.state.otherBrandOpen &&
                  this.state.filterOtherBrand.map(
                    (brand: string, i: number) => (
                      <button
                        onClick={() => this.handleFilterAdd(brand)}
                        key={i}
                        className={`btn btn-other ${
                          this.existFilter(brand) && "btn-other-active"
                        }`}
                      >
                        {brand}
                      </button>
                    ),
                  )}
              </div>
            </div>
          </div>
        )}

        <div className="container">
          <div className="row">
            <div className="col-lg-3 hidden-m">
              <div className="row">
                <div className="col-6">
                  <p className="p-18 black-2 roboto bold letter-space-4 mb-3">
                    FILTERS
                  </p>
                </div>
                <div className="col-6 text-right">
                  {this.state.filters.length > 0 && (
                    <p
                      onClick={this.handleReset}
                      className="p-14  roboto gray-2 cursor pt-1"
                    >
                      Reset <img src={imgReset} />
                    </p>
                  )}
                </div>
              </div>

              {this.state.filters.map((filter: string, i: number) => (
                <button
                  key={i}
                  onClick={() => this.handleFilterPop(filter)}
                  className="box-filter"
                >
                  {filter} <img src={imgClose} className="icon-close" />
                </button>
              ))}

              <p className="p-16 bold black-2 mt-4 mb-2">브랜드</p>

              {this.state.filterBrand.map((brand: string, i: number) => (
                <button
                  key={i}
                  onClick={() => this.handleFilterAdd(brand)}
                  className={`btn btn-brand ${
                    this.existFilter(brand) && "btn btn-brand-active"
                  }`}
                >
                  {brand}
                </button>
              ))}

              <button
                onClick={this.handleOtherBrand}
                className={`btn btn-brand ${
                  this.state.otherBrandOpen && "btn btn-brand"
                }`}
              >
                {"Other Brands".toUpperCase()}
              </button>
              {this.state.otherBrandOpen &&
                this.state.filterOtherBrand.map((brand: string, i: number) => (
                  <button
                    key={i}
                    onClick={() => this.handleFilterAdd(brand)}
                    className={`btn btn-other ${
                      this.existFilter(brand) && "btn btn-other-active"
                    }`}
                  >
                    {brand}
                  </button>
                ))}
            </div>

            <div className="hidden-p w-100 mt-5" />
            <div className="col-lg-9">
              <div className="row mb-4">
                <div className="col-10">
                  <p className="p-24 black-2">
                    <span className="bold">                        
                      <Clock format={'M월 D일 (ddd) H:mm:ss '} ticking={true} />                   
                    </span>
                  </p>
                  <p className="p-16 gray-2 today-count">
                    오늘의 발매 <span className="bold">{this.state.data.length}</span>개
                  </p>     
                </div>
              </div>
              <div className="row sneaker-mobile-row">
                {this.state.data.map((data: Release, i: number) => (
                  <Link
                    to={`/release/${data.index}`}
                    className="col-md-4 col-6 sneaker-mobile mb-4 cursor"
                    key={i}
                  >
                    <div
                      className="img-release"
                      style={{ backgroundImage: `url(${data.imageUrl})` }}
                    >
                      {/* <p
                        className={`release-date float-right ${
                          this.dateDisable(data.releaseTimestamp) &&
                          "date-disable"
                        }`}
                      >
                        {data.unknownRelease
                          ? new Date(data.releaseTimestamp).getMonth() +
                            1 +
                            "월중"
                          : timeFormat4(data.releaseTimestamp).toString()}
                      </p> */}
                    </div>

                    {data.type === 1 && (
                      <button className="btn-release">응모 예정</button>
                    )}
                    {/* type 0 */}
                    {data.type === 2 && (
                      <button className="btn-release">응모 시작</button>
                    )}
                    {/* type 1 */}
                    {data.type === 3 && (
                      <button className="btn-release">선착순 예정</button>
                    )}
                    {/* type 2 */}
                    {data.type === 4 && (
                      <button className="btn-pre">발매일 미정</button>
                    )}
                    {/* type 3 */}
                    {!data.type && this.dateDisable(data.releaseTimestamp) && (
                      <button className="btn-done">발매</button>
                    )}

                    <p className="p-12 gray-2 bold mt-3">{data.brand}</p>
                    <p className="sneaker-name p-14 black-2 mt-1 hidden-m">
                      {data.name.toUpperCase()}
                    </p>
                    <p className="sneaker-name p-14 black-2 mt-1 hidden-p">
                      {data.name.toUpperCase().substr(0, 40)}
                      {data.name.length > 40 && "..."}
                    </p>
                  </Link>
                ))}
              </div>

              <div className="text-center mt-5 mb-5">
                {this.state.data.length > 29 && (
                  <button
                    onClick={this.handleMore}
                    className="btn btn-custom w-100 gray-3 p-15"
                  >
                    더보기
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ReleaseTodayPage;
