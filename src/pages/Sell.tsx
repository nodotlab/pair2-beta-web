import React from "react";
import { Link } from "react-router-dom";

import "react-html5-camera-photo/build/css/index.css";
import DaumPostcode from "react-daum-postcode";
import AddBilling from "../components/AddBillingSell";
import CardState from "../reduce/BillingState";
import BankState from "../reduce/BankState";
import { currency } from "../Method";
import imgCloseX from "../images/Popup_Delete.svg";
import {
  getBank,
  getCard,
  getSneakerName,
  tradeUpload,
  tradeImageUpload,
  getUserVerify,
  getCurrentUser,
  cheatRecurrent,
  getCommision,
} from "../db/Actions";
import imgClose from "../images/tab_close.svg";
import imgOpen from "../images/tab_open.svg";
import imgThumb from "../images/thumb.png";
import bullet from "../images/bullet.svg";
import imgComplete from "../images/Step_Completed.svg";
import check from "../images/Condition_Check.svg";
import agreeDisable from "../images/Agree_Disabled.svg";
import agreeActive from "../images/Agree_Active.svg";
import sample1 from "../images/sample1.jpg";
import sample2 from "../images/sample2.jpg";
import sample3 from "../images/sample3.jpg";
import sample4 from "../images/sample4.jpg";
import sample5 from "../images/sample5.jpg";
import sample6 from "../images/sample6.jpg";
import sample7 from "../images/sample7.jpg";
import sample8 from "../images/sample8.jpg";
import sample9 from "../images/sample9.jpg";
import sample11 from "../images/sample11.jpg";
import sample12 from "../images/sample12.jpg";
import sample13 from "../images/sample13.jpg";
import sample14 from "../images/sample14.jpg";
import sample15 from "../images/sample15.jpg";
import camera from "../images/Camera.svg";
import imgDelete from "../images/Thumbnail_Delete.svg";
import modalClose from "../images/Popup_Delete.svg";
import filterX from "../images/GNB_Close.svg";
import imgPrice from "../images/Price_Sub.svg";
import UserVerify from "../components/UserVerify";
import imgPrev from "../images/backblack.svg";

class CurrentSneaker {
  public index: string;
  public brand: string;
  public name: string;
  public imgUrl: string;

  constructor(_index: string, _brand: string, _name: string, _imgUrl: string) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.imgUrl = _imgUrl;
  }
}

class Trading {
  public size: string;
  public price: number;
  public bidSize: number;
  public askSize: number;

  constructor(
    _size: string,
    _price: number,
    _bidSize: number,
    _askSize: number,
  ) {
    this.size = _size;
    this.price = _price;
    this.bidSize = _bidSize;
    this.askSize = _askSize;
  }
}

class History {
  public size: string;
  public price: number;
  public timestamp: number;

  constructor(_size: string, _price: number, _timestamp: number) {
    this.size = _size;
    this.price = _price;
    this.timestamp = _timestamp;
  }
}

interface SellState {
  cameraOrigin: boolean;
  currentSneaker: CurrentSneaker;
  sizes: string[];
  selectSize: string;
  openSize: boolean;
  openTrade: boolean;
  tradingList: Trading[];
  openHistory: boolean;
  historyList: History[];
  stepScreen: number;
  openCondition1: boolean;
  openCondition2: boolean;
  openCondition3: boolean;
  openCondition4: boolean;
  openCondition5: boolean;
  isNew: boolean;
  from: string;
  damage1: boolean;
  damage2: boolean;
  damage3: boolean;
  damage4: boolean;
  damageDesc: string;
  damageCheck: boolean;
  status1: boolean;
  status2: boolean;
  status3: boolean;
  statusDesc: string;
  statusCheck: boolean;
  modal: boolean;
  file: any[];
  shippingType: string;
  location: string[];
  modal2: boolean;
  modalAddress: boolean;
  priceInput: number;
  price: number;
  step2check: boolean;
  agree: boolean;
  index: number;
  cardState: CardState;
  bankState: BankState;
  owner: string;
  bank: string;
  account: string;
  blockSubmit: boolean;
  userVerify: boolean;
  phone: string;
  tripleZero: boolean;
  commision: number;
  sale: number;
}

class Sell extends React.Component<any, SellState> {
  constructor(props: any) {
    super(props);
    this.state = {
      cameraOrigin: false,
      currentSneaker: new CurrentSneaker(
        Date.now().toString(),
        "",
        "",
        imgThumb,
      ),
      sizes: this.initSizes(),
      selectSize: "",
      openSize: true,
      openTrade: false,
      tradingList: [],
      openHistory: false,
      historyList: [],
      stepScreen: 0, // default = 0
      openCondition1: true,
      openCondition2: false,
      openCondition3: false,
      openCondition4: false,
      openCondition5: true,
      isNew: false,
      from: "",
      damage1: false,
      damage2: false,
      damage3: false,
      damage4: false,
      damageDesc: "",
      damageCheck: false,
      status1: false,
      status2: false,
      status3: false,
      statusDesc: "",
      statusCheck: false,
      modal: false,
      file: [],
      shippingType: "",
      location: [],
      modal2: false,
      modalAddress: false,
      priceInput: 0,
      price: 0,
      step2check: false,
      agree: false,
      index: 0,
      cardState: new CardState("", ""),
      bankState: new BankState("", "", ""),
      owner: "",
      bank: "",
      account: "",
      blockSubmit: false,
      userVerify: true,
      phone: "",
      tripleZero: false,
      commision: 0,
      sale: 0,
    };
  }

  public componentDidMount() {
    if (!localStorage.getItem("pair2token"))
      this.props.history.push(`/login/sell/${this.props.match.params.id}`);
    if (navigator.platform.substr(0, 2) === "iP") {
    }
    this.setState({ cameraOrigin: true });

    this.userVerify();
    this.userPhone();
    this.getCurrentSneaker();
    this.commition();
  }

  private async commition() {
    const result = await getCommision();
    this.setState({
      commision: result.commision,
      sale: result.sale,
    });
  }

  private async userVerify() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getUserVerify(token);
      if (result !== true) {
        this.setState({ userVerify: false });
      }
    } catch (error) {}
  }

  private async userPhone() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCurrentUser(token);
      this.setState({ phone: result.phone });
    } catch (error) {}
  }

  private handleUserVerify = (): void => {
    this.setState({ userVerify: true });
  };

  private fileRef: any = React.createRef();
  private handleFileRef = (): void => this.fileRef.current.click();

  private handleFile = async (e: any) => {
    if (this.state.file.length > 14) return;
    const files: any = this.state.file;
    files.push(e.target.files[0]);
    this.setState({ file: files });
  };

  private handleTakePhoto = (dataUri: any) => {
    if (this.state.file.length > 14) return;

    const files: any = this.state.file;
    const blob = this.dataURItoBlob(dataUri);
    files.push(blob);
    this.setState({
      file: files,
      modal: false,
    });
  };

  private dataURItoBlob(dataURI: any) {
    const byteString = atob(dataURI.split(",")[1]);
    const mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; ++i) {
      ia[i] = byteString.charCodeAt(i);
    }
    const bb = new Blob([ab], { type: mimeString });
    return bb;
  }

  private handleImageDelete = (index: number) => {
    const file: any[] = [];
    for (let i = 0; i < this.state.file.length; ++i) {
      if (index !== i) file.push(this.state.file[i]);
    }

    this.setState({ file: file });
  };

  private async getCurrentSneaker() {
    const { result }: any = await getSneakerName(this.props.match.params.id);
    const sneaker: CurrentSneaker = new CurrentSneaker(
      result.index,
      result.brand,
      result.name,
      result.thumbUrl,
    );

    this.setState({ currentSneaker: sneaker });
  }

  private initSizes(): string[] {
    const tempSizes = [];
    for (let i = 220; i < 315; i += 5) {
      tempSizes.push(i.toString());
    }

    return tempSizes;
  }

  private sortTradingList(size: string): void {
    const tradingInitList: Trading[] = this.getTradingList();
    let tradingList: Trading[] = [];
    const askList: Trading[] = [];
    const bidList: Trading[] = [];

    for (let i = 0; i < tradingInitList.length; ++i) {
      if (size === tradingInitList[i].size) {
        tradingList.push(tradingInitList[i]);
      }
    }

    for (let i = 0; i < tradingList.length; ++i) {
      if (tradingList[i].bidSize === 0) {
        askList.push(tradingList[i]);
      } else {
        bidList.push(tradingList[i]);
      }
    }

    bidList.sort((a, b) => {
      if (a.price > b.price) {
        return -1;
      }
      if (a.price < b.price) {
        return 1;
      }
      return 0;
    });

    askList.sort((a, b) => {
      if (a.price > b.price) {
        return 1;
      }
      if (a.price < b.price) {
        return -1;
      }
      return 0;
    });

    tradingList = [];

    if (bidList.length < 3) {
      for (let i = 0; i < bidList.length; ++i) {
        tradingList.push(bidList[i]);
      }
    } else {
      for (let i = 0; i < 3; ++i) {
        tradingList.push(bidList[i]);
      }
    }
    tradingList.reverse();

    if (askList.length < 3) {
      for (let i = 0; i < askList.length; ++i) {
        tradingList.push(askList[i]);
      }
    } else {
      for (let i = 0; i < 3; ++i) {
        tradingList.push(askList[i]);
      }
    }

    this.setState({ tradingList: tradingList });
  }

  private getTradingList(): Trading[] {
    const getTradingList: Trading[] = [];
    return getTradingList;
  }

  private getHistoryList(size: string): void {
    const arr: History[] = [];

    for (let i = 0; i < 6; ++i) {
      arr.push(new History(size, 300000, Date.now()));
    }

    this.setState({ historyList: arr });
  }

  private handleSize = (size: string): void => {
    this.sortTradingList(size);
    this.getHistoryList(size);

    this.setState({
      selectSize: size,
      openSize: false,
      openTrade: true,
      openHistory: true,
    });
  };

  private handleText = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleDamageCheck = (): void => {
    this.setState({
      damageCheck: true,
      openCondition3: false,
      openCondition4: true,
    });
  };

  private handleStatusCheck = (): void => {
    this.setState({
      statusCheck: true,
      openCondition4: false,
      openCondition5: true,
    });
  };

  private step0Check(): boolean {
    if (
      this.state.selectSize !== "" &&
      this.state.isNew &&
      this.state.from !== "" &&
      this.state.damageCheck &&
      this.state.statusCheck &&
      this.state.file.length > 2
    ) {
      return true;
    }

    return false;
  }

  private step1Check(): boolean {
    if (
      this.state.shippingType === "직거래" &&
      this.state.location.length > 0 &&
      this.state.price > 0
    ) {
      return true;
    }

    if (
      this.state.shippingType === "직거래/택배" &&
      this.state.location.length > 0 &&
      this.state.price > 0
    ) {
      return true;
    }

    if (this.state.shippingType === "택배" && this.state.price > 0) {
      return true;
    }

    return false;
  }

  private anyDamage(): boolean {
    if (
      this.state.damage1 ||
      this.state.damage2 ||
      this.state.damage3 ||
      this.state.damage4
    ) {
      return true;
    } else {
      {
        return false;
      }
    }
  }

  private anyStatus(): boolean {
    if (this.state.status1 || this.state.status2 || this.state.status3) {
      return true;
    } else {
      {
        return false;
      }
    }
  }

  public handleCheckTrue = (): void => {
    this.setState({ step2check: true });
  };
  public handleCheckFalse = (): void => {
    this.setState({ step2check: false });
  };

  private handleAddress = (data: any): void => {
    const locations = this.state.location;

    if (locations.length < 2) {
      locations.push(data.sigungu);
    }

    this.setState({
      modalAddress: false,
      location: locations,
    });
  };

  private handlePopLocation = (index: number): void => {
    const locations: string[] = [];
    for (let i = 0; i < this.state.location.length; ++i) {
      if (index !== i) locations.push(this.state.location[i]);
    }

    this.setState({ location: locations });
  };

  private handlePrice = (e: any): void => {
    let n = e.target.value.replace(/\D/, "");
    n = parseInt(n);
    if (isNaN(n)) {
      this.setState({
        priceInput: 0,
        price: 0,
      });
    } else {
      this.setState({
        priceInput: n,
        price: n * 1000,
      } as any);
    }
  };

  private handlePrev = (): void => {
    this.setState({ stepScreen: this.state.stepScreen - 1 });
    window.scroll(0, 0);
  };

  private handleNext = (): void => {
    this.setState({ stepScreen: this.state.stepScreen + 1 });
    window.scroll(0, 0);
  };

  private handleSubmit = async () => {
    this.setState({ blockSubmit: true });
    const token: any = localStorage.getItem("pair2token");

    try {
      const result = await getCard(token);
      if (result !== "faild") {
        this.setState({
          cardState: new CardState(result.cardName, result.cardNumber),
        });
      }
    } catch (error) {}

    try {
      const result = await getBank(token);
      this.setState({
        owner: result.owner,
        bank: result.bankName,
        account: result.account,
        bankState: new BankState(result.bankName, result.account, result.owner),
      });
    } catch (error) {}

    const formData = new FormData();
    for (let i = 0; i < this.state.file.length; ++i) {
      formData.append("photos", this.state.file[i]);
    }

    const filesUrl: string[] = await tradeImageUpload(formData);

    const data = {
      token: token,
      sneakerIndex: this.state.currentSneaker.index,
      imageUrls: filesUrl,
      size: this.state.selectSize,
      isNew: this.state.isNew,
      from: this.state.from,
      damage1: this.state.damage1,
      damage2: this.state.damage2,
      damage3: this.state.damage3,
      damage4: this.state.damage4,
      damageDesc: this.state.damageDesc,
      status1: this.state.status1,
      status2: this.state.status2,
      status3: this.state.status3,
      statusDesc: this.state.statusDesc,
      shippingType: this.state.shippingType,
      location1: this.state.location[0],
      location2: this.state.location.length > 1 ? this.state.location[1] : "",
      price: this.state.price,
    };

    try {
      const result = await tradeUpload(data);
      if (result !== "faild") {
        this.setState({
          stepScreen: 4,
          index: result,
          blockSubmit: false,
        });
      } else {
        this.setState({ blockSubmit: false });
      }
    } catch (error) {
      this.setState({ blockSubmit: false });
    }

    try {
      cheatRecurrent(this.state.phone);
    } catch (error) {}
  };

  private isTripleZero(): boolean {
    if (this.state.tripleZero) return true;
    if (this.state.priceInput > 0) return true;
    return false;
  }

  public render() {
    return (
      <>
        {!this.state.userVerify && (
          <UserVerify handleUserVerify={this.handleUserVerify} />
        )}
        {this.state.userVerify && this.state.stepScreen < 4 && (
          <this.ComponentMain />
        )}
        {this.state.userVerify && this.state.stepScreen > 3 && (
          <this.ComponentScreen4 />
        )}
      </>
    );
  }

  private ComponentMain = () => {
    return (
      <>
        <div className="pt-4 pb-4 bg-gray border-bottom">
          <div className="container">
            <div className="row">
              <div className="col-4 pr-0">
                <div
                  className="img-buy-left"
                  style={{
                    backgroundImage: `url(${this.state.currentSneaker.imgUrl})`,
                  }}
                />
              </div>
              <div className="col-8">
                <p className="p-14 bold roboto gray-2">
                  {this.state.currentSneaker.brand}
                </p>
                <p className="p-16 roboto black-2 mt-1">
                  {this.state.currentSneaker.name}
                </p>
                <Link to={`/sneaker/${this.state.currentSneaker.name}`}>
                  <button className="btn btn-custom btn-xs mt-2">
                    상세보기
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>

        {this.state.stepScreen === 0 && (
          <div className="border-bottom">
            <div className="container mt-4 mb-4">
              {this.state.selectSize !== "" ? (
                <div
                  onClick={() =>
                    this.setState({ openSize: !this.state.openSize })
                  }
                  className="row mb-3"
                >
                  <div className="col-10">
                    <p className="p-16 bold black-2">
                      사이즈{" "}
                      <span className="p-18 roboto bold orange ml-2">
                        {this.state.selectSize}
                      </span>
                    </p>
                  </div>
                  <div className="col-2 text-right">
                    {this.state.openSize ? (
                      <img src={imgOpen} alt="" className="" />
                    ) : (
                      <img src={imgClose} alt="" className="" />
                    )}
                  </div>
                </div>
              ) : (
                <div className="row mb-2">
                  <div className="col-8">
                    <p className="p-16 bold black-2">사이즈를 선택해주세요</p>
                  </div>
                  <div className="col-4 text-right">
                    <p className="p-13 underline gray-2 pt-1">사이즈 안내</p>
                  </div>
                </div>
              )}

              {this.state.openSize &&
                this.state.sizes.map((size: string, i: number) => (
                  <button
                    key={i}
                    onClick={() => this.handleSize(size)}
                    className={`btn btn-filter w-18p ${
                      size === this.state.selectSize && "btn-filter-active"
                    }`}
                  >
                    {size}
                  </button>
                ))}
              {this.state.selectSize !== "" && !this.state.openSize && <hr />}
            </div>
          </div>
        )}
        {this.state.stepScreen === 0 && (
          <div className="pt-1 pb-1 bg-gray border-bottom" />
        )}

        <div className="pt-4 pb-4">
          <div className="container">
            <div className="text-center">
              {this.state.stepScreen < 1 ? (
                <div
                  className={`box-step ${
                    this.state.stepScreen === 0 && "box-step-active"
                  }`}
                >
                  1
                  <p className="step-p2">
                    {this.state.stepScreen === 0 && "제품 컨디션"}
                  </p>
                </div>
              ) : (
                <img src={imgComplete} alt="" />
              )}
              <div className="line-sell" />

              {this.state.stepScreen < 2 ? (
                <div
                  className={`box-step ${
                    this.state.stepScreen === 1 && "box-step-active"
                  }`}
                >
                  2
                  <p className="step-p3">
                    {this.state.stepScreen === 1 && "거래방식"}
                  </p>
                </div>
              ) : (
                <img src={imgComplete} alt="" />
              )}
              <div className="line-sell" />

              {this.state.stepScreen < 3 ? (
                <div
                  className={`box-step ${
                    this.state.stepScreen === 2 && "box-step-active"
                  }`}
                >
                  3
                  <p className="step-p">
                    {this.state.stepScreen === 2 && "카드/계좌 등록"}
                  </p>
                </div>
              ) : (
                <img src={imgComplete} alt="" />
              )}
              <div className="line-sell" />

              {this.state.stepScreen < 4 ? (
                <div
                  className={`box-step ${
                    this.state.stepScreen === 3 && "box-step-active"
                  }`}
                >
                  4
                  <p className="step-p2">
                    {this.state.stepScreen === 3 && "판매자 서약"}
                  </p>
                </div>
              ) : (
                <img src={imgComplete} alt="" />
              )}
            </div>
            {/* step end */}
          </div>

          {this.state.stepScreen === 0 && <this.ComponentScreen0 />}
          {this.state.stepScreen === 1 && <this.ComponentScreen1 />}
          {this.state.stepScreen === 2 && <this.ComponentScreen2 />}
          {this.state.stepScreen === 3 && <this.ComponentScreen3 />}
        </div>

        <div className="bottom-box p-3">
          {this.state.stepScreen === 0 && (
            <>
              {this.step0Check() ? (
                <button
                  onClick={this.handleNext}
                  className="btn-custom-active w-100"
                >
                  다음
                </button>
              ) : (
                <button className="btn-custom-disable w-100">다음</button>
              )}
            </>
          )}

          {this.state.stepScreen === 1 && (
            <div className="row">
              <div className="col-6 pr-1">
                <button onClick={this.handlePrev} className="btn-custom w-100">
                  이전
                </button>
              </div>
              <div className="col-6 pl-1">
                <>
                  {this.step1Check() ? (
                    <button
                      onClick={this.handleNext}
                      className="btn-custom-active w-100"
                    >
                      다음
                    </button>
                  ) : (
                    <button className="btn-custom-disable w-100">다음</button>
                  )}
                </>
              </div>
            </div>
          )}

          {this.state.stepScreen === 2 && (
            <div className="row">
              <div className="col-6 pr-1">
                <button onClick={this.handlePrev} className="btn-custom w-100">
                  이전
                </button>
              </div>
              <div className="col-6 pl-1">
                <>
                  {this.state.step2check ? (
                    <button
                      onClick={this.handleNext}
                      className="btn-custom-active w-100"
                    >
                      다음
                    </button>
                  ) : (
                    <button className="btn-custom-disable w-100">다음</button>
                  )}
                </>
              </div>
            </div>
          )}

          {this.state.stepScreen === 3 && (
            <div className="row">
              <div className="col-6 pr-1">
                <button onClick={this.handlePrev} className="btn-custom w-100">
                  이전
                </button>
              </div>
              <div className="col-6 pl-1">
                <>
                  {this.state.agree && !this.state.blockSubmit ? (
                    <button
                      onClick={this.handleSubmit}
                      className="btn-custom-active w-100"
                    >
                      다음
                    </button>
                  ) : (
                    <button className="btn-custom-disable w-100">
                      {this.state.blockSubmit ? "업로드 중입니다" : "다음"}
                    </button>
                  )}
                </>
              </div>
            </div>
          )}
        </div>
      </>
    );
  };

  private ComponentScreen0 = () => {
    return (
      <>
        <div className="container">
          <div
            onClick={() =>
              this.setState({ openCondition1: !this.state.openCondition1 })
            }
            className="row mt-5 cursor"
          >
            {/* <div className="row mt-5 cursor"> */}
            <div className="col-9">
              <p className="p-18 bold gray-4">
                01{" "}
                <span className="p-14 black-2 normal ml-1">새 제품인가요?</span>
              </p>
            </div>
            <div className="col-3 pt-2 text-right">
              <p className="p-14 roboto bold orange-3 float-left">
                {this.state.isNew && "OK"}
              </p>
              {this.state.openCondition1 ? (
                <img src={imgOpen} alt="" className="mt-m-8" />
              ) : (
                <img src={imgClose} alt="" className="mt-m-8" />
              )}
            </div>
          </div>
          {this.state.openCondition1 && (
            <div className="mt-2 mb-3">
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" />
                현재 새 제품만 판매가 가능합니다. 추후 중고 제품에도 판매할 수
                있도록 준비하겠습니다.
              </p>
              <div className="text-center mt-3">
                <button
                  onClick={() =>
                    this.setState({
                      isNew: true,
                      openCondition1: false,
                      openCondition2: true,
                    })
                  }
                  className={`btn-custom-sm w-30p ${
                    this.state.isNew && "btn-active-sm-border"
                  }`}
                >
                  네
                </button>
              </div>
            </div>
          )}
          <hr className="mt-2 mb-2" />
          {/* condition1 */}

          <div
            onClick={() =>
              this.setState({ openCondition2: !this.state.openCondition2 })
            }
            className="row mt-3 cursor"
          >
            <div className="col-9">
              <p className="p-18 bold gray-4">
                02{" "}
                <span className="p-14 black-2 normal ml-1">
                  구입 경로를 선택해 주세요.
                </span>
              </p>
            </div>
            <div className="col-3 pt-1 text-right">
              <p className="p-14 roboto bold orange-3 float-left">
                {this.state.from !== "" && "OK"}
              </p>
              {this.state.openCondition2 ? (
                <img src={imgOpen} alt="" className="mt-m-8" />
              ) : (
                <img src={imgClose} alt="" className="mt-m-8" />
              )}
            </div>
          </div>
          {this.state.openCondition2 && (
            <div className="mt-2 mb-3">
              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 수입 신고하지 않고
                해외에서 구입한 제품을 판매할 경우 법적 불이익이 받을 수
                있습니다.
              </p>
              <div className="text-center mt-3">
                <button
                  onClick={() =>
                    this.setState({
                      from: "국내제품",
                      openCondition2: false,
                      openCondition3: true,
                    })
                  }
                  className={`btn-custom-sm w-30p mr-1 ${
                    this.state.from === "국내제품" && "btn-active-sm-border"
                  }`}
                >
                  국내제품
                </button>
                <button
                  onClick={() =>
                    this.setState({
                      from: "해외제품",
                      openCondition2: false,
                      openCondition3: true,
                    })
                  }
                  className={`btn-custom-sm w-30p mr-1 ${
                    this.state.from === "해외제품" && "btn-active-sm-border"
                  }`}
                >
                  해외제품
                </button>
                <button
                  onClick={() =>
                    this.setState({
                      from: "알수없음",
                      openCondition2: false,
                      openCondition3: true,
                    })
                  }
                  className={`btn-custom-sm w-30p ${
                    this.state.from === "알수없음" && "btn-active-sm-border"
                  }`}
                >
                  알수없음
                </button>
              </div>
            </div>
          )}
          <hr className="mt-2 mb-2" />
          {/* condition2 */}

          <div
            onClick={() =>
              this.setState({ openCondition3: !this.state.openCondition3 })
            }
            className="row mt-3 cursor"
          >
            <div className="col-9">
              <p className="p-18 bold gray-4">
                03{" "}
                <span className="p-14 black-2 normal ml-1">
                  구성품 상태를 체크해 주세요.
                </span>
              </p>
            </div>
            <div className="col-3 pt-1 text-right">
              <p className="p-14 roboto bold orange-3 float-left">
                {this.state.damageCheck && "OK"}
              </p>
              {this.state.openCondition3 ? (
                <img src={imgOpen} alt="" className="mt-m-8" />
              ) : (
                <img src={imgClose} alt="" className="mt-m-8" />
              )}
            </div>
          </div>
          {this.state.openCondition3 && (
            <div className="mt-2 mb-3">
              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 박스, 속지, Tag,
                추가 구성품 등에 대한 상태를 체크해 주세요.
              </p>
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" /> 복수 선택이
                가능합니다.
              </p>
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" /> 이상이 없는 경우
                아래 이상 없음 버튼을 클릭해주세요.
              </p>
              <div className="mt-3 text-left">
                <div
                  onClick={() =>
                    this.setState({ damage1: !this.state.damage1 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 ${
                    this.state.damage1 && "btn-active-sm-border"
                  }`}
                >
                  박스 이상 (분실, 찌그러짐, 찢어짐 등){" "}
                  {this.state.damage1 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
                <div
                  onClick={() =>
                    this.setState({ damage2: !this.state.damage2 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                    this.state.damage2 && "btn-active-sm-border"
                  }`}
                >
                  속지 이상 (분실, 찢어짐 등){" "}
                  {this.state.damage2 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
                <div
                  onClick={() =>
                    this.setState({ damage3: !this.state.damage3 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                    this.state.damage3 && "btn-active-sm-border"
                  }`}
                >
                  Tag 이상 (분실, 떨어짐 등){" "}
                  {this.state.damage3 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
                <div
                  onClick={() =>
                    this.setState({ damage4: !this.state.damage4 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                    this.state.damage4 && "btn-active-sm-border"
                  }`}
                >
                  추가 구성품 이상 (여분 신발끈 분실 등){" "}
                  {this.state.damage4 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
              </div>

              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 기타 특이사항
              </p>
              <textarea
                className="form-control textarea-custom mt-2"
                value={this.state.damageDesc}
                onChange={this.handleText}
                name="damageDesc"
                rows={5}
                maxLength={50}
                placeholder="구성품에 대한 기타 특이사항이 있다면 50자 내로 입력해 주세요."
              />
              <p className="text-right p-13 gray-2 mt-1">
                {this.state.damageDesc.length}/50
              </p>
              <div className="text-center mt-2">
                <button
                  onClick={this.handleDamageCheck}
                  className="btn-second-sm w-100 h-50p bold"
                >
                  {this.anyDamage() ? "구성품 상태 체크완료" : "이상없음"}
                </button>
              </div>
            </div>
          )}
          <hr className="mt-2 mb-2" />
          {/* condition3 */}

          <div
            onClick={() =>
              this.setState({ openCondition4: !this.state.openCondition4 })
            }
            className="row mt-3 cursor"
          >
            <div className="col-9">
              <p className="p-18 bold gray-4">
                04{" "}
                <span className="p-14 black-2 normal ml-1">
                  신발 상태를 체크해주세요.
                </span>
              </p>
            </div>
            <div className="col-3 pt-1 text-right">
              <p className="p-14 roboto bold orange-3 float-left">
                {this.state.statusCheck && "OK"}
              </p>
              {this.state.openCondition4 ? (
                <img src={imgOpen} alt="" className="mt-m-8" />
              ) : (
                <img src={imgClose} alt="" className="mt-m-8" />
              )}
            </div>
          </div>
          {this.state.openCondition4 && (
            <div className="mt-2 mb-3">
              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 신발에 오염, 변색
                등에 대한 상태를 체크해주세요.
              </p>
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" /> 복수 선택이
                가능합니다.
              </p>
              <div className="mt-3 text-left">
                <div
                  onClick={() =>
                    this.setState({ status1: !this.state.status1 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 ${
                    this.state.status1 && "btn-active-sm-border"
                  }`}
                >
                  변색 또는 오염{" "}
                  {this.state.status1 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
                <div
                  onClick={() =>
                    this.setState({ status2: !this.state.status2 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                    this.state.status2 && "btn-active-sm-border"
                  }`}
                >
                  스크래치, 흠집, 기스, 상처{" "}
                  {this.state.status2 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
                <div
                  onClick={() =>
                    this.setState({ status3: !this.state.status3 })
                  }
                  className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                    this.state.status3 && "btn-active-sm-border"
                  }`}
                >
                  기본 봉제 이상{" "}
                  {this.state.status3 && (
                    <img src={check} alt="" className="float-right mr-3 mt-1" />
                  )}
                </div>
              </div>

              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 기타 특이사항
              </p>
              <textarea
                className="form-control textarea-custom mt-2"
                value={this.state.statusDesc}
                name="statusDesc"
                onChange={this.handleText}
                rows={5}
                maxLength={50}
                placeholder="제품에 대한 기타 특이사항이 있다면 50자 내로 입력해 주세요."
              />
              <p className="text-right p-13 gray-2 mt-1">
                {this.state.statusDesc.length}/50
              </p>
              <div className="text-center mt-2">
                <button
                  onClick={this.handleStatusCheck}
                  className="btn-second-sm w-100 h-50p bold"
                >
                  {this.anyStatus() ? "신발상태 체크완료" : "이상없음"}
                </button>
              </div>
            </div>
          )}
          <hr className="mt-2 mb-2" />
          {/* condition4 */}

          <div
            onClick={() =>
              this.setState({ openCondition5: !this.state.openCondition5 })
            }
            className="row mt-3 cursor"
          >
            <div className="col-9">
              <p className="p-18 bold gray-4">
                05{" "}
                <span className="p-14 black-2 normal ml-1">
                  신발 사진을 3장 이상 촬영해 주세요.
                </span>
              </p>
            </div>
            <div className="col-3 pt-1 text-right">
              <p className="p-14 roboto bold orange-3 float-left">
                {this.state.file.length > 2 && "OK"}
              </p>
              {this.state.openCondition5 ? (
                <img src={imgOpen} alt="" className="mt-m-8" />
              ) : (
                <img src={imgClose} alt="" className="mt-m-8" />
              )}
            </div>
          </div>
          {this.state.openCondition5 && (
            <div className="mt-2 mb-3">
              <p className="p-13 gray-2 mt-3">
                <img src={bullet} alt="" className="mr-1" /> 신발의 상태가 잘
                보이도록 촬영해 주세요. 앞서 체크했던 신발 상태의 특이사항을
                추가적으로 촬영해주시면 원활한 판매에 도움이 됩니다.
              </p>
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" /> 사진은 최소 3장 ~
                최대 15장까지 업로드할 수 있습니다.
              </p>
            </div>
          )}
        </div>
        {/* condition5 container end */}

        {this.state.openCondition5 && (
          <>
            <div className="bg-gray pt-4 pb-4">
              <div className="pl-3">
                <p className="p-13 gray-3 bold">촬영예시</p>
                <div className="horizontal-sell mt-2">
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample1})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">
                      박스 포함 전체 모습
                    </p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample2})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">측면</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample3})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">윗면</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample4})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">토박스 상세</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample5})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">아웃솔</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample6})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">아웃솔 상세</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample7})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">힐컵</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample8})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">박스 라벨</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample9})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">신발 택</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample11})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">추가 구성품</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample12})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">변색</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample13})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">박스 손상</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample14})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">박스 손상 2</p>
                  </div>
                  <div className="sell-exbox">
                    <div
                      className="sell-ex"
                      style={{ backgroundImage: `url(${sample15})` }}
                    />
                    <p className="gray-2 p-13 text-center mt-1">속지 손상</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex">
              {this.state.cameraOrigin ? (
                <div onClick={this.handleFileRef} className="sell-camerabox">
                  <div className="sell-camera">
                    <input
                      ref={this.fileRef}
                      name="file"
                      onChange={this.handleFile}
                      className="d-none"
                      type="file"
                      accept="image/*"
                      capture
                    />
                    <img src={camera} alt="" />
                    <p className="p-12 gray-2 roboto mt-1">
                      {this.state.file.length}/15
                    </p>
                  </div>
                </div>
              ) : (
                <div
                  onClick={() => this.setState({ modal: true })}
                  className="sell-camerabox"
                >
                  <div className="sell-camera">
                    <img src={camera} alt="" />
                    <p className="p-12 gray-2 roboto mt-1">
                      {this.state.file.length}/15
                    </p>
                  </div>
                </div>
              )}

              <div className="horizontal-sell">
                {this.state.file.map((file: any, i: number) => (
                  <div key={i} className="sell-photobox">
                    <img
                      className="sell-photo"
                      src={URL.createObjectURL(file)}
                    />
                    <img
                      onClick={() => this.handleImageDelete(i)}
                      src={imgDelete}
                      alt=""
                      className="sell-delete"
                    />
                  </div>
                ))}
              </div>
            </div>
          </>
        )}
        {/* condition5 end */}
        {/* <this.ModalCamera /> */}
      </>
    );
  };

  // private ModalCamera = () => {
  //   return (
  //     <div
  //       className="modal-custom-large z-5000"
  //       style={{ display: this.state.modal ? "block" : "none" }}
  //     >
  //       <div className="modal-content-large">
  //         <Camera
  //           idealFacingMode={FACING_MODES.ENVIRONMENT}
  //           onTakePhoto={(dataUri: any) => {
  //             this.handleTakePhoto(dataUri);
  //           }}
  //           sizeFactor={1}
  //           isFullscreen={true}
  //           isImageMirror={false}
  //         />
  //       </div>
  //     </div>
  //   );
  // };

  private ComponentScreen1 = () => {
    return (
      <>
        <div className="container mt-5 mb-5">
          <p className="p-14 black-2">거래방식 선택</p>
          <p className="p-13 gray-2 mt-1">
            <img src={bullet} alt="" className="mr-1" />
            가능한 거래 방식을 선택해 주세요.
          </p>
          <p className="p-13 gray-2 mt-1">
            <img src={bullet} alt="" className="mr-1" />
            두가지 거래방식이 모두 가능하다면 "직거래/택배"를 선택하세요.
          </p>

          <div className="row mt-3">
            <div className="col-4 pr-1">
              <button
                onClick={() => this.setState({ shippingType: "직거래" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "직거래" && "btn-active-sm-border"
                }`}
              >
                직거래
              </button>
            </div>
            <div className="col-4 pl-1 pr-1">
              <button
                onClick={() => this.setState({ shippingType: "택배" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "택배" && "btn-active-sm-border"
                }`}
              >
                택배
              </button>
            </div>
            <div className="col-4 pl-1">
              <button
                onClick={() => this.setState({ shippingType: "직거래/택배" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "직거래/택배" &&
                  "btn-active-sm-border"
                }`}
              >
                직거래/택배
              </button>
            </div>
          </div>

          {this.state.shippingType === "직거래" && (
            <div className="container bg-gray">
              <div className="row mt-3 pt-3 pb-3">
                <div className="col-8">
                  <p className="p-13 gray-3 mt-1">
                    거래지역
                    <b className="ml-2">
                      {this.state.location.length > 0 && this.state.location[0]}
                      {this.state.location.length > 1 && "/"}
                      {this.state.location.length > 1 && this.state.location[1]}
                    </b>
                  </p>
                </div>
                <div className="col-4 text-right">
                  <button
                    onClick={() => this.setState({ modal2: true })}
                    className="btn-custom-xs w-100"
                  >
                    {this.state.location.length === 0 ? "등록" : "수정"}
                  </button>
                </div>
              </div>
            </div>
          )}
          {this.state.shippingType === "직거래/택배" && (
            <div className="container bg-gray">
              <div className="row mt-3 pt-3 pb-3">
                <div className="col-8">
                  <p className="p-13 gray-3 mt-1">
                    거래지역
                    <b className="ml-2">
                      {this.state.location.length > 0 && this.state.location[0]}
                      {this.state.location.length > 1 && "/"}
                      {this.state.location.length > 1 && this.state.location[1]}
                    </b>
                  </p>
                </div>
                <div className="col-4 text-right">
                  <button
                    onClick={() => this.setState({ modal2: true })}
                    className="btn-custom-xs w-100"
                  >
                    {this.state.location.length === 0 ? "등록" : "수정"}
                  </button>
                </div>
              </div>
            </div>
          )}

          <p className="black-2 bold p-14 mt-4">판매가(원)</p>
          <div className="flex">
            <input
              onChange={this.handlePrice}
              style={{ paddingRight: !this.isTripleZero() ? 0 : 50 }}
              type="number"
              pattern="[0-9]*"
              inputMode="numeric"
              placeholder={!this.state.tripleZero ? "가격을 입력해주세요" : ""}
              onFocus={() => this.setState({ tripleZero: true })}
              onBlur={() => this.setState({ tripleZero: false })}
              className="form-control form-custom form-price mt-1 text-right p-18 roboto bold"
              value={this.state.priceInput === 0 ? "" : this.state.priceInput}
            />
            <p className="price-right p-18 roboto bold">
              {this.isTripleZero() && "000"}
            </p>
          </div>
          <div className="pt-4">
            <p className="p-13 gray-2 float-left gray-2">
              결제 수수료({this.state.commision}%)
            </p>
            <p className="p-14 roboto gray-2 float-right">
              -
              {currency(
                Math.round(this.state.price * (this.state.commision * 0.01)),
              )}
              <span className="p-13 noto">원</span>
            </p>
          </div>
          <div className="pt-4 pb-3">
            <p className="p-13 gray-2 float-left gray-2">수수료 할인</p>
            <p className="p-14 roboto gray-2 float-right">
              +
              {currency(
                Math.round(this.state.price * (this.state.sale * 0.01)),
              )}
              <span className="p-13 noto">원</span>
            </p>
          </div>
          <hr className="hr2 mt-4" />
          <div className="pt-1">
            <p className="p-14 black-2 bold float-left gray-2">정산예정금액</p>
            <p className="p-18 roboto orange bold float-right">
              {currency(
                this.state.price -
                  Math.round(
                    this.state.price * (this.state.commision * 0.01) -
                      this.state.price * (this.state.sale * 0.01),
                  ),
              )}
              <span className="p-14 noto normal">원</span>
            </p>
          </div>
        </div>
        <this.ModalLocation />
      </>
    );
  };

  private ModalLocation = () => {
    return (
      <div
        className="modal-full z-5000"
        style={{ display: this.state.modal2 ? "block" : "none" }}
      >
        {this.state.modalAddress ? (
          <>
            <div className="row p-3">
              <div className="col-6 offset-3">
                <p className="p-16 bold black-2 text-center ">거래지역 검색</p>
              </div>
              <div className="col-3 text-right">
                <img
                  onClick={() => this.setState({ modalAddress: false })}
                  src={imgCloseX}
                  alt=""
                  className="cursor"
                />
              </div>
            </div>
            <DaumPostcode
              onComplete={this.handleAddress}
              style={{ height: "100%", top: 48, position: "fixed" }}
            />
          </>
        ) : (
          <>
            <div className="container">
              <div className="row pt-2 pb-2">
                <div className="col-1">
                  <img
                    onClick={() => this.setState({ modal2: false })}
                    src={imgPrev}
                    alt=""
                    className="cursor black"
                  />
                </div>
                <div className="col-11">
                  <p className="p-16 bold black-2">거래지역 등록</p>
                </div>
              </div>
            </div>
            <div className="col-12">
              <div className="detail-border-top" />
            </div>

            <div className="container">
              {!this.state.modalAddress && this.state.location.length < 2 && (
                <button
                  onClick={() => this.setState({ modalAddress: true })}
                  className="btn-custom-active w-100"
                >
                  지역 검색
                </button>
              )}

              <p className="p-14 black-2 mt-3">선택지역</p>
              <p className="p-13 gray-2 mt-1">
                <img src={bullet} alt="" className="mr-1" />구 단위로 등록되며
                상세 주소는 저장되지 않습니다.
              </p>
              <p className="p-13 gray-2 mt-1">
                <img src={bullet} alt="" className="mr-1" />
                최대 두개 지역까지 가능합니다.
              </p>

              {this.state.location.length > 0 ? (
                <div className="pt-3 pb-3 mt-2">
                  {this.state.location.map((location: string, i: number) => (
                    <button key={i} className="btn btn-filtered">
                      {location}
                      <img
                        onClick={() => this.handlePopLocation(i)}
                        src={filterX}
                        className="icon-close ml-2"
                      />
                    </button>
                  ))}
                </div>
              ) : (
                <div className="bg-gray text-center pt-3 pb-3 mt-2">
                  <p className="p-14 black-2">등록된 지역이 없습니다.</p>
                </div>
              )}
            </div>

            <div className="bottom-box p-3">
              {this.state.location.length > 0 ? (
                <button
                  onClick={() => this.setState({ modal2: false })}
                  className="btn-custom-active w-100"
                >
                  등록하기
                </button>
              ) : (
                <button className="btn-custom-disable w-100">등록하기</button>
              )}
            </div>
          </>
        )}
      </div>
    );
  };

  private ComponentScreen2 = () => {
    return (
      <AddBilling
        handleCheckTrue={this.handleCheckTrue}
        handleCheckFalse={this.handleCheckFalse}
      />
    );
  };

  private ComponentScreen3 = () => {
    return (
      <div className="container mt-5">
        <p className="p-14 black-2">판매자 서약</p>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-1" />
          절대 가품을 팔지 않습니다.
        </p>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-1" />
          제품의 상태는 입력한 정보와 같습니다.
        </p>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-1" />
          택배 거래 시 결제 완료일 다음날 부터 2 영업일 이내에 발송할 것 입니다.
        </p>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-1" />
          결제 이후 합의되지 않은 직거래 노쇼, 가품 판매 및 제품 상태가 입력한
          정보와 상이할 경우 이용약관 제28조 제1항 각 호에 따라 등록된
          신용카드로 위약금 또는 보상금이 자동으로 결제되는 것에 동의합니다.
        </p>

        {this.state.agree ? (
          <button
            onClick={() => this.setState({ agree: false })}
            className="btn-second w-100 mt-4 mb-5"
          >
            판매자 서약에 동의합니다
            <img src={agreeActive} className="ml-2" alt="" />
          </button>
        ) : (
          <button
            onClick={() => this.setState({ agree: true })}
            className="btn-custom-disable w-100 mt-4 mb-5"
          >
            판매자 서약에 동의합니다
            <img src={agreeDisable} className="ml-2" alt="" />
          </button>
        )}
      </div>
    );
  };

  private ComponentScreen4 = () => {
    return (
      <>
        <div className="border-bottom">
          <div className="container">
            <div className="text-center pt-5 pb-4">
              <p className="p-22 orange">판매등록이 완료되었습니다!</p>
              <p className="p-14 gray-2 mt-1">매물번호: {this.state.index}</p>
            </div>
            <hr className="hr4" />

            <div className="row">
              <div className="col-3 pr-0 text-center">
                <div
                  className="img-sellcomplete"
                  style={{
                    backgroundImage: `url(${this.state.currentSneaker.imgUrl})`,
                  }}
                />
              </div>
              <div className="col-9">
                <p className="p-14 bold roboto gray-2">
                  {this.state.currentSneaker.brand}
                </p>
                <p className="p-16 roboto black-2 mt-1">
                  {this.state.currentSneaker.name}
                </p>
              </div>
            </div>
            <hr className="hr4" />

            <div className="row">
              <div className="col-6">
                <p className="p-14 black-2">사이즈</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-3">{this.state.selectSize}</p>
              </div>
            </div>

            <div className="row mt-2">
              <div className="col-6">
                <p className="p-14 black-2">정산예정금액</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-3 bold">
                  {currency(
                    this.state.price -
                      Math.round(
                        this.state.price * (this.state.commision * 0.01) -
                          this.state.price * (this.state.sale * 0.01),
                      ),
                  )}
                  원
                </p>
              </div>
            </div>

            <div className="container bg-gray mt-2 pt-3 pb-3 pl-4 pr-4">
              <div className="row">
                <div className="col-6">
                  <p className="p-13 gray-3">판매가</p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-13 gray-3">{currency(this.state.price)}원</p>
                </div>
              </div>
              <div className="row mt-1">
                <div className="col-6">
                  <p className="p-13 gray-3">
                    결제 수수료 ({this.state.commision}%)
                  </p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-13 gray-3">
                    -
                    {currency(
                      Math.round(
                        this.state.price * (this.state.commision * 0.01),
                      ),
                    )}
                    원
                  </p>
                </div>
              </div>
              <div className="row mt-1">
                <div className="col-6">
                  <p className="p-13 gray-3">수수료 할인</p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-13 gray-3">
                    +
                    {currency(
                      Math.round(this.state.price * (this.state.sale * 0.01)),
                    )}
                    원
                  </p>
                </div>
              </div>
            </div>

            <div className="row mt-2">
              <div className="col-6">
                <p className="p-14 black-2">거래방식</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-2">{this.state.shippingType}</p>
              </div>
            </div>

            <div className="row mt-2">
              <div className="col-6">
                <p className="p-14 black-2">신용카드</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-2">{this.state.cardState.cardName}</p>
              </div>
            </div>

            <div className="row mt-2 mb-5">
              <div className="col-6">
                <p className="p-14 black-2">정산계좌</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-2">
                  {this.state.bankState.onwer} / {this.state.bankState.bankName}{" "}
                  {this.state.bankState.account}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="bg-gray pt-4 pb-5">
          <div className="container">
            <p className="p-14 black-2">유의사항</p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-1" />
              가품 및 제품 상태 등 판매자 서약과 다를 경우 불이익이 발생할 수
              있습니다.
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-1" />
              택배 거래 시 구매자의 결제 이후 2영업일 내에 제품이 발송되지
              않으면 거래가 파기될 수 있으며 회원 평가 점수에 악영향을 미칩니다.
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-1" />
              구매자가 결제를 완료하면 판매자님께 거래 제안이 전달 되며 알림을
              드리겠습니다. 반드시 거래 수락 여부를 "마이페이지-판매내역" 에서
              결정 해주세요.
            </p>
          </div>
        </div>

        <div className="bottom-box p-3">
          <div className="row">
            <div className="col-6 pr-1">
              <Link to="/">
                <button onClick={this.handlePrev} className="btn-custom w-100">
                  홈 바로가기
                </button>
              </Link>
            </div>
            <div className="col-6 pl-1">
              <Link to="/mypage/sells">
                <button className="btn-custom-active w-100">
                  판매내역 확인
                </button>
              </Link>
            </div>
          </div>
        </div>
      </>
    );
  };
}

export default Sell;
