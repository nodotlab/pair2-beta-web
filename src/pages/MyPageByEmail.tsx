import React from "react";
import {
  getCurrentUser,
  tradingBuyer,
  tradingSeller,
  cheat,
} from "../db/Actions";
import { parsePhone } from "../Method";
import CurrentUser from "../reduce/CurrentUserByEmail";
import EditScreen from "../components/MypageEditByEmail";
import imgArrow from "../images/Menu_Arrow.svg";

interface MyPageState {
  currentUser: CurrentUser;
  screen: string;
}

class MyPage extends React.Component<any, MyPageState> {
  constructor(props: any) {
    super(props);
    this.state = {
      currentUser: new CurrentUser("", ""),
      screen: !this.props.match.params.screen
        ? "default"
        : this.props.match.params.screen
    };
  }

  public componentDidMount() {
    if (
      !localStorage.getItem("pair2token") ||
      localStorage.getItem("pair2signin") === "false"
    )
      this.props.history.push("/login/");

    this.initCurrentUser();
  }

  private async initCurrentUser() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCurrentUser(token);
      if (result === "faild") {
        this.handleSignout();
      }

      const user = new CurrentUser(
        result.email,
        result.nickname
      );
      this.setState({ currentUser: user });
    } catch (error) {
      this.props.history.push("/login/");
      this.handleSignout();
    }
  }


  private handleSignout = (): void => {
    localStorage.setItem("pair2signin", "false");
    localStorage.setItem("pair2token", "");
    localStorage.setItem("pair2nickname", "");
    this.props.history.push("/");
  };

  public render() {
    return (
      <div>
        <div className="container">
          <div className="row mt-5 m-mt0">
            <div className="col-md-3 mb-5 m-mb1">
              <hr className="hr3 hidden-m" />
              <div className="p-2">
                <p className="p-18 black-2 bold mt-2 hidden-m">
                  {this.state.currentUser.nickname}
                </p>
                <p className="p-16 black-2 bold mt-2 hidden-p">
                  {this.state.currentUser.nickname}
                </p>
                <p className="p-16 black-2 mt-1 hidden-m">
                  {this.state.currentUser.email}
                </p>
                <p className="p-14 black-2 mt-1 float-left hidden-p">
                  {this.state.currentUser.email}
                </p>
                <p
                  onClick={this.handleSignout}
                  className="p-13 gray-2 underline mt-4 cursor hidden-m"
                >
                  로그아웃
                </p>
                <p
                  onClick={this.handleSignout}
                  className="p-13 gray-2 underline cursor hidden-p float-right"
                >
                  로그아웃
                </p>
              </div>

              <hr className="hr4 mb-4 hidden-m" />
              <div className="hidden-m">
                <button
                  onClick={() => this.setState({ screen: "edit" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "edit" && "mypage-menu-active"
                  }`}
                >
                  개인정보 수정
                  {this.state.screen === "edit" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
              </div>
            </div>

            <div className="col-md-9">
              <div className="row hidden-p">
                <div className="border-topbottom mt-3" />
              </div>
              {this.state.screen === "edit" && (
                <EditScreen userId={this.state.currentUser.email} />
              )}
              {this.state.screen === "default" && (
                <EditScreen userId={this.state.currentUser.email} />
              )}
              {/* edit user */}
            </div>
          </div>
        </div>
        {/* container end */}
      </div>
    );
  }
}

export default MyPage;
