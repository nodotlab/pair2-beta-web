import React from "react";
import { timeFormat4, timeFormat5, getReleaseState, getReleaseStateTime, getCountdownTime } from "../Method";
import { releaseToday } from "../db/Actions";
import { filters } from "../db/Actions";
import { Link } from "react-router-dom";
import ReleaseToday from "../reduce/ReleaseToday";
import imgPrev from "../images/Arrow_Prev.svg";
import imgNext from "../images/Arrow_Next.svg";
import imgClose from "../images/GNB_Close.svg";
import imgReset from "../images/reset.svg";
import imgDown from "../images/tab_open.svg";
import imgFOpen from "../images/Filter_Open.svg";
import imgFClose from "../images/Filter_Close.svg";
import MetaData from "../components/MetaGenerator";
// import Clock from 'react-live-clock';
import 'moment/locale/ko';
import moment from 'moment-timezone';
import Countdown, { zeroPad } from 'react-countdown';
import * as _ from "lodash";
import { stat } from "fs";

import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { styled } from "@material-ui/core/styles";


const CssFormControl = styled(FormControl)({
  '& label.Mui-focused': {
    color: '#ff6600',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: 'green',
  },
  '& .MuiOutlinedInput-root': {
    '&.Mui-focused fieldset': {
      borderColor: '#ff6600',
    },
  },
});



interface RelaseState {
  initData: ReleaseToday[];
  data: ReleaseToday[];
  todayCount: number;
  filterCountry: string;
  filterStoreState: number;
}

class ReleaseTodayPage extends React.Component<any, RelaseState> {
  constructor(props: any) {
    super(props);
    this.state = {
      initData: [],
      data: [],
      todayCount: 0,
      filterCountry: "모두",
      filterStoreState: 0
    };
  }


  public componentDidMount() {
    this.initBrands();
  }

  private async initBrands() {

    moment.locale('ko'); //현재시간 실시간 카운트, 지역설정

    const tmpRelease: ReleaseToday[] = [];

    const _release = await releaseToday();

    console.log(_release);

    for (const store of _release) {
      tmpRelease.push(
        new ReleaseToday(
          store.storename,
          getReleaseState(store.storesaleType, store.releaseDate, store.releaseDateEnd),
          store.storeCountry,
          store.online,
          getReleaseStateTime(store.storesaleType, store.releaseDate, store.releaseDateEnd),
          getCountdownTime(store.storesaleType, store.releaseDate, store.releaseDateEnd),
          store.storeUrl,
          store.snkrs.nameKr,
          store.snkrs.imageUrl,
          store.snkrs.index,
          store.snkrs.serial
        )
      );
    }


    //응모,선착순 예정
    const soonRelease: ReleaseToday[] = _.sortBy(_.filter(tmpRelease, function (relese) { return relese.storeState < 3 }), ['countdownTime', 'storeName', 'nameKr']);
    //선착순 발매
    const released: ReleaseToday[] = _.sortBy(_.filter(tmpRelease, function (relese) { return relese.storeState === 3 }), ['countdownTime', 'storeName', 'nameKr']);
    //응모예정
    const soonRaffle: ReleaseToday[] = _.sortBy(_.filter(tmpRelease, function (relese) { return relese.storeState === 4 }), ['countdownTime', 'storeName', 'nameKr']);
    //응모종료
    const endRaffle: ReleaseToday[] = _.sortBy(_.filter(tmpRelease, function (relese) { return relese.storeState === 5 }), ['countdownTime', 'storeName', 'nameKr']);


    const sortedReleaseData: ReleaseToday[] = [
      ... soonRelease,
      ...soonRaffle,
      ...released,
      ...endRaffle
    ];


    // tmpRelease.sort((a: Release, b: Release) => {
    //   if (a.name > b.name) return -1;
    //   if (a.name > b.name) return 1;
    //   return 0;
    // });

    // tmpRelease.sort((a: Release, b: Release) => {
    //   if (a.releaseTimestamp > b.releaseTimestamp) return -1;
    //   if (a.releaseTimestamp > b.releaseTimestamp) return 1;
    //   return 0;
    // });

    // this.setState({ initData: tmpRelease }, () => this.currentFilter());
    this.setState({ todayCount: sortedReleaseData.length })
    this.setState({ initData: sortedReleaseData });
    this.setState({ data: sortedReleaseData });
    // this.setState({ data: tmpRelease });
  }


  private getThisYear = (): number => {
    return new Date().getFullYear();
  };

  private getThisMonth = (): number => {
    return new Date().getMonth() + 1;
    // return new Date().getMonth();
  }; // test mode

  private handleChangeCountry = (e: any): void => {
    const filter = e.target.value;
    this.setState({ filterCountry: filter }, () => this.currentFilter());
  };

  private handleChangeStoreState = (e: any): void => {
    const filter:number = parseInt(e.target.value);
    this.setState({ filterStoreState: filter }, () => this.currentFilter());
  };

  private currentFilter(): void {
    const initData = this.state.initData;
    let tmpData = [];
    let results = [];

    //국내외 데이터 필터링
    if (this.state.filterCountry !== "모두") {
      for (let i in initData) {
        if (this.state.filterCountry === initData[i].storeCountry) {
          tmpData.push(initData[i]);
        }
      }
    } else {
      tmpData = initData;
    }

    //스토어 발매 상태 필터링
    if(this.state.filterStoreState > 0){
      for (let i in tmpData) {
        if (this.state.filterStoreState === tmpData[i].storeState) {
          results.push(tmpData[i])
        }
      }
    } else {
      results = tmpData;
    }

    this.setState({ data: results });
  }

  public render() {

    const metaData = {
      title: "한정판 신발 스니커즈 실시간 발매 정보 | PAIR2(페어투)",
      desc : `오늘 발매 ${this.state.todayCount}건, 한정판 신발 스니커즈 발매 및 응모 링크를 지금 바로 확인해 보세요.`,
      url: window.location.pathname
    };
    
    return (
      <div className="ReleasePage">
        <MetaData data={metaData} />
        <div className="today-realtiem-margin hidden-m">
        </div>

        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="row mb-4 hidden-m">
                <div className="col-8">
                  <p className="p-16 gray-2 today-count">
                    오늘의 발매 <span className="bold">{this.state.todayCount}</span>건
                  </p>
                </div>
                <div className="col-1">
                  <Box sx={{ minWidth: 80 }}>
                    <CssFormControl fullWidth size="small" variant="outlined">
                      <InputLabel id="demo-simple-select-label">국가</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Age"
                        onChange={this.handleChangeCountry}
                        defaultValue={"모두"}
                      >
                        <MenuItem value={"모두"}>모두</MenuItem>
                        <MenuItem value={"국내"}>국내</MenuItem>
                        <MenuItem value={"해외"}>해외</MenuItem>
                      </Select>
                    </CssFormControl>
                  </Box>
                </div>
                <div className="col-2">
                  <Box sx={{ minWidth: 100 }}>
                    <CssFormControl fullWidth size="small" variant="outlined">
                      <InputLabel id="demo-simple-select-label">발매상태</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="발매상태"
                        onChange={this.handleChangeStoreState}
                        defaultValue={0}
                      >
                        <MenuItem value={0}>모두</MenuItem>
                        <MenuItem value={1}>응모중</MenuItem>
                        <MenuItem value={4}>응모예정</MenuItem>
                        <MenuItem value={5}>응모종료</MenuItem>
                        <MenuItem value={2}>선착순예정</MenuItem>
                        <MenuItem value={3}>선착순발매</MenuItem>
                      </Select>
                    </CssFormControl>
                  </Box>
                </div>
              </div>
              <div className="row mt-3 mb-4 hidden-p">
                <div className="col-12 mb-4">
                  <p className="p-16 gray-2 today-count">
                    오늘의 발매 <span className="bold">{this.state.todayCount}</span>건
                  </p>
                </div>
                <div className="col-6">
                  <Box sx={{ minWidth: 100 }}>
                    <CssFormControl fullWidth size="small" variant="outlined">
                      <InputLabel id="demo-simple-select-label">국가</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Age"
                        onChange={this.handleChangeCountry}
                        defaultValue={"모두"}
                      >
                        <MenuItem value={"모두"}>모두</MenuItem>
                        <MenuItem value={"국내"}>국내</MenuItem>
                        <MenuItem value={"해외"}>해외</MenuItem>
                      </Select>
                    </CssFormControl>
                  </Box>
                </div>
                <div className="col-6">
                  <Box sx={{ minWidth: 100 }}>
                    <CssFormControl fullWidth size="small" variant="outlined">
                      <InputLabel id="demo-simple-select-label">발매상태</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="발매상태"
                        onChange={this.handleChangeStoreState}
                        defaultValue={0}
                      >
                        <MenuItem value={0}>모두</MenuItem>
                        <MenuItem value={1}>응모중</MenuItem>
                        <MenuItem value={4}>응모예정</MenuItem>
                        <MenuItem value={5}>응모종료</MenuItem>
                        <MenuItem value={2}>선착순예정</MenuItem>
                        <MenuItem value={3}>선착순발매</MenuItem>
                      </Select>
                    </CssFormControl>
                  </Box>
                </div>
              </div>
              { this.state.data.length > 0 && (
                <div className="row sneaker-mobile-row">
                  {this.state.data.map((data: ReleaseToday) => (
                    <div className="col-md-4 col-12 sneaker-web sneaker-mobile mb-4">
                      {(data.storeState === 1 || data.storeState === 2) && (
                        <div>
                          <p className="p-12 bold neon">&bull; LIVE</p>
                        </div>
                      )}
                      <Link to={`/release/${data.snkrIdx}/`}>
                        <div className={`img-release ${data.storeState === 5 && 'raffle-end'}`} >
                          <img src={data.snkrImageUrl} alt={`${data.snkrNameKr} (${data.snkrSerial})`} />
                        </div>
                        <div className="release-state">
                          <div>
                            {data.storeState === 1 && (
                              <button className="btn-release">응모중</button>
                            )}
                            {data.storeState === 2 && (
                              <button className="btn-release">선착순 예정</button>
                            )}
                            {data.storeState === 3 && (
                              <button className="btn-done">선착순 발매</button>
                            )}
                            {data.storeState === 4 && (
                              <button className="btn-pre bold">응모 예정</button>
                            )}
                            {data.storeState === 5 && (
                              <button className="btn-done">응모 종료</button>
                            )}
                          </div>
                          <div className="reamin-time-tictok">
                            <p className="p-10">
                              {data.storeState === 1 && (
                                <Countdown date={data.countdownTime} renderer={({ days, hours, minutes, seconds }) => {
                                  return <span>종료까지  {days > 0 ? days + '일' : ''}  {zeroPad(hours)} : {zeroPad(minutes)} : {zeroPad(seconds)}</span>;
                                }
                                } />
                              )}
                              {data.storeState === 2 && (
                                <Countdown date={data.countdownTime} renderer={({ days, hours, minutes, seconds }) => {
                                  return <span>발매까지  {days}일 {zeroPad(hours)} : {zeroPad(minutes)} : {zeroPad(seconds)}</span>;
                                }
                                } />
                              )}
                            </p>
                          </div>
                        </div>
                        <div>
                          <p className="p-12 gray-2 bold storeBottomLine">
                            {data.snkrNameKr.length > 38 && "..." && data.snkrNameKr.toUpperCase().substr(0, 35)+"..." || data.snkrNameKr}
                          </p>
                        </div>
                      </Link>
                      <div>
                        <div className="release-store">
                          <div>
                            <p className="p-14 black-2 bold mt-1 hidden-m">
                              {data.storeName.length > 24 && data.storeName.substr(0, 23)+"..." || data.storeName}
                            </p>
                            <p className="p-12 black-2 mt-1 hidden-m">
                              {data.storeTime}
                            </p>
                            <p className="p-12 gray-2 mt-1 hidden-m">
                              {data.storeCountry} | {data.storeIsOnline ? "온라인 구매" : "오프라인 구매"}
                            </p>
                            <p className="p-14 black-2 bold mt-1 hidden-p">
                              {data.storeName.length > 24 && data.storeName.substr(0, 23)+"..." || data.storeName}
                            </p>
                            <p className="p-14 black-2 mt-1 hidden-p">
                              {data.storeTime}
                            </p>
                            <p className="p-14 gray-2 mt-1 hidden-p">
                              {data.storeCountry} | {data.storeIsOnline ? "온라인 구매" : "오프라인 구매"}
                            </p>
                          </div>
                          <a href={data.storeUrl} className="today-store-link" target="_new">
                            <p className="p-10 mt-1">바로가기</p>
                          </a>
                        </div>
                      </div>

                    </div>
                  ))}
                </div>
              )}
                {/* { this.state.data.length === 0 && (
                <div className="row sneaker-mobile-row">
                  
                  <Link to="/upcoming/">
                    <button className="btn-custom-sm mt-3 mb-6">발매 스케줄 보러가기</button>
                  </Link>
                </div>
                )} */}
              
              {this.state.data.length === 0 && (
                <div className="text-center mt-6 mb-6">
                  <p className="p-14 gray-3">오늘은 발매가 없습니다.</p>
                  <p className="p-14 gray-3">이어지는 발매 스케줄은 런칭캘린더를 확인해 보세요👌</p>
                  <Link to="/upcoming/">
                    <button className="btn-custom-active mt-4 mb-6">런칭캘린더 바로가기</button>
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ReleaseTodayPage;
