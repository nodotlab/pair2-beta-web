import React from "react";
import { currency } from "../Method";
import { payment } from "../db/Actions";

class PaymentTest extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      totalPay: 0,
      stuffs: new Array(30).fill(0),
      cart: [],
      payType: "card",
      payName: "",
    };
  }

  public componentDidMount() {
    if (!document.getElementById("iamport")) {
      const scriptJs = document.createElement("script");
      scriptJs.id = "iamport";
      scriptJs.src = "https://cdn.iamport.kr/js/iamport.payment-1.1.5.js";
      document.body.appendChild(scriptJs);
    }

    const tmpList: number[] = [];

    for (let i = 0; i < this.state.stuffs.length; ++i) {
      let num: number = Math.floor(Math.random() * 300000 + 40000);
      let txt: string = num.toString();
      txt = txt.substr(0, txt.length - 3);
      num = parseInt(txt + "000");
      tmpList.push(num);
    }
    this.setState({ stuffs: tmpList });
  }

  private requestPay = () => {
    // if (this.state.payType === "") return;

    const jsCode = `
      IMP.init("imp36196053");
      IMP.request_pay({
        pg: "inicis",
        pay_method: "${this.state.payType}",
        merchant_uid: "${this.merchantUid}",
        name: "아무 신발",
        amount: ${this.state.totalPay},
        buyer_email: "gildong@gmail.com",
        buyer_name: "홍길동",
        buyer_tel: "010-4242-4242",
        buyer_addr: "서울특별시 강남구 신사동",
        buyer_postcode: "01181"
      }, function(res) {
        if (res.success) {
          console.log(res.success);
        } else {
          console.log("Pay failed");
        }
      });`;
    new Function(jsCode)();
  };

  private handleBilling = () => {
    // if (this.state.payType === "") return;

    const jsCode = `
      IMP.init("imp36196053");
      IMP.request_pay({
        pg : 'inicis', // version 1.1.0부터 지원.
        pay_method : 'card',
        merchant_uid : Date.now(),
        customer_uid : 'your_customer_1234',
        amount : 1,
        name : '주문명:빌링키 발급을 위한 결제',
        buyer_email : 'iamport@siot.do',
        buyer_name : '구매자이름',
        buyer_tel : '010-1234-5678'
      }, function(rsp) {
        if (rsp.success) {
            var msg = '빌링키 발급이 완료되었습니다.';
            msg += '고유ID : ' + rsp.imp_uid;
            msg += '상점 거래ID : ' + rsp.merchant_uid;
        } else {
            var msg = '빌링키 발급에 실패하였습니다.';
            msg += '에러내용 : ' + rsp.error_msg;
        }
        alert(msg);
      });`;
    new Function(jsCode)();
  };

  private merchantUid = () => Date.now();

  private priceAdd = (getPrice: number): void => {
    const tmpCart: object[] = this.state.cart;
    tmpCart.push({ name: "ANY STUFF", price: getPrice });

    this.setState({
      totalPay: this.state.totalPay + getPrice,
      cart: tmpCart,
    });
  };

  private cartModification = (index: number): void => {
    let tmpTotalPay: number = this.state.totalPay;
    tmpTotalPay = tmpTotalPay - this.state.cart[index]["price"];

    const tmpCart: object[] = [];
    for (let i = 0; i < this.state.cart.length; ++i) {
      if (i !== index) {
        tmpCart.push(this.state.cart[i]);
      }
    }

    this.setState({
      totalPay: tmpTotalPay,
      cart: tmpCart,
    });
  };

  private handleSubmitPayment = async () => {
    const token = localStorage.getItem("pair2token");
    const result = await payment({
      token: token,
    });
  };

  public render() {
    return (
      <div className="container">
        <h3 className="mt-2">Payment test</h3>
        <p>{this.state.cart !== [] && "상품목록"}</p>
        {this.state.cart.map(
          (data: { name: string; price: number }, i: number) => (
            <div
              onClick={() => this.cartModification(i)}
              className="row mb-2"
              key={i}
            >
              <p className="pt-3">
                이름: {data.name} 가격: {currency(data.price)}원
              </p>
              <button className="btn btn-sm btn-warning ml-1">x</button>
            </div>
          )
        )}
        <hr />

        <h5>총 결제 금액: {currency(this.state.totalPay)}원</h5>

        {this.state.stuffs.map((data: number, i: number) => (
          <button
            onClick={() => this.priceAdd(data)}
            key={i}
            className="btn btn-primary m-1"
          >
            {currency(data)}원
          </button>
        ))}

        <hr />
        <div className="row">
          <button
            onClick={() =>
              this.setState({ payType: "kakaopay", payName: "카카오페이" })
            }
            className="btn btn-warning ml-3"
          >
            카카오페이
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "card", payName: "카드결제" })
            }
            className="btn btn-warning ml-3"
          >
            카드
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "samsung", payName: "삼성페이" })
            }
            className="btn btn-warning ml-3"
          >
            삼성페이
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "cultureland", payName: "문화상품권" })
            }
            className="btn btn-warning ml-3"
          >
            문상
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "phone", payName: "핸드폰결제" })
            }
            className="btn btn-warning ml-3"
          >
            핸드폰결제
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "trans", payName: "실시간계좌이체" })
            }
            className="btn btn-warning ml-3"
          >
            실시간계좌이체
          </button>
          <button
            onClick={() =>
              this.setState({ payType: "vbank", payName: "가상계좌" })
            }
            className="btn btn-warning ml-3"
          >
            가상계좌
          </button>
        </div>
        <hr />

        <div className="row mb-6">
          <h5>{this.state.payName}</h5>
          <button
            onClick={this.requestPay}
            className="btn btn-lg btn-success mr-2"
          >
            결제요청
          </button>
          <button
            onClick={this.handleBilling}
            className="btn btn-lg btn-success mr-2"
          >
            정기
          </button>
          <button
            onClick={this.handleSubmitPayment}
            className="btn btn-lg btn-success"
          >
            결제요청 (서버에서 바로 결제 카드를 이용하여)
          </button>
        </div>
      </div>
    );
  }
}

export default PaymentTest;
