import React from "react";
import { Link } from "react-router-dom";
import { signinByEmail } from "../db/Actions";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import logo from "../images/pair2_web_logo.png";

interface SignInState {
  email: string;
  password: string;
  save: boolean;
  warning: boolean;
}

class SignIn extends React.Component<any, SignInState> {
  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: "",
      save: true,
      warning: false,
    };
  }

  public componentDidMount() {
    if (localStorage.getItem("pair2signin") === "true")
      this.props.history.push("/");
  }

  private handleText = (e: React.ChangeEvent<any>) => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value } as {
      [i in keyof SignInState]: any;
    });
  };

  private verifyPhone = (): boolean => {
    const regex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/;

    if (regex.test(this.state.email)) {
      return true;
    } else {
      return false;
    }
  };

  private handleClear = (formName: string): any => {
    this.setState({ [formName]: "" } as any);
  };

  private handleSave = (): void => this.setState({ save: !this.state.save });

  private verifyPhoneWithPassword = (): boolean => {
    if (this.state.email.length === 0) return false;
    if (this.state.password.length === 0) return false;
    return true;
  };

  private handleSubmit = async (e: any) => {
    e.preventDefault();

    if (this.state.email.length === 0) return;
    if (this.state.password.length === 0) return;

    const result = await signinByEmail(this.state.email, this.state.password);

    if (result === "faild") {
      this.setState({ warning: true });
    } else {
      localStorage.setItem("pair2token", result);
      localStorage.setItem("pair2signin", "true");

      if (this.props.match.params.page === "buy") {
        this.props.history.push(`/buy/${this.props.match.params.id}/All`);
      } else if (this.props.match.params.page === "sell") {
        this.props.history.push(`/sell/${this.props.match.params.id}/All`);
      } else {
        this.props.history.push("/");
      }
    }
  };

  public render() {
    return (
      <div className="container">
        <div className="col-lg-6 offset-lg-3 mt-180 mb-180 box-login">
          <div className="text-center">
            <img className="logo" src={logo} alt="" />
          </div>

          <form onSubmit={this.handleSubmit}>
            <div className="mb-2 mt-6 login-mobile">
              <input
                onChange={this.handleText}
                value={this.state.email}
                name="email"
                type="email"
                // pattern="[0-9]*"
                // inputMode="numeric"
                className={`form-control form-custom`}
                placeholder="이메일"
              />
            </div>
            
            <div className="mb-2">
              <input
                onChange={this.handleText}
                value={this.state.password}
                name="password"
                className={`form-control form-custom`}
                type="password"
                placeholder="비밀번호"
              />
            </div>

            {this.state.warning && (
              <p className="p-14 red">
                이메일 또는 비밀번호를 확인해 주세요.
              </p>
            )}

            <div className="row mt-3 mb-3">
              <div onClick={this.handleSave} className="col-7 cursor">
                <img
                  src={this.state.save ? checked : check}
                  className="float-left"
                  alt=""
                />
                <p className="float-left p-14 gray-3 pl-2">로그인 상태 유지</p>
              </div>
              <div className="col-5 text-right">
                <Link to="/findpassword/">
                  <p className="underline p-13 gray-2">비밀번호 찾기</p>
                </Link>
              </div>
            </div>

            {this.verifyPhoneWithPassword() ? (
              <button
                onClick={this.handleSubmit}
                className="w-100 btn-custom-active"
              >
                로그인
              </button>
            ) : (
              <button className="w-100 btn-custom-disable">로그인</button>
            )}
          </form>

          <p className="text-center p-13 gray-2 mt-4">
            아직 PAIR2 회원이 아니신가요?
            <Link to="/signup/">
              <span className="gray-2 underline pl-2">회원가입</span>
            </Link>
          </p>
        </div>
      </div>
    );
  }
}

export default SignIn;
