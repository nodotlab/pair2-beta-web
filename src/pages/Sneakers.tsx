import React from "react";
import { Link } from "react-router-dom";
import { currency } from "../Method";
import { sneakers, filters } from "../db/Actions";
import Sneaker from "../reduce/Sneaker";
import thumb from "../images/thumb.png";
import imgClose from "../images/GNB_Close.svg";
import imgReset from "../images/reset.svg";
import imgDown from "../images/tab_open.svg";
import imgUp from "../images/tab_close.svg";
import imgFOpen from "../images/Filter_Open.svg";
import imgFClose from "../images/Filter_Close.svg";
import SneakerDetail from "../pages/SneakerDetailModal";

interface SneakersState {
  filters: string[];
  sizes: Size[];
  brands: Brand[];
  otherBrands: Brand[];
  openOtherBrand: boolean;
  sneakers: Sneaker[];
  openFilter: boolean;
  openSortFilter: boolean;
  filterName: string;
  modalDetail: boolean;
  detailSneaker: string;
}

class Size {
  public size: string;
  constructor(_size: string) {
    this.size = _size;
  }
}

class Brand {
  public name: string;

  constructor(_name: string) {
    this.name = _name;
  }
}

class Sneakers extends React.Component<any, SneakersState> {
  constructor(props: any) {
    super(props);
    this.state = {
      filters: [],
      sizes: this.initSizes(),
      brands: [],
      otherBrands: [],
      openOtherBrand: false,
      sneakers: [],
      openFilter: false,
      openSortFilter: false,
      filterName: "인기순",
      modalDetail: false,
      detailSneaker: "",
    };
  }

  public componentDidMount() {
    this.initSneakers();
    this.initBrands();
  }

  private initSizes(): Size[] {
    const tempSizes = [];
    for (let i = 220; i < 320; i += 5) {
      tempSizes.push(new Size(i.toString()));
    }
    return tempSizes;
  }

  private async initBrands() {
    const _filters: any = await filters();
    let brands: Brand[] = [];
    let otherBrands: Brand[] = [];

    for (let i = 0; i < _filters.length; ++i) {
      if (!_filters[i].other) {
        brands.push(new Brand(_filters[i].name.toUpperCase()));
      } else if (_filters[i].name.toUpperCase() !== "OTHER") {
        otherBrands.push(new Brand(_filters[i].name.toUpperCase()));
      }
    }

    brands = brands.sort((a: any, b: any) => {
      if (a.name > b.name) {
        return 1;
      } else {
        return -1;
      }
    });

    otherBrands = otherBrands.sort((a: any, b: any) => {
      if (a.name > b.name) {
        return 1;
      } else {
        return -1;
      }
    });

    otherBrands.push(new Brand("OTHER"));

    this.setState({
      brands: brands,
      otherBrands: otherBrands,
    });
  }

  private async initSneakers() {
    const result = await sneakers(this.state.filterName, {}, 30);
    const tempSneakers: Sneaker[] = [];

    for (let i = 0; i < result.length; ++i) {
      tempSneakers.push(
        new Sneaker(
          result[i].brand,
          result[i].name,
          "",
          isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
          result[i].thumbUrl ? result[i].thumbUrl : thumb,
          result[i].releaseDate
        )
      );
    }

    this.setState({ sneakers: tempSneakers });
  }

  private handleReset = (): void =>
    this.setState({ filters: [] }, () => this.filterRecurrent());

  private handleFilterDelete = (name: string): void => {
    const tempFilters: string[] = [];

    for (let i = 0; i < this.state.filters.length; ++i) {
      if (name !== this.state.filters[i]) {
        tempFilters.push(this.state.filters[i]);
      }
    }

    this.setState(
      {
        filters: tempFilters,
      },
      () => this.filterRecurrent()
    );
  };

  private handleFilter = (name: string, type: string): void => {
    if (name.substr(0, 1) !== "2" && name.substr(0, 1) !== "3") {
      const tempFilters: string[] = [];
      for (let i = 0; i < this.state.filters.length; ++i) {
        if (
          this.state.filters[i].substr(0, 1) === "2" ||
          this.state.filters[i].substr(0, 1) === "3"
        ) {
          tempFilters.push(this.state.filters[i]);
        }
      }
      if (!this.filterSelected(name)) tempFilters.push(name);

      this.setState({ filters: tempFilters }, () => this.filterRecurrent());
    } else {
      // size reter
      if (this.filterSelected(name)) {
        const tempFilters: string[] = [];
        for (let i = 0; i < this.state.filters.length; ++i) {
          if (name !== this.state.filters[i]) {
            tempFilters.push(this.state.filters[i]);
          }
        }
        this.setState({ filters: tempFilters }, () => this.filterRecurrent());
      } else {
        const tempFilters: string[] = this.state.filters;
        tempFilters.push(name);
        this.setState({ filters: tempFilters }, () => this.filterRecurrent());
      }
    }
  };

  private filterSelected(name: string): boolean {
    for (let i = 0; i < this.state.filters.length; ++i) {
      if (name === this.state.filters[i]) return true;
    }

    return false;
  }

  private async filterRecurrent() {
    const tempSneakers: Sneaker[] = [];
    const result = await sneakers(
      this.state.filterName,
      { ...this.state.filters },
      30
    );

    for (let i = 0; i < result.length; ++i) {
      // const price = await sneakerPrice(result[i].index);

      tempSneakers.push(
        new Sneaker(
          result[i].brand,
          result[i].name,
          "",
          isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
          result[i].thumbUrl ? result[i].thumbUrl : thumb,
          result[i].releaseDate
        )
      );
    }

    this.setState({ sneakers: tempSneakers });
  }

  private handleFilterOpen = (): void =>
    this.setState({ openFilter: !this.state.openFilter });
  private handleSortFilter = (): void =>
    this.setState({ openSortFilter: !this.state.openSortFilter });

  private handleSort = async (name: string) => {
    this.setState({
      filterName: name,
      openSortFilter: false,
    });

    const result = await sneakers(name, { ...this.state.filters }, 30);
    const tempSneakers: Sneaker[] = [];

    for (let i = 0; i < result.length; ++i) {
      tempSneakers.push(
        new Sneaker(
          result[i].brand,
          result[i].name,
          "",
          isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
          result[i].thumbUrl ? result[i].thumbUrl : thumb,
          result[i].releaseDate
        )
      );
    }

    this.setState({ sneakers: tempSneakers });
  };

  private handleOtherBrand = (): void =>
    this.setState({ openOtherBrand: !this.state.openOtherBrand });

  private handleMore = async () => {
    const tempSneakers: Sneaker[] = [];
    const result = await sneakers(
      this.state.filterName,
      { ...this.state.filters },
      this.state.sneakers.length + 30
    );

    for (let i = 0; i < result.length; ++i) {
      tempSneakers.push(
        new Sneaker(
          result[i].brand,
          result[i].name,
          "",
          isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
          result[i].thumbUrl ? result[i].thumbUrl : thumb,
          result[i].releaseDate
        )
      );
    }

    this.setState({ sneakers: tempSneakers });
  };

  private handleModalWithSneakerName = (name: string): void => {
    this.setState({
      modalDetail: true,
      detailSneaker: name,
    });
  };

  private handleModalDetail = (): void =>
    this.setState({ modalDetail: !this.state.modalDetail });

  public render() {
    return (
      <div>
        {this.state.modalDetail && <this.detailFullScreen />}

        <div
          onClick={this.handleFilterOpen}
          className="box-mobile-filter text-center hidden-p"
        >
          <p className="p-13 black-2 roboto bold letter-space-4">
            FILTERS
            <img
              src={this.state.openFilter ? imgFOpen : imgFClose}
              alt=""
              className="ml-1"
            />
          </p>
        </div>
        {this.state.openFilter && (
          <div className="horizontal-filter hidden-p">
            <div className="mobile-reset">
              <div className="mobile-reset-left"></div>
              <div className="mobile-reset-right">
                <p
                  onClick={this.handleReset}
                  className="p-14 roboto gray-2 cursor"
                >
                  Reset <img src={imgReset} />
                </p>
              </div>
            </div>
            {this.state.filters.map((filter: string, i: number) => (
              <button
                onClick={() => this.handleFilterDelete(filter)}
                key={i}
                className="btn btn-filtered horizontal-filter-btn"
              >
                {filter} <img src={imgClose} className="icon-close" />
              </button>
            ))}
          </div>
        )}

        {this.state.openFilter && (
          <div className="hidden-p mt-5">
            <div className="flex">
              <div className="col-3 pl-0 pr-0 text-center">
                <p className="p-16 bold mt-1">사이즈</p>
              </div>
              <div className="col-9 pl-0 pr-0">
                {this.state.sizes.map((size: Size, i: number) => (
                  <button
                    onClick={() => this.handleFilter(size.size, "size")}
                    key={i}
                    className={`btn btn-filter ${
                      this.filterSelected(size.size) && "btn btn-filter-active"
                    }`}
                  >
                    {size.size}
                  </button>
                ))}
              </div>
            </div>

            <div className="flex mt-4">
              <div className="col-3 pl-0 pr-0 text-center">
                <p className="p-16 bold mt-1">브랜드</p>
              </div>
              <div className="col-9 pl-0 pr-0">
                {this.state.brands.map((brand: Brand, i: number) => (
                  <button
                    onClick={() => this.handleFilter(brand.name, "brand")}
                    key={i}
                    className={`btn btn-brand ${
                      this.filterSelected(brand.name) && "btn btn-brand-active"
                    }`}
                  >
                    {brand.name}
                  </button>
                ))}
                {/* <button onClick={this.handleOtherBrand} className={`btn btn-brand ${this.state.openOtherBrand && "btn-brand-active"}`}> */}
                <button
                  onClick={this.handleOtherBrand}
                  className="btn btn-brand"
                >
                  {"Other Brands".toUpperCase()}
                </button>
                {this.state.openOtherBrand &&
                  this.state.otherBrands.map((brand: Brand, i: number) => (
                    <button
                      onClick={() => this.handleFilter(brand.name, "brand")}
                      key={i}
                      className={`btn btn-other ${
                        this.filterSelected(brand.name) && "btn-other-active"
                      }`}
                    >
                      {brand.name}
                    </button>
                  ))}
              </div>
            </div>
          </div>
        )}

        <div className="container">
          <div className="row mt-5">
            <div className="col-lg-3 pb-5 hidden-m">
              <div className="row lg-3">
                <div className="col-6">
                  <p className="p-18 black-2 roboto bold letter-space-4">
                    FILTERS
                  </p>
                </div>
                <div className="col-6 text-right">
                  {this.state.filters.length > 0 && (
                    <p
                      onClick={this.handleReset}
                      className="p-14 roboto gray-2 cursor pt-1"
                    >
                      Reset <img src={imgReset} />
                    </p>
                  )}
                </div>
              </div>

              {this.state.filters.map((filter: string, i: number) => (
                <button
                  onClick={() => this.handleFilterDelete(filter)}
                  key={i}
                  className="btn btn-filtered"
                >
                  {filter} <img src={imgClose} className="icon-close" />
                </button>
              ))}

              <p className="p-16 bold mt-4 mb-2">사이즈</p>
              {this.state.sizes.map((size: Size, i: number) => (
                <button
                  onClick={() => this.handleFilter(size.size, "size")}
                  key={i}
                  className={`btn btn-filter ${
                    this.filterSelected(size.size) && "btn btn-filter-active"
                  }`}
                >
                  {size.size}
                </button>
              ))}

              <p className="p-16 bold mt-4 mb-2">브랜드</p>
              {this.state.brands.map((brand: Brand, i: number) => (
                <button
                  onClick={() => this.handleFilter(brand.name, "brand")}
                  key={i}
                  className={`btn btn-brand ${
                    this.filterSelected(brand.name) && "btn btn-brand-active"
                  }`}
                >
                  {brand.name}
                </button>
              ))}

              <button onClick={this.handleOtherBrand} className="btn btn-brand">
                {"Other Brands".toUpperCase()}
              </button>
              {this.state.openOtherBrand &&
                this.state.otherBrands.map((brand: Brand, i: number) => (
                  <button
                    onClick={() => this.handleFilter(brand.name, "brand")}
                    key={i}
                    className={`btn btn-other ${
                      this.filterSelected(brand.name) && "btn-other-active"
                    }`}
                  >
                    {brand.name}
                  </button>
                ))}
            </div>

            <div className="col-lg-9">
              <div className="row mb-4">
                <div className="col-6">
                  <p className="p-14 black-2">
                    총{" "}
                    <span className="bold">{this.state.sneakers.length}</span>건
                  </p>
                </div>
                <div className="col-6 text-right">
                  <p
                    onClick={this.handleSortFilter}
                    className="p-14 black-2 cursor"
                  >
                    {this.state.filterName}
                    <img
                      src={this.state.openSortFilter ? imgUp : imgDown}
                      className="ml-1"
                      alt=""
                    />
                  </p>
                  <div
                    className={`dropdown-filter ${
                      !this.state.openSortFilter && "d-none"
                    }`}
                  >
                    <p
                      onClick={() => this.handleSort("최근 발매일순")}
                      className="p-14 black-2 cursor mt-1"
                    >
                      최근 발매일순
                    </p>
                    <p
                      onClick={() => this.handleSort("인기순")}
                      className="p-14 black-2 cursor mt-1"
                    >
                      인기순
                    </p>
                    <p
                      onClick={() => this.handleSort("최근 거래순")}
                      className="p-14 black-2 cursor mt-1"
                    >
                      최근 거래순
                    </p>
                  </div>
                </div>
              </div>

              <div className="row sneaker-mobile-row hidden-p">
                {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
                  <div key={i} className="col-md-4 col-6 mb-3 sneaker-mobile">
                    <div
                      onClick={() =>
                        this.handleModalWithSneakerName(sneaker.name)
                      }
                    >
                      <div
                        className="box-sneaker"
                        style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                      />
                      <p className="p-12 roboto bold gray-2 mt-3">
                        {sneaker.brand.toUpperCase()}
                      </p>
                      <p className="sneaker-name p-14 roboto black-2 mt-1 hidden-m">
                        {sneaker.name.toUpperCase()}
                      </p>
                      <p className="sneaker-name p-14 roboto black-2 mt-1 hidden-p">
                        {sneaker.name.toUpperCase().substr(0, 40)}
                        {sneaker.name.length > 40 && "..."}
                      </p>
                      <p className="float-left p-14 gray-2 mt-2 hidden-m">
                        최저판매가
                      </p>
                      <p className="sneaker-price float-right roboto p-18 bold orange hidden-m">
                        {sneaker.lowPrice === 0
                          ? "—"
                          : currency(sneaker.lowPrice)}
                        {sneaker.lowPrice > 0 && (
                          <span className="p-14 noto normal">원</span>
                        )}
                      </p>
                      <p className="text-right p-12 gray-2 mt-2 hidden-p">
                        최저판매가
                      </p>
                      <p className="sneaker-price text-right roboto p-14 bold orange hidden-p">
                        {sneaker.lowPrice === 0
                          ? "—"
                          : currency(sneaker.lowPrice)}
                        {sneaker.lowPrice > 0 && (
                          <span className="p-14 noto normal">원</span>
                        )}
                      </p>
                    </div>
                  </div>
                ))}
              </div>

              <div className="row sneaker-mobile-row hidden-m">
                {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
                  <div key={i} className="col-md-4 col-6 mb-3 sneaker-mobile">
                    <Link to={`/sneaker/${sneaker.name}`}>
                      <div
                        className="box-sneaker"
                        style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                      />
                      <p className="p-12 roboto bold gray-2 mt-3">
                        {sneaker.brand.toUpperCase()}
                      </p>
                      <p className="sneaker-name p-14 roboto black-2 mt-1 hidden-m">
                        {sneaker.name.toUpperCase()}
                      </p>
                      <p className="sneaker-name p-14 roboto black-2 mt-1 hidden-p">
                        {sneaker.name.toUpperCase().substr(0, 40)}
                        {sneaker.name.length > 40 && "..."}
                      </p>
                      <p className="float-left p-12 gray-2 mt-2 hidden-m">
                        최저판매가
                      </p>
                      <p className="sneaker-price float-right roboto p-14 bold orange hidden-m">
                        {sneaker.lowPrice === 0
                          ? "—"
                          : currency(sneaker.lowPrice)}
                        {sneaker.lowPrice > 0 && (
                          <span className="p-14 noto normal">원</span>
                        )}
                      </p>
                      <p className="text-right p-12 gray-2 mt-2 hidden-p">
                        최저판매가
                      </p>
                      <p className="sneaker-price text-right roboto p-14 bold orange hidden-p">
                        {sneaker.lowPrice === 0
                          ? "—"
                          : currency(sneaker.lowPrice)}
                        {sneaker.lowPrice > 0 && (
                          <span className="p-14 noto normal">원</span>
                        )}
                      </p>
                    </Link>
                  </div>
                ))}
              </div>

              {this.state.sneakers.length > 29 && (
                <button
                  onClick={this.handleMore}
                  className="btn btn-custom w-100 mt-3 mb-5"
                >
                  더보기
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  private detailFullScreen = () => {
    return (
      <div className="modal-fullscreen">
        <SneakerDetail
          name={this.state.detailSneaker}
          handleModal={this.handleModalDetail}
        />
      </div>
    );
  };
}

export default Sneakers;
