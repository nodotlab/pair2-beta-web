import React from "react";
import imgAbout from "../images/about-1.jpg";
import imgAbout2 from "../images/about-2.jpg";

class About extends React.Component<any> {
  public render() {
    return (
      <div className="mb-6">
        <div className="flex">
          <div className="col-md-6 col-12 box-about-left mt-m-0">
            <div className="about-left-quote hidden-p">"</div>
            <div className="about-left-in">
              <p>
                최신
                <br />
                스니커 정보를
                <br />
                전해드립니다.
              </p>
            </div>
            <div className="about-left-quote hidden-m">"</div>
          </div>

          <div className="col-6 box-about-right hidden-m">
            <p className="bold black-2 p-32">ENTER THE SHOE GAME</p>
            <p className="black-2 p-22 mt-4">
              "AIR"만 봐도 스니커를 떠올리는
              <br />
              스니커헤드들이 모여 직접 만드는
              <br />
              스니커 플랫폼 "PAIR2" 입니다.
              <br />
            </p>
          </div>
        </div>

        <div className="about-middle-mobile hidden-p">
          <p className="bold black-2 p-22">ENTER THE SHOE GAME</p>
          <p className="black-2 line-25 p-14 mt-4">
            "AIR"만 봐도 스니커를 떠올리는
            <br />
            스니커헤드들이 모여 직접 만드는
            <br />
            스니커 플랫폼 "PAIR2" 입니다.
            <br />
          </p>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-md-8 col-12 box-about-middle">
              <img src={imgAbout} alt="" className="img-fluid" />
            </div>
          </div>

          <div className="row hidden-m">
            <div className="col-6 offset-6 box-about-middle2 about-middle-text">
              <p className="black-2 p-20 line-29">
                <br />  
                <br />  
                <br />  
                <br />  
                스니커씬이 그 어느때보다 뜨거운 지금 많은 협업과 출시 소식이 넘쳐납니다.
                <br />
                <br />
                도처에 흩어져 수많은 정보들을 일일이 찾아보기도 힘들고 알고 있다가도 놓치기도 쉽죠. 
                <br />
                (만든이의 경험담, 한두번 놓치는게 아님...)
              </p>
            </div>
            <div className="col-6 offset-6 mt-6">
              <img src={imgAbout2} alt="" className="img-fluid" />
            </div>

            <div className="col-6 offset-6 about-middle-text">
              <p className="black-2 p-20 line-29 mt-6">
                그간의 경험을 바탕으로
                <br />
                스니커 소식를 누구나 쉽게 접할 수 있도록
                <br />
                잘 정리된 정보들을 발 빠르게 전해드립니다.
                <br />
              </p>
            </div>
          </div>

          <div className="about-mobile-bottom hidden-p">
            <p className="black-2 p-14 line-29 mt-5 mb-5">
              스니커씬이 그 어느때보다 뜨거운 지금 많은 협업과 출시 소식이 넘쳐납니다.
              <br />
              <br />
              도처에 흩어져 수많은 정보들을 일일이 찾아보기도 힘들고 알고 있다가도 놓치기기 일수죠.
              <br />
              (만든이의 경험담, 한두번 놓치는게 아님...)                          
            </p>
            <div className="text-right">
              <img src={imgAbout2} alt="" className="img-fluid" />
            </div>
            <p className="black-2 p-14 line-29 mt-5">
              그간의 경험을 바탕으로
              <br />
              스니커 소식를 누구나 쉽게 접할 수 있도록
              <br />
              잘 정리된 정보들을 발 빠르게 전해드립니다.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
