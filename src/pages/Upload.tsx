import React from "react";
import axios from "axios";
import { Consumer } from "../store";

class Upload extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      notification: "",
      file: undefined,
      fileUrl: undefined,
      file2: undefined,
      fileUrl2: undefined,
      file3: undefined,
      fileUrl3: undefined,
    };
  }

  public componentDidMount() {}

  public componentDidUpdate() {}

  private onSubmit = (e: any) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("images", this.state.file);
    formData.append("images", this.state.file2);
    formData.append("images", this.state.file3);

    const config = {
      headers: { "content-type": "multipart/form-data" },
    };

    axios
      .post("http://localhost:3000/api/imageupload/", formData, config)
      .then((res) => {})
      .catch((error) => {});
  };

  private onChange = (e: any) => {
    const fileType: string = e.target.files[0].type.substr(
      e.target.files[0].type.length - 3,
      e.target.files[0].type.length
    );
    if (fileType !== "png") {
      this.setState({ notification: "png 파일을 올려주세용" });
    } else {
      this.setState({
        file: e.target.files[0],
        fileUrl: URL.createObjectURL(e.target.files[0]),
      });
    }
  };

  private onChange2 = (e: any) => {
    this.setState({
      file2: e.target.files[0],
      fileUrl2: URL.createObjectURL(e.target.files[0]),
    });
  };

  private onChange3 = (e: any) => {
    this.setState({
      file3: e.target.files[0],
      fileUrl3: URL.createObjectURL(e.target.files[0]),
    });
  };

  public render() {
    return (
      <div className="container">
        <h4>{this.state.notification}</h4>
        <div className="col-8 offset-2">
          <form onSubmit={this.onSubmit}>
            <img src={this.state.fileUrl} className="img-thumbnail" />
            <input type="file" name="file" onChange={this.onChange} />

            <img src={this.state.fileUrl2} className="img-thumbnail" />
            <input type="file" name="file2" onChange={this.onChange2} />

            <img src={this.state.fileUrl3} className="img-thumbnail" />
            <input type="file" name="file3" onChange={this.onChange3} />

            <button type="submit">Upload</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Upload;
