import React from "react";
import { currency } from "../Method";
import { getMain, release, mainProduct } from "../db/Actions";
import imgPlus from "../images/Icon_Plus.svg";
import img7 from "../images/Main_02Event.png";
import { Link } from "react-router-dom";
import MetaData from "../components/MetaGenerator";
import Release from "../reduce/Release";


class Sneaker {
  public brand: string;
  public name: string;
  public lowPrice: number;
  public imageUrl: string;

  constructor(
    _brand: string,
    _name: string,
    _lowPrice: number,
    _imageUrl: string
  ) {
    this.brand = _brand;
    this.name = _name;
    this.lowPrice = _lowPrice;
    this.imageUrl = _imageUrl;
  }
}

interface MainState {
  highlightDeal: {
    name: string;
    name2: string;
    url: string;
    imageUrl: string;
    imageUrl2: string;
    imageUrl3: string;
  };
  scrollY: number;
  sneakers: Sneaker[];
  carouselIndex: number;
  game: {
    name: string;
    subName: string;
    imageUrl: string;
  };
  releases: Release[];
  carouselIndex2: number;
}

class Main extends React.Component<any, MainState> {
  constructor(props: any) {
    super(props);
    this.state = {
      highlightDeal: {
        name: "",
        name2: "",
        url: "",
        imageUrl: "",
        imageUrl2: "",
        imageUrl3: "",
      },
      scrollY: 0,
      sneakers: [],
      carouselIndex: 0,
      game: {
        name: "sacai x Nike LDWaffle Summit White",
        subName: "사카이 x 나이키 LD와플 화이트",
        imageUrl: img7,
      },
      releases: [],
      carouselIndex2: 0,
    };
  }

  public componentDidMount() {
    window.addEventListener("scroll", this.listenScroll);
    this.carouselIndex();
    this.getMainProduct();
    this.getSneakers();
    this.initReleases();
  }

  public componentWillUnmount() {
    window.removeEventListener("scroll", this.listenScroll);
  }

  private listenScroll = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;
    const scrolled = winScroll / height;
    this.setState({ scrollY: scrolled });
  };

  private async getMainProduct() {
    try {
      const result: any = await mainProduct();
      const highlightDeal = this.state.highlightDeal;
      highlightDeal.name = result.en;
      highlightDeal.name2 = result.ko;
      highlightDeal.url = result.url;
      highlightDeal.imageUrl = result.image1;
      highlightDeal.imageUrl2 = result.image2;
      highlightDeal.imageUrl3 = result.image3;
      this.setState({ highlightDeal: highlightDeal });
    } catch (error) { }
  }

  private async getSneakers() {
    try {
      const result: any = await getMain();
      const tempArrary: Sneaker[] = [];

      for (let i = 0; i < result.length; ++i) {
        tempArrary.push(
          new Sneaker(
            result[i].brand,
            result[i].name,
            isNaN(result[i].lowPrice) ? 0 : result[i].lowPrice,
            result[i].thumbUrl
          )
        );
      }

      this.setState({ sneakers: tempArrary });
    } catch (error) { }
  }

  private async initReleases() {
    try {
      const arr: Release[] = [];
      const _release = await release();

      for (const i in _release) {
        arr.push(
          new Release(
            _release[i].releaseDate,
            _release[i].brand.toUpperCase(),
            _release[i].name,
            _release[i].nameKr,
            _release[i].imageUrl,
            _release[i].unknownRelease,
            _release[i].type,
            _release[i].index,
          )
        );
      }

      arr.sort((a: Release, b: Release) => {
        if (a.name > b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
      arr.sort((a: Release, b: Release) => {
        if (a.releaseTimestamp > b.releaseTimestamp) return -1;
        if (a.releaseTimestamp > b.releaseTimestamp) return 1;
        return 0;
      });

      this.setState({ releases: arr.slice(0, 16) });
    } catch (error) { }
  }

  private carouselIndex(): void {
    setInterval(() => {
      if (this.state.carouselIndex === 3) {
        this.setState({ carouselIndex: 0 });
      } else {
        this.setState({ carouselIndex: this.state.carouselIndex + 1 });
      }

      if (this.state.carouselIndex2 === 3) {
        this.setState({ carouselIndex2: 0 });
      } else {
        this.setState({ carouselIndex2: this.state.carouselIndex2 + 1 });
      }
    }, 5000);
  }

  private handleCrouselIndex = (index: number): void => {
    this.setState({ carouselIndex: index });
  };

  private handleCrouselIndex2 = (index: number): void => {
    this.setState({ carouselIndex2: index });
  };

  public render() {

    const metaData = {
      image: this.state.highlightDeal.imageUrl2
    };

    return (
      <div>
        <MetaData data={metaData} />
        <div className="container">
          <div className="scroll-box" />
        </div>
        <p className="mainslide">
          <span>ENTER THE SHOE GAME</span>
        </p>

        <div className="container container-main">
          <Link to={this.state.highlightDeal.url}>
            <div className="col-lg-12 highlight-top" style={{ backgroundImage: `url(${this.state.highlightDeal.imageUrl})`, }}>
              <div className="col-lg-6 col-md-12 offset-lg-6 col-12 highlight-top">
                <p className="p-18 roboto bold black-2 o-60 letter-space-4 hidden-m">
                  Upcoming
                </p>
                <p className="p-40 roboto black-2 line-48 mt-2 hidden-m">
                  {this.state.highlightDeal.name}
                </p>
                <p className="p-16 orange bold mt-4 hidden-m">
                  {this.state.highlightDeal.name2}
                </p>

                <p className="p-13 roboto bold black-2 o-60 letter-space-4 hidden-p">
                Upcoming
                </p>
                <p className="p-24 roboto black-2 mt-2 hidden-p">
                  {this.state.highlightDeal.name}
                </p>
                <p className="p-13 orange bold mt-4 hidden-p">
                  {this.state.highlightDeal.name2}
                </p>
                <Link to={this.state.highlightDeal.url}>
                  <div className="box-plus box-plus-black mt-5 hidden-m">
                    <span className="box-plus-icon">
                      <img src={imgPlus} />
                    </span>
                    <p>발매소식보기</p>
                  </div>
                </Link>
              </div>
            </div>
            <div className="col-lg-8 offset-lg-4 highlight-middle hidden-m" style={{ marginTop: 10 + this.state.scrollY * -300 }}>
              <img src={this.state.highlightDeal.imageUrl2} alt={this.state.highlightDeal.name2} />
            </div>
            <div className="col-lg-5 offset-lg-1 highlight-bottom hidden-m" style={{ marginTop: 100 + this.state.scrollY * -400 }}>
              <img src={this.state.highlightDeal.imageUrl3} alt={this.state.highlightDeal.name2} />
            </div>

            <div className="highlight-middle hidden-p" style={{ marginTop: -100 + this.state.scrollY * 100 }} >
              <img src={this.state.highlightDeal.imageUrl2} alt={this.state.highlightDeal.name2} />
            </div>
            <div className="highlight-bottom hidden-p" style={{ marginTop: -25 + this.state.scrollY * -300 }} >
              <img src={this.state.highlightDeal.imageUrl3} alt={this.state.highlightDeal.name2} />
            </div>
          </Link>

        </div>

        <div className="bg-white relative main-popular">
          {/* <div className="container pt-5 hidden-m">
            <div className="row relative">
              <div className="col-md-6">
                <p className="p-18 roboto bold letter-space-4 mb-4">
                  MOST POPULAR
                </p>
              </div>

              <div className="col-md-6 text-right">
                <Link to="/sneakers">
                  <p className="p-14 gray-2 bold underline">더보기</p>
                </Link>
              </div>
              {this.state.sneakers
                .slice(
                  this.state.carouselIndex * 3,
                  this.state.carouselIndex * 3 + 3
                )
                .map((sneaker: Sneaker, i: number) => (
                  <Link
                    to={`/sneaker/${sneaker.name}`}
                    key={i}
                    className="col-md-4"
                  >
                    <div
                      className="box-popular"
                      style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                    />
                    <p className="p-12 roboto bold gray-2 mt-3">
                      {sneaker.brand.toUpperCase()}
                    </p>
                    <p className="sneaker-name p-14 roboto back-2 mt-1">
                      {sneaker.name.toUpperCase()}
                    </p>
                    <p className="float-left p-14 gray-2 mt-1">최저판매가</p>
                    <p className="float-right roboto p-18 bold orange">
                      {sneaker.lowPrice === 0
                        ? "—"
                        : currency(sneaker.lowPrice)}
                      <span className="p-14 noto normal">
                        {sneaker.lowPrice !== 0 && "원"}
                      </span>
                    </p>
                  </Link>
                ))}
            </div>

            <div className="box-carousel mt-5">
              <div
                onClick={() => this.handleCrouselIndex(0)}
                className={`icon-carousel ${
                  this.state.carouselIndex === 0 && "icon-carousel-active"
                }`}
              />
              <div
                onClick={() => this.handleCrouselIndex(1)}
                className={`icon-carousel ${
                  this.state.carouselIndex === 1 && "icon-carousel-active"
                }`}
              />
              <div
                onClick={() => this.handleCrouselIndex(2)}
                className={`icon-carousel ${
                  this.state.carouselIndex === 2 && "icon-carousel-active"
                }`}
              />
              <div
                onClick={() => this.handleCrouselIndex(3)}
                className={`icon-carousel ${
                  this.state.carouselIndex === 3 && "icon-carousel-active"
                }`}
              />
            </div>
          </div> */}

          {/* <div className="container pt-5 hidden-p">
            <div className="row">
              <div className="col-6">
                <p className="p-13 roboto bold letter-space-4 mb-3">
                  MOST POPULAR
                </p>
              </div>
              <div className="col-6 text-right">
                <Link to="/sneakers">
                  <p className="p-13 gray-2 underline">더보기</p>
                </Link>
              </div>
            </div>

            <div className="horizontal-wrapper">
              {this.state.sneakers.map((sneaker: Sneaker, i: number) => (
                <Link
                  to={`/sneaker/${sneaker.name}`}
                  key={i}
                  className="horizontal-card"
                >
                  <div
                    className="box-popular"
                    style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                  />
                  <p className="p-12 roboto bold gray-2 mt-3">
                    {sneaker.brand.toUpperCase()}
                  </p>
                  <p className="sneaker-name p-14 roboto black-2 mt-1">
                    {sneaker.name.toUpperCase().substr(0, 29)}
                  </p>
                  <p className="text-right p-14 gray-2 mt-2">최저판매가</p>
                  <p className="text-right roboto p-18 bold orange">
                    {sneaker.lowPrice === 0 ? "—" : currency(sneaker.lowPrice)}
                    <span className="p-14 noto normal">
                      {sneaker.lowPrice !== 0 && "원"}
                    </span>
                  </p>
                </Link>
              ))}
            </div>
          </div> */}

          <div className="box-main-release hidden-m">
            <div className="container">
              <div className="row">
                <div className="col-6">
                  <p className="p-18 roboto bold letter-space-4 mb-5">
                    LAUNCH CALENDAR
                  </p>
                </div>
                <div className="col-6 text-right">
                  <Link to="/release/">
                    <p className="p-14 gray-2 bold underline">더보기</p>
                  </Link>
                </div>

                {this.state.releases
                  .slice(
                    this.state.carouselIndex2 * 4,
                    this.state.carouselIndex2 * 4 + 4
                  )
                  .map((release: Release, i: number) => (
                    <Link to="/release/" key={i} className="col-md-3">
                      <p className="p-28 gray-3 roboto bold text-right">
                        {new Date(release.releaseTimestamp).getMonth() + 1}/
                        {new Date(release.releaseTimestamp).getDate()}
                      </p>
                      <div
                        className="main-release-in"
                        style={{ backgroundImage: `url(${release.imageUrl})` }}
                      />
                      <p className="p-12 roboto bold gray-2 mt-3">
                        {release.brand.toUpperCase()}
                      </p>
                      <p className="p-14 roboto back-2 mt-1">
                        {release.nameKr.toUpperCase()}
                      </p>
                    </Link>
                  ))}
              </div>
            </div>

            <div className="box-carousel mt-60">
              <div
                onClick={() => this.handleCrouselIndex2(0)}
                className={`icon-carousel ${this.state.carouselIndex2 === 0 && "icon-carousel-active"
                  }`}
              />
              <div
                onClick={() => this.handleCrouselIndex2(1)}
                className={`icon-carousel ${this.state.carouselIndex2 === 1 && "icon-carousel-active"
                  }`}
              />
              <div
                onClick={() => this.handleCrouselIndex2(2)}
                className={`icon-carousel ${this.state.carouselIndex2 === 2 && "icon-carousel-active"
                  }`}
              />
              <div
                onClick={() => this.handleCrouselIndex2(3)}
                className={`icon-carousel ${this.state.carouselIndex2 === 3 && "icon-carousel-active"
                  }`}
              />
            </div>
          </div>

          <div className="box-main-release hidden-p">
            <div className="container">
              <div className="row">
                <div className="col-8">
                  <p className="p-13 roboto bold letter-space-4 mb-5">
                    LAUNCH CALENDAR
                  </p>
                </div>
                <div className="col-4 text-right">
                  <Link to="/release/">
                    <p className="p-13 gray-2 underline">더보기</p>
                  </Link>
                </div>
              </div>
            </div>

            <div className="horizontal-wrapper">
              {this.state.releases.map((release: Release, i: number) => (
                <Link to="/release/" key={i} className="horizontal-card">
                  <p className="p-18 gray-3 roboto bold float-right mr-3 mt-1">
                    {new Date(release.releaseTimestamp).getMonth() + 1}/
                    {new Date(release.releaseTimestamp).getDate()}
                  </p>
                  <div
                    className="main-release-in"
                    style={{ backgroundImage: `url(${release.imageUrl})` }}
                  />
                  <p className="p-12 pl-3 roboto bold gray-2 mt-3">
                    {release.brand.toUpperCase()}
                  </p>
                  <p className="p-14 pl-3 roboto back-2 mt-1">
                    {release.nameKr.substr(0, 29)}
                  </p>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div >
    );
  }
}

export default Main;
