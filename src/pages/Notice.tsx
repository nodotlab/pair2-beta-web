import React from "react";
import { Link } from "react-router-dom";
import Post from "../reduce/Post";
import { timeFormat2 } from "../Method";

interface NoticeState {
  tab: string;
  notice: Post[]; // image description
  events: Post[];
}

class Notice extends React.Component<any, NoticeState> {
  constructor(props: any) {
    super(props);
    this.state = {
      tab: "notice",
      notice: this.initNotice(),
      events: this.initEvents(),
    };
  }

  private initNotice(): Post[] {
    const tempNotice: Post[] = [];
    for (let i = 0; i < 10; ++i) {
      tempNotice.push(
        new Post(
          i,
          Date.now(),
          "개인정보 처리방침 변경 사전 안내",
          "안녕하세요. \nair2 회원님께 더 나은 서비스를 제공할 수 있도록 개인정보 처리방침 문서를 변경하였음을 알려드립니다. 변경 내용 (v2.2) • 본인인증 추가로 인한 필수 수집 개인정보 항목 추가  • 법정대리인 동의가 필요한 경우 법정대리인 정보수집 항목 추가 • 개인정보 수탁자 표기 수정 (서비스명 → 회사명) • 매입상품 배송 대행 수탁자 변경 : (주)텐바이텐 → 로지포커스(주) • 주문정보 처리 수탁자 추가 : (주)에이치케이넷츠 (EC모니터) PAIR2는 앞으로도 회원님의 개인정보를 보다 안전하게 보호할 것을 약속드립니다. 고맙습니다.",
          false
        )
      );
    }

    return tempNotice.reverse();
  }

  private initEvents(): Post[] {
    const tempPosts: Post[] = [];
    for (let i = 0; i < 10; ++i) {
      tempPosts.push(
        new Post(i, Date.now(), "이벤트 사항", "안녕하세요. ", false)
      );
    }

    return tempPosts.reverse();
  }

  private handleOpenNotice = (index: number): void => {
    const tempNotice: Post[] = this.state.notice;
    tempNotice[index].isOpen = !tempNotice[index].isOpen;

    this.setState({ notice: tempNotice });
  };

  private handleOpenEvent = (index: number): void => {
    const tmepEvents: Post[] = this.state.events;
    tmepEvents[index].isOpen = !tmepEvents[index].isOpen;

    this.setState({ events: tmepEvents });
  };

  private handleMoreNotice = (): void => {
    const tempNotice: Post[] = this.state.notice.reverse();
    for (let i = 0; i < 10; ++i) {
      tempNotice.push(
        new Post(
          tempNotice.length,
          Date.now(),
          "개인정보 처리방침 변경 사전 안내",
          "안녕하세요. \nair2 회원님께 더 나은 서비스를 제공할 수 있도록 개인정보 처리방침 문서를 변경하였음을 알려드립니다. 변경 내용 (v2.2) • 본인인증 추가로 인한 필수 수집 개인정보 항목 추가  • 법정대리인 동의가 필요한 경우 법정대리인 정보수집 항목 추가 • 개인정보 수탁자 표기 수정 (서비스명 → 회사명) • 매입상품 배송 대행 수탁자 변경 : (주)텐바이텐 → 로지포커스(주) • 주문정보 처리 수탁자 추가 : (주)에이치케이넷츠 (EC모니터) PAIR2는 앞으로도 회원님의 개인정보를 보다 안전하게 보호할 것을 약속드립니다. 고맙습니다.",
          false
        )
      );
    }

    this.setState({ notice: tempNotice.reverse() });
  };

  public render() {
    return (
      <div>
        <div className="box-info">
          <p className="p-32 roboto bold">
            {this.state.tab === "notice" ? "공지사항" : "이벤트 당첨안내"}
          </p>
        </div>

        <div className="text-center">
          <div className="w-100 border-bottom-2">
            <div
              onClick={() => this.setState({ tab: "notice" })}
              className={`cursor btn-faq ${
                this.state.tab === "notice" && "btn-faq-active"
              }`}
            >
              공지사항
            </div>
            <div
              onClick={() => this.setState({ tab: "event" })}
              className={`cursor btn-faq ${
                this.state.tab === "event" && "btn-faq-active"
              }`}
            >
              이벤트 당첨안내
            </div>
          </div>
        </div>

        {this.state.tab === "notice" ? (
          <this.noticeComponent />
        ) : (
          <this.eventComponent />
        )}
      </div>
    );
  }

  private noticeComponent = () => {
    return (
      <div className="container mb-5">
        <div className="flex mt-6 box-post box-post-top hidden-m">
          <div className="col-1 p-13 bold gray-2">번호</div>
          <div className="col-10 p-13 bold gray-2">제목</div>
          <div className="col-1 p-13 bold gray-2">등록일</div>
        </div>

        <div className="box-post-top mt-4 mb-2 hidden-p" />

        <div className="hidden-m">
          {this.state.notice.map((post: Post, i: number) => (
            <div key={i}>
              <div
                onClick={() => this.handleOpenNotice(i)}
                className="flex box-post cursor"
              >
                <div className="col-1 p-14 roboto gray-3">{post.index + 1}</div>
                <div className="col-10 p-14 gray-3">{post.title}</div>
                <div className="col-1 p-14 roboto gray-3">
                  {timeFormat2(post.timestamp)}
                </div>
              </div>

              {post.isOpen && (
                <div className="flex">
                  <div className="col-1 box-post-description" />
                  <div className="col-10 box-post-description">
                    <p>{post.description}</p>
                  </div>
                  <div className="col-1 box-post-description" />
                </div>
              )}
            </div>
          ))}
        </div>

        <div className="hidden-p">
          {this.state.notice.map((post: Post, i: number) => (
            <div key={i}>
              <div
                onClick={() => this.handleOpenNotice(i)}
                className="box-post pt-3 pb-3 cursor"
              >
                <p className="p-13 gray-2">{timeFormat2(post.timestamp)}</p>
                <p className="p-14 gray-2">{post.title}</p>
              </div>

              {post.isOpen && (
                <div className="box-post-description pl-3 pr-3">
                  <p>{post.description}</p>
                </div>
              )}
            </div>
          ))}
        </div>

        <button
          onClick={this.handleMoreNotice}
          className="btn-custom-sm mt-5 w-100"
        >
          더보기
        </button>
      </div>
    );
  };

  private eventComponent = () => {
    return (
      <div className="container mb-5">
        <div className="flex mt-6 box-post box-post-top hidden-m">
          <div className="col-1 p-13 bold gray-2">번호</div>
          <div className="col-10 p-13 bold gray-2">제목</div>
          <div className="col-1 p-13 bold gray-2">등록일</div>
        </div>

        <div className="box-post-top mt-3 mb-3 hidden-p" />

        <div className="hidden-m">
          {this.state.events.map((post: Post, i: number) => (
            <div key={i}>
              <div
                onClick={() => this.handleOpenEvent(i)}
                className="flex box-post cursor"
              >
                <div className="col-1 p-14 roboto gray-3">{post.index + 1}</div>
                <div className="col-10 p-14 gray-3">{post.title}</div>
                <div className="col-1 p-14 roboto gray-3">
                  {timeFormat2(post.timestamp)}
                </div>
              </div>

              {post.isOpen && (
                <div className="flex">
                  <div className="col-1 box-post-description" />
                  <div className="col-10 box-post-description">
                    <p>{post.description}</p>
                  </div>
                  <div className="col-1 box-post-description" />
                </div>
              )}
            </div>
          ))}
        </div>

        <div className="hidden-p">
          {this.state.events.map((post: Post, i: number) => (
            <div key={i}>
              <div
                onClick={() => this.handleOpenEvent(i)}
                className="box-post pt-3 pb-3 cursor"
              >
                <p className="p-13 gray-2">{timeFormat2(post.timestamp)}</p>
                <p className="p-14 gray-2">{post.title}</p>
              </div>

              {post.isOpen && (
                <div className="box-post-description pl-3 pr-3">
                  <p>{post.description}</p>
                </div>
              )}
            </div>
          ))}
        </div>

        <button className="btn-custom-sm mt-5 w-100">더보기</button>
      </div>
    );
  };
}

export default Notice;
