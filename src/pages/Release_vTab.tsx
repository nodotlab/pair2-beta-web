import React from "react";
import { timeFormat4 } from "../Method";
import { release } from "../db/Actions";
import { filters } from "../db/Actions";
import { Link } from "react-router-dom";
import Release from "../reduce/Release";
import imgPrev from "../images/Arrow_Prev.svg";
import imgNext from "../images/Arrow_Next.svg";
import imgClose from "../images/GNB_Close.svg";
import imgReset from "../images/reset.svg";
import imgDown from "../images/tab_open.svg";
import imgFOpen from "../images/Filter_Open.svg";
import imgFClose from "../images/Filter_Close.svg";
import MetaData from "../components/MetaGenerator";

interface RelaseState {
  initData: Release[];
  data: Release[];
  upcoming: Release[];
  past: Release[];
  filters: string[];
  otherBrandOpen: boolean;
  filterBrand: string[];
  filterOtherBrand: string[];
  openFilter: boolean;
  selectedTab: string;
}

class ReleasePage extends React.Component<any, RelaseState> {
  constructor(props: any) {
    super(props);
    this.state = {
      initData: [],
      data: [],
      upcoming: [],
      past: [],
      filters: [],
      otherBrandOpen: false,
      filterBrand: [],
      filterOtherBrand: [],
      openFilter: false,
      selectedTab: ""
    };
  }

  public componentDidMount() {
    this.initBrands();
  }

  private async initBrands() {

    const tmpRelease: Release[] = [];
    const _release = await release();

    for (let i = 0; i < _release.length; ++i) {
      tmpRelease.push(
        new Release(
          _release[i].releaseDate,
          _release[i].brand.toUpperCase(),
          _release[i].name,
          _release[i].nameKr,
          _release[i].imageUrl,
          _release[i].serial,
          _release[i].unknownRelease,
          _release[i].type,
          _release[i].index
        ),
      );
    }

    /**
     * 발매일 미정 정렬
     * 각 월중에 발매일 미정인 것을 월의 마지막으로 정렬해야하는데 공수가 오래 걸릴 것 같아 운영 측면으로 풀기로 함
     * 발매일 미정인 스니커즈는 월 말일로 등록
     tmpRelease.sort((a: Release, b: Release) =>  a.unknownRelease === b.unknownRelease ? 0 : a.unknownRelease? 1 : -1);
     */
    
    //현재 날자
     const today = new Date().setHours(0, 0, 0, 0);;
    
    //발매 예정 목록 스테이트 저장
    this.setState({upcoming : tmpRelease.filter( v =>{
      if(v.releaseTimestamp >= today) return v;
    })});

    //발매 완료 목록 스테이트 저장
    this.setState({past : tmpRelease.filter( v =>{
      if(v.releaseTimestamp < today) return v;
    })});

    this.setState({ initData: tmpRelease }, () => this.currentTab());
  }

  private currentTab(): void {
    /**
     * 힌트
     * return this.props.history.push(`/search/${this.state.search}`);
     */
    const currentTab = this.props.history.location.pathname;

    if( currentTab === "/upcoming/" ) this.upcomingTab();
    else if(currentTab === "/launched/") this.pastTab();
    else this.upcomingTab();
  }

  private upcomingTab(): void {
    
    const data = this.state.upcoming;

    //스니커즈 한글명 내림차순
    data.sort((a: Release, b: Release) => {
      return a.nameKr < b.nameKr ? -1 : a.nameKr == b.nameKr ? 0 : 1
    });

    //발매일 기준 내림차순
    data.sort((a: Release, b: Release) => {
      return a.releaseTimestamp - b.releaseTimestamp;
    });

    this.setState({ data: data, selectedTab : "upcoming" });
  }

  private pastTab(): void {
    const data = this.state.past;

     //스니커즈 한글명 내림차순
     data.sort((a: Release, b: Release) => {
      return a.nameKr < b.nameKr ? -1 : a.nameKr == b.nameKr ? 0 : 1
    });

    //발매일 기준 오름차순   
    data.sort((a: Release, b: Release) => {
      return b.releaseTimestamp - a.releaseTimestamp;
    });

    this.setState({ data: data, selectedTab : "past" });
  }

  private dateDisable = (releaseTimestamp: number): boolean => {
    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    if (date.getTime() < releaseTimestamp + 1000) {
      return false;
    } else {
      return true;
    }
  };

  private handleMore = (): void => { };



  public render() {

    const metaData = {
      title: "한정판 신발 스니커즈 발매 일정 런칭캘린더 | PAIR2(페어투)",
      desc: "실시간으로 업데이트되는 나이키, 조던, 아디다스, 뉴발란스, 컨버스 등의 한정판 신발 스니커즈 발매 일정을 미리 확인해 보세요.",
      url: window.location.pathname
    };

    return (
      <div className="ReleasePage">
        <MetaData data={metaData} />
        <div className="box-calendar hidden-m">
          <div className="container">
            <p className="p-21 bold"></p>
          </div>
        </div>
        <div className="bg-gray border-bottom pt-2 hidden-p" />
        <div className="container">
          <div className="row">
            <div className="hidden-p w-100 mt-5" />
            <div className="col-lg-12">
              <div className="calendar-tab">
                <div className="calendar-tab-soon">
                  <Link to="/upcoming/">
                    <p className={`p-16 cursor ${this.state.selectedTab === "upcoming" ? "selected-tab" : "not-selected-tab" }`}>
                        발매 예정 <span className="bold">{this.state.upcoming.length}</span>
                      </p>
                  </Link>
                </div>
                <div>
                  <Link to="/launched/">
                    <p className={`p-16 cursor ${this.state.selectedTab === "past" ? "selected-tab" : "not-selected-tab" }`}>
                      발매 완료 <span className="bold">{this.state.past.length}</span>
                    </p>
                  </Link>
                </div>
              </div>
              <div className="row sneaker-mobile-row">
                {this.state.data.map((data: Release, i: number) => (
                  <Link to={`/release/${data.index}/`} className="col-md-4 col-6 sneaker-mobile mb-4 cursor" key={i}>
                    <div className="img-release-calender">
                      <p className={`release-date ${this.dateDisable(data.releaseTimestamp) && "date-disable"}`}>
                        {data.unknownRelease
                          ? new Date(data.releaseTimestamp).getMonth() +
                          1 +
                          "/-"
                          : timeFormat4(data.releaseTimestamp).toString()}
                      </p>
                      <img className="calendar" src={data.imageUrl} alt={`${data.nameKr} (${data.serial})`} />
                    </div>

                    <p className="p-12 gray-2 bold hidden-m">
                      {data.brand.length > 40 && data.brand.toUpperCase().substr(0, 38) + "..." || data.brand}
                    </p>
                    <p className="p-12 gray-2 bold hidden-p">
                      {data.brand.length > 18 && data.brand.toUpperCase().substr(0, 17) + "..." || data.brand}
                    </p>
                    <p className="sneaker-name p-14 black-2 hidden-m">
                      {data.nameKr}
                    </p>
                    <p className="sneaker-name p-12 black-2 hidden-p">
                      {data.nameKr}
                    </p>
                  </Link>
                ))}
              </div>

              {/* <div className="text-center mt-5 mb-5">
                {this.state.data.length > 29 && (
                  <button
                    onClick={this.handleMore}
                    className="btn btn-custom w-100 gray-3 p-15"
                  >
                    더보기
                  </button>
                )}
              </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ReleasePage;
