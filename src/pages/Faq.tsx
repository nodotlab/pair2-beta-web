import React from "react";
import Faq from "../reduce/Faq";
import kakao from "../images/kakao.png";

interface FaqState {
  data: Faq[];
  isFaq: boolean;
  categories: string[];
  selectCategory: string;
  title: string;
  textCategory: string;
  body: string;
  questionType: string;
  questionTitle: string;
  questionBody: string;
  questionName: string;
  questionPhoneNumber: string;
  questionAlert: boolean;
  email: string;
  agree: boolean;
  questionComplete: boolean;
  files: any[];
}

class FaqPage extends React.Component<any, FaqState> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: [],
      isFaq: false,
      categories: [],
      selectCategory: "",
      title: "",
      textCategory: "",
      body: "",
      questionType: "",
      questionTitle: "",
      questionBody: "",
      questionName: "",
      questionPhoneNumber: "",
      questionAlert: false,
      email: "",
      agree: false,
      questionComplete: false,
      files: [],
    };

  }

  public render() {
    return (
      <div>
        <div className="text-center">
          <div className="w-100 border-bottom-2">
            <div
              className="cursor btn-faq btn-faq-active" >
              1:1 문의하기
            </div>
          </div>

          <div className="mb-5 hidden-m" />
            {<this.ComponentKakao />}
        </div>
      </div>
    );
  }

  private ComponentKakao = () => {
    return (
      <div className="container pt-5">
        <p className="p-24 bold">카카오톡으로 1:1 문의하기</p>
        <p className="p-16">궁금한점은 말씀해주세요😎🙌</p>
        <div className="col-md-4 offset-md-4">
          <div className="box-kakao">
            <p>평일 10:00 ~ 18:00</p>
            <p>점심 12:00 ~ 13:00</p>
            <p>주말 및 공휴일 휴무</p>
          </div>

          <a
            href="https://pf.kakao.com/_xdNFtxb/chat"
            target="_new"
            className="btn-kakao"
          >
            <img src={kakao} alt="" />
            <p>문의하기</p>
          </a>
        </div>
      </div>
    );
  };

}

export default FaqPage;
