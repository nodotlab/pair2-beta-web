import React from "react";
import { withRouter, Link } from "react-router-dom";
import UserVerify from "../components/UserVerify";
import BuyStep from "../components/BuyStep";
import AskStep from "../components/AskStep";
import BuyDetail from "./BuyDetail";
import { currency } from "../Method";
import { getSneakerName, tradeList, getUserVerify } from "../db/Actions";
import img2 from "../images/used_01.jpg";
import imgQuestion from "../images/Tooltip.svg";
import imgClose from "../images/tab_close.svg";
import imgOpen from "../images/tab_open.svg";
import imgThumb from "../images/thumb.png";
import Tooltip from "../components/Tooltip";

class CurrentSneaker {
  public index: string;
  public brand: string;
  public name: string;
  public imgUrl: string;

  constructor(_index: string, _brand: string, _name: string, _imgUrl: string) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.imgUrl = _imgUrl;
  }
}

class Trading {
  public size: string;
  public price: number;
  public bidSize: number;
  public askSize: number;

  constructor(
    _size: string,
    _price: number,
    _bidSize: number,
    _askSize: number
  ) {
    this.size = _size;
    this.price = _price;
    this.bidSize = _bidSize;
    this.askSize = _askSize;
  }
}

class History {
  public size: string;
  public price: number;
  public timestamp: number;

  constructor(_size: string, _price: number, _timestamp: number) {
    this.size = _size;
    this.price = _price;
    this.timestamp = _timestamp;
  }
}

class Data {
  public index: string;
  public from: string;
  public status: string;
  public shippingType: string[];
  public price: number;
  public imgUrl: string;
  public size: string;
  public seller: number;

  constructor(
    _index: string,
    _from: string,
    _status: string,
    _shippingType: string[],
    _price: number,
    _imgUrl: string,
    _size: string,
    _seller: number
  ) {
    this.index = _index;
    this.from = _from;
    this.status = _status;
    this.shippingType = _shippingType;
    this.price = _price;
    this.imgUrl = _imgUrl;
    this.size = _size;
    this.seller = _seller;
  }
}

interface BuyState {
  userVerify: boolean;
  currentSneaker: CurrentSneaker;
  sizes: string[];
  selectSize: string;
  isBuy: boolean;
  initData: Data[];
  data: Data[];
  tradingList: Trading[];
  historyList: History[];
  openSize: boolean;
  openTrade: boolean;
  openHistory: boolean;
  filterType: string;
  filter1: string;
  filter2: string;
  filter3: string;
  screen: string;
  detailIndex: string; // buy list details index (not product index)
  shippingType: string;
  selectPrice: number;
  sellectSeller: number;
  fixedSize: string;
}

class BuyList extends React.Component<any, BuyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      userVerify: true,
      currentSneaker: new CurrentSneaker(
        Date.now().toString(),
        "",
        "",
        imgThumb
      ),
      sizes: this.initSizes(),
      selectSize: "",
      isBuy: true,
      initData: [],
      data: [],
      tradingList: [],
      historyList: [],
      openSize: true,
      openTrade: false,
      openHistory: false,
      filterType: "등록순",
      filter1: "",
      filter2: "",
      filter3: "",
      screen: this.props.match.params.sale ? "detail" : "list",
      detailIndex: this.props.match.params.sale
        ? this.props.match.params.sale
        : "init",
      shippingType: "직거래",
      selectPrice: 0,
      sellectSeller: 0,
      fixedSize: "",
    };
  }

  public componentDidMount() {
    if (!localStorage.getItem("pair2token"))
      this.props.history.push(`/login/buy/${this.props.match.params.id}`);

    this.setState({
      selectSize: this.props.match.params.size,
    });

    this.userVerify();
    this.initData();
  }

  private async userVerify() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getUserVerify(token);
      if (result !== true) this.setState({ userVerify: false });
    } catch (error) {}
  }

  private sortTradingList(size: string): void {
    const tradingInitList: Trading[] = this.getTradingList();
    let tradingList: Trading[] = [];
    const askList: Trading[] = [];
    const bidList: Trading[] = [];

    for (let i = 0; i < tradingInitList.length; ++i) {
      if (size === tradingInitList[i].size) {
        tradingList.push(tradingInitList[i]);
      }
    }

    for (let i = 0; i < tradingList.length; ++i) {
      if (tradingList[i].bidSize === 0) {
        askList.push(tradingList[i]);
      } else {
        bidList.push(tradingList[i]);
      }
    }

    bidList.sort((a, b) => {
      if (a.price > b.price) {
        return -1;
      }
      if (a.price < b.price) {
        return 1;
      }
      return 0;
    });

    askList.sort((a, b) => {
      if (a.price > b.price) {
        return 1;
      }
      if (a.price < b.price) {
        return -1;
      }
      return 0;
    });

    tradingList = [];

    if (bidList.length < 3) {
      for (let i = 0; i < bidList.length; ++i) {
        tradingList.push(bidList[i]);
      }
    } else {
      for (let i = 0; i < 3; ++i) {
        tradingList.push(bidList[i]);
      }
    }
    tradingList.reverse();

    if (askList.length < 3) {
      for (let i = 0; i < askList.length; ++i) {
        tradingList.push(askList[i]);
      }
    } else {
      for (let i = 0; i < 3; ++i) {
        tradingList.push(askList[i]);
      }
    }

    this.setState({ tradingList: tradingList });
  }

  private getTradingList(): Trading[] {
    const getTradingList: Trading[] = [];
    // getTradingList.push(new Trading("250", 350000, 1, 0));
    return getTradingList;
  } // init

  private getHistoryList(size: string): void {
    const arr: History[] = [];

    for (let i = 0; i < 6; ++i) {
      arr.push(new History(size, 300000, Date.now()));
    }

    this.setState({ historyList: arr });
  }

  private initSizes(): string[] {
    const tempSizes = [];
    tempSizes.push("All");
    for (let i = 220; i < 315; i += 5) {
      tempSizes.push(i.toString());
    }

    return tempSizes;
  }

  private initData = async () => {
    const { result }: any = await getSneakerName(this.props.match.params.id);
    const sneaker: CurrentSneaker = new CurrentSneaker(
      result.index,
      result.brand,
      result.name,
      result.thumbUrl
    );
    this.setState({ currentSneaker: sneaker });
    let trade = await tradeList(result.index);

    // trade = trade.filter((trade: any) => {
    //   const laterDay: number = (86400000 * 15) + trade.index;
    //   return Date.now() < laterDay;
    // });

    const tempData: Data[] = this.state.initData;

    for (let i in trade) {
      let shippingType: string[] = [];
      if (trade[i].shippingType === "직거래") {
        shippingType = ["직거래"];
      } else if (trade[i].shippingType === "택배") {
        shippingType = ["택배거래"];
      } else {
        shippingType = ["직거래", "택배거래"];
      }

      let condition: string = "Perfect";
      if (
        trade[i].damage1 ||
        trade[i].damage2 ||
        trade[i].damage3 ||
        trade[i].damage4
      )
        condition = "Defect1";
      if (condition === "Defect1") {
        if (trade[i].status1 || trade[i].status2 || trade[i].status3)
          condition = "Defect3";
      } else {
        if (trade[i].status1 || trade[i].status2 || trade[i].status3)
          condition = "Defect2";
      }

      tempData.push(
        new Data(
          trade[i].index,
          trade[i].from,
          condition,
          shippingType,
          trade[i].price,
          trade[i].imageUrls[0],
          trade[i].size,
          trade[i].seller
        )
      );
    }

    // this.setState({ initData: tempData }, () => this.filterData()); // origin
    this.setState(
      {
        initData: tempData,
        filter1: "전체",
        filter2: "전체",
        filter3: "전체",
      },
      () => this.filterData()
    ); // edit
  };

  private handleSize = (size: string): void => {
    this.setState(
      {
        selectSize: size,
        openTrade: true,
        openHistory: true,
        // openSize: false,
      },
      () => {
        this.filterData();
        this.sortTradingList(size);
        this.getHistoryList(size);
      }
    );
    // console.log(this.state.tradingList);
  };

  private handleFilter = (filter: string, name: string): void => {
    this.setState({ [name]: filter } as any, () => this.filterData());
  };

  private filterData(): void {
    const tempData: Data[] = [];
    const tempData2: Data[] = [];
    const tempData3: Data[] = [];
    let tmepData4: Data[] = [];

    for (let i = 0; i < this.state.initData.length; ++i) {
      if (this.state.filter1 === this.state.initData[i].from) {
        tempData.push(this.state.initData[i]);
      }
      if (this.state.filter1 === "전체") {
        tempData.push(this.state.initData[i]);
      }
    }

    for (let i = 0; i < tempData.length; ++i) {
      if (this.state.filter2 === tempData[i].status) {
        tempData2.push(tempData[i]);
      }
      if (this.state.filter2 === "전체") {
        tempData2.push(tempData[i]);
      }
    }

    for (let i = 0; i < tempData2.length; ++i) {
      if (
        this.state.filter3 === tempData2[i].shippingType[0] ||
        this.state.filter3 === tempData2[i].shippingType[1]
      ) {
        tempData3.push(tempData2[i]);
      }
      if (this.state.filter3 === "전체") {
        tempData3.push(tempData2[i]);
      }
    }

    if (this.state.selectSize !== "All") {
      for (let i = 0; i < tempData3.length; ++i) {
        if (this.state.selectSize === tempData3[i].size) {
          tmepData4.push(tempData3[i]);
        }
      }
    } else {
      tmepData4 = tempData3;
    }

    this.setState({ data: tmepData4 });
  }

  private handleFilterType = (): void => {
    if (this.state.filterType === "등록순") {
      this.setState({ filterType: "최신 발매일순" });
    } else {
      this.setState({ filterType: "등록순" });
    }
  };

  private handleUserVerify = (): void => this.setState({ userVerify: true });

  private handleDetailCancel = (): void => {
    this.setState({
      detailIndex: "",
      screen: "list",
    });
  };

  private handleStepNext = (): void => {
    this.setState({ screen: "step" });
    window.scroll(0, 0);
  };

  private handleToDetail = (): void => this.setState({ screen: "detail" });

  private handleShipping = (type: string): void =>
    this.setState({ shippingType: type });

  private handleSizeUpdate = (size: string) =>
    this.setState({ fixedSize: size });

  public render() {
    return (
      <>
        {!this.state.userVerify && (
          <UserVerify handleUserVerify={this.handleUserVerify} />
        )}
        {this.state.userVerify && <this.ComponentMain />}
      </>
    );
  }

  private ComponentMain = () => {
    return (
      <div className="row no-gutters">
        <this.ComponentLeft />

        {this.state.screen === "list" && this.state.isBuy && (
          <this.CompoentList />
        )}

        {this.state.screen === "detail" && this.state.isBuy && (
          <BuyDetail
            index={this.state.detailIndex}
            shippingType={this.state.shippingType}
            handleShipping={this.handleShipping}
            handleCancle={this.handleDetailCancel}
            handleGo={this.handleStepNext}
            handleSizeUpdate={this.handleSizeUpdate}
          />
        )}

        {this.state.screen === "step" && this.state.isBuy && (
          <div className="col-md-10">
            <div className="box-buy-right">
              <this.ComponenttBuyOrAsk />
              <BuyStep
                index={this.state.detailIndex}
                shippingType={this.state.shippingType}
                handleToDetail={this.handleToDetail}
                price={this.state.selectPrice}
                seller={this.state.sellectSeller}
                size={this.state.fixedSize}
                name={this.state.currentSneaker.name}
                brand={this.state.currentSneaker.brand}
              />
            </div>
          </div>
        )}

        {!this.state.isBuy && (
          <div className="col-md-10">
            <div className="box-buy-right">
              <this.ComponenttBuyOrAsk />
              <AskStep />
            </div>
          </div>
        )}
      </div>
    );
  };

  private ComponenttBuyOrAsk = () => {
    return (
      <div className="box-buy-hr">
        <button
          onClick={() => this.setState({ isBuy: true })}
          className={`btn buy-tab ${this.state.isBuy && "buy-tab-active"}`}
        >
          구매
        </button>
        {/* <button onClick={() => this.setState({ isBuy: false })}
        className={`btn buy-tab ${!this.state.isBuy && "buy-tab-active"}`}>구매요청</button> */}
      </div>
    );
  };

  private ComponentLeft = () => {
    return (
      <div
        className={`col-md-2 ${this.state.screen === "step" && "hidden-m"} ${
          !this.state.isBuy && "hidden-m"
        }`}
      >
        <div className="box-buy-left">
          <div className="row">
            <div className="col-4 pr-0">
              <div
                className="img-buy-left"
                style={{
                  backgroundImage: `url(${this.state.currentSneaker.imgUrl})`,
                }}
              />
            </div>
            <div className="col-8">
              <p className="p-14 bold roboto gray-2">
                {this.state.currentSneaker.brand}
              </p>
              <p className="p-16 roboto black-2 mt-1">
                {this.state.currentSneaker.name}
              </p>
              <Link to={`/sneaker/${this.state.currentSneaker.name}`}>
                <button className="btn btn-custom btn-xs mt-2">상세보기</button>
              </Link>
            </div>
          </div>

          {this.state.selectSize === "" || this.state.selectSize === "All" ? (
            <div className="row mt-5">
              <div className="col-8">
                <p className="p-16 bold black-2">사이즈를 선택해주세요</p>
              </div>
              <div className="col-4 text-right">
                {/* <p className="p-13 underline gray-2 pt-1">사이즈 안내</p> */}
              </div>
            </div>
          ) : (
            <div
              onClick={() => this.setState({ openSize: !this.state.openSize })}
              className="row mt-5 cursor"
            >
              <div className="col-10">
                <p className="p-16 bold black-2">
                  사이즈{" "}
                  <span className="p-18 roboto bold orange ml-2">
                    {this.state.selectSize}
                  </span>
                </p>
              </div>
              <div className="col-2 text-right">
                {this.state.openSize ? (
                  <img src={imgOpen} alt="" className="" />
                ) : (
                  <img src={imgClose} alt="" className="" />
                )}
              </div>
            </div>
          )}

          {this.state.openSize && (
            <div className="mt-3">
              {this.state.sizes.map((size: string, i: number) => (
                <button
                  key={i}
                  onClick={() => this.handleSize(size)}
                  className={`btn-filter btn-filter-m ${
                    size === this.state.selectSize && "btn-filter-active"
                  }`}
                >
                  {size}
                </button>
              ))}
            </div>
          )}
          {this.state.selectSize !== "" && !this.state.openSize && <hr />}

          <div className="hidden-m mt-5">
            <Link to={`/sneaker/${this.state.currentSneaker.name}`}>
              <button className="btn btn-second w-100">취소</button>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  private CompoentList = () => {
    return (
      <div className="col-md-10">
        <div className="box-buy-right">
          <this.ComponenttBuyOrAsk />

          <div className="container">
            <div className="box-buy-border hidden-m">
              <div className="buy-inline border-buy">
                <button className="btn btn-inline bold">출처</button>
                <button
                  onClick={() => this.handleFilter("전체", "filter1")}
                  className={`btn btn-inline2 ml-5 ${
                    this.state.filter1 === "전체" && "btn-inline2-active"
                  }`}
                >
                  전체
                </button>
                <button
                  onClick={() => this.handleFilter("국내", "filter1")}
                  className={`btn btn-inline2 ${
                    this.state.filter1 === "국내" && "btn-inline2-active"
                  }`}
                >
                  국내
                </button>
                <button
                  onClick={() => this.handleFilter("해외", "filter1")}
                  className={`btn btn-inline2 ${
                    this.state.filter1 === "해외" && "btn-inline2-active"
                  }`}
                >
                  해외
                </button>
                <button
                  onClick={() => this.handleFilter("알수없음", "filter1")}
                  className={`btn btn-inline2 ${
                    this.state.filter1 === "알수없음" && "btn-inline2-active"
                  }`}
                >
                  알수없음
                </button>
              </div>

              <div className="buy-inline border-buy">
                <button className="btn btn-inline">
                  제품상태{" "}
                  <span>
                    <img
                      src={imgQuestion}
                      data-tip
                      data-for="tooltip1"
                      className="img-question"
                    />
                  </span>
                </button>
                <button
                  onClick={() => this.handleFilter("전체", "filter2")}
                  className={`btn btn-inline2 ml-5 ${
                    this.state.filter2 === "전체" && "btn-inline2-active"
                  }`}
                >
                  전체
                </button>
                <button
                  onClick={() => this.handleFilter("Perfect", "filter2")}
                  className={`btn btn-inline2 ${
                    this.state.filter2 === "Perfect" && "btn-inline2-active"
                  }`}
                >
                  Perfect
                </button>
                <button
                  onClick={() => this.handleFilter("Defect1", "filter2")}
                  className={`btn btn-inline2 ${
                    this.state.filter2 === "Defect1" && "btn-inline2-active"
                  }`}
                >
                  Defect1
                </button>
                <button
                  onClick={() => this.handleFilter("Defect2", "filter2")}
                  className={`btn btn-inline2 ${
                    this.state.filter2 === "Defect2" && "btn-inline2-active"
                  }`}
                >
                  Defect2
                </button>
                <button
                  onClick={() => this.handleFilter("Defect3", "filter2")}
                  className={`btn btn-inline2 ${
                    this.state.filter2 === "Defect3" && "btn-inline2-active"
                  }`}
                >
                  Defect3
                </button>
              </div>

              <div className="buy-inline">
                <button className="btn btn-inline bold">거래방식</button>
                <button
                  onClick={() => this.handleFilter("전체", "filter3")}
                  className={`btn btn-inline2 ml-5 ${
                    this.state.filter3 === "전체" && "btn-inline2-active"
                  }`}
                >
                  전체
                </button>
                <button
                  onClick={() => this.handleFilter("직거래", "filter3")}
                  className={`btn btn-inline2 ${
                    this.state.filter3 === "직거래" && "btn-inline2-active"
                  }`}
                >
                  직거래
                </button>
                <button
                  onClick={() => this.handleFilter("택배거래", "filter3")}
                  className={`btn btn-inline2 ${
                    this.state.filter3 === "택배거래" && "btn-inline2-active"
                  }`}
                >
                  택배거래
                </button>
              </div>
            </div>

            <div className="box-buy-border hidden-p">
              <div className="buy-inline border-buy">
                <div className="row">
                  <div className="col-3 pl-0 pr-0">
                    <button className="btn btn-inline bold">출처</button>
                  </div>
                  <div className="col-9 pt-2">
                    <button
                      onClick={() => this.handleFilter("전체", "filter1")}
                      className={`btn btn-inline2 ml-5 ${
                        this.state.filter1 === "전체" && "btn-inline2-active"
                      }`}
                    >
                      전체
                    </button>
                    <button
                      onClick={() => this.handleFilter("국내", "filter1")}
                      className={`btn btn-inline2 ${
                        this.state.filter1 === "국내" && "btn-inline2-active"
                      }`}
                    >
                      국내
                    </button>
                    <button
                      onClick={() => this.handleFilter("해외", "filter1")}
                      className={`btn btn-inline2 ${
                        this.state.filter1 === "해외" && "btn-inline2-active"
                      }`}
                    >
                      해외
                    </button>
                    <button
                      onClick={() => this.handleFilter("알수없음", "filter1")}
                      className={`btn btn-inline2 ${
                        this.state.filter1 === "알수없음" &&
                        "btn-inline2-active"
                      }`}
                    >
                      알수없음
                    </button>
                  </div>
                </div>
              </div>

              <div className="buy-inline border-buy">
                <div className="row">
                  <div className="col-3 pl-0 pr-0 pt-2">
                    <button className="btn btn-inline">
                      제품상태{" "}
                      <span>
                        <img
                          src={imgQuestion}
                          data-tip
                          data-for="tooltip1"
                          className="img-question"
                        />
                      </span>
                    </button>
                  </div>
                  <div className="col-9 pt-2 pb-2">
                    <button
                      onClick={() => this.handleFilter("전체", "filter2")}
                      className={`btn btn-inline2 ml-5 ${
                        this.state.filter2 === "전체" && "btn-inline2-active"
                      }`}
                    >
                      전체
                    </button>
                    <button
                      onClick={() => this.handleFilter("Perfect", "filter2")}
                      className={`btn btn-inline2 ${
                        this.state.filter2 === "Perfect" && "btn-inline2-active"
                      }`}
                    >
                      Perfect
                    </button>
                    <button
                      onClick={() => this.handleFilter("Defect1", "filter2")}
                      className={`btn btn-inline2 ${
                        this.state.filter2 === "Defect1" && "btn-inline2-active"
                      }`}
                    >
                      Defect1
                    </button>
                    <button
                      onClick={() => this.handleFilter("Defect2", "filter2")}
                      className={`btn btn-inline2 ${
                        this.state.filter2 === "Defect2" && "btn-inline2-active"
                      }`}
                    >
                      Defect2
                    </button>
                    <button
                      onClick={() => this.handleFilter("Defect3", "filter2")}
                      className={`btn btn-inline2 ${
                        this.state.filter2 === "Defect3" && "btn-inline2-active"
                      }`}
                    >
                      Defect3
                    </button>
                  </div>
                </div>
              </div>

              <div className="buy-inline">
                <div className="row">
                  <div className="col-3 pl-0 pr-0">
                    <button className="btn btn-inline bold">거래방식</button>
                  </div>
                  <div className="col-9 pt-2">
                    <button
                      onClick={() => this.handleFilter("전체", "filter3")}
                      className={`btn btn-inline2 ml-5 ${
                        this.state.filter3 === "전체" && "btn-inline2-active"
                      }`}
                    >
                      전체
                    </button>
                    <button
                      onClick={() => this.handleFilter("직거래", "filter3")}
                      className={`btn btn-inline2 ${
                        this.state.filter3 === "직거래" && "btn-inline2-active"
                      }`}
                    >
                      직거래
                    </button>
                    <button
                      onClick={() => this.handleFilter("택배거래", "filter3")}
                      className={`btn btn-inline2 ${
                        this.state.filter3 === "택배거래" &&
                        "btn-inline2-active"
                      }`}
                    >
                      택배거래
                    </button>
                  </div>
                </div>
              </div>
            </div>

            {this.state.data.length === 0 && (
              <div className="text-center mt-5">
                <p className="p-14 gray-3">검색결과가 없습니다.</p>
              </div>
            )}

            {this.state.data.length > 0 && (
              <div className="row mt-5 sneaker-mobile-row buy-list-m">
                <div className="col-6 mb-3 pl-1">
                  <p className="p-14 black-2">
                    총 <b>{this.state.data.length}</b>건
                  </p>
                </div>
                <div className="col-6 text-right mb-3 pr-1">
                  <p
                    onClick={this.handleFilterType}
                    className="p-14 black-2 cursor"
                  >
                    {this.state.filterType} <img src={imgOpen} />
                  </p>
                </div>

                {this.state.data.map((data: Data, i: number) => (
                  <div
                    key={i}
                    onClick={() => {
                      this.setState({
                        detailIndex: data.index,
                        screen: "detail",
                        selectPrice: data.price,
                        shippingType: data.shippingType[0],
                        sellectSeller: data.seller,
                      });
                      window.scroll(0, 0);
                    }}
                    className="col-md-4 mb-5 col-6 box-buy-mobile cursor"
                  >
                    <div
                      className="img-buy-box"
                      style={{ backgroundImage: `url(${data.imgUrl})` }}
                    />
                    <p className="p-12 gray-2 mt-2 buy-tags">
                      {data.size} <span className="gray-7 m-1">|</span>
                      {data.from} <span className="gray-7 m-1">|</span>
                      {data.status} <span className="gray-7 m-1">|</span>
                      {data.shippingType[0]}{" "}
                      <span className="gray-7 m-1">|</span>
                      {data.shippingType[1] && data.shippingType[1]}
                    </p>
                    <p className="p-18 orange roboto bold text-right">
                      {currency(data.price)}
                      <span className="p-14 normal noto">원</span>
                    </p>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
        <Tooltip />
      </div>
    );
  };
}

export default withRouter(BuyList);
