import React from "react";
import complete from "../images/Completed.svg";
import { Link } from "react-router-dom";

class BuyComplete extends React.Component<any> {
  public render() {
    return (
      <div className="container">
        <div className="mt-180 mb-180">
          <div className="text-center">
            <img src={complete} alt="" />
          </div>

          <p className="text-center black-2 p-22 mt-3">
            거래요청이 완료되었습니다.
          </p>
          <p className="text-center p-14 gray-2 mt-2">
            판매자가 거래요청을 수락하면 등록하신 카드로 자동결제가
            이루어집니다.
            <br />
          </p>

          <div className="text-center mt-4 hidden-m">
            <Link to="/">
              <button className="btn-custom-sm w-160 mr-1">홈 바로가기</button>
            </Link>
            <Link to="/mypage/buys">
              <button className="btn-custom-sm w-160">
                구매내역 으로 이동
              </button>
            </Link>
          </div>

          <div className="row mt-4 hidden-p">
            <div className="col-6 pr-1">
              <Link to="/">
                <button
                  onClick={() => this.props.history.push("/")}
                  className="btn-custom w-100"
                >
                  홈 바로가기
                </button>
              </Link>
            </div>
            <div className="col-6 pl-1">
              <Link to="/mypage/buys">
                <button className="btn-custom-active w-100">
                  구매내역 확인
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BuyComplete;
