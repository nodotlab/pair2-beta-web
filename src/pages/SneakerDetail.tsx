import React from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";
import * as Chart from "chart.js";
import Sneaker from "../reduce/SneakerDetail";
import {
  getSneakerName,
  putLike,
  existLike,
  sneakerPricelist,
} from "../db/Actions";
import RecommendSneaker from "../reduce/RecommendSneaker";
import Modal from "../components/ModalSmall";
import ModalScroll from "../components/ModalScroll";
import {
  currency,
  timeFormat2,
  timeFormat5,
  timeFormat8,
  timeFormat9,
} from "../Method";
import thumb from "../images/thumb.png";
import videoSneaker from "../images/Product_Detail_07video.png";
import imgPlus from "../images/Icon_Plus_gray.svg";
import imgWrap from "../images/Product_Detail_Summary_BG.png";
import imgClose from "../images/Popup_Delete.svg";
import imgCheck from "../images/stroke-1.svg";
import { env } from "../reduce/Env";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

class Size {
  public size: string;
  public active: boolean;
  constructor(_size: string, _active: boolean) {
    this.size = _size;
    this.active = _active;
  }
}

class Trading {
  public index: string;
  public size: string;
  public price: number;
  public bidSize: number;
  public askSize: number;

  constructor(
    _index: string,
    _size: string,
    _price: number,
    _bidSize: number,
    _askSize: number
  ) {
    this.index = _index;
    this.size = _size;
    this.price = _price;
    this.bidSize = _bidSize;
    this.askSize = _askSize;
  }
}

class History {
  public size: string;
  public price: number;
  public tradeTime: number;

  constructor(_size: string, _price: number, _tradeTime: number) {
    this.size = _size;
    this.price = _price;
    this.tradeTime = _tradeTime;
  }
}

interface SneakerDetailState {
  sneaker: Sneaker;
  dollar: boolean;
  sneakerSelectImage: string;
  like: boolean;
  sizes: Size[];
  tradingInitList: Trading[];
  tradingList: Trading[];
  historys: History[];
  lowPrice: number;
  highPirce: number;
  tradingCount: number;
  tradingLowPrice: number;
  recommendSneakers: RecommendSneaker[];
  mouseX: number;
  mouseY: number;
  selectSize: string;
  modal: string;
  existLike: boolean;
  modalTab: string;
}

class SneakerDetail extends React.Component<any, SneakerDetailState> {
  constructor(props: any) {
    super(props);
    this.state = {
      sneaker: new Sneaker(
        "",
        "",
        "",
        "",
        0,
        thumb,
        thumb,
        thumb,
        thumb,
        thumb,
        videoSneaker,
        0,
        0,
        ""
      ),
      dollar: false,
      sneakerSelectImage: thumb,
      like: false,
      sizes: this.initSizes(),
      tradingInitList: [],
      tradingList: [],
      historys: [],
      lowPrice: 0,
      highPirce: 0,
      tradingCount: 0,
      tradingLowPrice: 0,
      recommendSneakers: [new RecommendSneaker("0", "", "", "", 0, thumb)],
      mouseX: 0,
      mouseY: 0,
      selectSize: "All",
      modal: "none",
      existLike: false,
      modalTab: "판매중",
    };
  }

  public componentWillMount() {
    Chart.pluginService.register({
      afterDraw: (chart: any, easing: any) => {
        if (chart.tooltip._active && chart.tooltip._active.length) {
          const activePoint = chart.controller.tooltip._active[0];
          const ctx = chart.ctx;
          const x = activePoint.tooltipPosition().x;
          const topY = chart.scales["y-axis-0"].top;
          const bottomY = chart.scales["y-axis-0"].bottom;
          ctx.save();
          ctx.beginPath();
          ctx.moveTo(x, topY);
          ctx.lineTo(x, bottomY);
          ctx.lineWidth = 2;
          ctx.strokeStyle = "rgba(255, 142, 32, 0.4)";
          ctx.stroke();
          ctx.restore();
        }
      },
    });
  }

  public componentDidMount() {
    this.initSneakerInformation();
    this.initLike();
  }

  private initSizes(): Size[] {
    const tempSizes = [];
    tempSizes.push(new Size("All", true));

    for (let i = 220; i < 315; i += 5) {
      tempSizes.push(new Size(i.toString(), false));
    }

    return tempSizes;
  }

  private async initSneakerInformation() {
    const { result, recommend } = await getSneakerName(
      this.props.match.params.id
    );
    this.getTradingList(result.index);

    const sneaker = new Sneaker(
      result.brand,
      result.name,
      result.nameKr,
      "250",
      0,
      result.imageUrl1 ? result.imageUrl1 : thumb,
      result.imageUrl2 ? result.imageUrl2 : thumb,
      result.imageUrl3 ? result.imageUrl3 : thumb,
      result.imageUrl4 ? result.imageUrl4 : thumb,
      result.imageUrl5 ? result.imageUrl5 : thumb,
      result.vedioUrl ? result.vedioUrl : "",
      result.releaseDate,
      result.releasePrice,
      result.serial,
      result.thumbUrl
    );

    this.setState({
      sneaker: sneaker,
      dollar: result.dollar ? true : false,
      sneakerSelectImage: result.imageUrl1,
    });

    const sneakers: RecommendSneaker[] = [];
    for (let i = 0; i < recommend.length; ++i) {
      sneakers.push(
        new RecommendSneaker(
          recommend[i].index,
          recommend[i].brand,
          recommend[i].name,
          "",
          isNaN(recommend[i].lowPrice) ? 0 : recommend[i].lowPrice,
          recommend[i].thumbUrl ? recommend[i].thumbUrl : thumb
        )
      );
    }

    this.setState({
      recommendSneakers: sneakers,
    });
  }

  private async initLike() {
    const token = localStorage.getItem("pair2token");
    if (token) {
      const exist = await existLike(token, this.props.match.params.id);
      this.setState({ existLike: exist === "true" ? true : false });
    }
  }

  private async getTradingList(sneakerIndex: number) {
    const result = await sneakerPricelist(sneakerIndex);

    const arr: Trading[] = [];
    for (const i in result) {
      if (!result[i].complete) {
        arr.push(new Trading("", result[i].size, result[i].price, 0, 0));
      }
    }

    arr.sort((a: any, b: any) => {
      if (a.price < b.price) {
        return -1;
      }
      if (a.price > b.price) {
        return 1;
      }
      return 0;
    });

    this.setState({
      tradingCount: arr.length,
      tradingLowPrice: arr.length === 0 ? 0 : arr[0].price,
    });

    const arr2: Trading[] = [];
    for (const i in arr) {
      let count = 0;
      const price = arr[i].price;
      const size = arr[i].size;

      for (const k in arr) {
        if (price === arr[k].price && size === arr[k].size) ++count;
      }

      arr2.push(new Trading("", arr[i].size, arr[i].price, 0, count));
    }

    const arr3: Trading[] = [];
    for (const i in arr2) {
      let exist: boolean = false;

      for (const k in arr3) {
        if (arr2[i].size === arr3[k].size && arr2[i].price === arr3[k].price) {
          exist = true;
        }
      }

      if (!exist)
        arr3.push(
          new Trading("", arr2[i].size, arr2[i].price, 0, arr2[i].askSize)
        );
    }

    arr3.sort((a: any, b: any) => {
      if (a.price < b.price) {
        return -1;
      }
      if (a.price > b.price) {
        return 1;
      }
      return 0;
    });

    this.setState({ tradingInitList: arr3 }, () => {
      this.setState({ tradingList: this.sortTradingList("All") });
    });

    const tempHistorys: History[] = [];
    for (const i in result) {
      if (result[i].complete) {
        tempHistorys.push(
          new History(
            result[i].size,
            result[i].price,
            result[i].completeTimestamp
          )
        );
      }
    }

    tempHistorys.sort((a: any, b: any) => {
      if (a.price > b.price) {
        return -1;
      }
      if (a.price < b.price) {
        return 1;
      }
      return 0;
    });

    this.setState({
      lowPrice:
        tempHistorys.length === 0
          ? 0
          : tempHistorys[tempHistorys.length - 1].price,
      highPirce: tempHistorys.length === 0 ? 0 : tempHistorys[0].price,
    });

    tempHistorys.sort((a: any, b: any) => {
      if (a.tradeTime < b.tradeTime) {
        return -1;
      }
      if (a.tradeTime > b.tradeTime) {
        return 1;
      }
      return 0;
    });

    this.setState({ historys: tempHistorys });
  }

  private sortTradingList(size: string): Trading[] {
    const askList: Trading[] = [];

    for (let i = 0; i < this.state.tradingInitList.length; ++i) {
      if (size === "All") {
        askList.push(this.state.tradingInitList[i]);
      }
      if (size === this.state.tradingInitList[i].size) {
        askList.push(this.state.tradingInitList[i]);
      }
    }

    askList.sort((a: any, b: any) => {
      if (a.price < b.price) {
        return -1;
      }
      if (a.price > b.price) {
        return 1;
      }
      return 0;
    });

    return askList.slice(0, 6);
  }

  private handleSelectImage = (imageUrl: string): void => {
    this.setState({ sneakerSelectImage: imageUrl });
  };

  private handleSizeActive = (size: string): void => {
    const tempSizes: Size[] = this.state.sizes;
    const tradingList: Trading[] = this.sortTradingList(size);

    for (let i = 0; i < tempSizes.length; ++i) {
      tempSizes[i].active = false;
      if (size === tempSizes[i].size) {
        tempSizes[i].active = true;
      }
    }

    if (size === "All") {
      tempSizes[0].active = true;
    } else {
      tempSizes[0].active = false;
    }

    this.setState({
      sizes: tempSizes,
      tradingList: tradingList,
      selectSize: size,
    });
  };

  private chartData = (canvas: any) => {
    const labelList: string[] = [];
    const priceList: number[] = [];
    const colorList: string[] = [];

    for (let i = 0; i < this.state.historys.length; ++i) {
      priceList.push(this.state.historys[i].price / 1000);
      labelList.push(timeFormat8(this.state.historys[i].tradeTime));
      colorList.push("rgb(255, 142, 32, 0)");
    }

    const ctx = canvas.getContext("2d");
    const gradient: any = ctx.createLinearGradient(0, 0, 0, 360);
    gradient.addColorStop(0, "rgba(255, 142, 32, 0.4)");
    gradient.addColorStop(1, "rgba(255, 211, 168, 0.1)");

    const data: any = {
      labels: labelList,
      datasets: [
        {
          data: priceList,
          lineTension: 0,
          backgroundColor: gradient,
          borderColor: ["rgb(255, 142, 32)"],
          pointBackgroundColor: colorList,
          pointBorderColor: colorList,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgb(255, 142, 32)",
          borderWidth: 1,
        },
      ],
    };

    return data;
  };

  private chartOption() {
    const tempHistorys: number[] = [];

    for (let i = 0; i < this.state.historys.length; ++i) {
      tempHistorys.push(this.state.historys[i].price / 1000);
    }

    let max: number = 0;
    let min: number = tempHistorys[0];

    for (let i = 0; i < tempHistorys.length; ++i) {
      if (tempHistorys[i] > max) {
        max = tempHistorys[i];
      }
      if (tempHistorys[i] < min) {
        min = tempHistorys[i];
      }
    }

    const options: any = {
      legend: {
        display: false,
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            ticks: {
              suggestedMin: min,
              suggestedMax: max,
              userCallback: function (label: any, index: any, labels: any) {
                return label;
              },
              stepSize: 100,
              fontSize: 14,
              fontColor: "#767676",
            },
          },
        ],
        xAxes: [
          {
            ticks: {
              fontSize: 14,
              fontColor: "#767676",
              maxTicksLimit: 7,
            },
            gridLines: {
              borderDash: [4, 2],
            },
          },
        ],
      },
      tooltips: {
        mode: "index",
        scaleID: "y-axis-0",
        intersect: false,
        xPadding: 20,
        yPadding: 10,
        titleFontFamily: "Montserrat",
        titleFontSize: 14,
        bodyFontSize: 13,
        bodySpacing: 5,
        displayColors: false,
        callbacks: {
          title: (tooltipItem: any, data: any) => {
            return timeFormat9(
              this.state.historys[tooltipItem[0].index].tradeTime
            );
          },
          label: (tooltipItem: any, data: any) => {
            return (
              "거래가 : " +
              currency(this.state.historys[tooltipItem.index].price)
            );
          },
        },
      },
      hover: {
        mode: "index",
        intersect: false,
      },
    };

    return options;
  }

  private handleLike = async () => {
    if (this.state.selectSize === "All") {
      alert("사이즈를 선택해주세요.");
    } else if (localStorage.getItem("pair2signin") !== "true") {
      alert("로그인을 해해주세요.");
    } else {
      const token = localStorage.getItem("pair2token");
      const result = await putLike({
        token: token,
        data: {
          brand: this.state.sneaker.brand,
          size: this.state.selectSize,
          name: this.state.sneaker.name,
          thumbUrl: this.state.sneaker.thumbUrl,
        },
      });

      this.setState({ modal: "none" });

      if (result === "success") {
        this.initLike();
        alert(
          `${this.state.sneaker.nameKr} 사이즈 ${this.state.selectSize} 를 추가했습니다.`
        );
      } else {
        alert(
          `${this.state.sneaker.nameKr} 사이즈 ${this.state.selectSize} 가 이미 관심제품에 있습니다.`
        );
      }
    }
  };

  public render() {
    return (
      <div>
        <div className="box-detail-first-sneaker">
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <p className="p-18 roboto bold black-2 o-60 letter-space-4 mb-2 hidden-m">
                  {this.state.sneaker.brand.toUpperCase()}
                </p>
                <p className="p-40 roboto black-2 line-48 hidden-m">
                  {this.state.sneaker.name.toUpperCase()}
                </p>
                <p className="p-13 roboto bold black-2 o-60 letter-space-3 mb-2 pl-3 hidden-p">
                  {this.state.sneaker.brand.toUpperCase()}
                </p>
                <p className="p-24 roboto black-2 hidden-p pl-3 pr-3 line-31 hidden-p">
                  {this.state.sneaker.name.toUpperCase()}
                </p>
                <p className="p-13 bold roboto orange mt-2 pl-3 pr-3 hidden-p">
                  {this.state.sneaker.nameKr}
                </p>
                <button
                  onClick={() => this.setState({ modal: "like" })}
                  className="btn-custom-sm mt-4 hidden-p ml-3"
                >
                  관심제품{" "}
                  <img
                    className="detail-plus ml-1"
                    src={this.state.existLike ? imgCheck : imgPlus}
                    alt=""
                  />
                </button>

                <div
                  className="box-detail-first-in hidden-m"
                  style={{
                    backgroundImage: `url(${this.state.sneakerSelectImage})`,
                  }}
                >
                  <p className="p-16 bold roboto orange mt-3 hidden-m">
                    {this.state.sneaker.nameKr}
                  </p>
                  <button
                    onClick={() => this.setState({ modal: "like" })}
                    className="btn-custom-sm mt-4 hidden-m"
                  >
                    관심제품{" "}
                    <img
                      className="detail-plus ml-1"
                      src={this.state.existLike ? imgCheck : imgPlus}
                      alt=""
                    />
                  </button>
                </div>
                <div className="col-md-8 offset-md-2 mt-4 hidden-m">
                  <div className="row horizontal-detail">
                    <div
                      onClick={() =>
                        this.handleSelectImage(this.state.sneaker.imageUrl)
                      }
                      className={`detail-bar cursor ${
                        this.state.sneaker.imageUrl ===
                          this.state.sneakerSelectImage && "detail-bar-active"
                      }`}
                      style={{
                        backgroundImage: `url(${this.state.sneaker.imageUrl})`,
                      }}
                    />
                    <div
                      onClick={() =>
                        this.handleSelectImage(this.state.sneaker.imageUrl2)
                      }
                      className={`detail-bar cursor ${
                        this.state.sneaker.imageUrl2 ===
                          this.state.sneakerSelectImage && "detail-bar-active"
                      }`}
                      style={{
                        backgroundImage: `url(${this.state.sneaker.imageUrl2})`,
                      }}
                    />
                    <div
                      onClick={() =>
                        this.handleSelectImage(this.state.sneaker.imageUrl3)
                      }
                      className={`detail-bar cursor ${
                        this.state.sneaker.imageUrl3 ===
                          this.state.sneakerSelectImage && "detail-bar-active"
                      }`}
                      style={{
                        backgroundImage: `url(${this.state.sneaker.imageUrl3})`,
                      }}
                    />
                    <div
                      onClick={() =>
                        this.handleSelectImage(this.state.sneaker.imageUrl4)
                      }
                      className={`detail-bar cursor ${
                        this.state.sneaker.imageUrl4 ===
                          this.state.sneakerSelectImage && "detail-bar-active"
                      }`}
                      style={{
                        backgroundImage: `url(${this.state.sneaker.imageUrl4})`,
                      }}
                    />
                    <div
                      onClick={() =>
                        this.handleSelectImage(this.state.sneaker.imageUrl5)
                      }
                      className={`detail-bar cursor ${
                        this.state.sneaker.imageUrl5 ===
                          this.state.sneakerSelectImage && "detail-bar-active"
                      }`}
                      style={{
                        backgroundImage: `url(${this.state.sneaker.imageUrl5})`,
                      }}
                    />
                    {this.state.sneaker.videoUrl !== "" && (
                      <div
                        onClick={() =>
                          this.handleSelectImage(this.state.sneaker.videoUrl)
                        }
                        className={`detail-bar cursor ${
                          this.state.sneaker.videoUrl ===
                            this.state.sneakerSelectImage && "detail-bar-active"
                        }`}
                        style={{
                          backgroundImage: `url(${this.state.sneaker.videoUrl})`,
                        }}
                      />
                    )}
                  </div>
                </div>

                <div className="col-md-8 offset-md-2 pl-0 pr-0">
                  <div className="horizontal-detail sneaker-detail-carousel hidden-p">
                    {this.state.sneaker.imageUrl2 !== thumb ? (
                      <Carousel>
                        <div
                          className="buydetail-carousel"
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl)
                          }
                        >
                          <img src={this.state.sneaker.imageUrl} />
                        </div>

                        <div
                          className="buydetail-carousel"
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl2)
                          }
                        >
                          <img src={this.state.sneaker.imageUrl2} />
                        </div>

                        <div
                          className="buydetail-carousel"
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl3)
                          }
                        >
                          <img src={this.state.sneaker.imageUrl3} />
                        </div>

                        <div
                          className="buydetail-carousel"
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl4)
                          }
                        >
                          <img src={this.state.sneaker.imageUrl4} />
                        </div>

                        <div
                          className="buydetail-carousel"
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl5)
                          }
                        >
                          <img src={this.state.sneaker.imageUrl5} />
                        </div>
                      </Carousel>
                    ) : (
                      <>
                        <div
                          className="box-detail-first-in"
                          style={{
                            backgroundImage: `url(${this.state.sneakerSelectImage})`,
                          }}
                        >
                          <p className="p-16 bold roboto orange mt-3 hidden-m">
                            {this.state.sneaker.nameKr}
                          </p>
                          <button
                            onClick={() => this.setState({ modal: "like" })}
                            className="btn-custom-sm mt-4 hidden-m"
                          >
                            관심제품{" "}
                            <img
                              className="detail-plus ml-1"
                              src={this.state.existLike ? imgCheck : imgPlus}
                              alt=""
                            />
                          </button>
                        </div>
                        <div
                          onClick={() =>
                            this.handleSelectImage(this.state.sneaker.imageUrl)
                          }
                          className={`detail-bar cursor mb-3 ${
                            this.state.sneaker.imageUrl ===
                              this.state.sneakerSelectImage &&
                            "detail-bar-active"
                          }`}
                          style={{
                            backgroundImage: `url(${this.state.sneaker.imageUrl})`,
                          }}
                        />
                      </>
                    )}
                  </div>
                </div>
              </div>

              <div className="col-md-4 pt-5 mb-5">
                <div className="hidden-m">
                  <div className="d-block">
                    <div className="detail-left-inline">
                      <p className="p-14 bold gray-2">발매일</p>
                    </div>
                    <div className="detail-right-inline">
                      <p className="p-14 gray-3">
                        {timeFormat2(this.state.sneaker.releaseDate)}
                      </p>
                    </div>
                  </div>
                  <div className="d-block mt-1">
                    <div className="detail-left-inline">
                      <p className="p-14 bold gray-2">발매가</p>
                    </div>
                    <div className="detail-right-inline">
                      {this.state.dollar ? (
                        <p className="p-14 gray-3">
                          ${currency(this.state.sneaker.releasePrice)}
                        </p>
                      ) : (
                        <p className="p-14 gray-3">
                          {currency(this.state.sneaker.releasePrice)}원
                        </p>
                      )}
                    </div>
                  </div>
                  <div className="d-block mt-1">
                    <div className="detail-left-inline">
                      <p className="p-14 bold gray-2">품번</p>
                    </div>
                    <div className="detail-right-inline">
                      <p className="p-14 gray-3">
                        {this.state.sneaker.serialNumber}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="hidden-p">
                  <div className="row detail-thirdbox">
                    <div className="col-4 thirdbox-border">
                      <p className="p-14 bold gray-2">발매일</p>
                      <p className="p-14 gray-3">
                        {timeFormat2(this.state.sneaker.releaseDate)}
                      </p>
                    </div>
                    <div className="col-4 thirdbox-border">
                      <p className="p-14 bold gray-2">발매가</p>
                      {this.state.dollar ? (
                        <p className="p-14 gray-3">
                          ${currency(this.state.sneaker.releasePrice)}
                        </p>
                      ) : (
                        <p className="p-14 gray-3">
                          {currency(this.state.sneaker.releasePrice)}원
                        </p>
                      )}
                    </div>
                    <div className="col-4 thirdbox-border thirdbox-noneborder">
                      <p className="p-14 bold gray-2">품번</p>
                      <p className="p-14 gray-3">
                        {this.state.sneaker.serialNumber}
                      </p>
                    </div>
                  </div>
                </div>

                <p className="p-16 black-2 bold mt-4 mb-2">사이즈</p>
                {this.state.sizes.map((size: Size, i: number) => (
                  <button
                    onClick={() => this.handleSizeActive(size.size)}
                    key={i}
                    className={`btn btn-filter w-18p ${
                      size.active && "btn-filter-active"
                    }`}
                  >
                    {size.size}
                  </button>
                ))}

                <div className="row mt-4">
                  <div className="col-6">
                    <p className="p-16 black-2 bold">거래현황</p>
                  </div>
                  <div className="col-6 text-right">
                    <p
                      onClick={() => this.setState({ modal: "all" })}
                      className="p-13 gray-2 cursor underline pt-1"
                    >
                      전체보기
                    </p>
                  </div>
                </div>

                <div className="row">
                  <div className="col-12">
                    <div className="detail-border-top" />
                  </div>
                  <div className="col-4 text-center">
                    <p className="p-13 bold gray-2">사이즈</p>
                  </div>
                  <div className="col-4 text-center">
                    <p className="p-13 bold gray-2">판매수량</p>
                  </div>
                  <div className="col-4 text-center">
                    <p className="p-13 bold gray-2">금액</p>
                  </div>
                  <div className="col-12">
                    <div className="detail-border-bottom" />
                  </div>
                </div>

                {this.state.tradingList.map((trading: Trading, i: number) => {
                  return (
                    <div key={i} className="row">
                      <div className="col-4 text-center">
                        <p className="p-14 roboto gray-3">{trading.size}</p>
                      </div>
                      <div className="col-4 text-center">
                        <p className="p-14 roboto gray-3">{trading.askSize}</p>
                      </div>
                      <div className="col-4 text-center">
                        <p className={`p-14 bold roboto orange`}>
                          {currency(trading.price)}
                          <span className="p-13 noto normal">원</span>
                        </p>
                      </div>
                      <div className="col-12">
                        <div className="detail-border-bottom" />
                      </div>
                    </div>
                  );
                })}
                {this.state.tradingList.length === 0 && (
                  <div className="row">
                    <div className="col-12 text-center">
                      <p className="p-12 gray-3">
                        현재 판매 중인 매물이 없습니다.
                      </p>
                    </div>
                    <div className="col-12">
                      <div className="detail-border-bottom" />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="detail-wrap-img hidden-m"
          style={{ backgroundImage: `url(${imgWrap})` }}
        >
          <div className="box-detail-second">
            <div className="container">
              <div className="row">
                <div className="col-md-3 text-center detail-border">
                  <p className="p-16 white bold">판매중</p>
                  <p className="p-40 roboto white">
                    {this.state.tradingCount === 0
                      ? "—"
                      : this.state.tradingCount}
                    <span className="p-14 normal"> 건</span>
                  </p>
                </div>
                <div className="col-md-3 text-center detail-border">
                  <p className="p-16 white bold">판매중 최저가</p>
                  <p className="p-40 roboto white">
                    {this.state.tradingLowPrice === 0
                      ? "—"
                      : currency(this.state.tradingLowPrice)}
                    <span className="p-14 normal"> 원</span>
                  </p>
                </div>
                <div className="col-md-3 text-center detail-border">
                  <p className="p-16 white bold">거래완료 최저가</p>
                  <p className="p-40 roboto white">
                    {this.state.lowPrice === 0
                      ? "—"
                      : currency(this.state.lowPrice)}
                    <span className="p-14 normal"> 원</span>
                  </p>
                </div>
                <div className="col-md-3 text-center">
                  <p className="p-16 white bold">거래완료 최고가</p>
                  <p className="p-40 roboto white">
                    {this.state.highPirce === 0
                      ? "—"
                      : currency(this.state.highPirce)}
                    <span className="p-14 normal"> 원</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="detail-wrap-img text-center hidden-p"
          style={{ backgroundImage: `url(${imgWrap})` }}
        >
          <div className="box-detail-second">
            <div className="detail-border">
              <p className="p-16 white bold">판매중</p>
              <p className="p-24 roboto white">
                {this.state.tradingCount === 0 ? "—" : this.state.tradingCount}
                <span className="p-14 normal"> 건</span>
              </p>
            </div>
            <div className="detail-border">
              <p className="p-16 white bold">판매중 최저가</p>
              <p className="p-24 roboto white">
                {this.state.tradingLowPrice === 0
                  ? "—"
                  : currency(this.state.tradingLowPrice)}
                <span className="p-14 normal"> 원</span>
              </p>
            </div>
            <div className="detail-border">
              <p className="p-16 white bold">거래완료 최저가</p>
              <p className="p-24 roboto white">
                {this.state.lowPrice === 0
                  ? "—"
                  : currency(this.state.lowPrice)}
                <span className="p-14 normal"> 원</span>
              </p>
            </div>
            <div>
              <p className="p-16 white bold">거래완료 최고가</p>
              <p className="p-24 roboto white">
                {this.state.highPirce === 0
                  ? "—"
                  : currency(this.state.highPirce)}
                <span className="p-14 normal"> 원</span>
              </p>
            </div>
          </div>
        </div>

        <div className="container">
          <p className="p-22 black-2 text-center mt-6 mb-4 hidden-m">
            최근 거래 내역
          </p>
          <p className="p-22 black-2 text-center mt-4 mb-2 hidden-p">
            최근 거래 내역
          </p>
          <div className="row ">
            <div className="col-md-7 chart-height">
              <Line data={this.chartData} options={this.chartOption()} />
            </div>

            <div className="col-md-5">
              <div className="row">
                <div className="col-12">
                  <div className="detail-border-top" />
                </div>
                <div className="col-3 text-center">
                  <p className="p-13 bold gray-2">사이즈</p>
                </div>
                <div className="col-3 text-center">
                  <p className="p-13 bold gray-2">판매가</p>
                </div>
                <div className="col-6 text-left">
                  <p className="p-13 bold gray-2">거래완료시간</p>
                </div>
                <div className="col-12">
                  <div className="detail-border-bottom" />
                </div>
              </div>

              {this.state.historys
                .slice(0, 8)
                .map((history: History, i: number) => (
                  <div key={i} className="row">
                    <div className="col-3 text-center">
                      <p className="p-14 roboto gray-3">{history.size}</p>
                    </div>
                    <div className="col-3 text-center">
                      <p className={`p-14 gray-3 bold roboto`}>
                        {currency(history.price)}
                        <span className="p-13 noto normal">원</span>
                      </p>
                    </div>
                    <div className="col-6 text-left">
                      <p className={`p-14 gray-3 roboto`}>
                        {timeFormat5(history.tradeTime)}
                      </p>
                    </div>
                    <div className="col-12">
                      <div className="detail-border-bottom" />
                    </div>
                  </div>
                ))}
              {this.state.historys.length === 0 && (
                <div className="row">
                  <div className="col-12 text-center">
                    <p className="p-12 gray-3">현재 거래내역이 없습니다.</p>
                  </div>
                </div>
              )}

              <p
                onClick={() => this.setState({ modal: "trade" })}
                className="p-13 gray-2 underline text-right mt-1 cursor"
              >
                거래내역 전체보기
              </p>
            </div>
          </div>
        </div>

        <div className="tooltip-detail-default tooltip-calc">
          <div className="row">
            <div className="col-6">
              <p className="p-16 bold black-2">
                선택 사이즈{" "}
                <span className="p-18 orange roboto ml-2">
                  {this.state.selectSize}
                </span>
              </p>
            </div>
            <div className="col-6 text-right">
              <p
                onClick={() => window.scrollTo(0, 0)}
                className="p-13 gray-2 underline pt-1 cursor hidden-m"
              >
                변경
              </p>
              <p
                onClick={() => window.scrollTo(0, 740)}
                className="p-13 gray-2 underline pt-1 cursor hidden-p"
              >
                변경
              </p>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-6 pr-1">
              <Link
                to={`/buy/${this.props.match.params.id}/${this.state.selectSize}`}
              >
                <button className="btn btn-custom w-100">구매</button>
              </Link>
            </div>
            <div className="col-6 pl-1">
              {env === "pc" ? (
                <button
                  onClick={() => this.setState({ modal: "sell" })}
                  className="btn btn-custom-active w-100"
                >
                  판매
                </button>
              ) : (
                <Link
                  to={`/sell/${this.props.match.params.id}/${this.state.selectSize}`}
                >
                  <button className="btn btn-custom-active w-100">판매</button>
                </Link>
              )}
            </div>
          </div>
        </div>

        <div className="box-alsolike hidden-m">
          <div className="container">
            <p className="p-18 roboto bold letter-space-4 black-2">
              YOU ALSO LIKE
            </p>

            <div className="row pt-4">
              {this.state.recommendSneakers.map(
                (sneaker: RecommendSneaker, i: number) => (
                  <a
                    href={`/sneaker/${sneaker.name}`}
                    key={i}
                    className="col-md-3 cursor"
                  >
                    <div
                      className="main-release-in"
                      style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                    />
                    <p className="p-12 roboto bold gray-2 mt-3">
                      {sneaker.brand.toUpperCase()}
                    </p>
                    <p className="sneaker-name p-14 roboto back-2 mt-1">
                      {sneaker.name.toUpperCase()}
                    </p>
                    <p className="float-left p-14 gray-2 mt-4">최저판매가</p>
                    <p className="float-right roboto p-18 bold orange mt-3 pt-1">
                      {sneaker.lowPrice === 0
                        ? "—"
                        : currency(sneaker.lowPrice)}
                      <span className="p-14 noto normal">
                        {sneaker.lowPrice !== 0 && "원"}
                      </span>
                    </p>
                  </a>
                )
              )}
            </div>
          </div>
        </div>

        <div className="box-alsolike hidden-p">
          <div className="container">
            <p className="p-13 roboto bold letter-space-3 black-2">
              YOU ALSO LIKE
            </p>
            <div className="horizontal-wrapper">
              {this.state.recommendSneakers.map(
                (sneaker: RecommendSneaker, i: number) => (
                  <a
                    href={`/sneaker/${sneaker.name}`}
                    key={i}
                    className="horizontal-card"
                  >
                    <div
                      className="main-release-in"
                      style={{ backgroundImage: `url(${sneaker.imageUrl})` }}
                    />
                    <p className="p-12 roboto bold gray-2 mt-3">
                      {sneaker.brand.toUpperCase()}
                    </p>
                    <p className="sneaker-name p-14 roboto back-2 mt-1">
                      {sneaker.name.toUpperCase().substr(0, 28)}
                    </p>
                    <p className="text-right p-14 gray-2">최저판매가</p>
                    <p className="text-right roboto p-18 bold orange">
                      {sneaker.lowPrice === 0
                        ? "—"
                        : currency(sneaker.lowPrice)}
                      <span className="p-14 noto normal">
                        {sneaker.lowPrice !== 0 && "원"}
                      </span>
                    </p>
                  </a>
                )
              )}
            </div>
          </div>
        </div>

        <Modal display={this.state.modal === "like" && "block"}>
          <this.ModalLike />
        </Modal>
        <Modal display={this.state.modal === "sell" && "block"}>
          <this.ModalSell />
        </Modal>
        <ModalScroll display={this.state.modal === "trade" && "block"}>
          <this.ModalTrade />
        </ModalScroll>
        <ModalScroll display={this.state.modal === "all" && "block"}>
          <this.ModalAll />
        </ModalScroll>
      </div>
    );
  }

  private ModalLike = () => {
    return (
      <div className="p-3">
        <div className="row">
          <div className="col-6">
            <p className="p-16 bold black-2">관심제품 등록</p>
          </div>
          <div className="col-6 text-right">
            <img
              onClick={() => this.setState({ modal: "none" })}
              src={imgClose}
              alt=""
              className="cursor"
            />
          </div>
        </div>
        <hr className="hr2" />
        <p className="p-14 gray-3 text-center mt-4 mb-3">
          사이즈를 선택해 주세요
        </p>
        {this.state.sizes.map((size: Size, i: number) => {
          if (i > 0) {
            return (
              <button
                onClick={() => this.handleSizeActive(size.size)}
                key={i}
                className={`btn-filter w-64 ${
                  size.active && "btn-filter-active"
                }`}
              >
                {size.size}
              </button>
            );
          }
        })}

        <div className="text-center mt-3 mb-3">
          <button
            onClick={this.handleLike}
            className="btn btn-custom-active btn-sm"
          >
            등록
          </button>
        </div>
      </div>
    );
  };

  private ModalSell = () => {
    return (
      <div className="p-3">
        <div className="row">
          <div className="col-6">
            <p className="p-16 bold black-2">알림</p>
          </div>
          <div className="col-6 text-right">
            <img
              onClick={() => this.setState({ modal: "none" })}
              src={imgClose}
              alt=""
              className="cursor"
            />
          </div>
        </div>
        <hr className="hr2" />
        <p className="p-14 gray-3 text-center mt-4">
          제품 판매는 모바일앱에서만 가능합니다.
        </p>
        <div className="text-center mt-4 mb-3">
          <button
            onClick={() => this.setState({ modal: "none" })}
            className="btn-custom-active btn-sm"
          >
            확인
          </button>
        </div>
      </div>
    );
  };

  private ModalAll = () => {
    return (
      <div className="p-3">
        <div className="row">
          <div className="col-6">
            <p className="p-16 bold black-2">거래현황</p>
          </div>
          <div className="col-6 text-right">
            <img
              onClick={() => this.setState({ modal: "none" })}
              alt=""
              src={imgClose}
              className="cursor"
            />
          </div>
        </div>

        <hr className="hr2" />
        <div className="text-center">
          <div
            onClick={() => this.setState({ modalTab: "판매중" })}
            className={`cursor detail-all ${
              this.state.modalTab === "판매중" && "detail-all-active"
            }`}
          >
            판매중
          </div>
          <div
            onClick={() => this.setState({ modalTab: "거래완료" })}
            className={`cursor detail-all ${
              this.state.modalTab === "거래완료" && "detail-all-active"
            }`}
          >
            거래완료
          </div>
        </div>

        {this.state.modalTab === "판매중" && (
          <>
            <div className="row mt-3">
              <div className="col-12">
                <div className="detail-border-top" />
              </div>
              <div className="col-4 text-center">
                <p className="p-13 bold gray-2">사이즈</p>
              </div>
              <div className="col-4 text-center">
                <p className="p-13 bold gray-2">판매수량</p>
              </div>
              <div className="col-4 text-center">
                <p className="p-13 bold gray-2">금액</p>
              </div>
              <div className="col-12">
                <div className="detail-border-bottom" />
              </div>
            </div>
            {!this.state.sizes[0].active &&
              this.state.tradingList.map((trading: Trading, i: number) => {
                return (
                  <div key={i} className="row">
                    <div className="col-4 text-center">
                      <p className="p-14 roboto gray-3">{trading.size}</p>
                    </div>
                    <div className="col-4 text-center">
                      <p className="p-14 roboto gray-3">{trading.askSize}</p>
                    </div>
                    <div className="col-4 text-center">
                      <p className="p-14 bold roboto orange">
                        {currency(trading.price)}
                        <span className="p-13 noto normal">원</span>
                      </p>
                    </div>
                    <div className="col-12">
                      <div className="detail-border-bottom" />
                    </div>
                  </div>
                );
              })}
            {this.state.sizes[0].active &&
              this.state.tradingInitList.map((trading: Trading, i: number) => {
                return (
                  <div key={i} className="row">
                    <div className="col-4 text-center">
                      <p className="p-14 roboto gray-3">{trading.size}</p>
                    </div>
                    <div className="col-4 text-center">
                      <p className="p-14 roboto gray-3">{trading.askSize}</p>
                    </div>
                    <div className="col-4 text-center">
                      <p className="p-14 bold roboto orange">
                        {currency(trading.price)}
                        <span className="p-13 noto normal">원</span>
                      </p>
                    </div>
                    <div className="col-12">
                      <div className="detail-border-bottom" />
                    </div>
                  </div>
                );
              })}
            {this.state.tradingList.length === 0 && (
              <div className="row">
                <div className="col-12 text-center">
                  <p className="p-12 gray-3">현재 거래내역이 없습니다.</p>
                </div>
              </div>
            )}
          </>
        )}

        {this.state.modalTab === "거래완료" && (
          <>
            <div className="row mt-3">
              <div className="col-12">
                <div className="detail-border-top" />
              </div>
              <div className="col-3 text-center">
                <p className="p-13 bold gray-2">사이즈</p>
              </div>
              <div className="col-4 text-center">
                <p className="p-13 bold gray-2">판매가</p>
              </div>
              <div className="col-5 text-left">
                <p className="p-13 bold gray-2">거래완료시간</p>
              </div>
              <div className="col-12">
                <div className="detail-border-bottom" />
              </div>
            </div>
            {this.state.historys.map((history: History, i: number) => (
              <div key={i} className="row">
                <div className="col-3 text-center">
                  <p className="p-14 roboto gray-3">{history.size}</p>
                </div>
                <div className="col-4 text-center">
                  <p className={`p-14 gray-3 bold roboto`}>
                    {currency(history.price)}
                    <span className="p-13 noto normal">원</span>
                  </p>
                </div>
                <div className="col-5 text-left">
                  <p className={`p-14 gray-3 roboto`}>
                    {timeFormat5(history.tradeTime)}
                  </p>
                </div>
                <div className="col-12">
                  <div className="detail-border-bottom" />
                </div>
              </div>
            ))}
            {this.state.historys.length === 0 && (
              <div className="row">
                <div className="col-12 text-center">
                  <p className="p-12 gray-3">현재 거래내역이 없습니다.</p>
                </div>
              </div>
            )}
          </>
        )}

        <div className="text-center mt-4 mb-3"></div>
      </div>
    );
  };

  private ModalTrade = () => {
    return (
      <div className="p-3">
        <div className="row">
          <div className="col-6">
            <p className="p-16 bold black-2">거래내역</p>
          </div>
          <div className="col-6 text-right">
            <img
              onClick={() => this.setState({ modal: "none" })}
              src={imgClose}
              alt=""
              className="cursor"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="detail-border-top" />
          </div>
          <div className="col-3 text-center">
            <p className="p-13 bold gray-2">사이즈</p>
          </div>
          <div className="col-4 text-center">
            <p className="p-13 bold gray-2">판매가</p>
          </div>
          <div className="col-5 text-left">
            <p className="p-13 bold gray-2">거래완료시간</p>
          </div>
          <div className="col-12">
            <div className="detail-border-bottom" />
          </div>
        </div>

        {this.state.historys.map((history: History, i: number) => (
          <div key={i} className="row">
            <div className="col-3 text-center">
              <p className="p-14 roboto gray-3">{history.size}</p>
            </div>
            <div className="col-4 text-center">
              <p className={`p-14 gray-3 bold roboto`}>
                {currency(history.price)}
                <span className="p-13 noto normal">원</span>
              </p>
            </div>
            <div className="col-5 text-left">
              <p className={`p-14 gray-3 roboto`}>
                {timeFormat5(history.tradeTime)}
              </p>
            </div>
            <div className="col-12">
              <div className="detail-border-bottom" />
            </div>
          </div>
        ))}

        {this.state.historys.length === 0 && (
          <div className="row">
            <div className="col-12 text-center">
              <p className="p-12 gray-3">현재 거래내역이 없습니다.</p>
            </div>
          </div>
        )}

        <div className="text-center mt-4 mb-3"></div>
      </div>
    );
  };
}

export default SneakerDetail;
