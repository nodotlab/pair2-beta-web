import React from "react";
import {
  getCurrentUser,
  tradingBuyer,
  tradingSeller,
  cheat,
} from "../db/Actions";
import { parsePhone } from "../Method";
import CurrentUser from "../reduce/CurrentUser";
import ReactTooltip from "react-tooltip";
import Bidding from "../reduce/BiddingSimple";
import Selling from "../reduce/SellingSimple";
// import DefaultScreen from "../components/MypageDefault";
import DefaultScreen from "../components/MypageDefault";
import LikeScreen from "../components/MypageLikes";
import BuyScreen from "../components/MypageBuys";
import SellScreen from "../components/MypageSells";
import BillingScreen from "../components/MypageBilling";
import ShippingScreen from "../components/MypageShipping";
import EditScreen from "../components/MypageEdit";
import imgArrow from "../images/Menu_Arrow.svg";

interface MyPageState {
  currentUser: CurrentUser;
  screen: string;
  likeTab: string;
  biddings: Bidding[];
  sellings: Selling[];
}

class MyPage extends React.Component<any, MyPageState> {
  constructor(props: any) {
    super(props);
    this.state = {
      currentUser: new CurrentUser("", "", false, false, 0, 0, ""),
      screen: !this.props.match.params.screen
        ? "default"
        : this.props.match.params.screen,
      likeTab: "products",
      biddings: [],
      sellings: [],
    };
  }

  public componentDidMount() {
    if (
      !localStorage.getItem("pair2token") ||
      localStorage.getItem("pair2signin") === "false"
    )
      this.props.history.push("/login/");

    this.initCurrentUser();
    this.initData();
  }

  private async initCurrentUser() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCurrentUser(token);
      if (result === "faild") {
        this.handleSignout();
      }

      const user = new CurrentUser(
        result.nickname,
        result.phone,
        result.userVerify === true ? true : false,
        false,
        0,
        0,
        result.email
      );
      this.setState({ currentUser: user });
    } catch (error) {
      this.props.history.push("/login/");
      this.handleSignout();
    }
  }

  private async initData() {
    const biddings: Bidding[] = [];
    const sellings: Bidding[] = [];

    try {
      const token: any = localStorage.getItem("pair2token");
      const bidResult = await tradingBuyer(token);
      const sellResult = await tradingSeller(token);

      for (let i in bidResult) {
        if (
          bidResult[i].status !== "거래완료" &&
          bidResult[i].status !== "거래취소" &&
          bidResult[i].status !== "거래거절"
        ) {
          biddings.push(
            new Bidding(
              bidResult[i].index,
              bidResult[i].imageUrl,
              `${bidResult[i].brand} ${bidResult[i].sneakerName}`,
              bidResult[i].status
            )
          );
        }
      }

      for (let i in sellResult) {
        if (!sellResult[i].complete) {
          sellings.push(
            new Selling(
              sellResult[i].index,
              sellResult[i].imageUrls[0],
              `${sellResult[i].brand} ${sellResult[i].name}`,
              "판매중"
            )
          );
        }
      }
    } catch (error) {}

    this.setState({
      biddings: biddings,
      sellings: sellings,
    });
  }

  private handleSignout = (): void => {
    localStorage.setItem("pair2signin", "false");
    localStorage.setItem("pair2token", "");
    localStorage.setItem("pair2nickname", "");
    this.props.history.push("/");
  };

  private handleLikeTab = (name: string): void => {
    this.setState({
      screen: "likes",
      likeTab: name,
    });
  };

  public render() {
    return (
      <div>
        <div className="container">
          <div className="row mt-5 m-mt0">
            <div className="col-md-3 mb-5 m-mb1">
              <hr className="hr3 hidden-m" />
              <div className="p-2">
                <p className="p-18 black-2 bold mt-2 hidden-m">
                  {this.state.currentUser.name}
                </p>
                <p className="p-16 black-2 bold mt-2 hidden-p">
                  {this.state.currentUser.name}
                </p>
                <p className="p-16 black-2 mt-1 hidden-m">
                  {parsePhone(this.state.currentUser.phone)}
                </p>
                <p className="p-14 black-2 mt-1 float-left hidden-p">
                  {parsePhone(this.state.currentUser.phone)}
                </p>
                <p
                  onClick={this.handleSignout}
                  className="p-13 gray-2 underline mt-4 cursor hidden-m"
                >
                  로그아웃
                </p>
                <p
                  onClick={this.handleSignout}
                  className="p-13 gray-2 underline cursor hidden-p float-right"
                >
                  로그아웃
                </p>
              </div>

              <hr className="hr4 mb-4 hidden-m" />
              <div className="hidden-m">
                {/* <button
                  onClick={() => this.setState({ screen: "likes" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "likes" && "mypage-menu-active"
                  }`}
                >
                  관심목록
                  {this.state.screen === "likes" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
                <button
                  onClick={() => this.setState({ screen: "buys" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "buys" && "mypage-menu-active"
                  }`}
                >
                  구매내역
                  {this.state.screen === "buys" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
                <button
                  onClick={() => this.setState({ screen: "sells" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "sells" && "mypage-menu-active"
                  }`}
                >
                  판매내역
                  {this.state.screen === "sells" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
                <button
                  onClick={() => this.setState({ screen: "billing" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "billing" && "mypage-menu-active"
                  }`}
                >
                  카드/계좌 관리
                  {this.state.screen === "billing" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
                <button
                  onClick={() => this.setState({ screen: "shipping" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "shipping" && "mypage-menu-active"
                  }`}
                >
                  배송지관리
                  {this.state.screen === "shipping" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button> */}
                <button
                  onClick={() => this.setState({ screen: "edit" })}
                  className={`btn-mypage-menu ${
                    this.state.screen === "edit" && "mypage-menu-active"
                  }`}
                >
                  개인정보 수정
                  {this.state.screen === "edit" && (
                    <img src={imgArrow} className="float-right mt-1" />
                  )}
                </button>
              </div>
            </div>

            <div className="col-md-9">
              {/* <div className="mypage-header">
                <div className="col-md-3 col-6 pl-4 pr-4 mypage-header-border">
                  <p className="p-16 white bold hidden-m">본인인증</p>
                  <p className="p-14 white bold hidden-p">본인인증</p>
                  <div className="row">
                    <div className="col-6 text-left pr-0 mt-4">
                      <p className="p-13 white o-60">휴대폰</p>
                    </div>
                    <div className="col-6 text-right pl-0 mt-4">
                      {this.state.currentUser.verifyPhone ? (
                        <p className="p-13 roboto orange-3 bold">OK</p>
                      ) : (
                        <p className="p-13 white underline">미인증</p>
                      )}
                    </div>
                  </div>
                </div>

                <div className="col-md-3 col-6 pl-4 pr-4 mypage-header-border">
                  <p className="p-16 white bold hidden-m">사기피해이력</p>
                  <p className="p-14 white bold hidden-p">사기피해이력</p>
                  <div className="row">
                    <div className="col-6 text-left pr-0 mt-4">
                      <p className="p-13 white o-60">휴대폰</p>
                      <p className="p-13 white o-60 mt-2">은행계좌</p>
                    </div>
                    <div className="col-6 text-right pl-0 pr-0 mt-4">
                      <p className="p-13 roboto orange-3 bold">
                        {this.state.currentUser.cheatPhone === 0
                          ? "Clean"
                          : "사기이력존재"}
                      </p>
                      <p className="p-13 roboto orange-3 bold mt-2">
                        {this.state.currentUser.cheatBank === 0
                          ? "Clean"
                          : "사기이력존재"}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-md-12 hidden-p">
                  <hr className="hr5 mt-0 mb-0" />
                </div>

                <div
                  onClick={() => this.setState({ screen: "buys" })}
                  className="col-md-3 col-6 pl-4 pr-4 mypage-header-border cursor"
                >
                  <p className="p-16 white bold hidden-m">구매 진행중</p>
                  <p className="p-14 white bold mt-3 hidden-p">구매 진행중</p>
                  <p className="roboto p-40 white mt-4 text-right hidden-m">
                    {this.state.biddings.length}
                    <span className="p-14 white noto ml-1">건</span>
                  </p>
                  <p className="roboto p-40 white mt-4 text-right hidden-p">
                    {this.state.biddings.length}
                    <span className="p-14 white noto ml-1">건</span>
                  </p>
                </div>

                <div
                  onClick={() => this.setState({ screen: "sells" })}
                  className="col-md-3 col-6 pl-4 pr-4 mypage-header-border cursor"
                >
                  <p className="p-16 white bold hidden-m">판매 진행중</p>
                  <p className="p-14 white bold mt-3 hidden-p">판매 진행중</p>
                  <p className="roboto p-40 white mt-4 text-right hidden-m">
                    {this.state.sellings.length}
                    <span className="p-14 white noto ml-1">건</span>
                  </p>
                  <p className="roboto p-40 white mt-4 text-right hidden-p">
                    {this.state.sellings.length}
                    <span className="p-14 white noto ml-1">건</span>
                  </p>
                </div>
              </div> */}

              <Tooltip1 biddings={this.state.biddings} />
              <Tooltip2 sellings={this.state.sellings} />

              <div className="row hidden-p">
                <div className="border-topbottom mt-3" />
              </div>
              {/* <div className="row hidden-p">
                <div
                  onClick={() => this.setState({ screen: "likes" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "likes" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  관심목록{" "}
                </div>
                <div
                  onClick={() => this.setState({ screen: "buys" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "buys" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  구매내역{" "}
                </div>
                <div
                  onClick={() => this.setState({ screen: "sells" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "sells" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  판매내역
                </div>
                <div
                  onClick={() => this.setState({ screen: "billing" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "billing" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  카드/계좌 관리
                </div>
                <div
                  onClick={() => this.setState({ screen: "shipping" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "shipping" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  배송지관리
                </div>
                <div
                  onClick={() => this.setState({ screen: "edit" })}
                  className={`gray-2 col-md-8 col-4 box-faq-left ${
                    this.state.screen === "edit" &&
                    "box-faq-left-active black-2"
                  }`}
                >
                  개인정보 수정
                </div>
              </div> */}

              {/* {this.state.screen === "default" && (
                <DefaultScreen handleLikeTab={this.handleLikeTab} />
              )} */}
              {this.state.screen === "likes" && (
                <LikeScreen tab={this.state.likeTab} />
              )}
              {this.state.screen === "buys" && (
                <BuyScreen email={this.state.currentUser.email} />
              )}
              {this.state.screen === "sells" && <SellScreen />}
              {this.state.screen === "billing" && <BillingScreen />}
              {this.state.screen === "shipping" && <ShippingScreen />}
              {this.state.screen === "edit" && (
                <EditScreen userId={this.state.currentUser.phone} />
              )}
              {this.state.screen === "default" && (
                <EditScreen userId={this.state.currentUser.phone} />
              )}
              {/* edit user */}
            </div>
          </div>
        </div>
        {/* container end */}
      </div>
    );
  }
}

class Tooltip1 extends React.Component<any, any> {
  public render() {
    return (
      <ReactTooltip
        className="tooltip-custom tooltip-mypage"
        id="tooltip1"
        place="bottom"
        type="light"
      >
        {this.props.biddings.map((bid: Bidding, i: number) => (
          <div key={i} className="row pb-2 pt-2">
            <div className="col-2">
              <div
                className="tooltip-image"
                style={{ backgroundImage: `url(${bid.imageUrl})` }}
              />
            </div>
            <div className="col-7">
              <p className="p-14 gray-3">
                {bid.name.substr(0, 40)}
                {bid.name.length > 39 && "..."}
              </p>
            </div>
            <div className="col-3 pt-2">
              <p className="p-13 gray-3">{bid.status}</p>
            </div>
          </div>
        ))}
      </ReactTooltip>
    );
  }
}

class Tooltip2 extends React.Component<any, any> {
  public render() {
    return (
      <ReactTooltip
        className="tooltip-custom tooltip-mypage"
        id="tooltip2"
        place="bottom"
        type="light"
      >
        {this.props.sellings.map((sell: Selling, i: number) => (
          <div key={i} className="row pb-2 pt-2">
            <div className="col-2">
              <div
                className="tooltip-image"
                style={{ backgroundImage: `url(${sell.imageUrl})` }}
              />
            </div>
            <div className="col-7">
              <p className="p-14 gray-3">
                {sell.name.substr(0, 40)}
                {sell.name.length > 39 && "..."}
              </p>
            </div>
            <div className="col-3 pt-2">
              <p className="p-13 gray-3">{sell.status}</p>
            </div>
          </div>
        ))}
      </ReactTooltip>
    );
  }
}

export default MyPage;
