import React from "react";

class Brand extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      brandList: ["나이키", "이지부스트", "아디다스"],
      textTamp: "",
    };
  }

  private handleText = (e: any) => this.setState({ textTamp: e.target.value });

  private handleClick = () => {
    const tmpList: string[] = this.state.brandList;
    tmpList.push(this.state.textTamp);
    this.setState({
      brandList: tmpList,
      tempText: "",
    });
  };

  public render() {
    return (
      <div className="container">
        <h3>Brand (admin)</h3>
        <p>브랜드 추가 기능...</p>
        <hr />

        <h5>현재 브랜드</h5>
        {this.state.brandList.map((data: string, i: number) => (
          <button key={i} className="btn btn-primary mr-1">
            {data}
          </button>
        ))}

        <h5 className="mt-5">추가 브랜드</h5>
        <input onChange={this.handleText} className="form-control" />
        <button onClick={this.handleClick} className="btn btn-primary mt-2">
          추가
        </button>
      </div>
    );
  }
}

export default Brand;
