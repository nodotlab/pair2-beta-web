import React, { useEffect, useState } from "react";
import "../styles/testmain.css";
import testlogo from "../images/testlogo.png";
import person from "../images/ic_person.png";
import imgN from "../images/nodot_1x.png";
import imgSearch from "../images/ic_search.png";
import { Link } from "react-router-dom";

const TestMain = () => {
  useEffect(() => {
    document.body.style.backgroundColor = "#FD7133";
  });

  const [label, setLabel] = useState("Updating...");

  return (
    <div className="testmain pt-4">
      <div className="container text-right pt-4"></div>

      <div className="container text-center">
        <div className="testheight text-hide animated fadeIn ani-delay02">
          <img src={testlogo} alt="" className="testlogo" />
        </div>

        <div className="col-md-6 offset-md-3">
          <form
            onClick={() => setLabel("")}
            className="frm-srch animated fadeIn ani-delay03 mt-5"
            action="/"
            method="get"
          >
            <label
              htmlFor="search"
              className="montserrat fix-label font-mont fw400 animated flash infinite"
            >
              {label}
            </label>
            <input
              className="form-test montserrat"
              type="text"
              id="search"
              name="search"
              title="검색어 입력"
            />
            <img src={imgSearch} alt="" className="search-test" />
          </form>
        </div>
        <p className="test-p mt-5 animated fadeInUp ani-delay03">2020 April.</p>
        <p className="test-p mt-2 animated fadeInUp ani-delay04">By</p>
        <img
          src={imgN}
          className="testimg-n mt-3 animated fadeInUp ani-delay05"
        />
      </div>

      <div className="testfooter">
        <p className="montserrat p-15 white hidden-m">
          © 2018 NODOT. All Right Reserved.
        </p>
        <p className="montserrat p-13 white hidden-p">
          © 2018 NODOT. All Right Reserved.
        </p>
      </div>
    </div>
  );
};

export default TestMain;
