export const currency = (num: number): string => {
  const n = Math.floor(num);
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  
  // return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const currencyPrice = (num: string): string => {
//   // const n = Math.floor(num);
//   // return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const parsePhone = (phone: string): string => {
  return `${phone.substr(0, 3)}-${phone.substr(3, 4)}-${phone.substr(7, 4)}`;
};

export const timeBeforeMonth = (month: number): number => {
  const date: Date = new Date();
  date.setMonth(date.getMonth() - month);

  return date.getTime();
};

export const timeBeforeDate = (_date: number): number => {
  const date: Date = new Date();
  date.setDate(date.getDate() - _date);

  return date.getTime();
};

export const timeFormat = (_date: number): string => {
  const date: Date = new Date(_date);

  return `${date.getFullYear()}-${date.getMonth() + 1}/${date.getDate()}`;
};

export const timeFormat2 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  let _date: string = date.getDate().toString();
  return `${date.getFullYear()}. ${date.getMonth() + 1}. ${_date}`;
};

export const timeFormat3 = (fullSecond: number): string => {
  const min: number = Math.floor(fullSecond / 60);
  const sec: number = fullSecond - min * 60;

  return `${min}:${sec}`;
};

export const timeFormat4 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  return `${date.getMonth() + 1}/${date.getDate()}`;
};

export const timeFormat5 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);

  const _month: string = (date.getMonth() + 1).toString();
  const _date: string = date.getDate().toString();
  let hour: string = date.getHours().toString();
  if (hour.length === 1) {
    hour = "0" + hour;
  }

  let min: string = date.getMinutes().toString();
  if (min.length === 1) {
    min = "0" + min;
  }

  return `${date.getFullYear()}. ${_month}. ${_date} ${hour}:${min}`;
};

export const timeFormat6 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  return `${date.getMonth() + 1} . ${date.getDate()}`;
};

export const timeFormat7 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  let hour: string = date.getHours().toString();
  if (hour.length === 1) {
    hour = "0" + hour;
  }

  let min: string = date.getMinutes().toString();
  if (min.length === 1) {
    min = "0" + min;
  }

  return `${hour} : ${min}`;
};

export const timeFormat8 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);

  let _month: string = (date.getMonth() + 1).toString();
  if (_month.length === 1) {
    _month = "0" + _month;
  }

  let _date: string = date.getDate().toString();
  if (_date.length === 1) {
    _date = "0" + _date;
  }

  return `${_month}.${_date}`;
}; // graph

export const timeFormat9 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  const _month: string = (date.getMonth() + 1).toString();
  const _date: string = date.getDate().toString();
  return `${date.getFullYear()}. ${_month}. ${_date}`;
}; // graph

export const timeFormat10 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  const _month: string = (date.getMonth() + 1).toString();
  const _date: string = date.getDate().toString();
  const _day = date.getDay();
  let _dayKr = "";
  switch (_day) {
    case 0:
      _dayKr = "일";
      break;
    case 1:
      _dayKr = "월";
      break;
    case 2:
      _dayKr = "화";
      break;
    case 3:
      _dayKr = "수";
      break;
    case 4:
      _dayKr = "목";
      break;
    case 5:
      _dayKr = "금";
      break;
    case 6:
      _dayKr = "토";
      break;
  }
  let hour: string = date.getHours().toString();
  if (hour.length === 1) {
    hour = "0" + hour;
  }
  let min: string = date.getMinutes().toString();
  if (min.length === 1) {
    min = "0" + min;
  }

  return `${_month}/${_date} (${_dayKr}) ${hour}:${min}`;
};

export const monthName = (timestamp: number): string => {
  const monthNames: string[] = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const month = new Date(timestamp).getMonth();

  return monthNames[month];
};

export const getLastDayOfMonth = (): Date => {
  const today = new Date();
  const lastDayOfMonth = new Date(
    today.getFullYear(),
    today.getMonth() + 1,
    0,
    24,
  );
  return lastDayOfMonth;
};

export const randomNumber = (): string => {
  const n: number = Math.floor(Math.random() * (1000000 - 100000)) + 100000;
  return n.toString();
};

export const dDay = (timestamp: number): string => {
  const gap: number = timestamp - new Date().getTime();
  const dday = Math.floor(gap / (1000 * 60 * 60 * 24));
  return "-" + (dday + 1).toString();
};

/**
   * 현재 시간을 기준으로 응모/선착순 시간에 따른 발매상태 태그 도출
   * @param startTime 응모/선착순 시작 시간
   * @param endTime 응모 종료 시간
   * @returns 발매 상태 숫자
   */
export const getReleaseState = (type:string, startTime: number, endTime: number) : number => {
  let currentTime = Date.now();
  if (type === "추첨") {
    if (currentTime < startTime) { //응모 예정
      return 4;
    } else if (currentTime >= startTime && currentTime < endTime) {//응모중
      return 1;
    } else { //응모 종료
      return 5;
    }    
  } else {
    if (currentTime < startTime) { //선착순
      return 2;
    } else {
      return 3; //선착순 발매
    }
  }
}  


export const getReleaseStateTime = (type: string, start: number, end: number): string => {
  if (type === "추첨") {
    let value: string = "";
    if (Date.now() < start) {
      value = `${timeFormat10(start)} 시작`;
    } else {
      value = `${timeFormat10(end)} 마감`;
    }

    return value;
  } else {
    return `${timeFormat10(start)} 발매`;
  }
}

export const getCountdownTime = (type: string, start: number, end: number): number => {
  if (type === "추첨") {
    if (Date.now() < start) {
      return start;
    } else {
      return end;
    }
  } else {
    return start;
  }
}
