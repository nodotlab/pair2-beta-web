class ReleaseToday {

  public storeName: string;
  public storeState: number;
  public storeCountry: string;
  public storeIsOnline: boolean;
  public storeTime: string;
  public countdownTime: number;
  public storeUrl: string;
  public snkrNameKr: string;
  public snkrImageUrl: string;
  public snkrIdx: number;
  public snkrSerial: string;

  constructor(
    storeName: string, 
    storeState: number,
    storeCountry: string,
    storeIsOnline: boolean,
    storeTime: string, 
    countdownTime: number,
    storeUrl: string, 
    snkrNameKr: string, 
    snkrImageUrl: string, 
    snkrIdx: number,
    snkrSerial: string
) {
    this.storeName = storeName;
    this.storeState = storeState;
    this.storeTime = storeTime;
    this.storeCountry = storeCountry;
    this.storeIsOnline = storeIsOnline;
    this.countdownTime = countdownTime;
    this.storeUrl = storeUrl;
    this.snkrNameKr = snkrNameKr;
    this.snkrImageUrl = snkrImageUrl;
    this.snkrIdx = snkrIdx;
    this.snkrSerial = snkrSerial;
  }  
}

export default ReleaseToday;
