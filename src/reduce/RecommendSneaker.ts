class RecommendSneaker {
  public index: string;
  public brand: string;
  public name: string;
  public size: string;
  public lowPrice: number;
  public imageUrl: string;
  public tooltip: boolean = false;

  constructor(
    _index: string,
    _brand: string,
    _name: string,
    _size: string,
    _lowPrice: number,
    _imageUrl: string
  ) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.lowPrice = _lowPrice;
    this.imageUrl = _imageUrl;
  }
}

export default RecommendSneaker;
