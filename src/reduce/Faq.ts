class Faq {
  public category: string;
  public title: string;
  public body: string;
  public isOpen: boolean;

  constructor(category: string, title: string, body: string) {
    this.category = category;
    this.title = title;
    this.body = body;
    this.isOpen = false;
  }
}

export default Faq;
