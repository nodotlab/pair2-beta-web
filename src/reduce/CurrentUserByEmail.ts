class CurrentUser {
  public email: string;
  public nickname: string;

  constructor(
    _email: string,
    _nickname: string
  ) {
    this.email = _email;
    this.nickname = _nickname;
  }
}

export default CurrentUser;
