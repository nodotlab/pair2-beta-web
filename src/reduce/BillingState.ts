class BillingState {
  public cardName: string;
  public number1: string;

  constructor(_cardName: string, _number1: string) {
    this.cardName = _cardName;
    this.number1 = _number1;
  }
}

export default BillingState;
