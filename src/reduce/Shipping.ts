class Shipping {
  public tag: string;
  public name: string;
  public default: boolean;
  public zip: string;
  public address: string;
  public addressDetail: string;
  public phone: string;

  constructor(
    _tag: string,
    _name: string,
    _default: boolean,
    _zip: string,
    _address: string,
    _addressDetail: string,
    _phone: string
  ) {
    this.tag = _tag;
    this.name = _name;
    this.default = _default;
    this.zip = _zip;
    this.address = _address;
    this.addressDetail = _addressDetail;
    this.phone = _phone;
  }
}

export default Shipping;
