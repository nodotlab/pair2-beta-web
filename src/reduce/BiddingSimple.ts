class Bidding {
  public index: string; // product origin index
  public imageUrl: string;
  public name: string;
  public status: string;

  constructor(
    _index: string,
    _imageUrl: string,
    _name: string,
    _status: string
  ) {
    this.index = _index;
    this.imageUrl = _imageUrl;
    this.name = _name;
    this.status = _status;
  }
}

export default Bidding;
