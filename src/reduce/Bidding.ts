class Bidding {
  public index: string; // product origin index
  public imageUrl: string;
  public brand: string;
  public name: string;
  public size: string;
  public dealPrice: number;
  public highPrice: number;
  public lowPrice: number;
  public endTime: number;
  public isTrading?: boolean;
  public tradeIndex?: string;

  constructor(
    _index: string,
    _imageUrl: string,
    _brand: string,
    _name: string,
    _size: string,
    _dealPrice: number,
    _highPrice: number,
    _lowPrice: number,
    _endTime: number,
    _isTrading?: boolean,
    _tradeIndex?: string
  ) {
    this.index = _index;
    this.imageUrl = _imageUrl;
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.dealPrice = _dealPrice;
    this.highPrice = _highPrice;
    this.lowPrice = _lowPrice;
    this.endTime = _endTime;
    this.isTrading = _isTrading;
    this.tradeIndex = _tradeIndex;
  }
}

export default Bidding;
