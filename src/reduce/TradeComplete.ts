class TradeComplete {
  public index: string; // product origin index
  public imageUrl: string;
  public name: string;
  public brand: string;
  public size: string;
  public dealPrice: number;
  public tradeTime: number;
  public tradeNumber: number;
  public status: string;
  public new?: boolean; // if not checking paid
  public completeTimestamp?: number;

  constructor(
    _index: string,
    _imageUrl: string,
    _name: string,
    _brand: string,
    _size: string,
    _dealPrice: number,
    _tradeTime: number,
    _tradeNumber: number,
    _status: string,
    _new?: boolean,
    _completeTimestamp?: number
  ) {
    this.index = _index;
    this.imageUrl = _imageUrl;
    this.name = _name;
    this.brand = _brand;
    this.size = _size;
    this.dealPrice = _dealPrice;
    this.tradeTime = _tradeTime;
    this.tradeNumber = _tradeNumber;
    this.status = _status;
    this.new = _new;
    this.completeTimestamp = _completeTimestamp;
  }
}

export default TradeComplete;
