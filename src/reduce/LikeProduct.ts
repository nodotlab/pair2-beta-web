class LikeProduct {
  public index: string; // product origin index
  public brand: string;
  public name: string;
  public size: string;
  public imageUrl: string;
  public select?: boolean = false;

  constructor(
    _index: string,
    _brand: string,
    _name: string,
    _size: string,
    _imageUrl: string
  ) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.imageUrl = _imageUrl;
  }
}

export default LikeProduct;
