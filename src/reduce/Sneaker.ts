class Sneaker {
  public brand: string;
  public name: string;
  public size: string;
  public lowPrice: number;
  public imageUrl: string;
  public releaseDate?: number;

  constructor(
    _brand: string,
    _name: string,
    _size: string,
    _lowPrice: number,
    _imageUrl: string,
    _releaseDate?: number
  ) {
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.lowPrice = _lowPrice;
    this.imageUrl = _imageUrl;
    this.releaseDate = _releaseDate;
  }
}

export default Sneaker;
