class UserInfo {
  public userId: string;
  public verifyPhone: boolean;
  public verifyBank: boolean;
  public cheatPhone: number;
  public cheatBank: number;

  constructor(
    _userId: string,
    _verifyPhone: boolean,
    _verifyBank: boolean,
    _cheatPhone: number,
    _cheatBank: number
  ) {
    this.userId = _userId;
    this.verifyPhone = _verifyPhone;
    this.verifyBank = _verifyBank;
    this.cheatPhone = _cheatPhone;
    this.cheatBank = _cheatBank;
  }
}

export default UserInfo;
