class LikeSale {
  public index: string; // product origin index
  public brand: string;
  public name: string;
  public size: string;
  public imageUrl: string;
  public price: number;
  public location: string;
  public status: string;
  public shippings: string[];
  public select?: boolean = false;
  public likeIndex?: any;

  constructor(
    _index: string,
    _brand: string,
    _name: string,
    _size: string,
    _imageUrl: string,
    _price: number,
    _location: string,
    _status: string,
    _shippings: string[],
    _likeIndex: any
  ) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.size = _size;
    this.imageUrl = _imageUrl;
    this.price = _price;
    this.location = _location;
    this.status = _status;
    this.shippings = _shippings;
    this.likeIndex = _likeIndex;
  }
}

export default LikeSale;
