class Release {
  public releaseTimestamp: number;
  public brand: string;
  public name: string;
  public nameKr: string;
  public imageUrl: string = "";
  public unknownRelease?: boolean;
  public type?: number;
  public index?: number;
  public body?: string;
  public serial?: string;

  constructor(
    releaseTimestamp: number,
    brand: string,
    name: string,
    nameKr: string,
    imageUrl: string,
    serial: string,
    unknownRelease?: boolean,
    type?: number,
    index?: number,
    body?: string
  ) {
    this.releaseTimestamp = releaseTimestamp;
    this.brand = brand;
    this.name = name;
    this.nameKr = nameKr;
    this.imageUrl = imageUrl;
    this.unknownRelease = unknownRelease;
    this.type = type;
    this.index = index;
    this.body = body;
    this.serial = serial;
  }
}

export default Release;
