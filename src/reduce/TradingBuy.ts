class TradingBuy {
  public index: string; // product origin index
  public imageUrl: string;
  public name: string;
  public brand: string;
  public size: string;
  public tradeNumber: number;
  public tradeTime: number;
  public dealPrice: number;
  public status: string;
  public shipping?: string;

  constructor(
    _index: string,
    _imageUrl: string,
    _name: string,
    _brand: string,
    _size: string,
    _tradeNumber: number,
    _tradeTime: number,
    _dealPrice: number,
    _status: string,
    _shipping?: string
  ) {
    this.index = _index;
    this.imageUrl = _imageUrl;
    this.name = _name;
    this.brand = _brand;
    this.size = _size;
    this.tradeNumber = _tradeNumber;
    this.tradeTime = _tradeTime;
    this.dealPrice = _dealPrice;
    this.status = _status;
    this.shipping = _shipping;
  }
}

export default TradingBuy;
