class Post {
  public index: number;
  public timestamp: number;
  public title: string;
  public description: string;
  public isOpen: boolean;

  constructor(
    _index: number,
    _timestamp: number,
    _title: string,
    _description: string,
    _isOpen: boolean
  ) {
    this.index = _index;
    this.timestamp = _timestamp;
    this.title = _title;
    this.description = _description;
    this.isOpen = _isOpen;
  }
}

export default Post;
