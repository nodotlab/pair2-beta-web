class SneakerDetail {
  public brand: string;
  public name: string;
  public nameKr: string;
  public size: string;
  public lowPrice: number;
  public imageUrl: string;
  public imageUrl2: string;
  public imageUrl3: string;
  public imageUrl4: string;
  public imageUrl5: string;
  public videoUrl: string;
  public releaseDate: number;
  public releasePrice: number;
  public serialNumber: string;
  public thumbUrl?: string;

  constructor(
    _brand: string,
    _name: string,
    _nameKr: string,
    _size: string,
    _lowPrice: number,
    _imageUrl: any,
    _imageUrl2: any,
    _imageUrl3: any,
    _imageUrl4: any,
    _imageUrl5: any,
    _videoUrl: any,
    _releaseDate: number,
    _releasePrice: number,
    _serialNumber: string,
    _thumbUrl?: string
  ) {
    this.brand = _brand;
    this.name = _name;
    this.nameKr = _nameKr;
    this.size = _size;
    this.lowPrice = _lowPrice;
    this.imageUrl = _imageUrl;
    this.imageUrl2 = _imageUrl2;
    this.imageUrl3 = _imageUrl3;
    this.imageUrl4 = _imageUrl4;
    this.imageUrl5 = _imageUrl5;
    this.videoUrl = _videoUrl;
    this.releaseDate = _releaseDate;
    this.releasePrice = _releasePrice;
    this.serialNumber = _serialNumber;
    this.thumbUrl = _thumbUrl;
  }
}

export default SneakerDetail;
