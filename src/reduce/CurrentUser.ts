class CurrentUser {
  public name: string;
  public phone: string;
  public verifyPhone: boolean;
  public verifyBank: boolean;
  public buyCount: number;
  public sellCount: number;
  public email?: string;

  constructor(
    _name: string,
    _phone: string,
    _verifyPhone: boolean,
    _verifyBank: boolean,
    _buyCount: number,
    _sellCount: number,
    _email?: string
  ) {
    this.name = _name;
    this.phone = _phone;
    this.verifyPhone = _verifyPhone;
    this.verifyBank = _verifyBank;
    this.buyCount = _buyCount;
    this.sellCount = _sellCount;
    this.email = _email;
  }
}

export default CurrentUser;
