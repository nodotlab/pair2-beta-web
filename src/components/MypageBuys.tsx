import React from "react";
import { withRouter, Link } from "react-router-dom";
import { tradingBuyer } from "../db/Actions";
import Bidding from "../reduce/Bidding";
import Trading from "../reduce/TradingBuy";
import TradeComplete from "../reduce/TradeComplete";
import EditBidding from "../components/EditBidding";
import BuyDetail from "../components/BuyDetail";
import BuyDetailModal from "../components/BuyDetailModal";
import { currency, timeFormat2 } from "../Method";

interface MypageBuysState {
  tab: string;
  biddings: Bidding[];
  tradings: Trading[];
  tradeComplete: TradeComplete[];
  modalBiddingEdit: boolean;
  selectBiddingIndex: string;
  selectTradingIndex: string;
}

class MypageBuys extends React.Component<any, MypageBuysState> {
  constructor(props: any) {
    super(props);
    this.state = {
      tab: "tradings",
      // tab: "tradingDetail",
      biddings: [],
      tradings: [],
      tradeComplete: [],
      modalBiddingEdit: false,
      selectBiddingIndex: "",
      selectTradingIndex: "",
    };
  }

  public componentDidMount() {
    this.initData();
  }

  private async initData() {
    const tradings: Trading[] = [];
    const complete: TradeComplete[] = [];

    try {
      const token: any = localStorage.getItem("pair2token");
      const trade = await tradingBuyer(token);

      for (let i in trade) {
        if (trade[i].status === "거래거절") {
          complete.push(
            new TradeComplete(
              trade[i].index,
              trade[i].imageUrl,
              trade[i].sneakerName,
              trade[i].brand,
              trade[i].size,
              trade[i].price,
              trade[i].tradeIndex,
              trade[i].index,
              trade[i].status
            )
          );
        } else if (trade[i].status === "거래완료") {
          complete.push(
            new TradeComplete(
              trade[i].index,
              trade[i].imageUrl,
              trade[i].sneakerName,
              trade[i].brand,
              trade[i].size,
              trade[i].price,
              trade[i].index,
              trade[i].tradeIndex,
              trade[i].status,
              false,
              trade[i].completeTimestamp
            )
          );
        } else {
          tradings.push(
            new Trading(
              trade[i].index,
              trade[i].imageUrl,
              trade[i].sneakerName,
              trade[i].brand,
              trade[i].size,
              trade[i].tradeIndex,
              trade[i].index,
              trade[i].price,
              trade[i].status
            )
          );
        }
      }
    } catch (error) {}

    tradings.sort(
      (a: Trading, b: Trading) => parseInt(b.index) - parseInt(a.index)
    );
    complete.sort(
      (a: any, b: any) => b.completeTimestamp - a.completeTimestamp
    );

    if (this.props.match.params.screen !== "none") {
      this.setState({
        // biddings: biddings,
        tradings: tradings,
        tradeComplete: complete,
      });
    }
  }

  private handleSelectBiddingIndex = (i: string): void => {
    this.setState({ selectBiddingIndex: i });
    this.handleModalBiddingEdit();
  };

  private handleModalBiddingEdit = (): void => {
    this.setState({ modalBiddingEdit: !this.state.modalBiddingEdit });
  };

  public render() {
    return (
      <div className="mt-5 mb-6 m-mt2">
        <div className="w-100 border-bottom-2 hidden-m">
          <div
            onClick={() => this.setState({ tab: "tradings" })}
            className={`cursor btn-faq ${
              this.state.tab === "tradings" && "btn-faq-active"
            } ${this.state.tab === "tradingDetail" && "btn-faq-active"}`}
          >
            거래중
          </div>
          <div
            onClick={() => this.setState({ tab: "complete" })}
            className={`cursor btn-faq ${
              this.state.tab === "complete" && "btn-faq-active"
            } ${this.state.tab === "tradeComplete" && "btn-faq-active"}`}
          >
            거래완료
          </div>
        </div>

        <div className="w-100 text-center hidden-p">
          <div
            onClick={() => this.setState({ tab: "tradings" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "tradings" && "btn-faq-active"
            } ${this.state.tab === "tradingDetail" && "btn-faq-active"}`}
          >
            거래중
          </div>
          <div
            onClick={() => this.setState({ tab: "complete" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "complete" && "btn-faq-active"
            } ${this.state.tab === "tradeComplete" && "btn-faq-active"}`}
          >
            거래완료
          </div>
        </div>

        {this.state.tab === "biddings" && <this.ComponentBiddings />}
        {this.state.tab === "tradings" && <this.ComponentTradings />}
        {this.state.tab === "complete" && <this.ComponentComplete />}
        {this.state.tab === "tradingDetail" && (
          <BuyDetail index={this.state.selectTradingIndex} />
        )}
        {this.state.tab === "tradingDetailModal" && (
          <BuyDetailModal
            index={this.state.selectTradingIndex}
            handleModal={this.handleModal}
          />
        )}
        {this.state.tab === "tradeComplete" && (
          <BuyDetail index={this.state.selectTradingIndex} complete={true} />
        )}
        {this.state.tab === "tradeCompleteModal" && (
          <BuyDetailModal
            index={this.state.selectTradingIndex}
            complete={true}
            handleModal={this.handleModal}
          />
        )}
      </div>
    );
  }

  private handleModal = (): void => {
    this.setState({
      tab: this.state.tab === "tradingDetailModal" ? "tradings" : "complete",
    });
  };

  private ComponentBiddings = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.biddings.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-5 text-left p-13 bold gray-2">제품</div>
          <div className="col-1 pl-0 pr-0 p-13 bold gray-2">구매요청가</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">
            구매요청 최고가
          </div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">
            판매요청 최저가
          </div>
          <div className="col-1 pl-0 pr-0 p-13 bold gray-2">요청만료일</div>
          <div className="col-1 p-13 bold gray-2"></div>
        </div>

        <div className="hidden-m">
          {this.state.biddings.map((bid: Bidding, i: number) => (
            <div key={i} className="flex box-bidding">
              <div className="col-2 pl-0">
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${bid.imageUrl})` }}
                />
              </div>
              <div className="col-3 pl-0 pr-1 p-14">
                <p className="roboto gray-3">{bid.brand}</p>
                <p className="roboto gray-3">{bid.name}</p>
                <p className="roboto gray-2 mt-1">{bid.size} mm</p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                {bid.dealPrice >= bid.highPrice ? (
                  <p className="orange roboto bold p-14 mt-4">
                    {currency(bid.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                ) : (
                  <p className="gray-3 roboto bold p-14 mt-4">
                    {currency(bid.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                )}
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(bid.lowPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(bid.highPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(bid.endTime)}
                </p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                <button
                  onClick={() => this.handleSelectBiddingIndex(bid.index)}
                  className="btn-custom-mypage pt-2 pb-2 mt-3"
                >
                  수정
                </button>
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.biddings.map((bid: Bidding, i: number) => (
            <div key={i} className="box-bidding">
              <div className="row">
                <div className="col-4">
                  <div
                    className="img-post"
                    style={{ backgroundImage: `url(${bid.imageUrl})` }}
                  />
                </div>
                <div className="col-8 pl-1 p-14">
                  <p className="roboto gray-3">{bid.brand}</p>
                  <p className="roboto gray-3">{bid.name}</p>
                  <p className="roboto gray-2">{bid.size} mm</p>
                  <p className="roboto orange bold p-14 mt-2">
                    <span className="noto normal gray-2 p-13 mr-2">
                      구매요청가
                    </span>
                    {currency(bid.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13 mt-1">
                    요청만료일{" "}
                    <span className="gray-3 ml-2">
                      {timeFormat2(bid.endTime)}
                    </span>
                  </p>
                </div>
              </div>
              <button
                onClick={() => this.handleSelectBiddingIndex(bid.index)}
                className="btn-custom-mypage pt-2 pb-2 w-100 mt-4"
              >
                수정
              </button>
            </div>
          ))}
        </div>
        {this.state.modalBiddingEdit && (
          <EditBidding
            handleModal={this.handleModalBiddingEdit}
            index={this.state.selectBiddingIndex}
          />
        )}
      </>
    );
  };

  private ComponentTradings = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.tradings.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-5 text-left p-13 bold gray-2">제품</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">거래번호</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">요청일</div>
          <div className="col-1 pl-0 pr-0 p-13 bold gray-2">금액</div>
          <div className="col-2 text-center pl-4 pr-0 p-13 bold gray-2">
            상태
          </div>
        </div>

        <div className="hidden-m">
          {this.state.tradings.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "tradingDetail",
                  selectTradingIndex: trade.index,
                })
              }
              key={i}
              className="flex box-bidding cursor"
            >
              <div className="col-2 pl-0">
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${trade.imageUrl})` }}
                />
              </div>
              <div className="col-3 pl-0 pr-1 p-14">
                <p className="roboto gray-3">{trade.brand}</p>
                <p className="roboto gray-3">{trade.name}</p>
                <p className="roboto gray-2 mt-1">{trade.size} mm</p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                {trade.status === "거래요청중" ? (
                  <p className="gray-3 roboto p-14 mt-4">ㅡ</p>
                ) : (
                  <p className="gray-3 roboto p-14 mt-4">
                    {trade.tradeNumber.toString()}
                  </p>
                )}
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(trade.tradeTime)}
                </p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                <p className="orange roboto bold p-14 mt-4">
                  {currency(trade.dealPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-2 text-center pl-4 pr-0">
                {trade.shipping ? (
                  <p className="roboto gray-3 p-14 mt-1">{trade.status}</p>
                ) : (
                  <p className="gray-3 p-14 mt-4">{trade.status}</p>
                )}
                {trade.shipping && (
                  <p className="roboto gray-2 underline p-13 mt-1">
                    {trade.shipping}
                  </p>
                )}
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.tradings.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "tradingDetailModal",
                  selectTradingIndex: trade.index,
                })
              }
              key={i}
              className="row box-bidding"
            >
              <div className="col-4 p-14">
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${trade.imageUrl})` }}
                />
              </div>
              <div className="col-8 pl-1">
                <p className="roboto gray-3 p-14">{trade.brand}</p>
                <p className="roboto gray-3 p-14">{trade.name}</p>
                <p className="roboto gray-2 p-13">{trade.size} mm</p>
                <p className="orange roboto bold p-14">
                  {currency(trade.dealPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
                <p className="gray-2 p-13 mt-1">
                  요청일{" "}
                  <span className="ml-2 gray-3">
                    {timeFormat2(trade.tradeTime)}
                  </span>
                </p>
                {trade.status === "거래요청중" ? (
                  <p className="gray-2 p-13 mt-2">
                    거래번호 <span className="ml-2">-</span>
                  </p>
                ) : (
                  <p className="gray-2 p-13">
                    거래번호{" "}
                    <span className="ml-2 gray-3">
                      {trade.tradeNumber.toString()}
                    </span>
                  </p>
                )}
                <p className="gray-3 p-13 bold float-left">{trade.status}</p>
                {trade.shipping && (
                  <p className="roboto gray-2 underline p-13 mt-1 float-right">
                    {trade.shipping}
                  </p>
                )}
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };

  private ComponentComplete = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.tradeComplete.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-7 text-left p-13 bold gray-2">제품</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">거래일자</div>
          <div className="col-1 pl-0 pr-0 p-13 bold gray-2">거래번호</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">상태</div>
        </div>

        <div className="hidden-m">
          {this.state.tradeComplete.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "tradeComplete",
                  selectTradingIndex: trade.index,
                })
              }
              key={i}
              className="flex box-bidding cursor"
            >
              <div className="col-2 pl-0">
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${trade.imageUrl})` }}
                />
              </div>
              <div className="col-5 pl-0 pr-1 p-14">
                <p className="roboto gray-3">{trade.brand}</p>
                <p className="roboto gray-3">{trade.name}</p>
                <p className="roboto gray-2 mt-1">{trade.size} mm</p>
                <p className="orange roboto bold p-14 mt-1">
                  {currency(trade.dealPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(trade.tradeTime)}
                </p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {trade.tradeNumber.toString()}
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="gray-3 p-14 mt-4">{trade.status}</p>
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.tradeComplete.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "tradeCompleteModal",
                  selectTradingIndex: trade.index,
                })
              }
              key={i}
              className="box-bidding"
            >
              <div className="row">
                <div className="col-3">
                  <div
                    className="sell-photo img-sellcomplete"
                    style={{ backgroundImage: `url(${trade.imageUrl})` }}
                  >
                    <div className="box-trade-cancle mt-5">거래완료</div>
                  </div>
                </div>
                <div className="col-9">
                  <p className="roboto gray-3 p-14">{trade.brand}</p>
                  <p className="roboto gray-3 p-14">{trade.name}</p>
                  <p className="roboto gray-2 p-13">{trade.size} mm</p>
                  <p className="orange roboto bold p-14">
                    {currency(trade.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13 mt-1">
                    거래일자{" "}
                    <span className="gray-3 ml-3">
                      {timeFormat2(trade.tradeTime)}
                    </span>
                  </p>
                  <p className="gray-2 p-13">
                    거래번호{" "}
                    <span className="gray-3 ml-3">
                      {trade.tradeNumber.toString()}
                    </span>
                  </p>
                  <p className="gray-3 p-13 bold">{trade.status}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };
}

export default withRouter(MypageBuys);
