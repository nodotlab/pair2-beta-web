import React from "react";
import { Helmet } from "react-helmet";
import { serviceUrl } from "../reduce/Urls";
import imgLogo from "../images/pair2_web_logo.jpg";

interface MetaDataProps {
    title?: string
    desc?: string,
    keywords?: string,
    url?: string,
    image?: string,
    siteName?: string    
}

const defaultTitle = "한정판 신발 스니커즈 발매 정보 실시간 공유 플랫폼 | PAIR2(페어투)"
const defaultDesc = "PAIR2(페어투)는 나이키, 조던, 아디다스, 뉴발란스, 컨버스 등 한정판 신발 스니커즈의 전세계 발매 정보를 실시간으로 제공하는 플랫폼 서비스 입니다.";
const defaultKeywords = "PAIR2, 페어투, 한정판, 신발, 스니커즈, 나이키, 에어 조던, 아디다스, 이지부스트, 뉴발란스, 컨버스, 응모, 드로우, 래플, 추첨, 패션, 스트릿패션, 발매정보, sneakers, shoes, nike, jordan, adidas, yeezy, converse";
const defaultUrl = serviceUrl+"/";
const defaultImage = serviceUrl + imgLogo;
const defaultSiteName = "PAIR2 - 페어투"
const test = process.env.PUBLIC_URL;

function MetaTags(props: { data: MetaDataProps }): any {

    const title = props.data.title ? props.data.title : defaultTitle;
    const desc = props.data.desc ? props.data.desc : defaultDesc
    const keywords = props.data.keywords ? props.data.keywords : defaultKeywords;
    const url = props.data.url ? serviceUrl + props.data.url : defaultUrl;
    const image = props.data.image ? props.data.image : defaultImage;
    const siteName = props.data.siteName ? props.data.siteName : defaultSiteName;

    return (
        <Helmet>
            <link rel="canonical" href={url} />
            <title>{title}</title>
            <meta name="title" content={title} />
            <meta name="description" content={desc} />
            <meta name="keywords" content={keywords} />
            <meta property="og:title" content={title} />
            <meta property="og:type" content="website" />
            <meta property="og:url" content={url} />
            <meta property="og:description" content={desc} />
            <meta property="og:image" content={image} />
            <meta property="og:site_name" content={siteName} />
            <meta property="twitter:card" content="summary_large_image" />
            <meta property="twitter:title" content={title} />
            <meta property="twitter:url" content={url} />
            <meta property="twitter:description" content={desc} />
            <meta property="twitter:image" content={image} />           
        </Helmet>
    );
};

MetaTags.defaultProps = {
    data: {
        title: defaultTitle,
        desc: defaultDesc,
        keywords: defaultKeywords,
        url: defaultUrl,
        image: defaultImage,
        siteName: defaultSiteName
    }
}

export default MetaTags;