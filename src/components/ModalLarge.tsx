import React from "react";

class Modal extends React.Component<any> {
  public render() {
    return (
      <div
        className="modal-custom-large"
        style={{ display: this.props.display }}
      >
        <div className="modal-content-large">{this.props.children}</div>
      </div>
    );
  }
}

export default Modal;
