import React from "react";
import { withRouter, Link } from "react-router-dom";
import { likes, likesTrade } from "../db/Actions";
import LikeProduct from "../reduce/LikeProduct";
import LikeSale from "../reduce/LikeSale";
import { currency } from "../Method";

interface MypageDefaultState {
  likeProducts: LikeProduct[];
  likeSale: LikeSale[];
}

class MypageDefault extends React.Component<any, MypageDefaultState> {
  constructor(props: any) {
    super(props);

    this.state = {
      likeProducts: [],
      likeSale: [],
    };
  }

  public componentDidMount() {
    this.initData();
  }

  private async initData() {
    const products: LikeProduct[] = [];
    const sale: LikeSale[] = [];

    const token: any = localStorage.getItem("pair2token");
    const resultLikes = await likes(token, 3);
    if (resultLikes !== "faild") {
      for (let i = 0; i < resultLikes.length; ++i) {
        products.push(
          new LikeProduct(
            "",
            resultLikes[i].brand,
            resultLikes[i].name,
            resultLikes[i].size,
            resultLikes[i].thumbUrl
          )
        );
      }
    }

    const resultTrades = await likesTrade(token, 3);
    if (resultTrades !== "faild") {
      for (let i = 0; i < resultTrades.length; ++i) {
        let where: string[] = [];
        if (resultTrades[i].shippingType === "택배") where = ["택배"];
        if (resultTrades[i].shippingType === "직거래") where = ["직거래"];
        if (resultTrades[i].shippingType === "직거래/택배")
          where = ["직거래 | 택배"];

        sale.push(
          new LikeSale(
            resultTrades[i].tradeIndex,
            resultTrades[i].brand,
            resultTrades[i].sneakerName,
            resultTrades[i].size,
            resultTrades[i].imageUrl,
            resultTrades[i].price,
            resultTrades[i].from,
            resultTrades[i].status,
            where,
            resultTrades[i].likeIndex
          )
        );
      }
    }

    if (this.props.match.params.screen !== "none") {
      this.setState({
        likeProducts: products,
        likeSale: sale,
      });
    }
  }

  public render() {
    return (
      <div className="mt-5 m-mt4">
        <div className="row">
          <div className="col-6 text-left">
            <p className="p-16 black-2 bold">관심제품</p>
          </div>
          <div className="col-6 text-right">
            <p
              onClick={() => this.props.handleLikeTab("products")}
              className="p-13 gray-2 underline float-right cursor hidden-m"
            >
              전체보기
            </p>
          </div>
        </div>

        <div className="row mt-2 hidden-m">
          {this.state.likeProducts.map((product: LikeProduct, i: number) => (
            <Link
              to={`/sneaker/${product.name}`}
              key={i}
              className="col-4 mb-4 cursor"
            >
              <div
                className="mypage-default-image"
                style={{ backgroundImage: `url(${product.imageUrl})` }}
              />
              <div className="row mt-3">
                <div className="col-6">
                  <p className="p-12 gray-2 bold roboto">
                    {product.brand.toUpperCase()}
                  </p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-12 gray-2 roboto">Size {product.size}</p>
                </div>
              </div>
              <p className="p-14 black-2 roboto mt-2">
                {product.name.toUpperCase()}
              </p>
            </Link>
          ))}
        </div>

        <div className="horizontal-wrapper mt-2 hidden-p">
          {this.state.likeProducts.map((product: LikeProduct, i: number) => (
            <Link
              to={`/sneaker/${product.name}`}
              key={i}
              className="horizontal-card horizontal-likes"
            >
              <div
                className="box-popular"
                style={{ backgroundImage: `url(${product.imageUrl})` }}
              />
              <div className="row">
                <div className="col-6 text-left pr-0">
                  <p className="p-12 roboto bold gray-2 mt-1">
                    {product.brand.toUpperCase()}
                  </p>
                </div>
                <div className="col-6 text-right pl-0">
                  <p className="p-12 gray-2 roboto mt-1">
                    Size {product.size.toUpperCase()}
                  </p>
                </div>
              </div>
              <p className="sneaker-name p-14 roboto black-2 mt-1">
                {product.name.toUpperCase().substr(0, 29)}
              </p>
            </Link>
          ))}
        </div>

        {this.state.likeProducts.length === 0 && (
          <div className="text-center mt-6">
            <p className="p-14 gray-3">등록된 관심제품이 없습니다.</p>
            <Link to="/sneakers">
              <button className="btn-custom-sm mt-3 mb-6">제품 보러가기</button>
            </Link>
          </div>
        )}

        <hr className="hr4 mt-5 m-mt0" />

        <div className="row mt-5 m-mt4">
          <div className="col-6 text-left">
            <p className="p-16 black-2 bold">관심매물</p>
          </div>
          <div className="col-6 text-right">
            <p
              onClick={() => this.props.handleLikeTab("sale")}
              className="p-13 gray-2 underline float-right cursor hidden-m"
            >
              전체보기
            </p>
          </div>
        </div>

        <div className="row mt-2 mb-5 hidden-m">
          {this.state.likeSale.map((sale: LikeSale, i: number) => (
            <Link
              to={`/buy/${sale.name}/${sale.size}/${sale.index}`}
              key={i}
              className="col-4 mb-4 cursor"
            >
              <div
                className="mypage-default-image"
                style={{ backgroundImage: `url(${sale.imageUrl})` }}
              />
              <p className="p-12 gray-2 mt-2 text-right">
                {sale.location} <span className="gray-7 m-1">|</span>
                {sale.status} <span className="gray-7 m-1">|</span>
                {sale.shippings[0]} <span className="gray-7 m-1"></span>
              </p>
              <div className="row mt-3">
                <div className="col-6">
                  <p className="p-12 gray-2 bold roboto">{sale.brand}</p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-12 gray-2 roboto">Size {sale.size}</p>
                </div>
              </div>
              <p className="p-14 black-2 roboto mt-2">{sale.name}</p>
              <p className="p-18 orange bold roboto float-right mt-3">
                {currency(sale.price)}
                <span className="p-13 noto normal">원</span>
              </p>
            </Link>
          ))}
        </div>

        <div className="horizontal-wrapper hidden-p pt-2 mb-5">
          {this.state.likeSale.map((sale: LikeSale, i: number) => (
            <Link
              to={`/buy/${sale.name}/${sale.size}/${sale.index}`}
              key={i}
              className="horizontal-card"
            >
              <div
                className="box-popular"
                style={{ backgroundImage: `url(${sale.imageUrl})` }}
              />
              <p className="p-12 gray-2 mt-2 text-right mypage-tags">
                {sale.location} <span className="gray-7 m-1">|</span>
                {sale.status} <span className="gray-7 m-1">|</span>
                {sale.shippings[0]}
              </p>
              <div className="row mt-3">
                <div className="col-6">
                  <p className="p-12 gray-2 bold roboto">{sale.brand}</p>
                </div>
                <div className="col-6 text-right">
                  <p className="p-12 gray-2 roboto">Size {sale.size}</p>
                </div>
              </div>
              <p className="sneaker-name p-14 roboto black-2 mt-1">
                {sale.name.toUpperCase().substr(0, 29)}
              </p>
              <p className="p-18 orange bold roboto float-right mt-0">
                {currency(sale.price)}
                <span className="p-13 noto normal">원</span>
              </p>
            </Link>
          ))}
        </div>

        {this.state.likeSale.length === 0 && (
          <div className="text-center mt-6 mb-100">
            <p className="p-14 gray-3">등록된 관심매물이 없습니다.</p>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(MypageDefault);
