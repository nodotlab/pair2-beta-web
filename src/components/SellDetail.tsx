import React from "react";
import { Link } from "react-router-dom";
import { currency, timeFormat2, timeFormat5, parsePhone } from "../Method";
import BankState from "../reduce/BankState";
import { getBank, putBank, tradingDetail, askUpdate, shippingCompanyList, shippingCode, shippingWhere, buyRefuse, tradeUpdate, shipping, getCommision } from "../db/Actions";
import imgComplete from "../images/Step_Completed.svg";
import plus from "../images/Plus.svg";
import equal from "../images/equal.svg";
import imgClose from "../images/Popup_Delete.svg";
import bullet from "../images/bullet.svg";
import check from "../images/Condition_Check.svg";
import bad from "../images/Bad_Normal.svg";
import badActive from "../images/Bad_Active.svg";
import normal from "../images/Normal_Normal.svg";
import normalActive from "../images/Normal_Active.svg";
import good from "../images/Good_Normal.svg";
import goodActive from "../images/Good_Active.svg";
import paycomplete from "../images/domestic.svg";
import imgCheck from "../images/Checkbox_Default.svg";
import imgChecked from "../images/Checkbox_Checked.svg";
import { env } from "../reduce/Env";


interface TradingDetailState {
  trade: {
    index: number;
    name: string;
    size: string;
    price: number;
    bidPrice: number; // client bid
    bidTimestamp: number;
    imageUrl: string;
    seller: string;
    sellerPhone: string;
    buyer: string;
    buyerPhone: string;
    buyerEmail: string;
    payment: string;
    paymentTimestamp: number;
    status: string; // stay, staytrade, complete
    isShipping: boolean;
    shippingName: string;
    shippingPhone: string;
    shippingEmail: string;
    shippingLocation: string;
    shippingRequest: string;
    isCancle: boolean;
    paidProgress: number;
  };
  progress: number;
  modal: string;
  cancleType: string;
  cancleDesc: string;
  completeComment: string;
  shippingLocation: string;
  bankState: BankState;
  owner: string;
  bank: string;
  account: string;
  bankList: string[];
  shippingCompany: string;
  shippingNumber: string;
  shippingCompanyList: string[];
  codeList: string[];
  shippingCode: string;
  nowLocation: string;
  paid: boolean;
  smsCheck: boolean;
  commision: number;
  sale: number;
}

class TradingDetail extends React.Component<any,  TradingDetailState> {
  constructor(props: any) {
    super(props);
    this.state = {
      trade: {
        isShipping: false, // direct
        status: "stay",
        index: 0,
        name: "",
        price: 0,
        bidPrice: 0,
        size: "",
        bidTimestamp: Date.now(),
        imageUrl: "",
        seller: "",
        sellerPhone: "",
        buyer: "",
        buyerPhone: "",
        buyerEmail: "",
        payment: "BC카드",
        paymentTimestamp: Date.now(),
        shippingName: "",
        shippingEmail: "",
        shippingPhone: "",
        shippingLocation: "",
        shippingRequest: "",
        isCancle: false, // this state from props
        paidProgress: 0,
      },
      progress: 1,
      modal: "none",
      cancleType: "",
      cancleDesc: "",
      completeComment: "",
      shippingLocation: "운송장번호조회",
      bankState: new BankState("", "", ""),
      owner: "",
      bank: "",
      account: "",
      bankList: this.initBankList(),
      shippingCompanyList: [],
      shippingCompany: "",
      shippingNumber: "",
      codeList: [],
      shippingCode: "",
      nowLocation: "",
      paid: false,
      smsCheck: false,
      commision: 0,
      sale: 0,
    };
  }

  public componentDidMount() {
    // if (this.state.trade.isShipping && this.state.progress === 3) {
    //   this.setState({ shippingLocation: "옥천허브" });
    // }

    this.initShipping();
    this.initData();
    this.getBank();
    this.initShippingCompanyList();
    this.commition();
  }

  private async commition() {
    const result = await getCommision();
    this.setState({
      commision: result.commision,
      sale: result.sale,
    });
  }

  private async initData() {
    const token: any = localStorage.getItem("pair2token");
    const result = await tradingDetail(token, this.props.index);
    let paidProgress = 0;
    try {
      if (result.paidProgress && result.paidProgress === 1) paidProgress = 1;
      if (result.paidProgress && result.paidProgress === 2) paidProgress = 2;
    } catch (error) {}

    const trade: any = {
      isShipping: result.isShipping,
      // index: this.props.index,
      index: result.tradeIndex,
      name: result.name,
      price: result.price,
      bidPrice: result.price,
      size: result.size,
      bidTimestamp: result.askIndex,
      imageUrl: result.imageUrls[0],
      seller: result.seller.name,
      sellerPhone: result.seller.phone,
      buyer: result.buyer.name,
      buyerEmail: result.buyer.email,
      buyerPhone: result.buyer.phone,
      payment: "", // call from buyer index
      paymentTimestamp: Date.now(),
      shippingName: result.shipping.name,
      shippingPhone: result.shipping.phone,
      shippingLocation: result.shipping.address,
      shippingRequest: "",
      isCancle: false, // this state from props
      paidProgress: paidProgress,
    };

    let progress: number = 1;
    if (!result.isShipping) {
      switch (result.status) {
        case "거래거절":
          progress = 5;
          trade.isCancle = true;
          break;
        case "거래요청중":
          progress = 1;
          break;
        case "직거래대기":
          progress = 2;
          break;
        case "거래완료":
          progress = 3;
          break;
      }
    } else {
      switch (result.status) {
        case "거래거절":
          progress = 5;
          trade.isCancle = true;
          break;
        case "거래요청중":
          progress = 1;
          break;
        case "배송준비중":
          progress = 2;
          break;
        case "배송중":
          progress = 3;
          break;
        case "배송완료":
          progress = 4;
          break;
        case "거래완료":
          progress = 5;
          break;
      }
    }

    this.setState({
      trade: trade,
      progress: progress,
    });


    if (progress === 3 && result.isShipping === true) {
      const shippingResult = await shipping(this.state.shippingCode, this.state.shippingNumber);
      if (shippingResult.completeYN === "Y") {
        await askUpdate(this.props.index, "배송완료");
        this.setState({ progress: 4 });
      }
    }
  }

  private async initShipping() {
    try {
      const result = await shippingWhere(this.props.index);
      this.setState({
        shippingCode: result.result.code,
        shippingNumber: result.result.number
      });
    } catch (error) {
      this.setState({ shippingLocation: "운송장번호에러" });
    }
  }

  private async getBank() {
    const token: any = localStorage.getItem("pair2token");

    try {
      const result = await getBank(token);
      this.setState({
        owner: result.owner,
        bank: result.bankName,
        account: result.account,
        bankState: new BankState(result.bankName, result.account, result.owner),
      });
    } catch (error) {}
  }

  private initBankList(): string[] {
    const list: string[] = [ "KB국민", "신한", "우리", "KEB하나", "케이뱅크"
    , "카카오뱅크", "KDB산업", "IBK기업", "NH농협", "수협", "대구"
    , "BNK부산", "BNK경남", "광주", "전북", "제주"
    , "한국씨티", "SC제일", "새마을금고", "우체국" ];
    return list;
  }

  private async initShippingCompanyList() {
   const result: any = await shippingCompanyList();
   const company = result["Company"];
   const arr: string[] = [];
   const arr2: string[] = [];
   arr[0] = "택배사 선택";
   arr2[0] = "택배사 선택";

   for (let i in company) {
    arr.push(company[i]["Name"]);
    arr2.push(company[i]["Code"]);
   }

   this.setState({
     shippingCompanyList: arr,
     codeList: arr2,
     shippingCompany: arr[0],
     shippingCode: arr2[0]
    });
  }

  private handleConvert = (): void => {
    const trade = this.state.trade;
    trade.isShipping = !trade.isShipping;

    this.setState({ trade: trade });
  }

  private handleCancleScreen = (): void => {
    const trade = this.state.trade;
    trade.isCancle = true;
    this.setState({ trade: trade });
  }

  private handleCancle = (): void => {
    if (window.confirm("구매요청을 취소하시겠습니까?")) {
      this.setState({});
    } else {

    }
  }

  private handleText = (e: any): void => { this.setState({ [e.target.name]: e.target.value } as any); };

  private handleSelect = (e: any): void => {
    this.setState({
      shippingCompany: e.target.value,
    });

    for (let i in this.state.shippingCompanyList) {
      if (this.state.shippingCompanyList[i] === e.target.value) {
        this.setState({ shippingCode: this.state.codeList[i] });
      }
    }
  };

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  }

  private handleSubmitCancle = (): void => {
    if (this.state.cancleType === "" || this.state.cancleDesc === "") {
      alert("사유를 선택, 입력해주세요.");
      return;
    }

    const token: any = localStorage.getItem("pair2token");

    buyRefuse({
      index: this.props.index,
      token: token,
      cancleType: this.state.cancleType,
      cancleDesc: this.state.cancleDesc,
    });

    this.setState({ modal: "none" });
    alert("신고가 접수 되었습니다. 확인 후 1~2 영업일(공휴일 제외) 내에 등록된 연락처로 PAIR2 중재팀에서 연락드립니다.");
  }

  private handleSubmitBuy = (): void => {
    this.setState({ modal: "none" });
  }

  private handleSubmitBank = async () => {
    const token = localStorage.getItem("pair2token");
    await putBank({
      token: token,
      bank: {
        bankName: this.state.bank,
        owner: this.state.owner,
        account: this.state.account,
      }
    });

    this.setState({
      modal: "none",
    });
    this.getBank();
  }

  private handleSubmitShipping = async () => {
    shippingCode(this.props.index, this.state.shippingCode, this.state.shippingNumber, "요청사항");
    await askUpdate(this.props.index, "배송중");

    this.setState({ modal: "none" });
    this.initData();
  }

  private handleSubmitPay = async () => {
    const token = localStorage.getItem("pair2token");
    const data = {
      token: token,
      amount: this.state.trade.bidPrice,
      tradeIndex: this.state.trade.index,
      tradeName: this.state.trade.name,
      tradeSize: this.state.trade.size,
      smsAgree: this.state.smsCheck
    };

    await tradeUpdate(data);

    this.setState({
      modal: "none",
      paid: true,
    });
  }

  private verifyBank(): boolean {
    if (this.state.bank === "") return false;
    if (this.state.owner === "") return false;
    if (this.state.account === "") return false;

    return true;
  }

  private verifyShipping(): boolean {
    if (this.state.shippingCompany === "" || this.state.shippingCompany === "택배사 선택") return false;
    if (this.state.shippingNumber === "") return false;

    return true;
  }

  private shippingCompany(): string {
    let name = "";
    for (const i in this.state.shippingCompanyList) {
      if (this.state.codeList[i] === this.state.shippingCode) name = this.state.shippingCompanyList[i];
    }

    return name;
  }

  public render() {
    return (
      <>
      {!this.state.trade.isCancle && this.state.trade.isShipping && !this.props.complete && <this.tradeShipping />}
      {!this.state.trade.isCancle && !this.state.trade.isShipping && !this.props.complete && <this.tradeDirect />}
      {this.state.trade.isCancle && this.props.complete && <this.tradeCancle />}
      {!this.state.trade.isCancle && this.props.complete && <this.tradeComplete />}
      </>
    );
  }

  private tradeDirect = () => {
    return (
      <>
      {/* <p>테스트버튼</p>
      <button onClick={this.handleConvert} className="btn-custom">직거래/택배 전환</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 2 })}>직거래대기모드</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 3 })}>거래완료모드</button>
      <button className="btn-custom" onClick={this.handleCancleScreen}>취소화면</button> */}


      <p className="gray-3 p-14 mt-4">요청일
      <span className="roboto black-2 bold ml-2 mr-2">{timeFormat2(this.state.trade.bidTimestamp)}</span>
      <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호 {this.state.trade.index}</p>

      <div className="bg-gray mt-2 pt-3 pb-5">
      <div className="buy-step mt-4 mb-4">
      {this.state.progress < 2 ?
      <div className={`box-step ${this.state.progress === 1 && "box-step-active"}`}>1
        {this.state.progress === 1 ? <p className="step-p2">거래요청수락</p> : <p className="step-p2 gray-4 normal">거래요청수락</p>}
      </div> :
      <div className="step-img"><img src={imgComplete} /><p className="step-p2 mt-1">거래요청수락</p></div>}
      <div className="line-buy-step" />
      {this.state.progress < 3 ?
      <div className={`box-step ${this.state.progress === 2 && "box-step-active"}`}>2
      {this.state.progress === 2 ? <><p className="step-p4">직거래대기</p><img src={paycomplete} alt="" className="img-paycomplete" /></>
      : <p className="step-p4 gray-4 normal">직거래대기</p>}
      </div> :
      <div className="step-img">
        <img src={imgComplete} />
        <p className="step-p4 mt-1">직거래대기</p>
        <img src={paycomplete} alt="" className="img-paycomplete" />
      </div>}
      <div className="line-buy-step" />
      <div className={`box-step ${this.state.progress === 3 && "box-step-active"}`}>3
        {this.state.progress === 3 ? <p className="step-p3">거래완료</p> : <p className="step-p4 gray-4 normal">거래완료</p>}
      </div>
      </div>
      </div>

      <div className="row">
      <div className="col-6">
      <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
      </div>
      <div className="col-6 text-right">
      <Link to="/faq"><p className="gray-2 p-14 underline mt-4">1:1 문의하기</p></Link>
      </div>
      </div>

      <hr className="hr2 mb-0 mt-2" />
      <div className="row border-bottom mt-0 pt-3 pb-3">
      <div className="col-md-2 col-4 pl-0 pr-0 text-center">
        <div className="stuff-info" style={{backgroundImage: `url(${this.state.trade.imageUrl})`}} />
      </div>
      <div className="col-md-5 col-8 pl-1">
        <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
        <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
        <p className="blue-2 roboto bold mt-2 p-14">{currency(this.state.trade.price)}<span className="p-13 normal">원</span></p>
      </div>
      <div className="col-md-2 text-center pt-4 hidden-m">
        {this.state.progress === 1 && <p className="gray-2 p-14">거래요청중</p>}
        {this.state.progress === 2 && <p className="gray-2 p-14">직거래대기</p>}
      </div>
      <div className="offset-4 mt-2 pl-1 hidden-p">
        {this.state.progress === 1 && <p className="gray-2 p-14 bold">거래요청중</p>}
        {this.state.progress === 2 && <p className="gray-2 p-14 bold">직거래대기</p>}
      </div>
      <div className="col-md-3 text-center">
      {this.state.progress === 1 &&
      <button onClick={this.handleCancle} className="btn-custom-sm mt-3">거래요청취소</button>}
      {this.state.progress === 2 &&
      <button onClick={() => this.setState({ modal: "cancle" })} className="btn-custom-sm mt-3 m-w100">거래파기신고</button>}
      </div>
      </div>

      <p className="black-2 bold p-16 mt-4">구매자 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-6 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
        <div className="row">
        <div className="col-6">
          <p className="black-2 p-14 bold">{this.state.trade.buyer}</p>
          <p className="gray-3 roboto p-14 mt-1">
          {this.state.progress === 1 && "010-****-****"}
          {this.state.progress === 2 && parsePhone(this.state.trade.buyerPhone)}
          </p>
        </div>
        <div className="col-6 text-right">
          {env === "pc" && this.state.progress === 2 && <button className="btn-custom-sm-disable btn-xs mt-3">연락하기</button>}
          {env === "app" && this.state.progress === 2 && <a href={`/sms/${this.state.trade.buyerPhone}`} target="_new"><button className="btn-custom-sm btn-xs mt-3">연락하기</button></a>}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      <div className="col-md-6 pt-4 pb-4 m-pt2 m-pb2">
        <p className="black-2 p-14 bold">본인인증</p>
        <div className="row">
        <div className="col-6">
        <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 휴대폰</p>
        </div>
        <div className="col-6 text-right">
        <p className="p-14 bold orange-3 mt-2">OK</p>
        </div>
        <div className="col-6">
        {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
        </div>
        <div className="col-6 text-right">
        {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      </div>
      <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" />
      {this.state.progress === 1 && "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
      {this.state.progress > 1 && "구매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
      </p>

      <p className="black-2 bold p-16 mt-4">정산 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-4 border-right-gray pt-4 pb-4 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1">판매금액</p>
        <p className="p-14 gray-2 mt-2 hidden-p">판매가</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 black-2 bold roboto mr-2">{currency(this.state.trade.bidPrice)}<span className="noto p-14 normal">원</span></p>
        <p className="p-14 gray-2 mt-2 mr-2 hidden-p">{currency(this.state.trade.bidPrice)}<span className="normal">원</span></p>
        <img src={plus} alt="" className="mypage-plus hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p">
        <img src={plus} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1 ml-1">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 black-2 mt-1 ml-1">수수료 할인({this.state.sale}%)</p>
        <p className="p-14 gray-2 hidden-p">수수료할인 ({this.state.sale}%)</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 black-2 roboto bold mr-2">-{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-14 normal">원</span></p>
        <p className="p-18 black-2 roboto bold mr-2">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-14 normal">원</span></p>
        <img src={equal} alt="" className="mypage-plus2 hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p w-100">
        <img src={equal} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 pt-4 pb-4 m-pt2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 orange bold mt-1 ml-2">정산예정금액</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 orange bold roboto">
        {currency(this.state.trade.price - Math.round( ((this.state.trade.price * (this.state.commision * .01))) - (this.state.trade.price * (this.state.sale * .01)) ))}
        <span className="noto p-14 normal">원</span></p>
      </div>
      </div>
        <p className="p-13 gray-2 mt-2 float-left hidden-p">
        <img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}
        </p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>
      <hr className="hr4 hidden-p mt-1 mb-0" />

      <div className="row mt-0 border-bottom hidden-m">
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-6">
        <p className="p-14 gray-3 mt-1">제품금액</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price)}<span className="noto p-normal">원</span></p>
      </div>
      </div>
      </div>
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-8 pr-0">
        <p className="p-14 gray-3 mt-2">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 gray-3 mt-2">수수료할인({this.state.sale}%)</p>
      </div>
      <div className="col-4 text-right pl-0">
        <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-normal">원</span></p>
        <p className="p-14 gray-3 mt-2 mr-2 roboto">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-normal">원</span></p>
        {/* <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price - Math.round( ((this.state.trade.price * (this.state.commision * .01))) - (this.state.trade.price * (this.state.sale * .01)) ))}<span className="noto p-normal">원</span></p> */}
      </div>
      </div>
      </div>
      <div className="col-md-4 pt-3 pb-4">
        <p className="p-13 gray-2 mt-2 float-left"><img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}</p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>

      {this.state.modal === "cancle" && <this.modalCancle />}
      {this.state.modal === "buyconfirm" && <this.buyConfirmModal />}
      {this.state.modal === "bank" && <this.ModalBank />}
      </>
    );
  }

  private modalCancle = () => {
    return (
      <div className="modal-custom modal-full" style={{display: "block"}}>
      <div className="modal-content-small">
      <div className="p-3">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">거래파기신고</p>
      </div>
      <div className="col-6 text-right">
        <img onClick={() => this.setState({ modal: "none" })} src={imgClose} alt="" className="cursor" />
      </div>
        <div className="col-12"><div className="border-bottom-black mt-3" /></div>
      </div>

      <p className="p-13 gray-2 mt-3">
        <img src={bullet} alt="" className="mr-2" />상대방의 귀책사유로 인해 일방적인 거래파기가 되었을때 신고를 해주세요. 사유를 명확히 입력해 주시면 저희가 상황을 파악한 후에 빠르게 조치해드리도록 하겠습니다.
      </p>
      <p className="p-13 gray-2 mt-2">
        <img src={bullet} alt="" className="mr-2" />추가 문의사항은 고객센터 1:1 문의 또는 070-4256-0099 로 연락주세요.
      </p>
      {/* <p className="p-13 gray-2 mt-2">
        <img src={bullet} alt="" className="mr-2" />추가 문의사항은 고객센터 1:1 문의 또는 070-4256-0099 로 연락주세요.
      </p> */}

      <p className="p-14 black-2 mt-4">거래취소 사유를 선택해주세요.</p>
      <button onClick={() => this.setState({ cancleType: "배송지연(결제 이후 +2영업일, 공휴일 제외)" })}
      className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${this.state.cancleType === "배송지연(결제 이후 +2영업일, 공휴일 제외)" && "btn-active-sm-border"}`}>
        배송지연(결제 이후 +2영업일, 공휴일 제외)
        {this.state.cancleType === "배송지연(결제 이후 +2영업일, 공휴일 제외)" && <img src={check} alt="" className="float-right mr-3 mt-1" />}
      </button>
      <button onClick={() => this.setState({ cancleType: "직거래 노쇼" })}
      className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${this.state.cancleType === "직거래 노쇼" && "btn-active-sm-border"}`}>
        직거래 노쇼
        {this.state.cancleType === "직거래 노쇼" && <img src={check} alt="" className="float-right mr-3 mt-1" />}
      </button>
      <button onClick={() => this.setState({ cancleType: "가품 의심" })}
      className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${this.state.cancleType === "가품 의심" && "btn-active-sm-border"}`}>
        가품 의심
        {this.state.cancleType === "가품 의심" && <img src={check} alt="" className="float-right mr-3 mt-1" />}
      </button>
      <button onClick={() => this.setState({ cancleType: "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)" })}
      className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${this.state.cancleType === "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)" && "btn-active-sm-border"}`}>
        제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)
        {this.state.cancleType === "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)" && <img src={check} alt="" className="float-right mr-3 mt-1" />}
      </button>
      <button onClick={() => this.setState({ cancleType: "기타(이 경우 보상을 받을 수 없습니다)" })}
      className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${this.state.cancleType === "기타(이 경우 보상을 받을 수 없습니다)" && "btn-active-sm-border"}`}>
        기타(이 경우 보상을 받을 수 없습니다)
        {this.state.cancleType === "기타(이 경우 보상을 받을 수 없습니다)" && <img src={check} alt="" className="float-right mr-3 mt-1" />}
      </button>

      <p className="p-13 gray-2 mt-3">
        <img src={bullet} alt="" className="mr-2" />구체적인 사유를 입력해주세요.
      </p>
      <textarea rows={4} onChange={this.handleText} name="cancleDesc" value={this.state.cancleDesc} maxLength={50} className="form-control textarea-custom mt-1"
        placeholder="제품교환에 대한 기타 특이사항이 있다면 50자 내로 입력해 주세요." />
      <p className="text-right p-13 gray-3 mt-1">{this.state.cancleDesc.length}/50</p>

      <div className="text-center">
        <button onClick={this.handleSubmitCancle} className="btn-custom-sm-active mt-3">신고</button>
      </div>
      </div>
      </div>
      </div>
    );
  } // cancle bidding to change

  private buyConfirmModal = () => {
    return (
      <div className="modal-custom modal-full" style={{display: "block"}}>
      <div className="modal-content-small">
      <div className="p-3">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">구매확정</p>
      </div>
      <div className="col-6 text-right">
        <img onClick={() => this.setState({ modal: "none" })} src={imgClose} alt="" className="cursor" />
      </div>
        <div className="col-12"><div className="border-bottom-black mt-3" /></div>
      </div>

      <div className="text-center">
      <p className="p-22 orange mt-5 mb-2">구매확정을 진행해주세요.</p>
      <p className="p-14 gray-2">구매확정 이후에는 환불/교환 신청이 불가합니다.</p> 
      </div>

      <hr className="hr-2 mt-5" />

      <div className="row">
      <div className="col-3 text-center">
      <div className="confirm-img" style={{backgroundImage: `url(${this.state.trade.imageUrl})`}} />
      </div>
      <div className="col-9">
      <p className="p-14 gray-3">{this.state.trade.name}</p>
      <p className="p-14 gray-2 roboto">{this.state.trade.size} mm</p>
      </div>
      </div>

      <div className="container mt-4">
      <div className="row">
      <div onClick={() => this.setState({ completeComment: "만족" })}
      className={`col-4 text-center border-confirmbox cursor ${this.state.completeComment === "만족" && "confirmbox-active"}`}>
        <img src={this.state.completeComment === "만족" ? goodActive : good} alt=""/>
        <p className={`p-14 black-2 mt-1 ${this.state.completeComment === "만족" && "orange bold"}`}>만족</p>
      </div>
      <div onClick={() => this.setState({ completeComment: "보통" })}
      className={`col-4 text-center border-confirmbox cursor ${this.state.completeComment === "보통" && "confirmbox-active"}`}>
        <img src={this.state.completeComment === "보통" ? normalActive : normal} alt=""/>
        <p className={`p-14 black-2 mt-1 ${this.state.completeComment === "보통" && "orange bold"}`}>보통</p>
      </div>
      <div onClick={() => this.setState({ completeComment: "불만족" })}
      className={`col-4 text-center border-confirmbox cursor ${this.state.completeComment === "불만족" && "confirmbox-active"}`}>
        <img src={this.state.completeComment === "불만족" ? badActive : bad} alt=""/>
        <p className={`p-14 black-2 mt-1 ${this.state.completeComment === "불만족" && "orange bold"}`}>불만족</p>
      </div>
      </div>
      </div>

      <div className="text-center mt-5 mb-3">
        <button onClick={() => this.setState({ modal: "none"})} className="btn-custom-sm mr-1">취소</button>
        <button onClick={this.handleSubmitBuy} className="btn-custom-sm-active">구매확정</button>
      </div>
      </div>
      </div>
      </div>
    );
  } // cancle bidding to change

  private tradeShipping = () => {
    return (
      <>
      {/* <p>테스트버튼</p>
      <button onClick={this.handleConvert} className="btn-custom">직거래/택배 전환</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 1 })}>거래요청</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 2 })}>배송준비중</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 3 })}>배송중</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 4 })}>배송완료</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 5 })}>거래완료</button>
      <button className="btn-custom" onClick={this.handleCancleScreen}>취소화면</button> */}

      <p className="gray-3 p-14 mt-4">요청일
      <span className="roboto black-2 bold ml-2 mr-2">{timeFormat2(this.state.trade.bidTimestamp)}</span>
      <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호 {this.state.trade.index}</p>

      <div className="bg-gray mt-2 pt-3 pb-5">
      <div className="buy-step mt-4 mb-4">
      {this.state.progress < 2 ?
      <div className={`box-step ${this.state.progress === 1 && "box-step-active"}`}>1
        {this.state.progress === 1 ? <p className="step-p4">거래요청중</p> : <p className="step-p4 gray-4 normal">거래요청중</p>}
      </div> :
      <div className="step-img"><img src={imgComplete} /><p className="step-p4 mt-1">거래요청중</p></div>}

      <div className="mypagebuy-step" />
      {this.state.progress < 3 ?
      <div className={`box-step ${this.state.progress === 2 && "box-step-active"}`}>2
      {this.state.progress === 2 ?
      <><p className="step-p4">배송준비중</p><img src={paycomplete} alt="" className="img-paycomplete" /></> :
      <p className="step-p4 gray-4 normal">배송준비중</p>}
      </div> :
      <div className="step-img">
        <img src={imgComplete} />
        <p className="step-p4 mt-1">배송준비중</p>
        <img src={paycomplete} alt="" className="img-paycomplete" />
      </div>}

      <div className="mypagebuy-step" />
      {this.state.progress < 4 ?
      <div className={`box-step ${this.state.progress === 3 && "box-step-active"}`}>3
        {this.state.progress === 3 ? <p className="step-p5">배송중</p> : <p className="step-p5 gray-4 normal">배송중</p>}
      </div> :
      <div className="step-img"><img src={imgComplete} /><p className="step-p5 mt-1">배송중</p></div>}

      <div className="mypagebuy-step" />
      {this.state.progress < 5 ?
      <div className={`box-step ${this.state.progress === 4 && "box-step-active"}`}>4
        {this.state.progress === 4 ? <p className="step-p3">배송완료</p> : <p className="step-p3 gray-4 normal">배송완료</p>}
      </div> :
      <div className="step-img"><img src={imgComplete} /><p className="step-p3 mt-1">배송완료</p></div>}

      <div className="mypagebuy-step" />
      <div className={`box-step ${this.state.progress === 5 && "box-step-active"}`}>5
        {this.state.progress === 5 ? <p className="step-p3">거래완료</p> : <p className="step-p3 gray-4 normal">거래완료</p>}
      </div>
      </div>
      </div>

      <div className="row">
      <div className="col-6">
      <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
      </div>
      <div className="col-6 text-right">
      <Link to="/faq"><p className="gray-2 p-14 underline mt-4">1:1 문의하기</p></Link>
      </div>
      </div>
      <hr className="hr2 mb-0 mt-2" />

      <div className="row border-bottom mt-0 pt-3 pb-3">
      <div className="col-md-2 col-4 pl-0 pr-0 text-center">
        <div className="stuff-info" style={{backgroundImage: `url(${this.state.trade.imageUrl})`}} />
      </div>
      <div className="col-md-5 col-8 pl-1">
        <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
        <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
        <p className="blue-2 roboto bold mt-2 p-14">{currency(this.state.trade.price)}<span className="p-13 normal">원</span></p>
      </div>

      <div className="col-md-2 text-center hidden-m">
        {this.state.progress === 1 && <p className="gray-2 p-14 mt-4">거래요청중</p>}
        {this.state.progress === 2 && <p className="gray-2 p-14 mt-4">배송준비중</p>}
        {this.state.progress === 3 &&
          <a target="_new" href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${this.state.shippingNumber}`}>
          <p className="gray-3 p-13 bold">배송중</p>
          <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </a>}
        {this.state.progress === 4 &&
        <a target="_new" href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${this.state.shippingNumber}`}>
          <p className="gray-3 p-13 bold">배송완료</p>
          <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </a>}
      </div>

      <div className="offset-4 mt-2 pl-1 hidden-p">
        {this.state.progress === 1 && <p className="gray-3 p-14 bold">거래요청중</p>}
        {this.state.progress === 2 && <p className="gray-3 p-14 bold">배송준비중</p>}
        {this.state.progress === 3 &&
        <a target="_new" href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${this.state.shippingNumber}`}>
          <p className="gray-3 p-13 bold">배송중</p>
          <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </a>}
        {this.state.progress === 4 &&
        <a target="_new" href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${this.state.shippingNumber}`}>
          <p className="gray-3 p-13 bold">배송완료</p>
          <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </a>}
      </div>
      <div className="col-md-3 text-right">
      {this.state.progress === 1 &&
      <button onClick={this.handleCancle} className="btn-custom-sm mt-3 m-w100">거래파기신고</button>}
      {this.state.progress === 2 && <button onClick={() => this.setState({ modal: "shipping" })} className="btn-custom-sm mt-3 m-w100">운송장번호 입력</button>}
      </div>
      </div>

      <p className="black-2 bold p-16 mt-4">구매자 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-6 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
        <div className="row">
        <div className="col-6">
          <p className="black-2 p-14 bold">{this.state.trade.buyer}</p>
          <p className="gray-3 roboto p-14 mt-1">{parsePhone(this.state.trade.buyerPhone)}</p>
        </div>
        <div className="col-6 text-right">
          {env === "pc" ?
          <button className="btn-custom-sm-disable btn-xs mt-3">연락하기</button> :
          <a href={`/sms/${this.state.trade.buyerPhone}`} target="_new"><button className="btn-custom-sm btn-xs mt-3">연락하기</button></a>}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      <div className="col-md-6 pt-4 pb-4 m-pt2 m-pb2">
        <p className="black-2 p-14 bold">본인인증</p>
        <div className="row">
        <div className="col-6">
        <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 휴대폰</p>
        </div>
        <div className="col-6 text-right">
        <p className="p-14 bold orange-3 mt-2">OK</p>
        </div>
        <div className="col-6">
        {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
        </div>
        <div className="col-6 text-right">
        {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      </div>
      <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" />
      {this.state.progress === 1 && "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
      {this.state.progress > 1 && "구매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
      </p>

      <p className="black-2 bold p-16 mt-4">배송지 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-7 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
        <div className="row">
        <div className="col-4">
        <p className="gray-3 p-14 bold">수령인</p>
        <p className="gray-2 p-14 mt-2">연락처</p>
        <p className="gray-2 p-14 h-shipping mt-2">배송지</p>
        <p className="gray-2 p-14">배송요청사항</p>
        </div>
        <div className="col-8 text-right">
        <p className="gray-3 p-14 bold">{this.state.trade.shippingName}</p>
        <p className="gray-2 p-14 mt-2">{parsePhone(this.state.trade.shippingPhone)}</p>
        <p className="gray-2 p-14 h-shipping mt-2">{this.state.trade.shippingLocation}</p>
        <p className="gray-2 p-14">{this.state.trade.shippingRequest}</p>
        </div>
        </div>
      </div>

      <div className="col-md-5 pt-4 pb-4 m-pt2 m-pb2">
      <hr className="hr4 hidden-p mt-0" />
      <div className="row">
        <div className="col-4">
        <p className="gray-3 p-14 bold">구매자</p>
        <p className="gray-2 p-14 mt-2">연락처</p>
        <p className="gray-2 p-14 h-shipping mt-2">이메일</p>
        </div>
        <div className="col-8 text-right">
        <p className="gray-3 p-14 bold">{this.state.trade.buyer}</p>
        <p className="gray-2 p-14 mt-2">{parsePhone(this.state.trade.buyerPhone)}</p>
        <p className="gray-2 p-14 h-shipping mt-2">{this.state.trade.buyerEmail}</p>
        </div>
        </div>
        <hr className="hr4 hidden-p mt-0" />
      </div>
      </div>

      <p className="black-2 bold p-16 mt-4 m-mt0">정산 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-4 border-right-gray pt-4 pb-4 pb-4 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1">판매금액</p>
        <p className="p-14 gray-2 mt-2 hidden-p">판매가</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 black-2 bold roboto mr-2">{currency(this.state.trade.bidPrice)}<span className="noto p-14 normal">원</span></p>
        <p className="p-14 gray-2 mt-2 mr-2 hidden-p">{currency(this.state.trade.bidPrice)}<span className="normal">원</span></p>
        <img src={plus} alt="" className="mypage-plus hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p">
        <img src={plus} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1 ml-1">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 black-2 mt-1 ml-1">수수료 할인({this.state.sale}%)</p>
        <p className="p-14 gray-2 hidden-p">수수료할인 ({this.state.sale}%)</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 black-2 roboto bold mr-2">-{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-14 normal">원</span></p>
        <p className="p-18 black-2 roboto bold mr-2">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-14 normal">원</span></p>
        <img src={equal} alt="" className="mypage-plus2 hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p w-100">
        <img src={equal} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 pt-4 pb-4 m-pt2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 orange bold mt-1 ml-2">정산예정금액</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 orange bold roboto">
        {/* {currency(this.state.trade.bidPrice - (this.state.trade.bidPrice * 0.07))} */}
        {currency(this.state.trade.bidPrice)}
        <span className="noto p-14 normal">원</span></p>
      </div>
      </div>
        <p className="p-13 gray-2 mt-2 float-left hidden-p"><img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}</p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>
      <hr className="hr4 hidden-p mt-1 mb-0" />

      <div className="row mt-0 border-bottom hidden-m">
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-6">
        <p className="p-14 gray-3 mt-2">제품금액</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price)}<span className="noto p-normal">원</span></p>
      </div>
      </div>
      </div>
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-8 pr-0">
      <p className="p-14 gray-3 mt-2">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 gray-3 mt-2">수수료할인({this.state.sale}%)</p>
      </div>
      <div className="col-4 text-right pl-0">
      <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-normal">원</span></p>
        <p className="p-14 gray-3 mt-2 mr-2 roboto">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-normal">원</span></p>
        {/* <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price - Math.round( ((this.state.trade.price * (this.state.commision * .01))) - (this.state.trade.price * (this.state.sale * .01)) ))}<span className="noto p-normal">원</span></p> */}
      </div>
      </div>
      </div>
      <div className="col-md-4 pt-3 pb-4">
        <p className="p-13 gray-2 mt-2 float-left"><img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}</p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>

      {this.state.modal === "cancle" && <this.modalCancle />}
      {this.state.modal === "bank" && <this.ModalBank />}
      {this.state.modal === "shipping" && <this.ModalShipping />}
      </>
    );
  }

  private ModalBank = () => {
    return (
      <div className="modal-custom modal-full" style={{display: this.state.modal === "bank" ? "block" : "none"}}>
      <div className="modal-content-small">
      <div className="p-3">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">환급계좌 등록</p>
      </div>
      <div className="col-6 text-right">
        <img onClick={() => this.setState({ modal: "none" })} src={imgClose} alt="" className="cursor" />
      </div>
      </div>
      <hr className="hr2" />
      <p className="p-13 gray-2"><img src={bullet} alt="" className="mr-1" />
        본인명의의 계좌만 등록할 수 있습니다.
      </p>
      <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-1" />
        휴대폰 번호 등으로 만든 평생계좌번호 및 가상계좌, 펀드/적금/정기예금 등에는 계좌는 등록할 수 없습니다.
      </p>

      <p className="p-14 black-2 mt-4 mb-2">은행</p>
      <select onChange={this.handleText} value={this.state.bank} className="form-control form-select" name="bank">
      <option value="" disabled>은행을 선택해 주세요.</option>
      {this.state.bankList.map((bank: string, i: number) => <option key={i}>{bank}</option>)}
      </select>

      <p className="p-14 black-2 mt-4 mb-2">계좌번호</p>
      <input className="form-control form-custom" value={this.state.account} type="text" name="account" onChange={this.handleChange} />

      <p className="p-14 black-2 mt-4 mb-2">예금주</p>
      <input className="form-control form-custom" value={this.state.owner} type="text" name="owner" maxLength={20} onChange={this.handleText} />
      </div>

      <div className="bottom-box p-3">
      {this.verifyBank() ?
        <button onClick={this.handleSubmitBank} className="btn-custom-active w-100">등록하기</button> :
        <button className="btn-custom-disable w-100">등록하기</button>}
      </div>
      </div>
      </div>
    );
  }

  private ModalShipping = () => {
    return (
      <div className="modal-custom modal-full" style={{display: this.state.modal === "shipping" ? "block" : "none"}}>
      <div className="modal-content-small">
      <div className="p-3">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">운송장번호 입력</p>
      </div>
      <div className="col-6 text-right">
        <img onClick={() => this.setState({ modal: "none" })} src={imgClose} alt="" className="cursor" />
      </div>
      </div>
      <hr className="hr2" />
      <p className="p-13 gray-2 mb-2"><img src={bullet} alt="" className="mr-1" />
        이용하신 택배사와 송장번호를 입력해 주세요.
      </p>

      <select onChange={this.handleSelect} value={this.state.shippingCompany} className="form-control form-select" name="shippingCompany">
        {/* <option value="" disabled>택배사 선택</option> */}
        {this.state.shippingCompanyList.map((company: string, i: number) => <option key={i}>{company}</option>)}
      </select>

      <input onChange={this.handleText} className="form-control form-custom mt-2" value={this.state.shippingNumber}
      placeholder={"송장번호를 입력해 주세요.(숫자만 입력)"} type="text" name="shippingNumber" maxLength={20}  />
      </div>

      <div className="bottom-box text-center p-3">
      {this.verifyShipping() ?
      <button onClick={this.handleSubmitShipping} className="btn-custom-sm-active">확인</button> :
      <button className="btn-custom-sm-disable">확인</button>}
      </div>
      </div>
      </div>
    );
  }

  private tradeComplete = () => {
    return (
      <>
      {!this.state.paid ?
      <>
      <p className="gray-3 p-14 mt-4">요청일
      <span className="roboto black-2 bold ml-2 mr-2">{timeFormat2(this.state.trade.bidTimestamp)}</span>
      <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호 {this.state.trade.index}</p>

      {this.state.trade.isShipping ?
      <div className="bg-gray mt-2 pt-3 pb-5">
      <div className="buy-step mt-4 mb-4">
      <div className="step-img"><img src={imgComplete} /><p className="step-p4 mt-1">거래요청중</p></div>
      <div className="mypagebuy-step" />
      <div className="step-img"><img src={imgComplete} /><p className="step-p5 mt-1">배송중</p></div>
      <div className="mypagebuy-step" />
      <div className="step-img"><img src={imgComplete} /><p className="step-p3 mt-1">배송완료</p></div>
      <div className="mypagebuy-step" />
      <div className="step-img"><img src={imgComplete} /><p className="step-p3 mt-1">거래완료</p>
      </div>
      </div>
      </div>
      :
      <div className="bg-gray mt-2 pt-3 pb-5">
      <div className="buy-step mt-4 mb-4">
      <div className="step-img"><img src={imgComplete} /><p className="step-p2 mt-1">거래요청수락</p></div>
      <div className="line-buy-step" />
      <div className="step-img">
        <img src={imgComplete} />
        <p className="step-p4 mt-1">직거래대기</p>
        <img src={paycomplete} alt="" className="img-paycomplete" />
      </div>
      <div className="line-buy-step" />
      <div className="step-img"><img src={imgComplete} /><p className="step-p3">거래완료</p></div>
      </div>
      </div>}

      <div className="row">
      <div className="col-6">
      <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
      </div>
      <div className="col-6 text-right">
      <Link to="/faq"><p className="gray-2 p-14 underline mt-4">1:1 문의하기</p></Link>
      </div>
      </div>

      <hr className="hr2 mb-0 mt-2" />
      <div className="row border-bottom mt-0 pt-3 pb-3">
      <div className="col-md-2 col-4 pl-0 pr-0 text-center">
        <div className="stuff-info pt-4" style={{backgroundImage: `url(${this.state.trade.imageUrl})`}}>
        <div className="box-trade-cancle">거래완료</div>
        </div>
      </div>
      <div className="col-md-4 col-8 pl-1">
        <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
        <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
        <p className="blue-2 roboto bold mt-2 p-14">{currency(this.state.trade.price)}<span className="p-13 normal">원</span></p>
      </div>
      <div className="col-md-3 text-center hidden-m">
        <p className="gray-2 p-14 mt-4">거래완료</p>
        {/* <form target="_blank" action="http://info.sweettracker.co.kr/tracking/5" method="post">
        <div className="d-none">
          <input type="text" className="form-control" id="t_key" name="t_key" value="plGP8DagkGlOmydpqvVPYw" placeholder="제공받은 APIKEY" />
          <input type="text" className="form-control" name="t_code" id="t_code" value={this.state.shippingCode} placeholder="택배사 코드" />
          <input type="text" className="form-control" name="t_invoice" id="t_invoice"  value={this.state.shippingNumber}  placeholder="운송장 번호" />
        </div>
        <button type="submit" className="btn pt-0 pb-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </form> */}
      </div>
      <div className="offset-4 pl-1 hidden-p">
        <p className="gray-2 p-14">거래완료</p>
        {/* <form target="_blank" action="http://info.sweettracker.co.kr/tracking/5" method="post">
        <div className="d-none">
          <input type="text" className="form-control" id="t_key" name="t_key" value="plGP8DagkGlOmydpqvVPYw" placeholder="제공받은 APIKEY" />
          <input type="text" className="form-control" name="t_code" id="t_code" value={this.state.shippingCode} placeholder="택배사 코드" />
          <input type="text" className="form-control" name="t_invoice" id="t_invoice"  value={this.state.shippingNumber}  placeholder="운송장 번호" />
        </div>
        <button type="submit" className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">{this.shippingCompany()} {this.state.shippingNumber}</button>
        </form> */}
      </div>
      <div className="col-md-3 text-right">
      {this.state.trade.paidProgress === 0 && <button onClick={() => this.setState({ modal: "pay" })} className="btn-custom-sm mt-3 m-w100">정산하기</button>}
      {this.state.trade.paidProgress === 1 && <button className="btn-custom-sm mt-3 m-w100">정산중</button>}
      {this.state.trade.paidProgress === 2 && <button className="btn-custom-sm mt-3 m-w100">정산완료</button>}
      </div>
      </div>

      <p className="black-2 bold p-16 mt-4">구매자 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-6 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
      <div className="col-md-6">
        <p className="black-2 p-14 bold">{this.state.trade.buyer}</p>
        <p className="gray-3 roboto p-14 mt-1">{parsePhone(this.state.trade.buyerPhone)}</p>
      </div>
      <hr className="hr4 mb-0 hidden-p" />
      </div>

      <div className="col-md-6 pt-4 pb-4 m-pt2 m-pb2 pl-4 pr-4">
        <p className="black-2 p-14 bold">본인인증</p>
        <div className="row">
        <div className="col-6">
        <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 휴대폰</p>
        </div>
        <div className="col-6 text-right">
        <p className="p-14 bold orange-3 mt-2">OK</p>
        </div>
        <div className="col-6">
        {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
        </div>
        <div className="col-6 text-right">
        {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      </div>
      <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" />
      {this.state.progress === 1 && "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
      {this.state.progress > 1 && "구매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
      </p>

      <p className="black-2 bold p-16 mt-4">배송지 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-7 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
        <div className="row">
        <div className="col-4">
        <p className="gray-3 p-14 bold">수령인</p>
        <p className="gray-2 p-14 mt-2">연락처</p>
        <p className="gray-2 p-14 h-shipping mt-2">배송지</p>
        <p className="gray-2 p-14">배송요청사항</p>
        </div>
        <div className="col-8 text-right">
        <p className="gray-3 p-14 bold">{this.state.trade.shippingName}</p>
        <p className="gray-2 p-14 mt-2">{parsePhone(this.state.trade.shippingPhone)}</p>
        <p className="gray-2 p-14 h-shipping mt-2">{this.state.trade.shippingLocation}</p>
        <p className="gray-2 p-14">{this.state.trade.shippingRequest}</p>
        </div>
        </div>
      </div>

      <div className="col-md-5 pt-4 pb-4 m-pt2 m-pb2">
      <hr className="hr4 hidden-p mt-0" />
      <div className="row">
        <div className="col-4">
        <p className="gray-3 p-14 bold">구매자</p>
        <p className="gray-2 p-14 mt-2">연락처</p>
        <p className="gray-2 p-14 h-shipping mt-2">이메일</p>
        </div>
        <div className="col-8 text-right">
        <p className="gray-3 p-14 bold">{this.state.trade.buyer}</p>
        <p className="gray-2 p-14 mt-2">{parsePhone(this.state.trade.buyerPhone)}</p>
        <p className="gray-2 p-14 h-shipping mt-2">{this.state.trade.buyerEmail}</p>
        </div>
        </div>
        <hr className="hr4 hidden-p mt-0" />
      </div>
      </div>

      <p className="black-2 bold p-16 mt-4">정산 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
      <div className="col-md-4 border-right-gray pt-4 pb-4 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1">판매금액</p>
        <p className="p-14 gray-2 mt-2 hidden-p">판매가</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 black-2 bold roboto mr-2">{currency(this.state.trade.bidPrice)}<span className="noto p-14 normal">원</span></p>
        <p className="p-14 gray-2 mt-2 mr-2 hidden-p">{currency(this.state.trade.bidPrice)}<span className="normal">원</span></p>
        <img src={plus} alt="" className="mypage-plus hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p">
        <img src={plus} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 black-2 mt-1 ml-1">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 black-2 mt-1 ml-1">수수료 할인({this.state.sale}%)</p>
        <p className="p-14 gray-2 hidden-p">수수료할인 ({this.state.sale}%)</p>
      </div>
      <div className="col-6 text-right">
      <p className="p-18 black-2 roboto bold mr-2">-{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-14 normal">원</span></p>
        <p className="p-18 black-2 roboto bold mr-2">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-14 normal">원</span></p>
        <img src={equal} alt="" className="mypage-plus2 hidden-m" />
      </div>
      </div>
      <div className="text-center hidden-p w-100">
        <img src={equal} alt="" className="mypage-plus m-plus" />
        <hr className="hr4 mb-0" />
      </div>
      </div>

      <div className="col-md-4 pt-4 pb-4 m-pt2">
      <div className="row">
      <div className="col-6">
        <p className="p-14 orange bold mt-1 ml-2">정산예정금액</p>
      </div>
      <div className="col-6 text-right">
      <p className="p-18 orange bold roboto">
        {currency(this.state.trade.price - Math.round( ((this.state.trade.price * (this.state.commision * .01))) - (this.state.trade.price * (this.state.sale * .01)) ))}
        <span className="noto p-14 normal">원</span></p>
      </div>
      </div>
        <p className="p-13 gray-2 mt-2 float-left hidden-p"><img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}</p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>
      <hr className="hr4 hidden-p mt-1 mb-0" />

      <div className="row mt-0 border-bottom hidden-m">
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-6">
        <p className="p-14 gray-3 mt-2">제품금액</p>
      </div>
      <div className="col-6 text-right">
      <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price)}<span className="noto p-normal">원</span></p>
      </div>
      </div>
      </div>
      <div className="col-md-4 border-right-gray pt-3 pb-4">
      <div className="row">
      <div className="col-8 pr-0">
        <p className="p-14 gray-3 mt-2">결제 수수료({this.state.commision}%)</p>
        <p className="p-14 gray-3 mt-2">수수료할인({this.state.sale}%)</p>
      </div>
      <div className="col-4 text-right pl-0">
      <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(Math.round(this.state.trade.price * (this.state.commision * .01)))}<span className="noto p-normal">원</span></p>
        <p className="p-14 gray-3 mt-2 mr-2 roboto">+{currency(Math.round(this.state.trade.price * (this.state.sale * .01)))}<span className="noto p-normal">원</span></p>
        {/* <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(this.state.trade.price - Math.round( ((this.state.trade.price * (this.state.commision * .01))) - (this.state.trade.price * (this.state.sale * .01)) ))}<span className="noto p-normal">원</span></p> */}
      </div>
      </div>
      </div>
      <div className="col-md-4 pt-3 pb-4">
        <p className="p-13 gray-2 mt-2 float-left"><img src={bullet} alt="" className="mr-2" />정산계좌: {this.state.bankState.bankName}/{this.state.bankState.account}</p>
        <p onClick={() => this.setState({ modal: "bank"})} className="p-13 gray-2 mt-2 underline cursor float-right">수정</p>
      </div>
      </div>
      </>
      :
      <div className="text-center mt-6 mb-6">
      <p className="p-22 black-2">정산신청이 완료되었습니다.</p>
      <p className="p-14 gray-2 mt-2">정산신청 시에 신청일 기준 +1영업일 이내로 회원님의 계좌로 입금됩니다. <br />(주말/공휴일 제외)</p>
      <button onClick={() => window.location.reload()} className="btn-custom-sm mt-4">확인</button>
      </div>}
      <this.ModalPay />
      <this.ModalBank />
      </>
    );
  }

  private ModalPay = () => {
    return (
      <div className="modal-custom modal-full" style={{display: this.state.modal === "pay" ? "block" : "none"}}>
      <div className="modal-content-small">
      <div className="p-3">
      <div className="row">
      <div className="col-6">
        <p className="p-16 bold black-2">정산</p>
      </div>
      <div className="col-6 text-right">
        <img onClick={() => this.setState({ modal: "none" })} src={imgClose} alt="" className="cursor" />
      </div>
      </div>
      <hr className="hr2" />
      <p className="p-13 gray-2 mb-2"><img src={bullet} alt="" className="mr-1" />
        계좌등록 및 수정은 [마이페이지 > 카드/계좌관리]에서 수정 가능합니다.
      </p>
      <p className="p-13 gray-2 mb-2"><img src={bullet} alt="" className="mr-1" />
        정산신청 시에 신청일 기준 +1영업일 이내로 회원님의 계좌로 입금됩니다.(주말/공휴일 제외)
      </p>

      <hr className="hr4 mt-4" />
      <div className="row">
      <div className="col-6">
      <p className="p-14 gray-3 bold">정산예정금액</p>
      </div>
      <div className="col-6 text-right">
        <p className="p-18 orange bold roboto">{currency(this.state.trade.bidPrice)}<span className="noto p-14 normal">원</span></p>
      </div>
      </div>
      <hr className="hr4" />

      <div className="bg-gray mt-3">
      <div className="row p-3">
      <div className="col-3">
        <p className="p-14 black-2">정산계좌</p>
      </div>
      <div className="col-9 text-right">
      <p className="p-14 gray-2">{this.state.bankState.onwer} / {this.state.bankState.bankName} {this.state.bankState.account}</p>
      </div>
      </div>
      </div>

      <p className="gray-3 p-14 mt-3 mb-3"><img onClick={() => this.setState({ smsCheck: !this.state.smsCheck})}
        src={this.state.smsCheck ? imgChecked : imgCheck} className="cursor mr-2" />
        입금 완료 시 SMS로 알림받기
      </p>
      </div>

      <div className="bottom-box text-center p-3">
      <button onClick={this.handleSubmitPay} className="btn-custom-sm-active">정산</button>
      </div>
      </div>
      </div>
    );
  }

  private tradeCancle = () => {
    return (
      <>
      {/* <button className="btn-custom modal-full" onClick={this.handleCancleScreen}>취소화면</button> */}

      <p className="gray-3 p-14 mt-4">요청일
      <span className="roboto black-2 bold ml-2 mr-2">{timeFormat2(this.state.trade.bidTimestamp)}</span>
      <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호 {this.state.trade.index}</p>

      <div className="row">
      <div className="col-6">
      <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
      </div>
      <div className="col-6 text-right">
      <Link to="/faq"><p className="gray-2 p-14 underline mt-4">1:1 문의하기</p></Link>
      </div>
      </div>

      <hr className="hr2 mb-0 mt-2" />
      <div className="row border-bottom mt-0 pt-3 pb-3">
      <div className="col-md-2 col-4 pl-0 pr-0 text-center">
        <div className="stuff-info" style={{backgroundImage: `url(${this.state.trade.imageUrl})`}}>
        </div>
      </div>
      <div className="col-md-5 col-8 pl-1">
        <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
        <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
        <p className="blue-2 roboto bold mt-2 p-14">{currency(this.state.trade.price)}<span className="p-13 normal">원</span></p>
      </div>
      <div className="col-md-2 text-center hidden-m">
        <p className="gray-2 p-14 mt-4">거래취소</p>
      </div>
      <div className="offset-4 mt-2 pl-1 hidden-p">
        <p className="gray-2 p-14 mt-4">거래취소</p>
      </div>
      <div className="col-md-3 text-right" />
      </div>

      <p className="black-2 bold p-16 mt-4">구매자 정보</p>
      <hr className="hr2 mb-0 mt-2" />
      <div className="row mt-0 border-bottom m-noborder">
        <div className="col-md-6 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
        <div className="row">
        <div className="col-6">
          <p className="black-2 p-14 bold">{this.state.trade.buyer}</p>
          <p className="gray-3 roboto p-14 mt-1">{parsePhone(this.state.trade.buyerPhone)}</p>
        </div>
        </div>
      <hr className="hr4 mb-0 hidden-p" />
      </div>
      <div className="col-md-6 pt-4 pb-4 m-pt2 m-pb2">
        <p className="black-2 p-14 bold">본인인증</p>
        <div className="row">
        <div className="col-6">
        <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 휴대폰</p>
        </div>
        <div className="col-6 text-right">
        <p className="p-14 bold orange-3 mt-2">OK</p>
        </div>
        <div className="col-6">
        {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
        </div>
        <div className="col-6 text-right">
        {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
        </div>
        </div>
        <hr className="hr4 mb-0 hidden-p" />
      </div>
      </div>
      <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" />
      {this.state.progress === 1 && "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
      {this.state.progress > 1 && "구매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
      </p>
      </>
    );
  }
}

export default TradingDetail;