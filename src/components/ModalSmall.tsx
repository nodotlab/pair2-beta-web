import React from "react";

class Modal extends React.Component<any> {
  public render() {
    return (
      <div
        className="modal-custom"
        style={{ display: this.props.display, height: this.props.height }}
      >
        <div className="modal-content-small">{this.props.children}</div>
      </div>
    );
  }
}

export default Modal;
