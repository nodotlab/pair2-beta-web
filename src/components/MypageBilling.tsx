import React from "react";
import { withRouter } from "react-router-dom";
import CardState from "../reduce/BillingState";
import BankState from "../reduce/BankState";
import imgClose from "../images/Popup_Delete.svg";
import bullet from "../images/bullet.svg";
import imgMore from "../images/View_More.svg";
import {
  putBank,
  getBank,
  addCard,
  getCard,
  deleteCard,
  paymentUserIndex,
  payment,
} from "../db/Actions";

interface MypageBillingState {
  hasCard: boolean;
  hasBank: boolean;
  modalCard: boolean;
  modalBank: boolean;
  bankList: string[];
  cardState: CardState;
  bankState: BankState;
  number1: string;
  number2: string;
  number3: string;
  number4: string;
  expiration: string;
  cvc: string;
  password: string;
  birth: string;
  agree1: boolean;
  agree2: boolean;
  agree3: boolean;
  bank: string;
  account: string;
  owner: string;
}

class MypageBilling extends React.Component<any, MypageBillingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      hasCard: false,
      hasBank: false,
      modalCard: false,
      modalBank: false,
      bankList: [],
      cardState: new CardState("", ""),
      bankState: new BankState("", "", ""),
      number1: "",
      number2: "",
      number3: "",
      number4: "",
      expiration: "",
      cvc: "",
      password: "",
      birth: "",
      agree1: false,
      agree2: false,
      agree3: false,
      bank: "",
      account: "",
      owner: "",
    };
  }

  public componentDidMount() {
    this.initData();
    this.getBank();
    this.getPayment();
  }

  private initData(): void {
    const list: string[] = [
      "KB국민",
      "신한",
      "우리",
      "KEB하나",
      "케이뱅크",
      "카카오뱅크",
      "KDB산업",
      "IBK기업",
      "NH농협",
      "수협",
      "대구",
      "BNK부산",
      "BNK경남",
      "광주",
      "전북",
      "제주",
      "한국씨티",
      "SC제일",
      "새마을금고",
      "우체국",
    ];

    this.setState({ bankList: list });
  }

  private async getPayment() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCard(token);
      if (result === "faild") {
        this.setState({
          hasCard: false,
        });
      } else {
        this.setState({
          cardState: new CardState(result.cardName, "0"),
          hasCard: true,
        });
      }
    } catch (error) {
      this.setState({ hasCard: false });
    }
  }

  private async getBank() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getBank(token);

      this.setState({
        owner: result.owner,
        bank: result.bankName,
        account: result.account,
        bankState: new BankState(result.bankName, result.account, result.owner),
        hasBank: true,
      });
    } catch (error) {
      this.setState({ hasBank: false });
    }
  }

  private handleModalBank = (): void =>
    this.setState({ modalBank: !this.state.modalBank });

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleText = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleDeleteCard = async () => {
    try {
      const token: any = localStorage.getItem("pair2token");
      await deleteCard(token);

      this.setState({
        hasCard: false,
        modalCard: false,
        cardState: new CardState("", ""),
      });

      this.getPayment();
    } catch (error) {}
  };

  private verifyBank(): boolean {
    if (this.state.bank === "") return false;
    if (this.state.owner === "") return false;
    if (this.state.account === "") return false;

    return true;
  }

  private handleDeleteBank = async () => {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await putBank({ token: token, bank: null });
      this.setState({
        owner: "",
        bank: "",
        account: "",
        bankState: new BankState("", "", ""),
        hasBank: false,
      });
    } catch (error) {}
  };

  private handleSubmitBank = async () => {
    const token = localStorage.getItem("pair2token");
    const result = await putBank({
      token: token,
      bank: {
        bankName: this.state.bank,
        owner: this.state.owner,
        account: this.state.account,
      },
    });

    this.setState({ modalBank: false });
    this.getBank();
  };

  private handleBilling = async () => {
    const token: any = localStorage.getItem("pair2token");
    const userIndex = await paymentUserIndex(token);
    const metchant = Date.now().toString();
    const { IMP }: any = window;
    IMP.init("imp36196053");

    const data = {
      pg: "dinal",
      pay_method: "card",
      merchant_uid: metchant,
      customer_uid: userIndex,
      name: "카드등록",
      amount: 0,
      buyer_email: "",
      buyer_name: "",
      buyer_tel: "",
      buyer_addr: "",
      buyer_postcode: "",
    };

    IMP.request_pay(data, async (res: any) => {
      if (res.success) {
        await addCard({
          token: token,
          merchant_uid: res.merchant_uid,
          card_name: res.card_name,
        });
        this.getPayment();
      } else {
        alert("다시 시도해주세요.");
      }
    });
  };

  private handlePay = async () => {
    const token: any = localStorage.getItem("pair2token");

    const result = await payment({
      token: token,
      price: 100,
    });

    // console.log(result);
  };

  public render() {
    return (
      <div className="mt-5 mb-6">
        <p className="gray-2 p-13">
          <img src={bullet} className="mr-1" alt="" />
          투명한 거래를 위해 본인명의의 신용/체크카드와 계좌번호를 등록해주세요.
        </p>
        <p className="gray-2 p-13 mt-2">
          <img src={bullet} className="mr-1" alt="" />
          신용/체크카드와 환급계좌는 각 하나씩만 등록 가능합니다.
        </p>
        <div className="box-card row pb-4">
          <div className="col-md-6 col-12">
            <p className="p-14 black-2 mb-2">신용카드</p>
            {this.state.hasCard ? (
              <div className="box-card-info">
                <p className="p-16 bold gray-3 float-right mr-5 mt-4">
                  {this.state.cardState.cardName}
                </p>
                <p className="p-18 bold black-2 text-center mt-100 boxcard-margin"></p>
                <button
                  onClick={this.handleDeleteCard}
                  className="btn-custom-xs mt-2 mr-1"
                >
                  삭제
                </button>
              </div>
            ) : (
              <div className="box-card-in">
                <p className="black-2 p-14 mb-2">카드 등록하기</p>
                <img
                  onClick={this.handleBilling}
                  src={imgMore}
                  alt=""
                  className="cursor plus-mobile"
                />
              </div>
            )}
          </div>

          <div className="col-md-6 col-12 m-mt2">
            <p className="p-14 black-2 mb-2">환급계좌</p>
            {this.state.hasBank ? (
              <div className="box-card-info">
                <p className="p-16 bold gray-3 float-right mr-5 mt-4">
                  {this.state.bankState.bankName}
                </p>
                <p className="p-18 bold black-2 text-center mt-100 boxcard-margin">
                  {this.state.bankState.account}
                </p>
                <button
                  onClick={this.handleDeleteBank}
                  className="btn-custom-xs mt-2 mr-1"
                >
                  삭제
                </button>
                <button
                  onClick={this.handleModalBank}
                  className="btn-second-xs mt-2"
                >
                  수정
                </button>
              </div>
            ) : (
              <div className="box-card-in">
                <p className="black-2 p-14 mb-2">계좌 등록하기</p>
                <img
                  onClick={this.handleModalBank}
                  src={imgMore}
                  alt=""
                  className="cursor plus-mobile"
                />
              </div>
            )}
          </div>
        </div>

        <this.ComponentBankModal />
      </div>
    );
  }

  private ComponentBankModal = () => {
    return (
      <>
        <div
          className="modal-full modal-overflow hidden-p"
          style={{ display: this.state.modalBank ? "block" : "none" }}
        >
          <div className="p-3">
            <div className="row">
              <div className="col-6">
                <p className="p-16 bold black-2">환급계좌 등록</p>
              </div>
              <div className="col-6 text-right">
                <img
                  onClick={this.handleModalBank}
                  src={imgClose}
                  alt=""
                  className="cursor"
                />
              </div>
            </div>
            <hr className="hr2" />
            <p className="p-13 gray-2">
              <img src={bullet} alt="" className="mr-1" />
              본인명의의 계좌만 등록할 수 있습니다.
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-1" />
              휴대폰 번호 등으로 만든 평생계좌번호 및 가상계좌,
              펀드/적금/정기예금 등에는 계좌는 등록할 수 없습니다.
            </p>

            <p className="p-14 black-2 mt-4 mb-2">은행</p>
            <select
              onChange={this.handleText}
              value={this.state.bank}
              className="form-control form-select"
              name="bank"
            >
              <option value="" disabled>
                은행을 선택해 주세요.
              </option>
              {this.state.bankList.map((bank: string, i: number) => (
                <option key={i}>{bank}</option>
              ))}
            </select>

            <p className="p-14 black-2 mt-4 mb-2">계좌번호</p>
            <input
              className="form-control form-custom"
              value={this.state.account}
              type="text"
              name="account"
              onChange={this.handleChange}
            />

            <p className="p-14 black-2 mt-4 mb-2">예금주</p>
            <input
              className="form-control form-custom"
              value={this.state.owner}
              type="text"
              name="owner"
              maxLength={20}
              onChange={this.handleText}
            />

            <div className="text-center mt-5">
              {this.verifyBank() ? (
                <button
                  onClick={this.handleSubmitBank}
                  className="btn-custom-sm-active mb-3"
                >
                  등록
                </button>
              ) : (
                <button className="btn-custom-sm-disable mb-3">등록</button>
              )}
            </div>
          </div>
        </div>

        <div
          className="modal-custom modal-billing modal-full hidden-m"
          style={{ display: this.state.modalBank ? "block" : "none" }}
        >
          <div className="modal-content-small">
            <div className="p-3">
              <div className="row">
                <div className="col-6">
                  <p className="p-16 bold black-2">환급계좌 등록</p>
                </div>
                <div className="col-6 text-right">
                  <img
                    onClick={this.handleModalBank}
                    src={imgClose}
                    alt=""
                    className="cursor"
                  />
                </div>
              </div>
              <hr className="hr2" />
              <p className="p-13 gray-2">
                <img src={bullet} alt="" className="mr-1" />
                본인명의의 계좌만 등록할 수 있습니다.
              </p>
              <p className="p-13 gray-2 mt-2">
                <img src={bullet} alt="" className="mr-1" />
                휴대폰 번호 등으로 만든 평생계좌번호 및 가상계좌,
                펀드/적금/정기예금 등에는 계좌는 등록할 수 없습니다.
              </p>

              <p className="p-14 black-2 mt-4 mb-2">은행</p>
              <select
                onChange={this.handleText}
                value={this.state.bank}
                className="form-control form-select"
                name="bank"
              >
                <option value="" disabled>
                  은행을 선택해 주세요.
                </option>
                {this.state.bankList.map((bank: string, i: number) => (
                  <option key={i}>{bank}</option>
                ))}
              </select>

              <p className="p-14 black-2 mt-4 mb-2">계좌번호</p>
              <input
                className="form-control form-custom"
                value={this.state.account}
                type="text"
                name="account"
                onChange={this.handleChange}
              />

              <p className="p-14 black-2 mt-4 mb-2">예금주</p>
              <input
                className="form-control form-custom"
                value={this.state.owner}
                type="text"
                name="owner"
                maxLength={20}
                onChange={this.handleText}
              />

              <div className="text-center mt-5">
                {this.verifyBank() ? (
                  <button
                    onClick={this.handleSubmitBank}
                    className="btn-custom-sm-active mb-3"
                  >
                    등록
                  </button>
                ) : (
                  <button className="btn-custom-sm-disable mb-3">등록</button>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
}

export default withRouter(MypageBilling);
