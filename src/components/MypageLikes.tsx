import React from "react";
import { withRouter, Link } from "react-router-dom";
import LikeProduct from "../reduce/LikeProduct";
import LikeSale from "../reduce/LikeSale";
import { likes, deleteLike, likesTrade, deleteLikeTrade } from "../db/Actions";
import { currency } from "../Method";
import sale1 from "../images/used_01.jpg";
import sale2 from "../images/used_02.jpg";
import sale3 from "../images/used_03.jpg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";

interface MypageLikeState {
  tab: string;
  likeProducts: LikeProduct[];
  likeSale: LikeSale[];
  checkAllProducts: boolean;
  checkAllSale: boolean;
}

class MypageLike extends React.Component<any, MypageLikeState> {
  constructor(props: any) {
    super(props);
    this.state = {
      tab: "products",
      likeProducts: [],
      likeSale: [],
      checkAllProducts: false,
      checkAllSale: false,
    };
  }

  public componentDidMount() {
    this.setState({ tab: this.props.tab });

    this.getProducts();
    this.getSale();
  }

  private async getProducts() {
    const token: any = localStorage.getItem("pair2token");

    const products: LikeProduct[] = [];
    const result = await likes(token, 1000);
    if (result !== "faild") {
      for (let i = 0; i < result.length; ++i) {
        products.push(
          new LikeProduct(
            result[i].index,
            result[i].brand,
            result[i].name,
            result[i].size,
            result[i].thumbUrl
          )
        );
      }
    }

    this.setState({
      likeProducts: products,
    });
  }

  private async getSale() {
    const sale: LikeSale[] = [];
    const token: any = localStorage.getItem("pair2token");

    const resultTrades = await likesTrade(token, 1000);
    if (resultTrades !== "faild") {
      for (let i = 0; i < resultTrades.length; ++i) {
        let where: string[] = [];
        if (resultTrades[i].shippingType === "택배") where = ["택배"];
        if (resultTrades[i].shippingType === "직거래") where = ["직거래"];
        if (resultTrades[i].shippingType === "직거래/택배")
          where = ["직거래 | 택배"];

        sale.push(
          new LikeSale(
            resultTrades[i].tradeIndex,
            resultTrades[i].brand,
            resultTrades[i].sneakerName,
            resultTrades[i].size,
            resultTrades[i].imageUrl,
            resultTrades[i].price,
            resultTrades[i].from,
            resultTrades[i].status,
            where,
            resultTrades[i].likeIndex
          )
        );
      }
    }

    this.setState({
      likeSale: sale,
    });
  }

  private handleCheckAllProduct = (on: boolean): void => {
    const array = this.state.likeProducts;
    if (on) {
      for (let i = 0; i < array.length; ++i) {
        array[i].select = true;
      }
      this.setState({
        likeProducts: array,
        checkAllProducts: true,
      });
    } else {
      for (let i = 0; i < array.length; ++i) {
        array[i].select = false;
      }
      this.setState({
        likeProducts: array,
        checkAllProducts: false,
      });
    }
  };

  private handleCheckProduct = (i: number): void => {
    const array = this.state.likeProducts;
    array[i].select = !array[i].select;
    this.setState({ likeProducts: array });
  };

  private handleCheckAllSale = (on: boolean): void => {
    const array = this.state.likeSale;
    if (on) {
      for (let i = 0; i < array.length; ++i) {
        array[i].select = true;
      }
      this.setState({
        likeSale: array,
        checkAllSale: true,
      });
    } else {
      for (let i = 0; i < array.length; ++i) {
        array[i].select = false;
      }
      this.setState({
        likeSale: array,
        checkAllSale: false,
      });
    }
  };

  private handleCheckSale = (i: number): void => {
    const array = this.state.likeSale;
    array[i].select = !array[i].select;
    this.setState({ likeSale: array });
  };

  private handleDeleteProduct = async () => {
    const token: any = localStorage.getItem("pair2token");

    for (let i = 0; i < this.state.likeProducts.length; ++i) {
      if (this.state.likeProducts[i].select) {
        await deleteLike(token, parseInt(this.state.likeProducts[i].index));
      }
    }

    this.getProducts();
  };

  private handleDeleteSale = async () => {
    const token: any = localStorage.getItem("pair2token");

    for (let i = 0; i < this.state.likeSale.length; ++i) {
      if (this.state.likeSale[i].select) {
        await deleteLikeTrade(
          token,
          parseInt(this.state.likeSale[i].likeIndex)
        );
      }
    }

    this.getSale();
  };

  public render() {
    return (
      <div className="mt-5 m-mt2">
        <div className="w-100 border-bottom-2 hidden-m">
          <div
            onClick={() => this.setState({ tab: "products" })}
            className={`cursor btn-faq ${
              this.state.tab === "products" && "btn-faq-active"
            }`}
          >
            관심제품
          </div>
          <div
            onClick={() => this.setState({ tab: "sale" })}
            className={`cursor btn-faq ${
              this.state.tab === "sale" && "btn-faq-active"
            }`}
          >
            관심매물
          </div>
        </div>
        <div className="w-100 text-center hidden-p">
          <div
            onClick={() => this.setState({ tab: "products" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "products" && "btn-faq-active"
            }`}
          >
            관심제품
          </div>
          <div
            onClick={() => this.setState({ tab: "sale" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "sale" && "btn-faq-active"
            }`}
          >
            관심매물
          </div>
        </div>

        {this.state.tab === "products" && <this.ComponentProduct />}
        {this.state.tab === "sale" && <this.ComponentSale />}
      </div>
    );
  }

  private ComponentProduct = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4 inline-block pr-2 border-right-gray mr-2 hidden-m">
          총 <b>{this.state.likeProducts.length}</b>건
        </p>
        <p className="p-14 black-2 mt-4 inline-block pr-2 mr-2 hidden-p">
          총 <b>{this.state.likeProducts.length}</b>건
        </p>
        <br className="hidden-p mb-0" />
        {this.state.checkAllProducts ? (
          <img
            onClick={() => this.handleCheckAllProduct(false)}
            src={checked}
            className="cursor inline-block mr-2"
            alt=""
          />
        ) : (
          <img
            onClick={() => this.handleCheckAllProduct(true)}
            src={check}
            className="cursor inline-block mr-2"
            alt=""
          />
        )}
        <p className="p-14 gray-3 mt-2 inline-block">
          전체선택({this.state.likeProducts.length})
        </p>
        <button
          onClick={this.handleDeleteProduct}
          className="btn-custom-xs inline-block ml-2"
        >
          삭제
        </button>

        <div className="row mt-3 mb-5 sneaker-mobile-row">
          {this.state.likeProducts.map((product: LikeProduct, i: number) => (
            <div key={i} className="col-md-4 col-6 mb-4 sneaker-mobile">
              <img
                onClick={() => this.handleCheckProduct(i)}
                src={product.select ? checked : check}
                className="cursor float-right mt-2 mr-2"
                alt=""
              />
              <div
                className="mypage-default-image box-sneaker"
                style={{ backgroundImage: `url(${product.imageUrl})` }}
              />
              <Link to={`/sneaker/${product.name}`}>
                <div className="row mt-3">
                  <div className="col-6">
                    <p className="p-12 gray-2 bold roboto">
                      {product.brand.toUpperCase()}
                    </p>
                  </div>
                  <div className="col-6 text-right">
                    <p className="p-12 gray-2 roboto">Size {product.size}</p>
                  </div>
                </div>
                <p className="p-14 black-2 roboto mt-2 sneaker-name">
                  {product.name.toUpperCase()}
                </p>
              </Link>
            </div>
          ))}
        </div>

        {this.state.likeProducts.length > 0 && (
          <button className="btn-custom w-100 mb-6">더보기</button>
        )}

        {this.state.likeProducts.length === 0 && (
          <div className="text-center mt-6 mb-6">
            <p className="p-14 gray-3">등록된 관심제품이 없습니다.</p>
            <Link to="/sneakers">
              <button className="btn-custom-sm mt-3 mb-6">제품 보러가기</button>
            </Link>
          </div>
        )}
      </>
    );
  };

  private ComponentSale = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4 inline-block pr-2 border-right-gray">
          총 <b>{this.state.likeSale.length}</b>건
        </p>
        {this.state.checkAllSale ? (
          <img
            onClick={() => this.handleCheckAllSale(false)}
            src={checked}
            className="cursor inline-block ml-2 mr-2"
            alt=""
          />
        ) : (
          <img
            onClick={() => this.handleCheckAllSale(true)}
            src={check}
            className="cursor inline-block ml-2 mr-2"
            alt=""
          />
        )}
        <p className="p-14 gray-3 mt-4 inline-block">
          전체선택({this.state.likeSale.length})
        </p>
        <button
          onClick={this.handleDeleteSale}
          className="btn-custom-xs inline-block ml-2"
        >
          삭제
        </button>

        <div className="row mt-3 mb-5 sneaker-mobile-row">
          {this.state.likeSale.map((sale: LikeSale, i: number) => (
            <div key={i} className="col-md-4 col-6 mb-4 sneaker-mobile">
              <img
                onClick={() => this.handleCheckSale(i)}
                src={sale.select ? checked : check}
                className="cursor float-right mt-2 mr-2"
                alt=""
              />
              <div
                className="mypage-default-image box-sneaker"
                style={{ backgroundImage: `url(${sale.imageUrl})` }}
              />
              <p className="p-12 gray-2 mt-2 text-right mypage-tags">
                {sale.location} <span className="gray-7 m-1">|</span>
                {sale.status} <span className="gray-7 m-1">|</span>
                {sale.shippings[0]} <span className="gray-7 m-1"></span>
              </p>
              <Link to={`/buy/${sale.name}/${sale.size}/${sale.index}`}>
                <div className="row mt-3">
                  <div className="col-6">
                    <p className="p-12 gray-2 bold roboto">{sale.brand}</p>
                  </div>
                  <div className="col-6 text-right">
                    <p className="p-12 gray-2 roboto">Size {sale.size}</p>
                  </div>
                </div>
              </Link>
              <Link to={`/buy/${sale.name}/${sale.size}/${sale.index}`}>
                <p className="p-14 black-2 roboto mt-1 sneaker-name">
                  {sale.name.toUpperCase().substr(0, 40)}
                </p>
              </Link>
              <Link to={`/buy/${sale.name}/${sale.size}/${sale.index}`}>
                <p className="p-18 orange bold roboto float-right">
                  {currency(sale.price)}
                  <span className="p-13 noto normal">원</span>
                </p>
              </Link>
            </div>
          ))}
        </div>

        {this.state.likeSale.length > 0 && (
          <button className="btn-custom w-100 mb-6">더보기</button>
        )}

        {this.state.likeSale.length === 0 && (
          <div className="text-center mt-6 mb-6">
            <p className="p-14 gray-3">등록된 관심매물이 없습니다.</p>
          </div>
        )}
      </>
    );
  };
}

export default withRouter(MypageLike);
