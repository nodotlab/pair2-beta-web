import React from "react";
import { putShippings, getShippings } from "../db/Actions";
import Shipping from "../reduce/Shipping";
import DaumPostcode from "react-daum-postcode";
import imgClose from "../images/Popup_Delete.svg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import prev from "../images/backblack.svg";
import bullet from "../images/bullet.svg";

interface MypageShippingState {
  modal: boolean;
  editMode: boolean;
  editIndex: number;
  modalAddress: boolean;
  modalAddressMobile: boolean;
  hasShipping: boolean;
  shippings: Shipping[];
  tag: string;
  name: string;
  isDefault: boolean;
  zip: string;
  address: string;
  addressDetail: string;
  phone: string;
}

class MypageShipping extends React.Component<any, MypageShippingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      modal: false,
      editMode: false,
      editIndex: 0,
      modalAddress: false,
      modalAddressMobile: false,
      hasShipping: false,
      shippings: [],
      tag: "",
      name: "",
      isDefault: true,
      zip: "",
      address: "",
      addressDetail: "",
      phone: "",
    };
  }

  public componentDidMount() {
    this.getShippings();
  }

  private async getShippings() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result: Shipping[] = await getShippings(token);

      this.setState({
        shippings: result,
      });

      if (result.length > 0) {
        this.setState({ hasShipping: true });
      } else {
        this.setState({ hasShipping: false });
      }
    } catch (error) {
      this.setState({ hasShipping: false });
    }
  }

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handlePhone = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleModal = (): void => {
    this.setState({
      modal: !this.state.modal,
      editMode: false,
      tag: "",
      name: "",
      zip: "",
      address: "",
      addressDetail: "",
      phone: "",
      isDefault: false,
    });
  };

  private handleModalAddress = (): void => {
    this.setState({ modalAddress: !this.state.modalAddress });
  };

  private handleModalAddressMobile = (): void => {
    this.setState({ modalAddressMobile: !this.state.modalAddressMobile });
  };

  private handleAddress = (data: any): void => {
    this.setState({
      address: data.address,
      zip: data.zonecode,
      modalAddress: false,
      modalAddressMobile: false,
    });
  };

  private verifyAll(): boolean {
    if (this.state.tag === "") return false;
    if (this.state.name === "") return false;
    if (this.state.zip === "") return false;
    if (this.state.address === "") return false;
    if (this.state.addressDetail === "") return false;
    if (this.state.phone === "") return false;

    return true;
  }

  private handleEdit = async (index: number) => {
    this.setState({
      modal: true,
      editMode: true,
      editIndex: index,
      tag: this.state.shippings[index].tag,
      name: this.state.shippings[index].name,
      zip: this.state.shippings[index].zip,
      address: this.state.shippings[index].address,
      addressDetail: this.state.shippings[index].addressDetail,
      phone: this.state.shippings[index].phone,
      isDefault: this.state.shippings[index].default,
    });
  };

  private handleDelete = async (index: number) => {
    const tempShippings: Shipping[] = [];

    for (let i = 0; i < this.state.shippings.length; ++i) {
      if (i !== index) tempShippings.push(this.state.shippings[i]);
    }

    const token: any = localStorage.getItem("pair2token");

    await putShippings({
      token: token,
      shippings: tempShippings,
    });

    this.getShippings();
  };

  private handleEditSubmit = async () => {
    const token: any = localStorage.getItem("pair2token");

    if (this.state.isDefault) {
      const tempShippings: Shipping[] = this.state.shippings;
      for (let i in tempShippings) {
        tempShippings[i].default = false;
      }
      tempShippings[this.state.editIndex].tag = this.state.tag;
      tempShippings[this.state.editIndex].name = this.state.name;
      tempShippings[this.state.editIndex].zip = this.state.zip;
      tempShippings[this.state.editIndex].address = this.state.address;
      tempShippings[
        this.state.editIndex
      ].addressDetail = this.state.addressDetail;
      tempShippings[this.state.editIndex].phone = this.state.phone;
      tempShippings[this.state.editIndex].default = this.state.isDefault;

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    } else {
      const tempShippings: Shipping[] = this.state.shippings;
      tempShippings[this.state.editIndex].tag = this.state.tag;
      tempShippings[this.state.editIndex].name = this.state.name;
      tempShippings[this.state.editIndex].zip = this.state.zip;
      tempShippings[this.state.editIndex].address = this.state.address;
      tempShippings[
        this.state.editIndex
      ].addressDetail = this.state.addressDetail;
      tempShippings[this.state.editIndex].phone = this.state.phone;
      tempShippings[this.state.editIndex].default = this.state.isDefault;

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    }

    this.handleModal();
    this.getShippings();
  };

  private handleSubmit = async () => {
    const token: any = localStorage.getItem("pair2token");

    if (this.state.isDefault) {
      const tempShippings: Shipping[] = this.state.shippings
        ? this.state.shippings
        : [];
      for (let i in tempShippings) {
        tempShippings[i].default = false;
      }
      tempShippings.push(
        new Shipping(
          this.state.tag,
          this.state.name,
          this.state.isDefault,
          this.state.zip,
          this.state.address,
          this.state.addressDetail,
          this.state.phone,
        ),
      );

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    } else {
      const tempShippings: Shipping[] = this.state.shippings
        ? this.state.shippings
        : [];
      tempShippings.push(
        new Shipping(
          this.state.tag,
          this.state.name,
          this.state.isDefault,
          this.state.zip,
          this.state.address,
          this.state.addressDetail,
          this.state.phone,
        ),
      );

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    }

    this.handleModal();
    this.getShippings();
  };

  public render() {
    return (
      <div className="mt-5 mb-6 m-mt4">
        <p className="p-13 gray-2 hidden-p">
          <img src={bullet} alt="" className="mr-1 " />
          PAIR2에서 택배거래로 제품 구매 시 배송되는 주소록입니다. 배송지를
          추가/수정.삭제하여 관리할 수 있습니다.
        </p>

        {this.state.hasShipping && (
          <div className="row box-post box-post-top pl-0 pr-0 hidden-m">
            <div className="col-2 p-13 bold gray-2 pl-4 pr-0">배송지</div>
            <div className="col-6 p-13 bold gray-2 pl-1 pr-0">주소</div>
            <div className="col-2 p-13 bold gray-2 pl-0 pr-0">연락처</div>
            <div className="col-2 p-13 bold gray-2 pl-0 pr-0">수정/삭제</div>
          </div>
        )}

        <hr className="hr2 hidden-p mb-0" />

        {this.state.hasShipping &&
          this.state.shippings.map((shipping: Shipping, i: number) => (
            <div key={i} className="box-address">
              <div className="row">
                <div className="col-md-2 col-3 pl-4 pr-0">
                  {shipping.default && (
                    <div className="btn-active-sm mb-2 hidden-m">
                      기본 배송지
                    </div>
                  )}
                  <p className="bold">{shipping.tag}</p>
                  <p>{shipping.name}</p>
                  {shipping.default && (
                    <div className="btn-active-sm hidden-p mt-2">
                      기본 배송지
                    </div>
                  )}
                </div>
                <div className="col-md-6 col-9 pl-1 pr-3">
                  <p>{shipping.zip}</p>
                  <p>
                    {shipping.address} {shipping.addressDetail}
                  </p>
                  <p>{shipping.addressDetail}</p>
                  <p className="hidden-p mt-2">{shipping.phone}</p>
                  <button
                    onClick={() => this.handleEdit(i)}
                    className="btn-custom-mypage p-2 hidden-p mt-3 mr-2"
                  >
                    수정
                  </button>
                  <button
                    onClick={() => this.handleDelete(i)}
                    className="btn-custom-mypage p-2 hidden-p mt-2"
                  >
                    삭제
                  </button>
                </div>
                <div className="col-md-2 pl-0 pr-0 pt-4 hidden-m">
                  <p>{shipping.phone}</p>
                </div>
                <div className="col-md-2 pl-0 pr-0 hidden-m">
                  <button
                    onClick={() => this.handleEdit(i)}
                    className="btn-custom-mypage p-2 mr-2 mt-3"
                  >
                    수정
                  </button>
                  <button
                    onClick={() => this.handleDelete(i)}
                    className="btn-custom-mypage p-2 mt-3"
                  >
                    삭제
                  </button>
                </div>
              </div>
            </div>
          ))}
        {this.state.hasShipping && (
          <div className="text-right">
            <button onClick={this.handleModal} className="btn-shipping mt-3">
              배송지등록하기
            </button>
          </div>
        )}

        {!this.state.hasShipping && <this.ComponentNone />}
        <this.modalComponent />
      </div>
    );
  }

  private ComponentNone = () => {
    return (
      <div className="box-shipping-add mt-3">
        <p className="p-14 gray-3 pt-90">등록된 배송지가 없습니다.</p>
        <button onClick={this.handleModal} className="btn-shipping mt-1">
          배송지등록하기
        </button>
      </div>
    );
  };

  private modalComponent = () => {
    return (
      <>
        <div
          className="modal-full modal-overflow hidden-p"
          style={{ display: this.state.modal ? "block" : "none" }}
        >
          {this.state.modalAddressMobile ? (
            <>
              <div className="row p-3">
                <div className="col-6 offset-3">
                  <p className="p-16 bold black-2 text-center ">우편번호검색</p>
                </div>
                <div className="col-3 text-right">
                  <img
                    onClick={() => this.setState({ modalAddressMobile: false })}
                    src={imgClose}
                    alt=""
                    className="cursor"
                  />
                </div>
              </div>
              <DaumPostcode
                style={{
                  height: "100vh",
                  position: "absolute",
                  top: 50,
                  padding: 10,
                }}
                onComplete={this.handleAddress}
              />
            </>
          ) : (
            <div className="p-3">
              <div className="row">
                <div className="col-1">
                  <img
                    onClick={this.handleModal}
                    src={prev}
                    alt=""
                    className="cursor"
                  />
                </div>
                <div className="col-11">
                  <p className="p-16 bold black-2">배송지 등록</p>
                </div>
              </div>
              <hr className="hr2 mb-4" />

              <p className="p-14 black-2 mb-2 float-left">배송지명</p>
              <p className="p-13 orange mb-2 float-right">*필수입력</p>
              <input
                onChange={this.handleChange}
                value={this.state.tag}
                maxLength={10}
                placeholder={"10자 이내로 입력해주세요"}
                className="form-control form-custom "
                name="tag"
              />
              <p className="placeholder-right p-13 gray-2">
                {this.state.tag.length}/10
              </p>

              <div className="mt-4">
                <p className="p-14 black-2 mb-2">
                  수령인<span className="orange">*</span>
                </p>
                <input
                  onChange={this.handleChange}
                  value={this.state.name}
                  maxLength={10}
                  placeholder={"10자 이내로 입력해주세요"}
                  className="form-control form-custom "
                  name="name"
                />
                <p className="placeholder-right p-13 gray-2">
                  {this.state.name.length}/10
                </p>
              </div>

              <div className="mt-4">
                <p className="p-14 black-2 mb-2">
                  주소<span className="orange">*</span>
                </p>
                <div className="flex">
                  <div className="col-8 pl-0 pr-1">
                    <input
                      value={this.state.zip}
                      placeholder={"10자 이내로 입력해주세요"}
                      className="form-control form-custom "
                      name="zip"
                    />
                  </div>
                  <div className="col-4 pl-0 pr-0">
                    <button
                      onClick={this.handleModalAddressMobile}
                      className="btn-second-sm w-100 h-100"
                    >
                      우편번호찾기
                    </button>
                  </div>
                </div>
              </div>

              <input
                value={this.state.address}
                className="form-control form-custom mt-2"
                name="address"
              />
              <input
                onChange={this.handleChange}
                value={this.state.addressDetail}
                placeholder={"상세 주소를 입력해주세요"}
                className="form-control form-custom mt-2"
                name="addressDetail"
              />

              <p className="p-14 black-2 mt-3 mb-2">
                연락처<span className="orange">*</span>
              </p>
              <input
                onChange={this.handlePhone}
                value={this.state.phone}
                placeholder={"숫자만 입력해주세요"}
                className="form-control form-custom "
                name="phone"
              />

              <div className="mt-3">
                <img
                  onClick={() =>
                    this.setState({ isDefault: !this.state.isDefault })
                  }
                  src={this.state.isDefault ? checked : check}
                  className="cursor mr-2 float-left"
                />
                <p className="gray-3 p-14 cursor">기본 배송지로 설정</p>
              </div>

              <div className="text-center mt-5">
                {this.verifyAll() ? (
                  <button
                    onClick={
                      this.state.editMode
                        ? this.handleEditSubmit
                        : this.handleSubmit
                    }
                    className="btn-custom-sm-active"
                  >
                    저장
                  </button>
                ) : (
                  <button className="btn-custom-sm-disable">저장</button>
                )}
              </div>
            </div>
          )}
        </div>

        <div
          className="modal-custom modal-billing modal-full hidden-m"
          style={{ display: this.state.modal ? "block" : "none" }}
        >
          <div className="modal-content-small">
            {this.state.modalAddress && (
              <>
                <div className="row p-3">
                  <div className="col-8 offset-2 text-center">
                    <p className="p-16 bold black-2">우편번호 검색</p>
                  </div>
                  <div className="col-2 text-right">
                    <img
                      onClick={() => this.setState({ modalAddress: false })}
                      src={imgClose}
                      alt=""
                      className="cursor"
                    />
                  </div>
                </div>
                <DaumPostcode
                  onComplete={this.handleAddress}
                  style={{ height: "100%" }}
                />
              </>
            )}

            {!this.state.modalAddress && (
              <div className="p-3">
                <div className="row">
                  <div className="col-1">
                    <img
                      onClick={this.handleModal}
                      src={prev}
                      alt=""
                      className="cursor"
                    />
                  </div>
                  <div className="col-11 text-left">
                    <p className="p-16 bold black-2">배송지 등록</p>
                  </div>
                </div>
                <hr className="hr2 mb-4" />

                <p className="p-14 black-2 mb-2 float-left">배송지명</p>
                <p className="p-13 orange mb-2 float-right">*필수입력</p>
                <input
                  onChange={this.handleChange}
                  value={this.state.tag}
                  maxLength={10}
                  placeholder={"10자 이내로 입력해주세요"}
                  className="form-control form-custom "
                  name="tag"
                />
                <p className="placeholder-right p-13 gray-2">
                  {this.state.tag.length}/10
                </p>

                <div className="mt-4">
                  <p className="p-14 black-2 mb-2">
                    수령인<span className="orange">*</span>
                  </p>
                  <input
                    onChange={this.handleChange}
                    value={this.state.name}
                    maxLength={10}
                    placeholder={"10자 이내로 입력해주세요"}
                    className="form-control form-custom "
                    name="name"
                  />
                  <p className="placeholder-right p-13 gray-2">
                    {this.state.name.length}/10
                  </p>
                </div>

                <div className="mt-4">
                  <p className="p-14 black-2 mb-2">
                    주소<span className="orange">*</span>
                  </p>
                  <div className="flex">
                    <div className="col-8 pl-0 pr-1">
                      <input
                        value={this.state.zip}
                        placeholder={"10자 이내로 입력해주세요"}
                        className="form-control form-custom "
                        name="zip"
                      />
                    </div>
                    <div className="col-4 pl-0 pr-0">
                      <button
                        onClick={this.handleModalAddress}
                        className="btn-second-sm w-100 h-100"
                      >
                        우편번호찾기
                      </button>
                    </div>
                  </div>
                </div>

                <input
                  value={this.state.address}
                  className="form-control form-custom mt-2"
                  name="address"
                />
                <input
                  onChange={this.handleChange}
                  value={this.state.addressDetail}
                  placeholder={"상세 주소를 입력해주세요"}
                  className="form-control form-custom mt-2"
                  name="addressDetail"
                />

                <p className="p-14 black-2 mt-3 mb-2">
                  연락처<span className="orange">*</span>
                </p>
                <input
                  onChange={this.handlePhone}
                  value={this.state.phone}
                  placeholder={"숫자만 입력해주세요"}
                  className="form-control form-custom "
                  name="phone"
                />

                <div className="mt-3">
                  <img
                    onClick={() =>
                      this.setState({ isDefault: !this.state.isDefault })
                    }
                    src={this.state.isDefault ? checked : check}
                    className="cursor mr-2 float-left"
                  />
                  <p className="gray-3 p-14 cursor">기본 배송지로 설정</p>
                </div>

                <div className="text-center mt-5">
                  {this.verifyAll() ? (
                    <button
                      onClick={
                        this.state.editMode
                          ? this.handleEditSubmit
                          : this.handleSubmit
                      }
                      className="btn-custom-sm-active"
                    >
                      저장
                    </button>
                  ) : (
                    <button className="btn-custom-sm-disable">저장</button>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </>
    );
  };
}

export default MypageShipping;
