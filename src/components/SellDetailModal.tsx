import React from "react";
import Modal from "../components/ModalLarge";
import User from "../reduce/UserInfo";
import { tradeDetail, getUserInfo, cheat } from "../db/Actions";
import bullet from "../images/bullet.svg";
import imgClose from "../images/Popup_Delete.svg";
import imgClose2 from "../images/GNB_Close.svg";
import imgNext from "../images/GNB_Next.svg";
import imgPrev from "../images/GNB_Prev.svg";
import imgCheck from "../images/Checkbox_Default.svg";
import imgChecked from "../images/Checkbox_Checked.svg";
import { currency } from "../Method";

class Data {
  public from: string;
  public status: string;
  public shipping: string[];
  public price: number;
  public images: string[];
  public location: string;
  public size: string;
  public damageDesc: string;
  public statusDesc: string;

  constructor(
    _from: string,
    _status: string,
    _shipping: string[],
    _price: number,
    _images: string[],
    _location: string,
    _size: string,
    _damageDesc: string,
    _statusDesc: string
  ) {
    this.from = _from;
    this.status = _status;
    this.shipping = _shipping;
    this.price = _price;
    this.images = _images;
    this.location = _location;
    this.size = _size;
    this.damageDesc = _damageDesc;
    this.statusDesc = _statusDesc;
  }
}

interface SellDetailState {
  userData: User;
  data: Data;
  like: boolean;
  modalImage: boolean;
  selectImage: number;
  damage1: boolean;
  damage2: boolean;
  damage3: boolean;
  damage4: boolean;
}

class SellDetail extends React.Component<any, SellDetailState> {
  constructor(props: any) {
    super(props);
    this.state = {
      userData: new User("", false, false, 0, 0),
      data: new Data("", "", [""], 0, ["", "", "", ""], "", "", "", ""),
      like: false,
      modalImage: false,
      selectImage: 0,
      damage1: false,
      damage2: false,
      damage3: false,
      damage4: false,
    };
  }

  public componentDidMount() {
    this.initData();
  }

  private async initData() {
    const result: any = await tradeDetail(this.props.index);
    const token: any = localStorage.getItem("pair2token");
    const currentUser = await getUserInfo(token);
    const isCheater = await cheat(currentUser.phone);
    const cheatCount = isCheater === false ? 0 : 1;

    let shippingType: string[] = [];
    if (result.shippingType === "직거래") {
      shippingType = ["직거래"];
    } else if (result.shippingType === "택배") {
      shippingType = ["택배"];
    } else {
      shippingType = ["직거래", "택배거래"];
    }

    let condition: string = "Perfect";
    if (result.damage1 || result.damage2 || result.damage3 || result.damage4)
      condition = "Defect1";
    if (condition === "Defect1") {
      if (result.status1 || result.status2 || result.status3)
        condition = "Defect3";
    } else {
      if (result.status1 || result.status2 || result.status3)
        condition = "Defect2";
    }

    let location = result.location1;
    if (result.location2 !== null && result.location2 !== undefined)
      location = location + "/" + result.location2;

    this.setState({
      data: new Data(
        result.from,
        condition,
        shippingType,
        result.price,
        result.imageUrls,
        location,
        result.size,
        result.damageDesc,
        result.statusDesc
      ),
      userData: new User(
        result.nickname,
        result.verifyPhone === true ? true : false,
        true,
        cheatCount,
        cheatCount
      ),
      damage1: result.damage1 ? true : false,
      damage2: result.damage2 ? true : false,
      damage3: result.damage3 ? true : false,
      damage4: result.damage4 ? true : false,
    });
  }

  private handleImageSelect = (type: string): void => {
    if (type === "-" && this.state.selectImage > 0) {
      this.setState({ selectImage: this.state.selectImage - 1 });
    }
    if (
      type === "+" &&
      this.state.selectImage < this.state.data.images.length - 1
    ) {
      this.setState({ selectImage: this.state.selectImage + 1 });
    }
  };

  public render() {
    return (
      <div className="modal-custom modal-full" style={{ display: "block" }}>
        <div className="modal-selldetail">
          <div className="buydetail-m">
            <div className="row no-gutters">
              <div className="hidden-p w-100 pt-3 pb-1">
                <div className="row">
                  <div className="col-6">
                    <p className="black-2 p-16 bold">매물 상세</p>
                  </div>
                  <div className="col-6 text-right">
                    <img
                      src={imgClose}
                      onClick={this.props.handleCancle}
                      className="cursor"
                    />
                  </div>
                </div>
                <hr className="hr2" />
              </div>

              <div className="col-md-6 hidden-m">
                {this.state.data.images.map((image: string, i: number) => (
                  // <div key={i} className="img-buy-detail" style={{backgroundImage: `url(${image})`}} />
                  <img
                    onClick={() =>
                      this.setState({ modalImage: true, selectImage: i })
                    }
                    key={i}
                    src={image}
                    className="img-fluid"
                  />
                ))}
              </div>

              <div className="hidden-p w-100">
                <div
                  className="buydetail-image"
                  onClick={() => this.setState({ modalImage: true })}
                  style={{
                    backgroundImage: `url(${
                      this.state.data.images[this.state.selectImage]
                    })`,
                  }}
                />
                <div className="w-100 box-carousel pt-2">
                  {this.state.data.images.map((image: string, i: number) => (
                    <div
                      onClick={() => this.setState({ selectImage: i })}
                      key={i}
                      className={`icon-carousel ${
                        this.state.selectImage === i && "icon-carousel-active"
                      }`}
                    />
                  ))}
                </div>
              </div>

              <div className="col-md-6">
                <div className="text-right hidden-m">
                  <img
                    src={imgClose}
                    onClick={this.props.handleCancle}
                    className="cursor pr-4 pt-4"
                  />
                </div>

                <div className="pl-6 mt-5 buydetail-left">
                  <div className="row no-gutters">
                    <div className="col-md-3 col-3">
                      <p className="p-14 black-2">판매자정보</p>
                    </div>
                    <div className="col-md-5 col-9">
                      <p className="p-14 gray-3 bold">
                        {this.state.userData.userId}
                      </p>
                      <div className="buy-detail-gray mt-3">
                        <p className="p-14 gray-3 bold">본인인증</p>
                        <p className="p-13 gray-3 mt-2">
                          <img src={bullet} className="mr-1" /> 휴대폰
                          <span className="float-right orange-3 p-14 bold">
                            {this.state.userData.verifyPhone ? "OK" : "NO"}
                          </span>
                        </p>
                        {/* <p className="p-13 gray-3 mt-2"><img src={bullet} className="mr-1" /> 은행계좌
        <span className="float-right orange-3 p-14 bold">{this.state.userData.verifyBank ? "OK" : "NO"}</span>
        </p> */}
                      </div>
                      <div className="buy-detail-gray mt-3 mt-10-m">
                        <p className="p-14 gray-3 bold">사기피해이력</p>
                        <p className="p-13 gray-3 mt-2">
                          <img src={bullet} className="mr-1" /> 휴대폰
                          <span className="float-right orange-3 p-14 bold">
                            {this.state.userData.cheatPhone === 0
                              ? "Clean"
                              : "사기이력존재"}
                          </span>
                        </p>
                        <p className="p-13 gray-3 mt-2">
                          <img src={bullet} className="mr-1" /> 은행계좌
                          <span className="float-right orange-3 p-14 bold">
                            {this.state.userData.cheatPhone === 0
                              ? "Clean"
                              : "사기이력존재"}
                          </span>
                        </p>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <hr />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-3">
                      <p className="p-14 black-2 pt-1">사이즈</p>
                    </div>
                    <div className="col-md-5 col-9">
                      <button className="btn btn-black">
                        {this.state.data.size}
                      </button>
                    </div>
                    <div className="col-md-8">
                      <hr />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-3">
                      <p className="p-14 black-2 pt-1">출처</p>
                    </div>
                    <div className="col-md-5 col-9">
                      <button className="btn btn-black">
                        {this.state.data.from}
                      </button>
                    </div>
                    <div className="col-md-8">
                      <hr />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-3">
                      <p className="p-14 black-2 pt-1">제품상태</p>
                    </div>
                    <div className="col-md-5 col-9">
                      <button className="btn btn-sky">
                        {this.state.data.status}
                      </button>
                      {this.state.damage1 && (
                        <p className="p-13 gray-3 mt-2 bold">
                          <img src={bullet} className="mr-1" /> 박스 이상 (분실,
                          찌그러짐, 찢어짐 등)
                        </p>
                      )}
                      {this.state.damage2 && (
                        <p className="p-13 gray-3 mt-2 bold">
                          <img src={bullet} className="mr-1" /> 속지 이상 (분실,
                          찌그러짐 등)
                        </p>
                      )}
                      {this.state.damage3 && (
                        <p className="p-13 gray-3 mt-2 bold">
                          <img src={bullet} className="mr-1" /> Tag 이상 (분실,
                          떨어짐 등)
                        </p>
                      )}
                      {this.state.damage4 && (
                        <p className="p-13 gray-3 mt-2 bold">
                          <img src={bullet} className="mr-1" /> 추가 구성품 이상
                          (여분 신발끈 분실 등)
                        </p>
                      )}

                      {!this.state.data.damageDesc &&
                        !this.state.data.statusDesc && (
                          <p className="p-13 gray-3 mt-2">이상없음</p>
                        )}
                      {this.state.data.damageDesc && (
                        <p className="p-13 gray-3 mt-2">
                          {" "}
                          {this.state.data.damageDesc}
                        </p>
                      )}
                      {this.state.data.statusDesc && (
                        <p className="p-13 gray-3 mt-2">
                          {" "}
                          {this.state.data.statusDesc}
                        </p>
                      )}
                    </div>
                    <div className="col-md-8 col-12">
                      <hr />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-3">
                      <p className="p-14 black-2 pt-1">거래방식</p>
                    </div>
                    <div className="col-md-5 col-9">
                      <div>
                        {this.props.shippingType !== "직거래" &&
                          this.state.data.shipping.length === 1 &&
                          this.state.data.shipping[0] === "직거래" && (
                            <span className="cursor p-14 gray-6 mr-4">
                              <img src={imgCheck} className="mr-2" />
                              직거래
                            </span>
                          )}
                        {this.props.shippingType === "직거래" &&
                          this.state.data.shipping.length === 1 &&
                          this.state.data.shipping[0] === "직거래" && (
                            <span className="p-14 gray-3 mr-4">
                              <img src={imgChecked} className="mr-2" />
                              직거래
                            </span>
                          )}

                        {this.props.shippingType !== "택배거래" &&
                          this.state.data.shipping.length === 1 &&
                          this.state.data.shipping[0] === "택배" && (
                            <span className="cursor p-14 gray-6">
                              <img src={imgCheck} className="mr-2" />
                              택배
                            </span>
                          )}
                        {this.props.shippingType === "택배거래" &&
                          this.state.data.shipping.length === 1 &&
                          this.state.data.shipping[0] === "택배" && (
                            <span className="p-14 gray-3">
                              <img src={imgChecked} className="mr-2" />
                              택배
                            </span>
                          )}

                        {this.props.shippingType !== "직거래" &&
                          this.state.data.shipping.length === 2 && (
                            <span className="cursor p-14 gray-6 mr-4">
                              <img src={imgCheck} className="mr-2" />
                              직거래
                            </span>
                          )}
                        {this.props.shippingType === "직거래" &&
                          this.state.data.shipping.length === 2 && (
                            <span className="p-14 gray-3 mr-4">
                              <img src={imgChecked} className="mr-2" />
                              직거래
                            </span>
                          )}

                        {this.props.shippingType !== "택배거래" &&
                          this.state.data.shipping.length === 2 && (
                            <span className="cursor p-14 gray-6">
                              <img src={imgCheck} className="mr-2" />
                              택배
                            </span>
                          )}
                        {this.props.shippingType === "택배거래" &&
                          this.state.data.shipping.length === 2 && (
                            <span className="p-14 gray-3">
                              <img src={imgChecked} className="mr-2" />
                              택배
                            </span>
                          )}
                      </div>
                      <p className="p-13 gray-3 mt-2">
                        <img src={bullet} className="mr-1" />{" "}
                        {this.state.data.location}
                      </p>
                    </div>
                    <div className="col-md-8 col-12">
                      <hr />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-6">
                      <p className="p-14 gray-3">판매가</p>
                      {/* {this.props.shippingType === "택배거래" && <p className="p-14 gray-3 mt-3">택배비</p>} */}
                    </div>
                    <div className="col-md-5 col-6 text-right">
                      <p className="p-16 gray-3 roboto">
                        {currency(this.state.data.price)}
                        <span className="p-14 noto">원</span>
                      </p>
                      {/* {this.props.shippingType === "택배거래" &&
        <p className="p-16 gray-3 roboto mt-3">{currency(4000)}<span className="p-14 noto">원</span></p>} */}
                    </div>
                    <div className="col-md-8 col-12">
                      <hr className="hr2" />
                    </div>
                  </div>

                  <div className="row no-gutters">
                    <div className="col-md-3 col-6">
                      <p className="p-14 bold black-2">결제예정금액</p>
                    </div>
                    <div className="col-md-5 col-6 text-right">
                      <p className="p-18 bold orange roboto">
                        {/* {this.props.shippingType === "택배거래" ? currency(this.state.data.price + 4000) : currency(this.state.data.price)} */}
                        {currency(this.state.data.price)}
                        <span className="p-14 noto normal">원</span>
                      </p>
                    </div>
                  </div>

                  <div className="mt-6 mb-5 pt-1 w-100 hidden-p" />
                </div>
              </div>
            </div>
          </div>

          <Modal display={this.state.modalImage && "block"}>
            <this.ModalImage />
          </Modal>
        </div>
      </div>
    );
  }

  private ModalImage = () => {
    return (
      <div
        className="buy-modal-image"
        style={{
          backgroundImage: `url(${
            this.state.data.images[this.state.selectImage]
          })`,
        }}
      >
        <div className="box-modal-bottom">
          <button
            onClick={() => this.handleImageSelect("-")}
            className="box-arrow-black text-center"
          >
            <img src={imgPrev} style={{ width: 10, height: 18 }} />
          </button>
          <button
            onClick={() => this.setState({ modalImage: false })}
            className="box-arrow-orange text-center"
          >
            <img src={imgClose2} />
          </button>
          <button
            onClick={() => this.handleImageSelect("+")}
            className="box-arrow-black text-center"
          >
            <img src={imgNext} style={{ width: 10, height: 18 }} />
          </button>
        </div>
      </div>
    );
  };
}

export default SellDetail;
