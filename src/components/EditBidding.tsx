import React from "react";
import { currency, timeFormat2 } from "../Method";
import imgClose from "../images/Popup_Delete.svg";

interface EditBiddingState {
  index: string;
  askLow: number;
  askHigh: number;
  tradingCount: number;
  shippings: string[];
  shippingType: string;
  inputPrice: string;
  askEnd: string;
}

class EditBidding extends React.Component<any, EditBiddingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      index: "",
      askLow: 0,
      askHigh: 0,
      tradingCount: 0,
      shippings: [],
      shippingType: "",
      inputPrice: "100000",
      askEnd: "",
    };
  }

  public componentDidMount() {
    this.setState({ index: this.props.index });
    // call axios index use
    this.setState({
      askLow: 0,
      askHigh: 0,
      tradingCount: 0,
      shippings: ["직거래", "택배거래"],
      shippingType: "택배거래",
    });
  }

  private handlePrice = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handlePricePlus = (n: number): void => {
    if (this.state.inputPrice === "") {
      this.setState({ inputPrice: n.toString() });
    } else {
      const merge = parseInt(this.state.inputPrice) + n;
      this.setState({ inputPrice: merge.toString() });
    }
  };

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private existShipping(name: string): boolean {
    for (let i = 0; i < this.state.shippings.length; ++i) {
      if (this.state.shippings[i] === name) return true;
    }

    return false;
  }

  private handleSubmit = (): void => {
    // alert("수정 되었습니다. (테스트용 창)");
    this.props.handleModal();
  };

  public render() {
    return (
      <>
        <div
          className="modal-custom modal-full modal-billing"
          style={{ display: "block" }}
        >
          <div className="modal-content-small">
            <div className="p-3">
              <div className="row">
                <div className="col-6">
                  <p className="p-16 bold black-2">구매요청 상세</p>
                </div>

                <div className="col-6 text-right">
                  <img
                    onClick={this.props.handleModal}
                    src={imgClose}
                    alt=""
                    className="cursor"
                  />
                </div>
              </div>
              <hr className="hr2" />

              <div className="flex bg-gray p-3 pt-4 pb-4">
                <div className="col-4 pl-0 pr-0 border-right-gray text-center">
                  <p className="black-2 p-14">24시간 거래량</p>
                  <p className="black-2 p-14 mt-2">
                    <span className="bold roboto p-18">
                      {this.state.tradingCount}
                    </span>
                    건
                  </p>
                </div>
                <div className="col-4 pl-0 pr-0 border-right-gray text-center">
                  <p className="black-2 p-14">구매요청 최고가</p>
                  <p className="orange p-14 orange mt-2">
                    <span className="bold roboto p-18">
                      {currency(this.state.askHigh)}
                    </span>
                    원
                  </p>
                </div>
                <div className="col-4 pl-0 pr-0 text-center">
                  <p className="black-2 p-14">판매요청 최저가</p>
                  <p className="blue-2 p-14 blue-2 mt-2">
                    <span className="bold roboto p-18">
                      {currency(this.state.askLow)}
                    </span>
                    원
                  </p>
                </div>
              </div>

              <div className="mt-5">
                <p className="black-2 p-14 mb-2">구매요청금액(원)</p>
                <input
                  onChange={this.handlePrice}
                  value={this.state.inputPrice}
                  name="inputPrice"
                  type="text"
                  placeholder="원하시는 금액을 입력해 주세요"
                  className="form-control form-custom mbt-2"
                />

                <div className="row mt-2">
                  <div className="col-3 pr-1">
                    <button
                      onClick={() => this.handlePricePlus(1000)}
                      className="btn-custom-sm w-100"
                    >
                      +1천
                    </button>
                  </div>
                  <div className="col-3 pr-1 pl-0">
                    <button
                      onClick={() => this.handlePricePlus(10000)}
                      className="btn-custom-sm w-100"
                    >
                      +1만
                    </button>
                  </div>
                  <div className="col-3 pr-1 pl-0">
                    <button
                      onClick={() => this.handlePricePlus(100000)}
                      className="btn-custom-sm w-100"
                    >
                      +10만
                    </button>
                  </div>
                  <div className="col-3 pl-0">
                    <button
                      onClick={() => this.handlePricePlus(1000000)}
                      className="btn-custom-sm w-100"
                    >
                      +100만
                    </button>
                  </div>
                </div>
              </div>

              <div className="mt-4">
                <p className="black-2 p-14 mb-2">구매요청 만료일</p>
                <select
                  onChange={this.handleChange}
                  value={this.state.askEnd}
                  defaultValue={"default"}
                  name="askEnd"
                  className="form-control form-select"
                >
                  <option value="default" disabled>
                    구매요청 만료일 선택
                  </option>
                  <option>7일 후 구매요청 만료</option>
                  <option>3일 후 구매요청 만료</option>
                  <option>1일 후 구매요청 만료</option>
                </select>
              </div>

              <div className="mt-4">
                <p className="black-2 p-14 mb-2">거래방법</p>
                <div className="row">
                  <div className="col-6 pr-1">
                    {this.existShipping("직거래") ? (
                      <button
                        onClick={() =>
                          this.setState({ shippingType: "직거래" })
                        }
                        className={`btn-custom-sm w-100 ${
                          this.state.shippingType === "직거래" &&
                          "btn-active-sm-border"
                        }`}
                      >
                        직거래
                      </button>
                    ) : (
                      <button className="btn-custom-sm-disable w-100">
                        직거래
                      </button>
                    )}
                  </div>
                  <div className="col-6 pl-1">
                    {this.existShipping("택배거래") ? (
                      <button
                        onClick={() =>
                          this.setState({ shippingType: "택배거래" })
                        }
                        className={`btn-custom-sm w-100 ${
                          this.state.shippingType === "택배거래" &&
                          "btn-active-sm-border"
                        }`}
                      >
                        택배거래
                      </button>
                    ) : (
                      <button className="btn-custom-sm-disable w-100">
                        택배거래
                      </button>
                    )}
                  </div>
                </div>
              </div>

              <div className="text-center mt-5 mb-3 hidden-m">
                <button
                  onClick={this.props.handleModal}
                  className="btn-custom-sm mr-2"
                >
                  요청취소
                </button>
                <button
                  onClick={this.handleSubmit}
                  className="btn-custom-sm-active"
                >
                  수정
                </button>
              </div>
            </div>
          </div>
          <div className="bottom-box flex p-3 hidden-p">
            <button
              onClick={this.props.handleModal}
              className="btn-custom mr-1"
            >
              요청취소
            </button>
            <button
              onClick={this.handleSubmit}
              className="btn-custom-active ml-1"
            >
              수정
            </button>
          </div>
        </div>
      </>
    );
  }
}

export default EditBidding;
