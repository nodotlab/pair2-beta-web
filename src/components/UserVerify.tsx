import React from "react";
import img1 from "../images/Certification_Completed.svg";
import { withRouter } from "react-router-dom";
import { userVerifyupdate } from "../db/Actions";

class UserVerify extends React.Component<any> {
  private handleVerify = async () => {
    const { IMP }: any = window;
    IMP.init("imp36196053");

    IMP.certification(
      {
        // param
        merchant_uid: "ORD20180131-0000011", // 옵션 값
      },
      (rsp: any) => {
        // callback
        if (rsp.success) {
          const token: any = localStorage.getItem("pair2token");
          userVerifyupdate(token);
          this.props.handleUserVerify();
        } else {
          alert("본인인증을 완료할 수 없습니다. 다시 시도해주세요.");
        }
      }
    );
  };

  public render() {
    return (
      <div className="container text-center">
        <div className="box-userverify">
          <img src={img1} />
          <p className="p-22 black-2 mt-3">본인인증</p>
          <p className="p-14 gray-2 mt-3">
            제품 구매/판매를 위해 최초 1회 본인 인증이 필요합니다. <br />
            본인명의로 등록된휴대번호 인증을 통해 본인인증을 진행해 주세요.
          </p>

          <div className="mt-5">
            <button
              onClick={() => this.props.history.goBack()}
              className="btn btn-custom hidden-m"
            >
              취소
            </button>
            {/* <button onClick={this.props.handleUserVerify} className="btn btn-custom-active ml-3 hidden-m">인증하기</button> */}
            <button
              onClick={this.handleVerify}
              className="btn btn-custom-active ml-3 hidden-m"
            >
              인증하기
            </button>
            <button
              onClick={() => this.props.history.goBack()}
              className="btn-custom-sm mr-1 w-120 hidden-p"
            >
              취소
            </button>
            <button
              onClick={this.handleVerify}
              className="btn-custom-sm-active w-120 hidden-p"
            >
              인증하기
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(UserVerify);
