import React from "react";
import AddBilling from "../components/AddBilling";
import AddShipping from "../components/AddShipping";
import imgBullet from "../images/bullet.svg";
import imgComplete from "../images/Step_Completed.svg";
import { withRouter } from "react-router-dom";
import { currency } from "../Method";

interface AskStepState {
  screen: number;
  shippingType: string;
  selectShipping: number;
  inputPrice: string;
  tradingCount: number;
  askHigh: number;
  askLow: number;
  askEnd: string;
}

class AskStep extends React.Component<any, AskStepState> {
  constructor(props: any) {
    super(props);
    this.state = {
      screen: 1, // default 1
      shippingType: "직거래",
      selectShipping: 0,
      inputPrice: "",
      tradingCount: 30,
      askHigh: 100000,
      askLow: 90000,
      askEnd: "",
    };
  }

  public componentDidMount() {}

  private handlePrice = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handlePricePlus = (n: number): void => {
    if (this.state.inputPrice === "") {
      this.setState({ inputPrice: n.toString() });
    } else {
      const merge = parseInt(this.state.inputPrice) + n;
      this.setState({ inputPrice: merge.toString() });
    }
  };

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleBack = (): void => {
    this.setState({ screen: this.state.screen - 1 });
  };

  private handleNext = (): void => {
    this.setState({ screen: this.state.screen + 1 });
  };

  public render() {
    return (
      <div className="col-md-4 offset-md-4">
        <div className="buy-step">
          {this.state.screen < 2 ? (
            <div
              className={`box-step ${
                this.state.screen === 1 && "box-step-active"
              }`}
            >
              1{this.state.screen === 1 && <p className="step-p3">금액입력</p>}
            </div>
          ) : (
            <img src={imgComplete} alt="" />
          )}

          <div className="line-ask-step" />

          {this.state.screen < 3 ? (
            <div
              className={`box-step ${
                this.state.screen === 2 && "box-step-active"
              }`}
            >
              2
              {this.state.screen === 2 && (
                <p className="step-p">신용카드 등록</p>
              )}
            </div>
          ) : (
            <img src={imgComplete} alt="" />
          )}

          <div className="line-ask-step" />

          {this.state.screen < 4 ? (
            <div
              className={`box-step ${
                this.state.screen === 3 && "box-step-active"
              }`}
            >
              3
              {this.state.screen === 3 && (
                <p className="step-p2">배송지 등록</p>
              )}
            </div>
          ) : (
            <img src={imgComplete} alt="" />
          )}

          <div className="line-ask-step" />

          <div
            className={`box-step ${
              this.state.screen === 4 && "box-step-active"
            }`}
          >
            4
            {this.state.screen === 4 && <p className="step-p">구매요청 내역</p>}
          </div>
        </div>
        {this.state.screen === 1 && <this.ComponentScreen1 />}
        {this.state.screen === 2 && <this.ComponentScreen2 />}
        {this.state.screen === 3 && <this.ComponentScreen3 />}
        {this.state.screen === 4 && <this.ComponentScreen4 />}
      </div>
    );
  }

  private ComponentScreen1 = () => {
    return (
      <div>
        <div className="flex bg-gray p-3 pt-4 pb-4">
          <div className="col-4 ask-box border-right-gray text-center">
            <p className="black-2 p-14">24시간 거래량</p>
            <p className="black-2 p-14 mt-2">
              <span className="bold roboto p-18">
                {this.state.tradingCount}
              </span>
              건
            </p>
          </div>
          <div className="col-4 ask-box border-right-gray text-center">
            <p className="black-2 p-14">구매요청 최고가</p>
            <p className="orange p-14 orange mt-2">
              <span className="bold roboto p-18">
                {currency(this.state.askLow)}
              </span>
              원
            </p>
          </div>
          <div className="col-4 ask-box text-center">
            <p className="black-2 p-14">판매요청 최저가</p>
            <p className="blue-2 p-14 blue-2 mt-2">
              <span className="bold roboto p-18">
                {currency(this.state.askHigh)}
              </span>
              원
            </p>
          </div>
        </div>

        <div className="row mt-5">
          <div className="col-md-4">
            <p className="black-2 p-14 pt-2">구매요청금액(원)</p>
          </div>
          <div className="col-md-8">
            <input
              onChange={this.handlePrice}
              value={this.state.inputPrice}
              name="inputPrice"
              type="text"
              placeholder="원하시는 금액을 입력해 주세요"
              className="form-control form-custom"
            />

            <div className="row mt-3">
              <div className="col-3 pr-1">
                <button
                  onClick={() => this.handlePricePlus(1000)}
                  className="btn-custom-sm w-100"
                >
                  +1천
                </button>
              </div>
              <div className="col-3 pr-1 pl-0">
                <button
                  onClick={() => this.handlePricePlus(10000)}
                  className="btn-custom-sm w-100"
                >
                  +1만
                </button>
              </div>
              <div className="col-3 pr-1 pl-0">
                <button
                  onClick={() => this.handlePricePlus(100000)}
                  className="btn-custom-sm w-100"
                >
                  +10만
                </button>
              </div>
              <div className="col-3 pl-0">
                <button
                  onClick={() => this.handlePricePlus(1000000)}
                  className="btn-custom-sm w-100"
                >
                  +100만
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-4">
          <div className="col-md-4">
            <p className="black-2 p-14 pt-2">구매요청 만료일</p>
          </div>
          <div className="col-md-8">
            <select
              onChange={this.handleChange}
              value={this.state.askEnd}
              defaultValue={"default"}
              name="askEnd"
              className="form-control form-select"
            >
              <option value="default" disabled>
                구매요청 만료일 선택
              </option>
              <option>7일 후 구매요청 만료</option>
              <option>3일 후 구매요청 만료</option>
              <option>1일 후 구매요청 만료</option>
            </select>
          </div>
        </div>

        <div className="row mt-4">
          <div className="col-md-4">
            <p className="black-2 p-14 pt-2">거래방법</p>
          </div>
          <div className="col-md-8">
            <div className="row">
              <div className="col-6 pr-1">
                <button
                  onClick={() => this.setState({ shippingType: "직거래" })}
                  className={`btn-custom-sm w-100 ${
                    this.state.shippingType === "직거래" &&
                    "btn-active-sm-border"
                  }`}
                >
                  직거래
                </button>
              </div>
              <div className="col-6 pl-1">
                <button
                  onClick={() => this.setState({ shippingType: "택배거래" })}
                  className={`btn-custom-sm w-100 ${
                    this.state.shippingType === "택배거래" &&
                    "btn-active-sm-border"
                  }`}
                >
                  택배거래
                </button>
              </div>
            </div>
          </div>
        </div>

        <button
          onClick={this.handleNext}
          className="btn-custom-active mt-5 w-100 hidden-m"
        >
          다음
        </button>

        <div className="row bottom-box p-3">
          <button
            onClick={this.handleNext}
            className="btn-custom-active w-100 hidden-p"
          >
            다음
          </button>
        </div>
      </div>
    );
  };

  private ComponentScreen2 = () => {
    return (
      <AddBilling handleNext={this.handleNext} handleBack={this.handleBack} />
    );
  };

  private ComponentScreen3 = () => {
    return (
      <AddShipping handleNext={this.handleNext} handleBack={this.handleBack} />
    );
  };

  private ComponentScreen4 = () => {
    return (
      <div>
        <p className="p-14 black-2">구매요청 내역</p>
        <p className="gray-2 p-13 mt-2">
          <img src={imgBullet} alt="" /> 거래방식에 따라 최종 결제 금액이 달라질
          수 있습니다.
        </p>
        <p className="gray-2 p-13 mt-2">
          <img src={imgBullet} alt="" /> 택배거래의 경우 택배비 4,000원이
          추가됩니다.
        </p>

        <div className="box-pay-info">
          <div className="row">
            <div className="col-6">
              <p className="gray-3 p-14">구매요청 금액</p>
            </div>
            <div className="col-6 text-right">
              <p className="gray-3 p-16">
                {currency(parseInt(this.state.inputPrice))}
                <span className="p-14">원</span>
              </p>
            </div>
          </div>
          {this.state.shippingType === "택배거래" && (
            <div className="row mt-2">
              <div className="col-6">
                <p className="gray-3 p-14">택배비</p>
              </div>
              <div className="col-6 text-right">
                <p className="gray-3 p-16">
                  {currency(4000)}
                  <span className="p-14">원</span>
                </p>
              </div>
            </div>
          )}

          <hr className="hr2 mb-1" />

          <div className="row mt-4">
            <div className="col-6">
              <p className="gray-3 p-14 bold">결제예정금액</p>
            </div>
            <div className="col-6 text-right">
              {this.state.shippingType === "택배거래" ? (
                <p className="orange p-18 bold">
                  {currency(parseInt(this.state.inputPrice) + 4000)}
                  <span className="p-14">원</span>
                </p>
              ) : (
                <p className="orange p-18 bold">
                  {currency(parseInt(this.state.inputPrice))}
                  <span className="p-14">원</span>
                </p>
              )}
            </div>
          </div>
        </div>

        <div className="row mt-5 hidden-m">
          <div className="col-md-6">
            <button onClick={this.handleBack} className="btn-custom w-100">
              이전
            </button>
          </div>
          <div className="col-md-6">
            {
              <button
                onClick={() => this.props.history.push("/askcomplete")}
                className="btn-custom-active w-100"
              >
                구매요청
              </button>
            }
          </div>
        </div>

        <div className="row bottom-box hidden-p p-3">
          <div className="col-6 pl-0 pr-1">
            <button onClick={this.handleBack} className="btn-custom w-100">
              이전
            </button>
          </div>
          <div className="col-6 pl-1 pr-0">
            {
              <button
                onClick={() => this.props.history.push("/askcomplete")}
                className="btn-custom-active w-100"
              >
                구매요청
              </button>
            }
          </div>
        </div>
      </div>
    );
  };
}

export default withRouter(AskStep);
