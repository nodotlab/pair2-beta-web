import React from "react";
import DaumPostcode from "react-daum-postcode";
import CardState from "../reduce/BillingState";
import BankState from "../reduce/BankState";
import { currency } from "../Method";
import {
  getBank,
  getCard,
  getCommision,
  tradeEdit,
  putBank,
  tradeDetail,
  getSneakerIndex,
} from "../db/Actions";
import imgClose from "../images/tab_close.svg";
import camera from "../images/Camera.svg";
import imgOpen from "../images/tab_open.svg";
import imgThumb from "../images/thumb.png";
import bullet from "../images/bullet.svg";
import check from "../images/Condition_Check.svg";
import modalClose from "../images/Popup_Delete.svg";
import filterX from "../images/GNB_Close.svg";
import imgPrice from "../images/Price_Sub.svg";
import imgPrev from "../images/back.svg";
import prev from "../images/backblack.svg";
import imgCloseX from "../images/Popup_Delete.svg";

class CurrentSneaker {
  public index: string;
  public brand: string;
  public name: string;
  public imgUrl: string;

  constructor(_index: string, _brand: string, _name: string, _imgUrl: string) {
    this.index = _index;
    this.brand = _brand;
    this.name = _name;
    this.imgUrl = _imgUrl;
  }
}

interface SellEditState {
  currentSneaker: CurrentSneaker;
  openCondition2: boolean;
  openCondition3: boolean;
  openCondition4: boolean;
  openCondition5: boolean;
  size: string;
  from: string;
  damage1: boolean;
  damage2: boolean;
  damage3: boolean;
  damage4: boolean;
  damageDesc: string;
  damageCheck: boolean;
  status1: boolean;
  status2: boolean;
  status3: boolean;
  statusDesc: string;
  statusCheck: boolean;
  imageUrls: string[];
  file: any[];
  shippingType: string;
  location: string[];
  modal: boolean;
  modal2: boolean;
  modalAddress: boolean;
  price: number;
  priceInput: number;
  step2check: boolean;
  index: number; // trade index
  cardState: CardState;
  bankState: BankState;
  owner: string;
  bank: string;
  account: string;
  bankList: string[];
  commision: number;
  sale: number;
}

class SellEdit extends React.Component<any, SellEditState> {
  constructor(props: any) {
    super(props);
    this.state = {
      currentSneaker: new CurrentSneaker(
        Date.now().toString(),
        "",
        "",
        imgThumb,
      ),
      openCondition2: false,
      openCondition3: false,
      openCondition4: false,
      openCondition5: true,
      size: "",
      from: "",
      damage1: false,
      damage2: false,
      damage3: false,
      damage4: false,
      damageDesc: "",
      damageCheck: false,
      status1: false,
      status2: false,
      status3: false,
      statusDesc: "",
      statusCheck: false,
      imageUrls: [],
      modal: false,
      file: [],
      shippingType: "",
      location: [],
      modal2: false,
      modalAddress: false,
      price: 0,
      priceInput: 0,
      step2check: false,
      index: Date.now(),
      cardState: new CardState("", ""),
      bankState: new BankState("", "", ""),
      owner: "",
      bank: "",
      account: "",
      bankList: this.initBankList(),
      commision: 0,
      sale: 0,
    };
  }

  public componentDidMount() {
    const signin = localStorage.getItem("pair2signin");
    if (signin !== "true") this.props.history.push("/login/");

    this.initData();
    this.commition();
  }

  private async commition() {
    const result = await getCommision();
    this.setState({
      commision: result.commision,
      sale: result.sale,
    });
  }

  private async initData() {
    const result: any = await tradeDetail(this.props.index);
    this.setState({
      from: result.from,
      damage1: result.damage1,
      damage2: result.damage2,
      damage3: result.damage3,
      damage4: result.damage4,
      damageDesc: result.damageDesc ? result.damageDesc : "",
      damageCheck: true,
      status1: result.status1,
      status2: result.status2,
      status3: result.status3,
      statusDesc: result.statusDesc ? result.statusDesc : "",
      statusCheck: true,
      imageUrls: result.imageUrls,
      shippingType: result.shippingType,
      price: result.price,
      priceInput: result.price / 1000,
      size: result.size,
    });

    if (result.location2) {
      this.setState({ location: [result.location1, result.location2] });
    } else {
      this.setState({ location: [result.location1] });
    }

    this.getCurrentSneaker(result.sneakerIndex);
    this.getBank();
  }

  private async getCurrentSneaker(index: string) {
    const { result }: any = await getSneakerIndex(index);
    const sneaker: CurrentSneaker = new CurrentSneaker(
      result.index,
      result.brand,
      result.name,
      result.thumbUrl,
    );

    this.setState({ currentSneaker: sneaker });
  }

  private fileRef: any = React.createRef();
  private handleFileRef = (): void => this.fileRef.current.click();

  private async getBank() {
    const token: any = localStorage.getItem("pair2token");

    // try {
    //   const result = await getCard(token);
    //   if (result !== "faild") {
    //     this.setState({
    //       cardState: new CardState(result.cardName, result.cardNumber),
    //     });
    //   }
    // } catch (error) {}
    try {
      const result = await getBank(token);
      this.setState({
        owner: result.owner,
        bank: result.bankName,
        account: result.account,
        bankState: new BankState(result.bankName, result.account, result.owner),
      });
    } catch (error) {}
  }

  private initBankList(): string[] {
    const list: string[] = [
      "KB국민",
      "신한",
      "우리",
      "KEB하나",
      "케이뱅크",
      "카카오뱅크",
      "KDB산업",
      "IBK기업",
      "NH농협",
      "수협",
      "대구",
      "BNK부산",
      "BNK경남",
      "광주",
      "전북",
      "제주",
      "한국씨티",
      "SC제일",
      "새마을금고",
      "우체국",
    ];

    return list;
  }

  private handleFile = (e: any): void => {
    if (this.state.file.length > 14) return;
    const file = this.state.file;
    file.push(e.target.files[0]);

    this.setState({ file: file });
  };

  private handleImageDelete = (index: number) => {
    const file: any[] = [];
    for (let i = 0; i < this.state.file.length; ++i) {
      if (index !== i) file.push(this.state.file[i]);
    }

    this.setState({ file: file });
  };

  private handleText = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleDamageCheck = (): void => {
    this.setState({
      damageCheck: true,
      openCondition3: false,
      // openCondition4: true,
      // modal: true,
    });
  };

  private handleStatusCheck = (): void => {
    this.setState({
      statusCheck: true,
      openCondition4: false,
      // openCondition5: true,
    });
  };

  public handleCheckTrue = (): void => {
    this.setState({ step2check: true });
  };
  public handleCheckFalse = (): void => {
    this.setState({ step2check: false });
  }; // step2 check handler pass to props

  private handleAddress = (data: any): void => {
    const locations = this.state.location;

    if (locations.length < 2) {
      locations.push(data.sigungu);
    }

    this.setState({
      modalAddress: false,
      location: locations,
    });
  };

  private handlePopLocation = (index: number): void => {
    const locations: string[] = [];
    for (let i = 0; i < this.state.location.length; ++i) {
      if (index !== i) locations.push(this.state.location[i]);
    }

    this.setState({ location: locations });
  };

  private handlePrice = (e: any): void => {
    let n = e.target.value.replace(/\D/, "");
    n = parseInt(n);
    if (isNaN(n)) {
      this.setState({
        priceInput: 0,
        price: 0,
      });
    } else {
      this.setState({
        priceInput: n,
        price: n * 1000,
      } as any);
    }
  };

  private handlePricePlus = (value: number) => {
    let price: number = this.state.price;
    price += value;
    this.setState({ price: price });
  };

  private handleModal = (): void => {
    this.setState({ modal: !this.state.modal });
  };

  private verifyBank(): boolean {
    if (this.state.bank === "") return false;
    if (this.state.owner === "") return false;
    if (this.state.account === "") return false;

    return true;
  }

  private handleSubmitBank = async () => {
    const token = localStorage.getItem("pair2token");
    const result = await putBank({
      token: token,
      bank: {
        bankName: this.state.bank,
        owner: this.state.owner,
        account: this.state.account,
      },
    });

    this.setState({ modal: false });
    this.getBank();
  };

  private handleSubmit = async () => {
    const data = {
      index: this.props.index,
      from: this.state.from,
      damage1: this.state.damage1,
      damage2: this.state.damage2,
      damage3: this.state.damage3,
      damage4: this.state.damage4,
      damageDesc: this.state.damageDesc,
      status1: this.state.status1,
      status2: this.state.status2,
      status3: this.state.status3,
      statusDesc: this.state.statusDesc,
      imageUrls: this.state.imageUrls,
      shippingType: this.state.shippingType,
      price: this.state.price,
      location1: this.state.location[0] ? this.state.location[0] : "",
      location2: this.state.location[1] ? this.state.location[1] : "",
    };

    const result = await tradeEdit(data);
    this.props.handleToSell();
  };

  public render() {
    return (
      <div className="modal-fullscreen pb-5">
        <div>
          <nav className="navbar fixed-top navbar-expand-lg navbar-dark navbar-sneaker-detail">
            <div
              onClick={this.props.handleToSell}
              className="navbar-brand navbar-title"
            >
              <img src={imgPrev} className="navbar-prev" alt="" />
            </div>
          </nav>
        </div>

        <div className="pt-4 pb-4 bg-gray m-norow">
          <div className="container">
            <div className="row">
              <div className="col-4 pr-0">
                <div
                  className="img-buy-left"
                  style={{
                    backgroundImage: `url(${this.state.currentSneaker.imgUrl})`,
                  }}
                />
              </div>
              <div className="col-8">
                <p className="p-14 bold roboto gray-2">
                  {this.state.currentSneaker.brand}
                </p>
                <p className="p-16 roboto black-2 mt-1">
                  {this.state.currentSneaker.name}
                </p>
                <p className="p-14 roboto gray-2 mt-1">
                  Size {this.state.size}
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <this.ComponentScreen0 />
          <this.ComponentScreen1 />
        </div>
      </div>
    );
  }

  private ComponentScreen0 = () => {
    return (
      <>
        <hr className="mt-4 mb-2" />
        <div className="row o-60">
          <div className="col-9">
            <p className="p-18 bold gray-4">
              01{" "}
              <span className="p-14 black-2 normal ml-1">새 제품인가요?</span>
            </p>
          </div>
          <div className="col-3 pt-2 text-right">
            <p className="p-14 roboto bold orange-3 float-left">OK</p>
            <img src={imgClose} alt="" className="mt-m-8" />
          </div>
        </div>
        <hr className="mt-2 mb-2" />
        {/* condition1 */}

        <div
          onClick={() =>
            this.setState({ openCondition2: !this.state.openCondition2 })
          }
          className="row mt-3 cursor"
        >
          <div className="col-9">
            <p className="p-18 bold gray-4">
              02{" "}
              <span className="p-14 black-2 normal ml-1">
                구입 경로를 선택해 주세요.
              </span>
            </p>
          </div>
          <div className="col-3 pt-1 text-right">
            <p className="p-14 roboto bold orange-3 float-left">
              {this.state.from !== "" && "OK"}
            </p>
            {this.state.openCondition2 ? (
              <img src={imgOpen} alt="" className="mt-m-8" />
            ) : (
              <img src={imgClose} alt="" className="mt-m-8" />
            )}
          </div>
        </div>
        {this.state.openCondition2 && (
          <div className="mt-2 mb-3">
            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-1" /> 수입 신고하지 않고
              해외에서 구입한 제품을 판매할 경우 법적 불이익이 받을 수 있습니다.
            </p>
            <div className="text-center mt-3">
              <button
                onClick={() =>
                  this.setState({ from: "국내제품", openCondition2: false })
                }
                className={`btn-custom-sm w-30p mr-1 ${
                  this.state.from === "국내제품" && "btn-active-sm-border"
                }`}
              >
                국내제품
              </button>
              <button
                onClick={() =>
                  this.setState({ from: "해외제품", openCondition2: false })
                }
                className={`btn-custom-sm w-30p mr-1 ${
                  this.state.from === "해외제품" && "btn-active-sm-border"
                }`}
              >
                해외제품
              </button>
              <button
                onClick={() =>
                  this.setState({ from: "알수없음", openCondition2: false })
                }
                className={`btn-custom-sm w-30p ${
                  this.state.from === "알수없음" && "btn-active-sm-border"
                }`}
              >
                알수없음
              </button>
            </div>
          </div>
        )}
        <hr className="mt-2 mb-2" />
        {/* condition2 */}

        <div
          onClick={() =>
            this.setState({ openCondition3: !this.state.openCondition3 })
          }
          className="row mt-3 cursor"
        >
          <div className="col-9">
            <p className="p-18 bold gray-4">
              03{" "}
              <span className="p-14 black-2 normal ml-1">
                구성품 상태를 체크해 주세요.
              </span>
            </p>
          </div>
          <div className="col-3 pt-1 text-right">
            <p className="p-14 roboto bold orange-3 float-left">
              {this.state.damageCheck && "OK"}
            </p>
            {this.state.openCondition3 ? (
              <img src={imgOpen} alt="" className="mt-m-8" />
            ) : (
              <img src={imgClose} alt="" className="mt-m-8" />
            )}
          </div>
        </div>
        {this.state.openCondition3 && (
          <div className="mt-2 mb-3">
            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-1" /> 박스, 속지, Tag, 추가
              구성품 등에 대한 상태를 체크해 주세요.
            </p>
            <p className="p-13 gray-2 mb-3">
              <img src={bullet} alt="" className="mr-1" /> 복수 선택이
              가능합니다.
            </p>
            <p className="p-13 gray-2">
              <img src={bullet} alt="" className="mr-1" /> 이상이 없는 경우 아래
              이상 없음 버튼을 클릭해주세요.
            </p>
            <div className="mt-3 text-left">
              <div
                onClick={() => this.setState({ damage1: !this.state.damage1 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 ${
                  this.state.damage1 && "btn-active-sm-border"
                }`}
              >
                박스 이상 (분실, 찌그러짐, 찢어짐 등){" "}
                {this.state.damage1 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
              <div
                onClick={() => this.setState({ damage2: !this.state.damage2 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                  this.state.damage2 && "btn-active-sm-border"
                }`}
              >
                속지 이상 (분실, 찢어짐 등){" "}
                {this.state.damage2 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
              <div
                onClick={() => this.setState({ damage3: !this.state.damage3 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                  this.state.damage3 && "btn-active-sm-border"
                }`}
              >
                Tag 이상 (분실, 떨어짐 등){" "}
                {this.state.damage3 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
              <div
                onClick={() => this.setState({ damage4: !this.state.damage4 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                  this.state.damage4 && "btn-active-sm-border"
                }`}
              >
                추가 구성품 이상 (여분 신발끈 분실 등){" "}
                {this.state.damage4 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
            </div>

            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-1" /> 기타 특이사항
            </p>
            <textarea
              className="form-control textarea-custom mt-2"
              value={this.state.damageDesc}
              onChange={this.handleText}
              name="damageDesc"
              rows={5}
              maxLength={50}
              placeholder="박스, 속지, 기본 구성품 상태에 대한 특이사항이 있다면 50자 내로 입력해 주세요."
            />
            <p className="text-right p-13 gray-2 mt-1">
              {this.state.damageDesc.length}/50
            </p>
            <div className="text-center mt-2">
              <button
                onClick={this.handleDamageCheck}
                className="btn-second-sm w-100 h-50p bold"
              >
                구성품 상태 체크완료
              </button>
            </div>
          </div>
        )}
        <hr className="mt-2 mb-2" />
        {/* condition3 */}

        <div
          onClick={() =>
            this.setState({ openCondition4: !this.state.openCondition4 })
          }
          className="row mt-3 cursor"
        >
          <div className="col-9">
            <p className="p-18 bold gray-4">
              04{" "}
              <span className="p-14 black-2 normal ml-1">
                신발 상태를 체크해주세요.
              </span>
            </p>
          </div>
          <div className="col-3 pt-1 text-right">
            <p className="p-14 roboto bold orange-3 float-left">
              {this.state.statusCheck && "OK"}
            </p>
            {this.state.openCondition4 ? (
              <img src={imgOpen} alt="" className="mt-m-8" />
            ) : (
              <img src={imgClose} alt="" className="mt-m-8" />
            )}
          </div>
        </div>
        {this.state.openCondition4 && (
          <div className="mt-2 mb-3">
            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-1" /> 신발에 오염, 변색
              등에 대한 상태를 체크해주세요.
            </p>
            <p className="p-13 gray-2">
              <img src={bullet} alt="" className="mr-1" /> 복수 선택이
              가능합니다.
            </p>
            <div className="mt-3 text-left">
              <div
                onClick={() => this.setState({ status1: !this.state.status1 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 ${
                  this.state.status1 && "btn-active-sm-border"
                }`}
              >
                변색 또는 오염{" "}
                {this.state.status1 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
              <div
                onClick={() => this.setState({ status2: !this.state.status2 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                  this.state.status2 && "btn-active-sm-border"
                }`}
              >
                스크래치, 흠집, 기스, 상처{" "}
                {this.state.status2 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
              <div
                onClick={() => this.setState({ status3: !this.state.status3 })}
                className={`btn-custom-sm w-100 cursor pl-3 pt-2 mt-1 ${
                  this.state.status3 && "btn-active-sm-border"
                }`}
              >
                기본 봉제 이상{" "}
                {this.state.status3 && (
                  <img src={check} alt="" className="float-right mr-3 mt-1" />
                )}
              </div>
            </div>

            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-1" /> 기타 특이사항
            </p>
            <textarea
              className="form-control textarea-custom mt-2"
              value={this.state.statusDesc}
              name="statusDesc"
              onChange={this.handleText}
              rows={5}
              maxLength={50}
              placeholder="제품에 대한 기타 특이사항이 있다면 50자 내로 입력해 주세요."
            />
            <p className="text-right p-13 gray-2 mt-1">
              {this.state.statusDesc.length}/50
            </p>
            <div className="text-center mt-2">
              <button
                onClick={this.handleStatusCheck}
                className="btn-second-sm w-100 h-50p bold"
              >
                신발상태 체크완료
              </button>
            </div>
          </div>
        )}
        <hr className="mt-2 mb-2" />
        {/* condition4 */}

        <div
          onClick={() =>
            this.setState({ openCondition5: !this.state.openCondition5 })
          }
          className="row mt-3 cursor"
        >
          <div className="col-9">
            <p className="p-18 bold gray-4">
              05{" "}
              <span className="p-14 black-2 normal ml-1">
                신발 사진을 3장 이상 촬영해 주세요.
              </span>
            </p>
          </div>
          <div className="col-3 pt-1 text-right">
            <p className="p-14 roboto bold orange-3 float-left">
              {this.state.file.length > 2 && "OK"}
            </p>
            {this.state.openCondition5 ? (
              <img src={imgOpen} alt="" className="mt-m-8" />
            ) : (
              <img src={imgClose} alt="" className="mt-m-8" />
            )}
          </div>
        </div>
        {/* condition5 container end */}

        {this.state.openCondition5 && (
          <>
            <div className="flex">
              <div className="sell-camerabox">
                <div className="sell-camera">
                  <img src={camera} alt="" />
                  <p className="p-12 gray-2 roboto mt-1">
                    {this.state.imageUrls.length}/15
                  </p>
                </div>
              </div>
              <div className="horizontal-sell">
                {this.state.imageUrls.map((file: any, i: number) => (
                  <div key={i} className="sell-photobox">
                    {/* <div className="sell-photo" style={{backgroundImage: `url(${URL.createObjectURL(file)})`}} /> */}
                    <div
                      className="sell-photo"
                      style={{ backgroundImage: `url(${file})` }}
                    />
                  </div>
                ))}
              </div>
            </div>
          </>
        )}
        <hr className="mt-2 mb-2" />
        {/* condition5 end */}
      </>
    );
  };

  private ComponentScreen1 = () => {
    return (
      <>
        <div className="mt-4 mb-5">
          <p className="p-14 black-2">거래방식 선택</p>
          <p className="p-13 gray-2 mt-1">
            <img src={bullet} alt="" className="mr-1" />
            가능한 거래 방식을 선택해 주세요.
          </p>
          <p className="p-13 gray-2 mt-1">
            <img src={bullet} alt="" className="mr-1" />
            두가지 거래방식이 모두 가능하다면 "직거래/택배"를 선택하세요.
          </p>

          <div className="row mt-3">
            <div className="col-4 pr-1">
              <button
                onClick={() => this.setState({ shippingType: "직거래" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "직거래" && "btn-active-sm-border"
                }`}
              >
                직거래
              </button>
            </div>
            <div className="col-4 pl-1 pr-1">
              <button
                onClick={() => this.setState({ shippingType: "택배" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "택배" && "btn-active-sm-border"
                }`}
              >
                택배
              </button>
            </div>
            <div className="col-4 pl-1">
              <button
                onClick={() => this.setState({ shippingType: "직거래/택배" })}
                className={`btn-custom-sm w-100 ${
                  this.state.shippingType === "직거래/택배" &&
                  "btn-active-sm-border"
                }`}
              >
                직거래/택배
              </button>
            </div>
          </div>

          <div className="container bg-gray">
            <div className="row mt-3 pt-3 pb-3">
              <div className="col-8">
                <p className="p-13 gray-3 mt-1">
                  거래지역
                  <b className="ml-2">
                    {this.state.location.length > 0 && this.state.location[0]}
                    {this.state.location.length > 1 && "/"}
                    {this.state.location.length > 1 && this.state.location[1]}
                  </b>
                </p>
              </div>
              <div className="col-4 text-right">
                <button
                  onClick={() => this.setState({ modal2: true })}
                  className="btn-custom-xs"
                >
                  {this.state.location.length === 0 ? "등록" : "수정"}
                </button>
              </div>
            </div>
          </div>

          <p className="black-2 bold p-14 mt-4">판매가(원)</p>
          <div className="flex">
            <input
              onChange={this.handlePrice}
              type="number"
              pattern="[0-9]*"
              inputMode="numeric"
              className="form-control form-custom form-price form-price-edit mt-1 text-right p-18 roboto bold"
              value={this.state.priceInput === 0 ? "" : this.state.priceInput}
            />
            <p className="price-right-mypage p-18 roboto bold">
              {this.state.priceInput > 0 && "000"}
            </p>
          </div>

          <hr />
          <div className="pt-2 pb-1">
            <p className="p-14 gray-3 float-left">판매가</p>
            <p className="p-16 roboto gray-3 float-right">
              {currency(this.state.price)}
              <span className="p-14 noto">원</span>
            </p>
          </div>
          <div className="pt-4">
            <p className="p-13 gray-2 float-left gray-2">
              <img src={imgPrice} alt="" className="mr-2" />
              결제 수수료({this.state.commision}%)
            </p>
            <p className="p-14 roboto gray-2 float-right">
              -
              {currency(
                Math.round(this.state.price * (this.state.commision * 0.01)),
              )}
              <span className="p-13 noto">원</span>
            </p>
          </div>
          <div className="pt-4 pb-3">
            <p className="p-13 gray-2 float-left gray-2">
              <img src={imgPrice} alt="" className="mr-2" />
              수수료 할인
            </p>
            <p className="p-14 roboto gray-2 float-right">
              +
              {currency(
                Math.round(this.state.price * (this.state.sale * 0.01)),
              )}
              <span className="p-13 noto">원</span>
            </p>
          </div>
          <hr className="hr2 mt-4" />
          <div className="pt-1">
            <p className="p-14 black-2 bold float-left gray-2">정산예정금액</p>
            <p className="p-18 roboto orange bold float-right">
              {currency(
                this.state.price -
                  Math.round(
                    this.state.price * (this.state.commision * 0.01) -
                      this.state.price * (this.state.sale * 0.01),
                  ),
              )}
              <span className="p-14 noto normal">원</span>
            </p>
          </div>
        </div>

        {/* <div className="bg-gray">
      <div className="row p-3">
      <div className="col-8">
        <p className="p-14 black-2 mt-1">신용카드 (PG 테스트중)</p>
      </div>
      <div className="col-4 text-right">
        <button className="btn-second-xs">수정</button>
      </div>
      </div>
      </div> */}

        {/* <div className="bg-gray mt-2">
      <div className="row p-3">
      <div className="col-md-3 col-3">
      <p className="p-14 black-2">정산계좌</p>
      </div>
      <div className="col-md-7 col-9 text-right">
        <p className="p-14 gray-2">{this.state.bankState.onwer} / {this.state.bankState.bankName} {this.state.bankState.account}</p>
      </div>
      <div className="col-md-2 text-right hidden-m">
        <button onClick={() => this.setState({ modal: true })} className="btn-second-xs float-right">수정</button>
      </div>

      <div className="text-right col-12 mt-2 hidden-p">
        <button onClick={() => this.setState({ modal: true })} className="btn-second-xs">수정</button>
      </div>
      </div>
      </div> */}

        <div className="row mt-5 hidden-m">
          <div className="col-6 pr-1">
            <button
              onClick={this.props.handleToSell}
              className="btn-custom w-100"
            >
              이전
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className="btn-custom-active w-100"
            >
              수정완료
            </button>
          </div>
        </div>

        <div className="row pt-2 pb-2 bottom-box hidden-p">
          <div className="col-6 pr-1">
            <button
              onClick={this.props.handleToSell}
              className="btn-custom w-100"
            >
              이전
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className="btn-custom-active w-100"
            >
              수정완료
            </button>
          </div>
        </div>
        <this.ModalLocation />
        <this.ModalBank />
      </>
    );
  };

  private ModalBank = () => {
    return (
      <div
        className="modal-custom modal-full"
        style={{ display: this.state.modal ? "block" : "none" }}
      >
        <div className="modal-content-small">
          <div className="p-3">
            <div className="row">
              <div className="col-6">
                <p className="p-16 bold black-2">환급계좌 등록</p>
              </div>
              <div className="col-6 text-right">
                <img
                  onClick={this.handleModal}
                  src={modalClose}
                  alt=""
                  className="cursor"
                />
              </div>
            </div>
            <hr className="hr2" />
            <p className="p-13 gray-2">
              <img src={bullet} alt="" className="mr-1" />
              본인명의의 계좌만 등록할 수 있습니다.
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-1" />
              휴대폰 번호 등으로 만든 평생계좌번호 및 가상계좌,
              펀드/적금/정기예금 등에는 계좌는 등록할 수 없습니다.
            </p>

            <p className="p-14 black-2 mt-4 mb-2">은행</p>
            <select
              onChange={this.handleText}
              value={this.state.bank}
              className="form-control form-select"
              name="bank"
            >
              <option value="" disabled>
                은행을 선택해 주세요.
              </option>
              {this.state.bankList.map((bank: string, i: number) => (
                <option key={i}>{bank}</option>
              ))}
            </select>

            <p className="p-14 black-2 mt-4 mb-2">계좌번호</p>
            <input
              className="form-control form-custom"
              value={this.state.account}
              type="text"
              name="account"
              onChange={this.handleChange}
            />

            <p className="p-14 black-2 mt-4 mb-2">예금주</p>
            <input
              className="form-control form-custom"
              value={this.state.owner}
              type="text"
              name="owner"
              maxLength={20}
              onChange={this.handleText}
            />
          </div>

          <div className="bottom-box p-3">
            {this.verifyBank() ? (
              <button
                onClick={this.handleSubmitBank}
                className="btn-custom-active w-100"
              >
                등록하기
              </button>
            ) : (
              <button className="btn-custom-disable w-100">등록하기</button>
            )}
          </div>
        </div>
      </div>
    );
  };

  private ModalLocation = () => {
    return (
      <div
        className="modal-full modal-overflow z-6000"
        style={{ display: this.state.modal2 ? "block" : "none" }}
      >
        {this.state.modalAddress ? (
          <div className="modal-content-small">
            <div className="container">
              <div className="row p-3">
                <div className="col-6 offset-3">
                  <p className="p-16 bold black-2 text-center ">
                    거래지역 검색
                  </p>
                </div>
                <div className="col-3 text-right">
                  <img
                    onClick={() => this.setState({ modalAddress: false })}
                    src={imgCloseX}
                    alt=""
                    className="cursor"
                  />
                </div>
              </div>
              <DaumPostcode
                onComplete={this.handleAddress}
                style={{ position: "absolute" }}
              />
            </div>
          </div>
        ) : (
          <div className="modal-content-small">
            <div className="container">
              <div className="row pt-2 pb-2">
                <div className="col-1">
                  <img
                    onClick={() => this.setState({ modal2: false })}
                    src={prev}
                    alt=""
                    className="cursor"
                  />
                </div>
                <div className="col-11">
                  <p className="p-16 bold black-2">거래지역 등록</p>
                </div>
              </div>
            </div>
            <div className="col-12">
              <div className="detail-border-top" />
            </div>

            <div className="container">
              {!this.state.modalAddress && this.state.location.length < 2 && (
                <button
                  onClick={() => this.setState({ modalAddress: true })}
                  className="btn-custom-active w-100"
                >
                  지역 검색
                </button>
              )}

              <p className="p-14 black-2 mt-3">선택지역</p>
              <p className="p-13 gray-2 mt-1">
                <img src={bullet} alt="" className="mr-1" />
                최대 2지역까지 등록 가능합니다.
              </p>

              {this.state.location.length > 0 ? (
                <div className="pt-3 pb-3 mt-2">
                  {this.state.location.map((location: string, i: number) => (
                    <button key={i} className="btn btn-filtered">
                      {location}
                      <img
                        onClick={() => this.handlePopLocation(i)}
                        src={filterX}
                        className="icon-close ml-2"
                      />
                    </button>
                  ))}
                </div>
              ) : (
                <div className="bg-gray text-center pt-3 pb-3 mt-2">
                  <p className="p-14 black-2">등록된 지역이 없습니다.</p>
                </div>
              )}
            </div>

            <div className="bottom-box p-3">
              {this.state.location.length > 0 ? (
                <button
                  onClick={() => this.setState({ modal2: false })}
                  className="btn-custom-active w-100"
                >
                  등록하기
                </button>
              ) : (
                <button className="btn-custom-disable w-100">등록하기</button>
              )}
            </div>
          </div>
        )}
      </div>
    );
  };
}

export default SellEdit;
