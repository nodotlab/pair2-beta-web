import React from "react";
import { withRouter, Link } from "react-router-dom";
import SellDetailModal from "./SellDetailModal";
import SellDetailModalPrev from "./SellDetailModalPrev";
import {
  tradingSeller,
  askSeller,
  askExist,
  askBuyerInfo,
  askRefuse,
  askAccept,
} from "../db/Actions";
import SellEdit from "./SellEdit";
import SellEditModal from "./SellEditModal";
import SellDetail from "./SellDetail";
import Bidding from "../reduce/Bidding";
import Trading from "../reduce/TradingBuy";
import TradeComplete from "../reduce/TradeComplete";
import { currency, timeFormat2 } from "../Method";
import imgClose from "../images/Popup_Delete.svg";
import bullet from "../images/bullet.svg";
import imgNew from "../images/New.svg";

interface Buyer {
  name: string;
  verifyPhone: boolean;
  shippingType: string;
}

interface MypageSellsState {
  tab: string;
  sellings: Bidding[];
  ask: Bidding[];
  tradings: Trading[];
  tradeComplete: TradeComplete[];
  modalSelling: boolean;
  modalDetail: boolean;
  modalType: string;
  buyer: Buyer;
  selectIndex: string;
}

class MypageSells extends React.Component<any, MypageSellsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      tab: "sellings",
      ask: [],
      sellings: [],
      tradings: [],
      tradeComplete: [],
      modalSelling: false,
      modalDetail: false,
      modalType: "",
      buyer: { name: "", verifyPhone: false, shippingType: "" },
      selectIndex: "",
    };
  }

  public componentDidMount() {
    this.initData();
  }

  private async initData() {
    const sellings: Bidding[] = [];
    const ask: Bidding[] = [];
    const tradings: Trading[] = [];
    const complete: TradeComplete[] = [];

    try {
      const token: any = localStorage.getItem("pair2token");
      const resultTradings = await tradingSeller(token);

      for (let i in resultTradings) {
        if (resultTradings[i].step === 0) {
          sellings.push(
            new Bidding(
              resultTradings[i].index,
              resultTradings[i].imageUrls[0],
              resultTradings[i].brand,
              resultTradings[i].name,
              resultTradings[i].size,
              resultTradings[i].price,
              0,
              0,
              Date.now() + 3600000 * 24,
              resultTradings[i].isTrading
            )
          );
        }
      }

      const resultAsk = await askSeller(token);

      for (let i in resultAsk) {
        if (resultAsk[i].status === "거래요청중") {
          ask.push(
            new Bidding(
              resultAsk[i].index,
              resultAsk[i].imageUrl,
              resultAsk[i].brand,
              resultAsk[i].sneakerName,
              resultAsk[i].size,
              resultAsk[i].price,
              0,
              0,
              resultAsk[i].index + 3600000 * 24,
              true,
              resultAsk[i].tradeIndex
            )
          );
        } else if (resultAsk[i].status === "거래완료") {
          complete.push(
            new TradeComplete(
              resultAsk[i].index,
              resultAsk[i].imageUrl,
              resultAsk[i].sneakerName,
              resultAsk[i].brand,
              resultAsk[i].size,
              resultAsk[i].price,
              resultAsk[i].index,
              resultAsk[i].tradeIndex,
              resultAsk[i].status,
              false,
              resultAsk[i].completeTimestamp
            )
          );
        } else if (resultAsk[i].status === "거래거절") {
          complete.push(
            new TradeComplete(
              resultAsk[i].index,
              resultAsk[i].imageUrl,
              resultAsk[i].sneakerName,
              resultAsk[i].brand,
              resultAsk[i].size,
              resultAsk[i].price,
              resultAsk[i].index,
              resultAsk[i].tradeIndex,
              resultAsk[i].status,
              false,
              resultAsk[i].completeTimestamp
              // true 정산
            )
          );
        } else {
          tradings.push(
            new Trading(
              resultAsk[i].index,
              resultAsk[i].imageUrl,
              resultAsk[i].sneakerName,
              resultAsk[i].brand,
              resultAsk[i].size,
              resultAsk[i].tradeIndex,
              resultAsk[i].index,
              resultAsk[i].price,
              resultAsk[i].status
            )
          );
        }
      }
    } catch (error) {}

    sellings.sort(
      (a: Bidding, b: Bidding) => parseInt(b.index) - parseInt(a.index)
    );
    ask.sort((a: Bidding, b: Bidding) => parseInt(b.index) - parseInt(a.index));
    complete.sort(
      (a: any, b: any) => b.completeTimestamp - a.completeTimestamp
    );
    tradings.sort(
      (a: Trading, b: Trading) => parseInt(b.index) - parseInt(a.index)
    ); // accept timestamp need

    this.setState({
      sellings: sellings,
      ask: ask,
      tradings: tradings,
      tradeComplete: complete,
    });
  }

  private handleModalSelling = (type: string, index: string): void => {
    this.setState(
      {
        modalSelling: !this.state.modalSelling,
        modalType: type,
        selectIndex: index,
      },
      async () => {
        if (this.state.modalSelling) {
          const result = await askBuyerInfo(index);
          this.setState({
            buyer: {
              name: result.nickname,
              verifyPhone: true,
              shippingType: result.isShipping ? "택배거래" : "직거래",
            },
          });
        }
      }
    );
  };

  private handleModalDetail = (index: any): void => {
    this.setState({
      modalDetail: !this.state.modalDetail,
      selectIndex: index,
    });
  };

  private handleSubmitRefuse = async () => {
    this.setState({
      modalSelling: !this.state.modalSelling,
      modalType: "",
      selectIndex: "",
    });

    await askRefuse(this.state.selectIndex);
    this.initData();
  };

  private handleSumitConfirm = async () => {
    this.setState({
      modalSelling: !this.state.modalSelling,
      modalType: "",
      selectIndex: "",
    });

    const token: any = localStorage.getItem("pair2token");
    const result = await askAccept(token, this.state.selectIndex);
    if (result === "pay faild") {
      alert("구매자의 결제 카드 문제로 인하여 거래가 자동거절 됩니다.");
    }
    this.initData();
  };

  private handleToSell = (): void => {
    this.setState({ tab: "sellings" });
    this.initData();
  };

  private handleEdit = async (index: string) => {
    const exist = await askExist(index);

    if (exist > 0) {
      alert("거래요청이 있는 상품입니다. 거래요청을 완료해주세요.");
      this.setState({ tab: "ask" });
    } else {
      this.setState({
        tab: "sellEdit",
        selectIndex: index,
      });
    }
  };

  private handleEditModal = async (index: string) => {
    const exist = await askExist(index);

    if (exist > 0) {
      alert("거래요청이 있는 상품입니다. 거래요청을 완료해주세요.");
      this.setState({ tab: "ask" });
    } else {
      this.setState({
        tab: "sellEditModal",
        selectIndex: index,
      });
    }
  };

  public render() {
    return (
      <div className="mt-5 mb-6 m-mt2">
        <div className="w-100 border-bottom-2 hidden-m">
          <div
            onClick={() => this.setState({ tab: "sellings" })}
            className={`cursor btn-faq ${
              this.state.tab === "sellings" && "btn-faq-active"
            } ${this.state.tab === "sellEdit" && "btn-faq-active"}`}
          >
            판매중
          </div>
          <div
            onClick={() => this.setState({ tab: "ask" })}
            className={`cursor btn-faq ${
              this.state.tab === "ask" && "btn-faq-active"
            }`}
          >
            거래요청
          </div>
          <div
            onClick={() => this.setState({ tab: "tradings" })}
            className={`cursor btn-faq ${
              this.state.tab === "tradings" && "btn-faq-active"
            } ${this.state.tab === "sellDetail" && "btn-faq-active"}`}
          >
            거래중
          </div>
          <div
            onClick={() => this.setState({ tab: "complete" })}
            className={`cursor btn-faq ${
              this.state.tab === "complete" && "btn-faq-active"
            } ${this.state.tab === "sellComplete" && "btn-faq-active"}`}
          >
            거래완료
          </div>
        </div>

        <div className="w-100 text-center hidden-p">
          <div
            onClick={() => this.setState({ tab: "sellings" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "sellings" && "btn-faq-active"
            } ${this.state.tab === "sellEdit" && "btn-faq-active"}`}
          >
            판매중
          </div>
          <div
            onClick={() => this.setState({ tab: "ask" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "ask" && "btn-faq-active"
            }`}
          >
            거래요청
          </div>
          <div
            onClick={() => this.setState({ tab: "tradings" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "tradings" && "btn-faq-active"
            } ${this.state.tab === "sellDetail" && "btn-faq-active"}`}
          >
            거래중
          </div>
          <div
            onClick={() => this.setState({ tab: "complete" })}
            className={`w-90 border-bottom-2 btn-faq ${
              this.state.tab === "complete" && "btn-faq-active"
            } ${this.state.tab === "sellComplete" && "btn-faq-active"}`}
          >
            거래완료
          </div>
        </div>

        {this.state.tab === "sellings" && <this.ComponentSelling />}
        {this.state.tab === "sellEdit" && (
          <SellEdit
            handleToSell={this.handleToSell}
            index={this.state.selectIndex}
          />
        )}
        {this.state.tab === "sellEditModal" && (
          <SellEditModal
            handleToSell={this.handleToSell}
            index={this.state.selectIndex}
          />
        )}
        {this.state.tab === "ask" && <this.ComponentAsk />}
        {this.state.tab === "tradings" && <this.ComponentTradings />}
        {this.state.tab === "sellDetail" && (
          <SellDetail
            handleToSell={this.handleToSell}
            index={this.state.selectIndex}
          />
        )}
        {this.state.tab === "sellDetailModalPrev" && (
          <SellDetailModalPrev
            handleToSell={this.handleToSell}
            index={this.state.selectIndex}
            handleModal={this.handleModal}
          />
        )}
        {this.state.tab === "complete" && <this.ComponentComplete />}
        {this.state.tab === "sellComplete" && (
          <SellDetail complete={true} index={this.state.selectIndex} />
        )}
        {this.state.tab === "sellCompleteModalPrev" && (
          <SellDetailModalPrev
            complete={true}
            index={this.state.selectIndex}
            handleModal={this.handleModal}
          />
        )}
      </div>
    );
  }

  private handleModal = (): void => {
    this.setState({
      tab: this.state.tab === "sellDetailModalPrev" ? "tradings" : "complete",
    });
  };

  private ComponentSelling = () => {
    return (
      <div>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.sellings.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-5 text-left p-13 bold gray-2">제품</div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">판매희망가</div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">
            최근거래완료가격
          </div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">
            판매요청 최저가
          </div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">요청만료일</div>
          <div className="col-12p p-13 bold gray-2"></div>
        </div>

        <div className="hidden-m">
          {this.state.sellings.map((sell: Bidding, i: number) => (
            <div key={i} className="flex box-bidding">
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-2 pl-0 cursor"
              >
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${sell.imageUrl})` }}
                />
              </div>
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-3 pl-0 pr-1 p-14 cursor"
              >
                <p className="roboto gray-3">{sell.brand}</p>
                <p className="roboto gray-3">{sell.name}</p>
                <p className="roboto gray-2 mt-1">{sell.size} mm</p>
              </div>
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                {sell.dealPrice >= sell.highPrice ? (
                  <p className="blue-2 roboto bold p-14 mt-4">
                    {currency(sell.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                ) : (
                  <p className="blue-2 roboto bold p-14 mt-4">
                    {currency(sell.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                )}
              </div>
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(sell.lowPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(sell.highPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div
                onClick={() => this.handleModalDetail(sell.index)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(sell.endTime)}
                </p>
              </div>
              <div className="col-12p text-center pl-0 pr-0">
                <button
                  onClick={() => this.handleEdit(sell.index)}
                  className="btn-custom-mypage p-2 mt-3"
                >
                  수정
                </button>
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.sellings.map((sell: Bidding, i: number) => (
            <div key={i} className="box-bidding">
              <div className="row">
                <div
                  onClick={() => this.handleModalDetail(sell.index)}
                  className="col-4"
                >
                  <div
                    className="img-post"
                    style={{ backgroundImage: `url(${sell.imageUrl})` }}
                  />
                </div>
                <div
                  onClick={() => this.handleModalDetail(sell.index)}
                  className="col-8 pl-1 p-14"
                >
                  <p className="roboto gray-3">{sell.brand}</p>
                  <p className="roboto gray-3">{sell.name}</p>
                  <p className="roboto gray-2">{sell.size} mm</p>
                  <p className="roboto blue-2 bold p-14 mt-1">
                    <span className="noto normal gray-2 p-13 mr-2">
                      판매희망가
                    </span>
                    {currency(sell.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13">
                    요청만료일{" "}
                    <span className="gray-3 ml-2">
                      {timeFormat2(sell.endTime)}
                    </span>
                  </p>
                </div>
              </div>
              <button
                onClick={() => this.handleEditModal(sell.index)}
                className="btn-custom-mypage pt-2 pb-2 w-100 mt-4"
              >
                수정
              </button>
            </div>
          ))}
        </div>
        {this.state.modalSelling && <this.ComponentModalSells />}
        {this.state.modalDetail && (
          <SellDetailModal
            handleCancle={() => this.handleModalDetail("")}
            index={this.state.selectIndex}
          />
        )}
      </div>
    );
  };

  private ComponentAsk = () => {
    return (
      <div>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.ask.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-5 text-left p-13 bold gray-2">제품</div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">판매희망가</div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">
            최근거래완료가격
          </div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">
            판매요청 최저가
          </div>
          <div className="col-12p pl-0 pr-0 p-13 bold gray-2">요청만료일</div>
          <div className="col-12p p-13 bold gray-2"></div>
        </div>

        <div className="hidden-m">
          {this.state.ask.map((ask: Bidding, i: number) => (
            <div key={i} className="flex box-bidding">
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-2 pl-0 cursor"
              >
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${ask.imageUrl})` }}
                />
              </div>
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-3 pl-0 pr-1 p-14 cursor"
              >
                <p className="roboto gray-3">{ask.brand}</p>
                <p className="roboto gray-3">{ask.name}</p>
                <p className="roboto gray-2 mt-1">{ask.size} mm</p>
              </div>
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                {ask.dealPrice >= ask.highPrice ? (
                  <p className="blue-2 roboto bold p-14 mt-4">
                    {currency(ask.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                ) : (
                  <p className="blue-2 roboto bold p-14 mt-4">
                    {currency(ask.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                )}
              </div>
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(ask.lowPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="gray-3 roboto bold p-14 mt-4">
                  {currency(ask.highPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div
                onClick={() => this.handleModalDetail(ask.tradeIndex)}
                className="col-12p text-center pl-0 pr-0 cursor"
              >
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(ask.endTime)}
                </p>
              </div>
              <div className="col-12p text-center pl-0 pr-0">
                <button
                  onClick={() => this.handleModalSelling("confirm", ask.index)}
                  className="btn-second-mypage pt-2 pb-2"
                >
                  거래수락
                </button>
                <br />
                <button
                  onClick={() => this.handleModalSelling("cancle", ask.index)}
                  className="btn-custom-mypage pt-2 pb-2 mt-1"
                >
                  거래거절
                </button>
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.ask.map((ask: Bidding, i: number) => (
            <div key={i} className="box-bidding">
              <div className="row">
                <div
                  onClick={() => this.handleModalDetail(ask.tradeIndex)}
                  className="col-4"
                >
                  <div
                    className="img-post"
                    style={{ backgroundImage: `url(${ask.imageUrl})` }}
                  />
                </div>
                <div
                  onClick={() => this.handleModalDetail(ask.tradeIndex)}
                  className="col-8 pl-1 p-14"
                >
                  <p className="roboto gray-3">{ask.brand}</p>
                  <p className="roboto gray-3">{ask.name}</p>
                  <p className="roboto gray-2">{ask.size} mm</p>
                  <p className="roboto blue-2 bold p-14 mt-1">
                    <span className="noto normal gray-2 p-13 mr-2">
                      판매희망가
                    </span>
                    {currency(ask.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13">
                    요청만료일{" "}
                    <span className="gray-3 ml-2">
                      {timeFormat2(ask.endTime)}
                    </span>
                  </p>
                </div>
              </div>
              <div className="row mt-4">
                <div className="col-6 pr-1">
                  <button
                    onClick={() => this.handleModalSelling("cancle", ask.index)}
                    className="btn-custom-mypage pt-2 pb-2 w-100"
                  >
                    거래거절
                  </button>
                </div>
                <div className="col-6 pl-1">
                  <button
                    onClick={() =>
                      this.handleModalSelling("confirm", ask.index)
                    }
                    className="btn-second-mypage pt-2 pb-2 w-100"
                  >
                    거래수락
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
        {this.state.modalSelling && <this.ComponentModalSells />}
        {this.state.modalDetail && (
          <SellDetailModal
            handleCancle={() => this.handleModalDetail("")}
            index={this.state.selectIndex}
          />
        )}
      </div>
    );
  };

  private ComponentModalSells = () => {
    return (
      <div
        className="modal-custom modal-billing modal-full"
        style={{ display: "block" }}
      >
        <div className="modal-content-small">
          <div className="p-3">
            <div className="row">
              <div className="col-6">
                <p className="p-16 bold black-2">알림</p>
              </div>

              <div className="col-6 text-right">
                <img
                  onClick={() => this.handleModalSelling("", "")}
                  src={imgClose}
                  alt=""
                  className="cursor"
                />
              </div>
            </div>
            <hr className="hr2" />
            <div className="text-center">
              {this.state.modalType === "confirm" ? (
                <p className="p-14 black-2 mt-4">
                  구매요청을 수락하시겠습니까?
                </p>
              ) : (
                <p className="p-14 black-2 mt-4">
                  구매요청을 거절하시겠습니까?
                </p>
              )}

              {this.state.modalType === "confirm" ? (
                <p className="p-13 gray-2 mt-2">
                  거래수락 시 자동결제가 이뤄 집니다. <br />
                  확실한 거래 의향이 있을때만 수락하시기 바랍니다.
                </p>
              ) : (
                <p className="p-13 gray-2 mt-2">
                  잦은 거래거절은 판매실적에 악영향을 미칠 수 있습니다. 가격이
                  맞지 않거나 이미 판매되었다면 판매정보를 업데이트 해주세요.
                </p>
              )}
              <p className="p-14 bold black-2 mt-4 bold">구매자 정보</p>
              <p className="p-14 gray-3 mt-1">
                {this.state.buyer.name} / 010-****-****
              </p>
            </div>

            <div className="flex bg-gray pt-4 pb-4 mt-2">
              <div className="col-6 pl-3 border-right-gray text-left">
                <p className="black-3 bold p-14">본인인증</p>
                <p className="gray-3 p-13 mt-2 float-left">
                  <img src={bullet} alt="" className="mr-1" />
                  휴대폰
                </p>
                {this.state.buyer.verifyPhone ? (
                  <p className="orange-3 bold roboto p-14 mt-2 float-right">
                    OK
                  </p>
                ) : (
                  <p className="blue-2 bold roboto p-14 mt-2 float-right">NO</p>
                )}
              </div>
              <div className="col-6 pl-3 text-left">
                <p className="black-2 bold p-14">거래방식</p>
                <p className="gray-3 p-13 mt-2">
                  <img src={bullet} alt="" className="mr-1" />
                  {this.state.buyer.shippingType}
                </p>
              </div>
            </div>

            <div className="col-10 offset-1 mt-5 mb-3">
              <button
                onClick={() => this.handleModalSelling("", "")}
                className="btn-custom-sm mr-2"
              >
                아니요
              </button>
              {this.state.modalType === "confirm" ? (
                <button
                  onClick={this.handleSumitConfirm}
                  className="btn-custom-sm-active w-120"
                >
                  네
                </button>
              ) : (
                <button
                  onClick={this.handleSubmitRefuse}
                  className="btn-custom-sm-active w-120"
                >
                  네
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  };

  private ComponentTradings = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.tradings.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-5 text-left p-13 bold gray-2">제품</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">거래번호</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">요청일</div>
          <div className="col-1 pl-0 pr-0 p-13 bold gray-2">금액</div>
          <div className="col-2 text-center pl-4 pr-0 p-13 bold gray-2">
            상태
          </div>
        </div>

        <div className="hidden-m">
          {this.state.tradings.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({ tab: "sellDetail", selectIndex: trade.index })
              }
              key={i}
              className="flex box-bidding cursor"
            >
              <div className="col-2 pl-0">
                <div
                  className="img-post"
                  style={{ backgroundImage: `url(${trade.imageUrl})` }}
                />
              </div>
              <div className="col-3 pl-0 pr-1 p-14">
                <p className="roboto gray-3">{trade.brand}</p>
                <p className="roboto gray-3">{trade.name}</p>
                <p className="roboto gray-2 mt-1">{trade.size} mm</p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                {trade.status === "거래요청중" ? (
                  <p className="gray-3 roboto p-14 mt-4">ㅡ</p>
                ) : (
                  <p className="gray-3 roboto p-14 mt-4">
                    {trade.tradeNumber.toString()}
                  </p>
                )}
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(trade.tradeTime)}
                </p>
              </div>
              <div className="col-1 text-center pl-0 pr-0">
                <p className="blue-2 roboto bold p-14 mt-4">
                  {currency(trade.dealPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-2 text-center pl-4 pr-0">
                {trade.shipping ? (
                  <p className="roboto gray-3 p-14 mt-1">{trade.status}</p>
                ) : (
                  <p className="gray-3 p-14 mt-4">{trade.status}</p>
                )}
                {trade.shipping && (
                  <p className="roboto gray-2 underline p-13 mt-1">
                    {trade.shipping}
                  </p>
                )}
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.tradings.map((trade: Trading, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "sellDetailModalPrev",
                  selectIndex: trade.index,
                })
              }
              key={i}
              className="box-bidding"
            >
              <div className="row">
                <div className="col-4">
                  <div
                    className="img-post"
                    style={{ backgroundImage: `url(${trade.imageUrl})` }}
                  />
                </div>
                <div className="col-8 pl-1 p-14">
                  <p className="roboto gray-3">{trade.brand}</p>
                  <p className="roboto gray-3">{trade.name}</p>
                  <p className="roboto gray-2">{trade.size} mm</p>
                  <p className="roboto blue-2 bold p-14 mt-1">
                    <span className="noto normal gray-2 p-13 mr-2">금액</span>
                    {currency(trade.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13">
                    요청일{" "}
                    <span className="gray-3 ml-2">
                      {timeFormat2(trade.tradeTime)}
                    </span>
                  </p>
                  <p className="gray-2 p-13">
                    거래번호
                    <span className="ml-2">
                      {trade.status === "거래요청중"
                        ? "ㅡ"
                        : trade.tradeNumber.toString()}
                    </span>
                  </p>
                  <p className="gray-3 p-13 bold float-left">{trade.status}</p>
                  {trade.shipping && (
                    <p className="roboto gray-2 underline p-13 mt-1 float-right">
                      {trade.shipping}
                    </p>
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };

  private ComponentComplete = () => {
    return (
      <>
        <p className="p-14 black-2 mt-4">
          총 <b>{this.state.tradeComplete.length}</b>건
        </p>
        <div className="flex text-center mt-3 box-bidding pt-3 pb-3 box-post-top hidden-m">
          <div className="col-6 text-left p-13 bold gray-2">제품</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">거래일자</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">거래번호</div>
          <div className="col-2 pl-0 pr-0 p-13 bold gray-2">상태</div>
        </div>

        <div className="hidden-m">
          {this.state.tradeComplete.map((trade: TradeComplete, i: number) => (
            <div
              onClick={() =>
                this.setState({ tab: "sellComplete", selectIndex: trade.index })
              }
              key={i}
              className="flex box-bidding cursor"
            >
              <div className="col-2 pl-0">
                {trade.new && (
                  <img src={imgNew} className="float-right" alt="" />
                )}
                <div
                  className="img-post pt-4"
                  style={{ backgroundImage: `url(${trade.imageUrl})` }}
                >
                  {trade.status === "거래완료" && (
                    <div className="box-trade-cancle">거래완료</div>
                  )}
                </div>
              </div>
              <div className="col-4 pl-0 pr-1 p-14">
                <p className="roboto gray-3">{trade.brand}</p>
                <p className="roboto gray-3">{trade.name}</p>
                <p className="roboto gray-2 mt-1">{trade.size} mm</p>
                <p className="blue-2 roboto bold p-14 mt-1">
                  {currency(trade.dealPrice)}
                  <span className="noto normal p-13">원</span>
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {timeFormat2(trade.tradeTime)}
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="roboto gray-3 p-14 mt-4">
                  {trade.tradeNumber.toString()}
                </p>
              </div>
              <div className="col-2 text-center pl-0 pr-0">
                <p className="gray-3 p-14 mt-4">{trade.status}</p>
              </div>
            </div>
          ))}
        </div>

        <hr className="hr2 hidden-p mb-0" />
        <div className="hidden-p">
          {this.state.tradeComplete.map((trade: TradeComplete, i: number) => (
            <div
              onClick={() =>
                this.setState({
                  tab: "sellCompleteModalPrev",
                  selectIndex: trade.index,
                })
              }
              key={i}
              className="box-bidding"
            >
              <div className="row">
                <div className="col-4 pr-1">
                  {trade.new && (
                    <img src={imgNew} className="float-right" alt="" />
                  )}
                  <div
                    className="img-post pt-4"
                    style={{ backgroundImage: `url(${trade.imageUrl})` }}
                  >
                    {trade.status === "거래완료" && (
                      <div className="box-trade-cancle">거래완료</div>
                    )}
                  </div>
                </div>
                <div className="col-8 ">
                  <p className="roboto gray-3 p-14">{trade.brand}</p>
                  <p className="roboto gray-3 p-14">{trade.name}</p>
                  <p className="roboto gray-2 p-14">{trade.size} mm</p>
                  <p className="roboto blue-2 bold p-14 mt-1">
                    <span className="noto normal gray-2 p-13 mr-2">금액</span>
                    {currency(trade.dealPrice)}
                    <span className="noto normal p-13">원</span>
                  </p>
                  <p className="gray-2 p-13">
                    거래일자{" "}
                    <span className="gray-3 ml-3">
                      {timeFormat2(trade.tradeTime)}
                    </span>
                  </p>
                  <p className="gray-2 p-13">
                    거래번호{" "}
                    <span className="gray-3 ml-3">
                      {trade.tradeNumber.toString()}
                    </span>
                  </p>
                  <p className="gray-3 p-13 bold">{trade.status}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };
}

export default withRouter(MypageSells);
