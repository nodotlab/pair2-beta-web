import React from "react";
import {
  smsSend,
  smsVerify,
  signinByEmail,
  getUserInfo,
  eidtUser,
} from "../db/Actions";
import { withRouter } from "react-router-dom";
import bullet from "../images/bullet.svg";
import inputDelete from "../images/Input_Delete_LightBG.svg";
import radio from "../images/Radio_Default.svg";
import radioChecked from "../images/Radio_Checked.svg";
import agreeDisable from "../images/Agree_Disabled.svg";
import agreeActive from "../images/Agree_Active.svg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import warining from "../images/Input_Warning.svg";
import imgClose from "../images/Popup_Delete.svg";

interface MypageEditState {
  signinPassword: string;
  screen: string;
  password: string;
  passwordConfirm: string;
  email: string;
  nickname: string;
  smsAgree: boolean;
  emailAgree: boolean;
  agree1: boolean;
  agree2: boolean;
  noti: string;
}

class MypageEdit extends React.Component<any, MypageEditState> {
  constructor(props: any) {
    super(props);

    this.state = {
      signinPassword: "",
      screen: "password",
      password: "",
      passwordConfirm: "",
      email: "",
      nickname: "",
      smsAgree: true,
      emailAgree: true,
      agree1: false,
      agree2: false,
      noti: "",
    };
  }

  public componentDidMount() {}

  private handleText = (e: React.ChangeEvent<any>) => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value } as {
      [i in keyof MypageEditState]: any;
    });
  };

  private handlePhone = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleInputClear = (formName: string): any => {
    this.setState({ [formName]: "" } as any);
  };

  private handleAgree = (name: string) => {
    if (name === "agree1") this.setState({ agree1: !this.state.agree1 });
    if (name === "agree2") this.setState({ agree2: !this.state.agree2 });
    if (name === "all")
      this.setState({ agree1: true, agree2: true });
  };

  private agreeAll(): boolean {
    if (this.state.agree1 && this.state.agree2) {
      return true;
    } else {
      return false;
    }
  }

  private passwordCheck = (): boolean => {
    if (this.state.signinPassword.length === 0) return false;

    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=_]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.signinPassword)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPassword = (): boolean => {
    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=_]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.password)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPasswordConfirm = (): boolean => {
    if (this.state.password === this.state.passwordConfirm) {
      return true;
    } else {
      return false;
    }
  };

  private verifyEmail = (): boolean => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(this.state.email.toLowerCase());
  };

  private handleScreen = async (e: any) => {
    e.preventDefault();
    const token = await signinByEmail(this.props.userId, this.state.signinPassword);
    if (token === "faild") {
      this.setState({ noti: "비밀번호가 일치하지 않습니다." });
      return;
    } else {
      localStorage.setItem("pair2token", token);
      this.setState({ screen: "form" });
      const result = await getUserInfo(token);
      this.setState({
        nickname: result.nickname,
        email: result.email,
        agree1: result.agree1,
        agree2: result.agree2,
        noti: "",
      });
    }
  };

  private verifyAll = (): boolean => {
    // if (this.state.name.length === 0) return false;
    // if (!this.verifyPhone() || this.state.phone.length === 0) return false;
    if (!this.verifyPassword() || this.state.password.length === 0)
      return false;
    if (!this.verifyPasswordConfirm()) return false;
    if (this.state.nickname.length === 0) return false;
    if (!this.verifyEmail() || this.state.email.length === 0) return false;
    if (!this.state.agree1) return false;
    if (!this.state.agree2) return false;

    return true;
  };

  private handleSubmit = async () => {
    if (!this.verifyPassword() || this.state.password.length === 0) return;
    if (!this.verifyPasswordConfirm()) return;
    if (this.state.nickname.length === 0) return;
    if (!this.verifyEmail() || this.state.email.length === 0) return;
    if (!this.state.agree1) return;
    if (!this.state.agree2) return;

    const token: any = localStorage.getItem("pair2token");

    const result = await eidtUser(
      token,
      this.state.password,
      this.state.nickname,
      this.state.email,
      this.state.agree1,
      this.state.agree2,
    );

    if (result === "failed") {
      alert("휴대폰을 인증한 후 5분내로 가입해주세요.");
    } else {
      localStorage.setItem("pair2token", result);
      alert("수정되었습니다.");
      this.setState({
        screen: "password",
        password: "",
        passwordConfirm: "",
        signinPassword: "",
      });

      window.location.reload();
    }
  };

  public render() {
    return (
      <>
        {this.state.screen === "password" && <this.ComponentPassword />}
        {this.state.screen === "form" && <this.ComponentForm />}
        {this.state.screen === "form" && <this.ComponentFormMobile />}
      </>
    );
  }

  private ComponentForm = () => {
    return (
      <div className="hidden-m">
        <div className="box-signup mt-3">
          <div className="col-lg-8 offset-lg-2">


            <p className="p-14 black-2 mt-4 mb-2">
              이메일
            </p>
            <div className="form-disable">{this.state.email}</div>
            {/* <input
              onChange={this.handleText}
              value={this.state.email}
              name="email"
              type="text"
              placeholder="이메일주소를 입력해주세요"
              className={`form-control form-custom ${
                !this.verifyEmail() &&
                this.state.email.length > 0 &&
                "form-warning"
              }`}
            />
            {this.state.email.length > 0 && (
              <img
                src={inputDelete}
                onClick={() => this.handleInputClear("email")}
                className="placeholder-right cursor"
              />
            )}
            {!this.verifyEmail() && this.state.email.length > 0 && (
              <p className="p-13 red mt-1 mb-2">
                <img src={warining} className="mr-2" />
                이메일 형식이 잘못되었습니다.
              </p>
            )} */}

            <p className="p-14 black-2 mt-4 mb-2">
              닉네임
            </p>
            <input
              onChange={this.handleText}
              value={this.state.nickname}
              name="nickname"
              type="text"
              maxLength={19}
              placeholder="닉네임을 입력해주세요"
              className={`form-control form-custom`}
            />
            <p className="p-13 gray-4 placeholder-right">
              {this.state.nickname.length}/20
            </p>

            <p className="p-14 black-2 mt-4 mb-2">
              비밀번호
            </p>
            <input
              onChange={this.handleText}
              value={this.state.password}
              name="password"
              maxLength={20}
              type="password"
              placeholder="8-20자리의 영문, 숫자, 특수문자"
              className={`form-control form-custom ${
                !this.verifyPassword() &&
                this.state.password.length > 0 &&
                "form-warning"
              }`}
            />
            {this.state.password.length > 0 && (
              <img
                src={inputDelete}
                onClick={() => this.handleInputClear("email")}
                className="placeholder-right cursor"
              />
            )}
            {!this.verifyPassword() && this.state.password.length > 0 && (
              <p className="p-13 red mt-1 mb-3">
                <img src={warining} className="mr-2" />
                8-20자 영문, 숫자, 특수문자를 사용하세요.
              </p>
            )}

            <input
              onChange={this.handleText}
              value={this.state.passwordConfirm}
              maxLength={20}
              name="passwordConfirm"
              type="password"
              placeholder="비밀번호 확인"
              className={`form-control form-custom mt-2 ${
                !this.verifyPasswordConfirm() &&
                this.state.passwordConfirm.length > 0 &&
                "form-warning"
              }`}
            />
            {!this.verifyPasswordConfirm() &&
              this.state.passwordConfirm.length > 0 && (
                <p className="p-13 red mt-1 mb-3">
                  <img src={warining} className="mr-2" />
                  비밀번호가 잘못되었습니다.
                </p>
              )}

            

            

            <button
              onClick={() => this.handleAgree("all")}
              className={`w-100 mt-4 ${
                this.agreeAll() ? "btn-second" : "btn-custom-disable"
              }`}
            >
              전체 약관에 동의{" "}
              <img
                src={this.agreeAll() ? agreeActive : agreeDisable}
                className="ml-2"
              />
            </button>

            <div className="mt-3 p-2 w-100">
              <img
                onClick={() => this.handleAgree("agree1")}
                src={this.state.agree1 ? checked : check}
                className="cursor float-left"
              />
              <p
                onClick={() => this.handleAgree("agree1")}
                className="gray-6 p-14 float-left cursor ml-2"
              >
                PAIR2 이용약관 동의 (필수)
              </p>
              <p className="gray-2 p-13 underline cursor float-right">보기</p>
            </div>
            <div className="mt-4 p-2 w-100">
              <img
                onClick={() => this.handleAgree("agree2")}
                src={this.state.agree2 ? checked : check}
                className="cursor float-left"
              />
              <p
                onClick={() => this.handleAgree("agree2")}
                className="gray-6 p-14 float-left cursor ml-2"
              >
                개인정보 수집 및 이용에 대한 안내 (필수)
              </p>
              <p className="gray-2 p-13 underline cursor float-right">보기</p>
            </div>
          </div>
        </div>

        <div className="mt-6 mb-100 text-center hidden-m">
          <button
            onClick={() => this.setState({ screen: "password" })}
            className="btn-custom mr-2"
          >
            취소
          </button>
          <button
            onClick={this.handleSubmit}
            className={
              this.verifyAll() ? "btn-custom-active" : "btn-custom-disable"
            }
          >
            수정
          </button>
        </div>
        <div className="row bottom-box pt-2 pb-2 hidden-p">
          <div className="col-6 pr-1">
            <button
              onClick={() => this.setState({ screen: "password" })}
              className="btn-custom w-100"
            >
              취소
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className={
                this.verifyAll()
                  ? "btn-custom-active w-100"
                  : "btn-custom-disable w-100"
              }
            >
              수정
            </button>
          </div>
        </div>
      </div>
    );
  };

  private ComponentFormMobile = () => {
    return (
      <>
        <div className="hidden-p modal-fullscreen-mobile pb-5">
          <div className="w-100 pt-2 pb-2">
            <div className="flex">
              <div className="col-6">
                <p className="black-2 p-16 bold"></p>
              </div>
              <div className="col-6 text-right">
                <img
                  src={imgClose}
                  onClick={() => this.setState({ screen: "password" })}
                  className="cursor"
                />
              </div>
            </div>
            <hr className="hr2" />
          </div>

          <div className="box-signup border-0 mt-0 pt-0">
            <p className="text-right mr-2 p-13 red">*필수입력</p>
            <div className="col-lg-8 offset-lg-2">

              <p className="p-14 black-2 mt-4 mb-2">
                비밀번호<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.password}
                name="password"
                maxLength={20}
                type="password"
                placeholder="8-20자리의 영문, 숫자, 특수문자"
                className={`form-control form-custom ${
                  !this.verifyPassword() &&
                  this.state.password.length > 0 &&
                  "form-warning"
                }`}
              />
              {!this.verifyPassword() && this.state.password.length > 0 && (
                <p className="p-13 red mt-1 mb-3">
                  <img src={warining} className="mr-2" />
                  8~20자 영문, 숫자, 특수문자를 사용하세요.
                </p>
              )}

              <input
                onChange={this.handleText}
                value={this.state.passwordConfirm}
                maxLength={20}
                name="passwordConfirm"
                type="password"
                placeholder="비밀번호를 재입력해주세요"
                className={`form-control form-custom mt-2 ${
                  !this.verifyPasswordConfirm() &&
                  this.state.passwordConfirm.length > 0 &&
                  "form-warning"
                }`}
              />
              {!this.verifyPasswordConfirm() &&
                this.state.passwordConfirm.length > 0 && (
                  <p className="p-13 red mt-1 mb-3">
                    <img src={warining} className="mr-2" />
                    비밀번호가 잘못되었습니다.
                  </p>
                )}

              <p className="p-14 black-2 mt-4 mb-2">
                닉네임<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.nickname}
                name="nickname"
                type="text"
                maxLength={19}
                placeholder="닉네임을 입력해주세요"
                className={`form-control form-custom`}
              />
              <p className="p-13 gray-4 placeholder-right">
                {this.state.nickname.length}/20
              </p>

              <p className="p-14 black-2 mt-4 mb-2">
                이메일<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.email}
                name="email"
                type="text"
                placeholder="이메일주소를 입력해주세요"
                className={`form-control form-custom ${
                  !this.verifyEmail() &&
                  this.state.email.length > 0 &&
                  "form-warning"
                }`}
              />
              {this.state.email.length > 0 && (
                <img
                  src={inputDelete}
                  onClick={() => this.handleInputClear("email")}
                  className="placeholder-right cursor"
                />
              )}
              {!this.verifyEmail() && this.state.email.length > 0 && (
                <p className="p-13 red mt-1 mb-2">
                  <img src={warining} className="mr-2" />
                  이메일 형식이 잘못되었습니다.
                </p>
              )}

              <p className="black-2 p-14 mt-4">SMS/이메일 수신여부</p>
              <p className="gray-2 p-13 mt-2">
                <img src={bullet} /> PAIR2의 다양한 정보를 받아보시겠습니까?
              </p>

              <div className="bg-gray mt-3 p-4">
                <div className="row">
                  <div className="col-4">
                    <p className="black-2 bold p-13">SMS</p>
                    <p className="black-2 bold p-13 mt-3">이메일</p>
                  </div>
                  <div className="col-4">
                    <p
                      onClick={() => this.setState({ smsAgree: true })}
                      className={`p-14 cursor ${
                        this.state.smsAgree ? "gray-3" : "gray-6"
                      }`}
                    >
                      <img
                        src={this.state.smsAgree ? radioChecked : radio}
                        className="mr-2"
                      />
                      동의함
                    </p>
                    <p
                      onClick={() => this.setState({ emailAgree: true })}
                      className={`p-14 cursor mt-3 ${
                        this.state.emailAgree ? "gray-3" : "gray-6"
                      }`}
                    >
                      <img
                        src={this.state.emailAgree ? radioChecked : radio}
                        className="mr-2"
                      />
                      동의함
                    </p>
                  </div>
                  <div className="col-4">
                    <p
                      onClick={() => this.setState({ smsAgree: false })}
                      className={`p-14 cursor ${
                        this.state.smsAgree ? "gray-6" : "gray-3"
                      }`}
                    >
                      <img
                        src={this.state.smsAgree ? radio : radioChecked}
                        className="mr-2"
                      />
                      동의안함
                    </p>
                    <p
                      onClick={() => this.setState({ emailAgree: false })}
                      className={`p-14 cursor mt-3 ${
                        this.state.emailAgree ? "gray-6" : "gray-3"
                      }`}
                    >
                      <img
                        src={this.state.emailAgree ? radio : radioChecked}
                        className="mr-2"
                      />
                      동의안함
                    </p>
                  </div>
                </div>
              </div>

              <hr className="br2 mt-5" />

              <button
                onClick={() => this.handleAgree("all")}
                className={`w-100 mt-4 ${
                  this.agreeAll() ? "btn-second" : "btn-custom-disable"
                }`}
              >
                전체 약관에 동의{" "}
                <img
                  src={this.agreeAll() ? agreeActive : agreeDisable}
                  className="ml-2"
                />
              </button>

              <div className="mt-3 p-2 w-100">
                <img
                  onClick={() => this.handleAgree("agree1")}
                  src={this.state.agree1 ? checked : check}
                  className="cursor float-left"
                />
                <p
                  onClick={() => this.handleAgree("agree1")}
                  className="gray-6 p-14 float-left cursor ml-2"
                >
                  PAIR2 이용약관 동의 (필수)
                </p>
                <p className="gray-2 p-13 underline cursor float-right">보기</p>
              </div>
              <div className="mt-4 p-2 w-100">
                <img
                  onClick={() => this.handleAgree("agree2")}
                  src={this.state.agree2 ? checked : check}
                  className="cursor float-left"
                />
                <p
                  onClick={() => this.handleAgree("agree2")}
                  className="gray-6 p-14 float-left cursor ml-2"
                >
                  개인정보 수집 및 이용에 대한 안내 (필수)
                </p>
                <p className="gray-2 p-13 underline cursor- float-right">보기</p>
              </div>
            </div>
          </div>
        </div>

        <div className="row bottom-box pt-2 pb-2 hidden-p">
          <div className="col-6 pr-1">
            <button
              onClick={() => this.setState({ screen: "password" })}
              className="btn-custom w-100"
            >
              취소
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className={
                this.verifyAll()
                  ? "btn-custom-active w-100"
                  : "btn-custom-disable w-100"
              }
            >
              수정
            </button>
          </div>
        </div>
      </>
    );
  };

  private ComponentPassword = () => {
    return (
      <div className="mt-5 mb-6">
        <p className="black-2 p-22">비밀번호 재확인</p>
        <p className="gray-2 p-13 mt-3">
          <img src={bullet} className="mr-1" alt="" />
          회원님의 소중한 정보보호를 위해 비밀번호를 재확인하고 있습니다.
        </p>
        <p className="gray-2 p-13 mt-2">
          <img src={bullet} className="mr-1" alt="" />
          회원님의 개인정보를 신중히 취급하며, 회원님의 동의 없이는 기재하신
          회원정보를 공개 및 변경하지 않습니다.
        </p>

        <form onSubmit={this.handleScreen}>
          <div className="box-signup mt-5">
            <div className="col-md-8 offset-md-2">
              <p className="black-2 p-14 mb-2">비밀번호</p>
              <input
                onChange={this.handleText}
                value={this.state.signinPassword}
                name="signinPassword"
                maxLength={20}
                className="form-control form-custom"
                type="password"
                placeholder="8-20자리의 영문, 숫자, 특수문자"
              />

              <p className="p-13 red mt-1">{this.state.noti}</p>
            </div>
          </div>
          <div className="text-center mt-5 hidden-m">
            <button onClick={this.handleScreen} className="btn-custom-active">
              확인
            </button>
          </div>
          <div className="text-center mt-5 hidden-p">
            <button
              onClick={this.handleScreen}
              className="btn-custom-sm-active w-120"
            >
              확인
            </button>
          </div>
        </form>
      </div>
    );
  };
}

export default withRouter(MypageEdit);
