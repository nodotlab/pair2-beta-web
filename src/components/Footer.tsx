import React from "react";
import { Link } from "react-router-dom";
import imgTop from "../images/TOP_Arrow.svg";
import imgInsta from "../images/SNS_Instagram.svg";
import imgIOS from "../images/ic_appstore.png";
import imgAndroid from "../images/ic_googleplaystore.png";
import imgDown from "../images/tab_open.svg";
import imgUp from "../images/tab_close.svg";
import { env } from "../reduce/Env";

class Footer extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      open: this.props.main,
      openGame: true,
    };
  }

  public componentDidMount() {}

  private handleOpen = (): void => {
    this.setState({ open: !this.state.open });
  };

  private handleOpenGame = (): void => {
    this.setState({ openGame: !this.state.openGame });
  };

  public render() {
    return (
      <>
        {!this.props.main && this.props.match.params.id !== "game" && (
          <this.normal />
        )}
        {!this.props.main && this.props.match.params.id === "game" && (
          <this.game />
        )}
        {this.props.main && <this.normal />}
      </>
    );
  }

  private normal = () => {
    return (
      <>
        <div className="footer">
          <div className="container">
            <div className="text-right mt-3 mb-3">
              {/* <img onClick={this.handleOpen} src={imgUp} className="cursor" /> */}
            </div>

            <div className="row mt-5">
              <div className="col-lg-10">
                <div className="float-right hidden-p">
                  <p
                    onClick={() => window.scrollTo(0, 0)}
                    className="p-14 roboto gray-2 bold cursor"
                  >
                    <span>
                      <img src={imgTop} />
                    </span>{" "}
                    TOP
                  </p>
                </div>

                <div className="row no-gutters">
                  <div className="col-lg-2 text-left mb-3">
                    <Link to="/support/">
                      <p className="footer-a">고객센터</p>
                    </Link>
                  </div>
                  {/* <div className="col-lg-2 text-left mb-3">
        <Link to="/notice"><p className="footer-a">공지사항</p></Link>
      </div> */}
                  <div className="col-lg-2 text-left mb-3">
                    <a href="/tos/page1">
                      <p className="footer-a">서비스 이용약관</p>
                    </a>
                  </div>
                  {/* <div className="col-lg-3 text-left mb-3">
        <a href="/tos/page2"><p className="footer-a">위치기반서비스 이용약관</p></a>
      </div> */}
                  <div className="col-lg-3 text-left">
                    <a href="/tos/page2">
                      <p className="footer-a cursor bold">개인정보처리방침</p>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-2 text-right hidden-m">
                <a href="https://www.instagram.com/pair2.xyz/" target="_new">
                  <img src={imgInsta} />
                </a>
                {/* {env === "pc" && (
                  <>
                    <a
                      href="https://itunes.apple.com/kr/app/apple-store/id1507129922"
                      target="_new"
                    >
                      <img className="ml-2" src={imgIOS} />
                    </a>
                    <a
                      href="http://play.google.com/store/apps/details?id=com.nodot.pair2"
                      target="_new"
                    >
                      <img className="ml-2" src={imgAndroid} />
                    </a>
                  </>
                )} */}
              </div>
            </div>

            <div className="row mt-4">
              <div className="col-lg-10">
                <div>
                  <p className="footer-p">주식회사 노닷 </p>
                  <p className="footer-border" />
                  <p className="footer-p">대표 김동환</p>
                  <p className="footer-border" />
                  <p className="footer-p">
                    서울특별시 마포구 월드컵로10길 9, 6층
                  </p>
                  <p className="footer-border" />
                  <p className="footer-p">
                    사업자등록번호 512-86-01786{" "}
                    <a
                      href="http://www.ftc.go.kr/bizCommPop.do?wrkr_no=5128601786"
                      target="new"
                      className="underline"
                    >
                      사업자확인
                    </a>
                  </p>
                </div>
                <div>
                  <p className="footer-p">통신판매사업자 2020-서울마포-1155</p>
                  <p className="footer-border" />
                  <p className="footer-p">070-4256-0099</p>
                  <p className="footer-border" />
                  <p className="footer-p">support@pair2.xyz</p>
                  <p className="footer-border" />
                  <p className="footer-p">Hosting by Amazon Web Service</p>
                </div>
                <div>
                  <p className="footer-p mt-4">
                    &copy; 2020 NODOT Inc. All Right Reserved.
                  </p>
                </div>
              </div>

              <div className="col-lg-2 text-right hidden-m">
                <a href="#top">
                  <p className="p-14 roboto gray-2 bold">
                    <span>
                      <img src={imgTop} />
                    </span>{" "}
                    TOP
                  </p>
                </a>
              </div>

              <div className="col-lg-2 text-left mt-4 mb-4 hidden-p">
                <a href="https://www.instagram.com/pair2.xyz/" target="_new">
                  <img src={imgInsta} />
                </a>
                {/* {env === "pc" && (
                  <>
                    <a
                      href="https://itunes.apple.com/kr/app/apple-store/id1507129922"
                      target="_new"
                    >
                      <img className="ml-2" src={imgIOS} />
                    </a>
                    <a
                      href="http://play.google.com/store/apps/details?id=com.nodot.pair2"
                      target="_new"
                    >
                      <img className="ml-2" src={imgAndroid} />
                    </a>
                  </>
                )} */}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  private game = () => {
    return (
      <>
        <div className="footer">
          <div className="container">
            <div className="text-right mt-3 mb-3">
              {/* <img onClick={this.handleOpenGame} src={imgUp} className="cursor" /> */}
            </div>

            <div className="row mt-5">
              <div className="col-lg-10">
                <div className="float-right hidden-p">
                  <p
                    onClick={() => window.scrollTo(0, 0)}
                    className="p-14 roboto gray-2 bold cursor"
                  >
                    <span>
                      <img src={imgTop} />
                    </span>{" "}
                    TOP
                  </p>
                </div>

                <div className="row no-gutters">
                  <div className="col-lg-2 text-left mb-3">
                    <Link to="/support/">
                      <p className="footer-a">고객센터</p>
                    </Link>
                  </div>
                  {/* <div className="col-lg-2 text-left mb-3">
        <Link to="/notice"><p className="footer-a">공지사항</p></Link>
      </div> */}
                  <div className="col-lg-2 text-left mb-3">
                    <a href="/tos/page1">
                      <p className="footer-a">서비스 이용약관</p>
                    </a>
                  </div>
                  {/* <div className="col-lg-3 text-left mb-3">
        <a href="/tos/page2"><p className="footer-a">위치기반서비스 이용약관</p></a>
      </div> */}
                  <div className="col-lg-3 text-left">
                    <a href="/tos/page2">
                      <p className="footer-a bold">개인정보처리방침</p>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-2 text-right hidden-m">
                <a href="https://www.instagram.com/pair2.xyz/" target="_new">
                  <img src={imgInsta} />
                </a>
                {/* {env === "pc" && (
                  <>
                    <a
                      href="https://itunes.apple.com/kr/app/apple-store/id1507129922"
                      target="_new"
                    >
                      <img className="ml-2" src={imgIOS} />
                    </a>
                    <a
                      href="http://play.google.com/store/apps/details?id=com.nodot.pair2"
                      target="_new"
                    >
                      <img className="ml-2" src={imgAndroid} />
                    </a>
                  </>
                )} */}
              </div>
            </div>

            <div className="row mt-4">
              <div className="col-lg-10">
                <div>
                  <p className="footer-p">주식회사 노닷 </p>
                  <p className="footer-border" />
                  <p className="footer-p">대표 김동환</p>
                  <p className="footer-border" />
                  <p className="footer-p">
                    서울특별시 마포구 월드컵로10길 9, 6층
                  </p>
                  <p className="footer-border" />
                  <p className="footer-p">
                    사업자등록번호 512-86-01786{" "}
                    <a
                      href="http://www.ftc.go.kr/bizCommPop.do?wrkr_no=5128601786"
                      target="new"
                      className="underline"
                    >
                      사업자확인
                    </a>
                  </p>
                </div>
                <div>
                  <p className="footer-p">통신판매사업자 2020-서울마포-1155</p>
                  <p className="footer-border" />
                  <p className="footer-p">070-4256-0099</p>
                  <p className="footer-border" />
                  <p className="footer-p">support@pair2.xyz</p>
                  <p className="footer-border" />
                  <p className="footer-p">Hosting by Amazon Web Service</p>
                </div>
                <div>
                  <p className="footer-p mt-4">
                    &copy; 2020 NODOT Inc. All Right Reserved.
                  </p>
                </div>
              </div>

              <div className="col-lg-2 text-right hidden-m">
                <a href="#top">
                  <p className="p-14 roboto gray-2 bold">
                    <span>
                      <img src={imgTop} />
                    </span>{" "}
                    TOP
                  </p>
                </a>
              </div>

              <div className="col-lg-2 text-left mt-4 mb-4 hidden-p">
                <a href="https://www.instagram.com/pair2.xyz/" target="_new">
                  <img src={imgInsta} />
                </a>
                {/* {env === "pc" && (
                  <>
                    <a
                      href="https://itunes.apple.com/kr/app/apple-store/id1507129922"
                      target="_new"
                    >
                      <img className="ml-2" src={imgIOS} />
                    </a>
                    <a
                      href="http://play.google.com/store/apps/details?id=com.nodot.pair2"
                      target="_new"
                    >
                      <img className="ml-2" src={imgAndroid} />
                    </a>
                  </>
                )} */}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
}

export default Footer;
