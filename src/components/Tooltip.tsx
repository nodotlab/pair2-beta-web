import React from "react";
import ReactTooltip from "react-tooltip";

export default class Tooltip1 extends React.Component<any> {
  public render() {
    return (
      <>
        <div className="hidden-m">
          <ReactTooltip
            className="tooltip-custom"
            id="tooltip1"
            place="bottom"
            type="dark"
          >
            <div>
              <div className="mb-3 mt-2">
                <span className="box-tooltip">PERFECT</span>
                <span className="p-tooltip">완벽한 상태</span>
              </div>
              <div className="mb-3">
                <span className="box-tooltip">DEFECT1</span>
                <span className="p-tooltip">
                  박스, 속지 등 구성품에 특이사항이 존재
                </span>
              </div>
              <div className="mb-3">
                <span className="box-tooltip">DEFECT2</span>
                <span className="p-tooltip">
                  변색, 흠집 등 신발에 특이사항이 존재
                </span>
              </div>
              <div className="mb-3">
                <span className="box-tooltip">DEFECT3</span>
                <span className="p-tooltip">
                  구성품, 신발에 특이사항이 존재
                </span>
              </div>
            </div>
          </ReactTooltip>
        </div>
        <div className="hidden-p">
          <ReactTooltip
            className="tooltip-custom"
            id="tooltip1"
            place="right"
            type="dark"
          >
            <div>
              <div className="mb-3 mt-2">
                <div className="row">
                  <div className="col-4">
                    <span className="box-tooltip">PERFECT</span>
                  </div>
                  <div className="col-8">
                    <span className="p-tooltip-mobile">완벽한 상태</span>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="row">
                  <div className="col-4">
                    <span className="box-tooltip">DEFECT1</span>
                  </div>
                  <div className="col-8">
                    <span className="p-tooltip-mobile">
                      박스, 속지 등 구성품에 특이사항이 존재
                    </span>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="row">
                  <div className="col-4">
                    <span className="box-tooltip">DEFECT2</span>
                  </div>
                  <div className="col-8">
                    <span className="p-tooltip-mobile">
                      변색, 흠집 등 신발에 특이사항이 존재
                    </span>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <div className="row">
                  <div className="col-4">
                    <span className="box-tooltip">DEFECT3</span>
                  </div>
                  <div className="col-8">
                    <span className="p-tooltip-mobile">
                      구성품, 신발에 특이사항이 존재
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </ReactTooltip>
        </div>
      </>
    );
  }
}
