import React from "react";
import { withRouter, Redirect } from "react-router-dom";

class ScrollToTop extends React.Component<any> {

  saveScrollPosition: any = {};
  temp: any = [];
  prevPathNameArr: any = [];


  public componentWillMount() {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual"
    }
    // console.log('scrollTop.js - willMount')
    // window.scrollTo(0, 0)
  }

  public componentDidMount() {
    this.prevPathNameArr.push(this.props.location.pathname);
  }

  public componentDidUpdate(prevProps: any) {
    const {
      history: { action },
      location: { pathname: newPath = "root" }
    } = this.props;

    const {
      location: { pathname = "root" }
    } = prevProps;

    if (action === 'POP') {
      const pos = this.saveScrollPosition[this.props.location.pathname];
      if (pos) {
        if(this.props.location.pathname === '/today/'){
          console.log(this.props.location.pathname);
          setTimeout(() => window.scrollTo(pos[0], pos[1]), 1200);
        } else {
          console.log(this.props.location.pathname);
          setTimeout(() => window.scrollTo(pos[0], pos[1]), 500);
        }
        this.prevPathNameArr.push(this.props.location.pathname);
      }
    } else if (action === 'PUSH') {
      this.saveScrollPosition[prevProps.location.pathname] = this.temp[this.props.location.pathname];
      setTimeout(() => window.scrollTo(0, 0), 50);
    }

  }

  render() {
    (() => {
      const {
        history: { action },
        location: { pathname: pathname }
      } = this.props;
      if (action === 'PUSH') {
        this.temp[this.props.location.pathname] = [window.pageXOffset, window.pageYOffset];
      }
    })()
    return null;
  }
}

export default withRouter(ScrollToTop);
