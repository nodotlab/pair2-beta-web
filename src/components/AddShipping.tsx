import React from "react";
import Shipping from "../reduce/Shipping";
import DaumPostcode from "react-daum-postcode";
import imgClose from "../images/Popup_Delete.svg";
import imgCheck from "../images/Checkbox_Default.svg";
import imgChecked from "../images/Checkbox_Checked.svg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import prev from "../images/backblack.svg";
import { putShippings, getShippings } from "../db/Actions";

interface AddShippingState {
  modal: boolean;
  modal2: boolean;
  modalAddress: boolean;
  hasShipping: boolean;
  shippings: Shipping[];
  selectShipping: number;
  tag: string;
  name: string;
  isDefault: boolean;
  zip: string;
  address: string;
  addressDetail: string;
  phone: string;
}

class AddShipping extends React.Component<any, AddShippingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      modal: false,
      modal2: false,
      modalAddress: false,
      hasShipping: false,
      shippings: [],
      selectShipping: 0,
      tag: "",
      name: "",
      isDefault: true,
      zip: "",
      address: "",
      addressDetail: "",
      phone: "",
    };
  }

  public componentDidMount() {
    this.getShippings();
  }

  private async getShippings() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result: Shipping[] = await getShippings(token);

      this.setState({
        shippings: result,
      });

      if (result.length > 0) {
        this.setState({ hasShipping: true });
      } else {
        this.setState({ hasShipping: false });
      }
    } catch (error) {
      this.setState({ hasShipping: false });
    }
  }

  private handleChange = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handlePhone = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleModal = (): void => {
    this.setState({ modal: !this.state.modal });
  };
  private handleModal2 = (): void => {
    this.setState({ modal2: !this.state.modal2 });
  };

  private handleModalAddress = (): void => {
    this.setState({ modalAddress: !this.state.modalAddress });
  };

  private handleAddress = (data: any): void => {
    this.setState({
      address: data.address,
      zip: data.zonecode,
      modalAddress: false,
    });
  };

  private handleSubmit = async () => {
    const token: any = localStorage.getItem("pair2token");

    if (this.state.isDefault) {
      const tempShippings: Shipping[] = this.state.shippings
        ? this.state.shippings
        : [];

      for (let i in tempShippings) {
        tempShippings[i].default = false;
      }
      tempShippings.push(
        new Shipping(
          this.state.tag,
          this.state.name,
          this.state.isDefault,
          this.state.zip,
          this.state.address,
          this.state.addressDetail,
          this.state.phone,
        ),
      );

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    } else {
      const tempShippings: Shipping[] = this.state.shippings
        ? this.state.shippings
        : [];
      tempShippings.push(
        new Shipping(
          this.state.tag,
          this.state.name,
          this.state.isDefault,
          this.state.zip,
          this.state.address,
          this.state.addressDetail,
          this.state.phone,
        ),
      );

      await putShippings({
        token: token,
        shippings: tempShippings,
      });
    }

    this.setState({
      modal: false,
      modal2: false,
    });

    this.getShippings();
  };

  private handleNext = (): void => {
    this.props.handleShippingUpdate(
      this.state.shippings[this.state.selectShipping],
    );
    this.props.handleNext();
  };

  public render() {
    return (
      <div>
        <p className="p-14 black-2">배송지 등록</p>
        {this.state.hasShipping && <hr className="hr2 mb-1" />}
        {this.state.hasShipping &&
          this.state.shippings.map((shipping: Shipping, i: number) => (
            <div key={i} className="box-address">
              <div className="row">
                <div className="col-1">
                  {this.state.selectShipping === i ? (
                    <img src={imgChecked} alt="" />
                  ) : (
                    <img
                      onClick={() => this.setState({ selectShipping: i })}
                      src={imgCheck}
                      className="cursor"
                    />
                  )}
                </div>
                <div className="col-3">
                  <p className="bold">{shipping.tag}</p>
                  <p>{shipping.name}</p>
                  {shipping.default && (
                    <div className="btn-active-sm mt-2">기본 배송지</div>
                  )}
                </div>
                <div className="col-8">
                  <p>{shipping.zip}</p>
                  <p>
                    {shipping.address} {shipping.addressDetail}
                  </p>
                  <p className="mt-2">{shipping.phone}</p>
                </div>
              </div>
            </div>
          ))}
        {this.state.hasShipping && (
          <div className="text-right mb-3">
            <button
              onClick={this.handleModal}
              className="btn-shipping mt-3 hidden-m"
            >
              배송지등록하기
            </button>
            <button
              onClick={this.handleModal2}
              className="btn-shipping mt-3 hidden-p"
            >
              배송지등록하기
            </button>
          </div>
        )}

        {!this.state.hasShipping && (
          <div className="box-shipping-add mt-3 mb-3">
            <p className="p-14 gray-3 pt-90">등록된 배송지가 없습니다.</p>
            <button
              onClick={this.handleModal}
              className="btn-shipping mt-1 hidden-m"
            >
              배송지등록하기
            </button>
            <button
              onClick={this.handleModal2}
              className="btn-shipping mt-1 hidden-p"
            >
              배송지등록하기
            </button>
          </div>
        )}

        <div className="row mt-5 hidden-m">
          <div className="col-md-6">
            <button
              onClick={this.props.handleBack}
              className="btn-custom w-100"
            >
              이전
            </button>
          </div>
          <div className="col-md-6">
            {this.state.hasShipping ? (
              <button
                onClick={this.handleNext}
                className="btn-custom-active w-100"
              >
                다음
              </button>
            ) : (
              <button className="btn-custom-disable w-100">다음</button>
            )}
          </div>
        </div>

        {!this.state.modal && (
          <div className="row bottom-box hidden-p p-3">
            <div className="col-6 pl-0 pr-1">
              <button
                onClick={this.props.handleBack}
                className="btn-custom w-100"
              >
                이전
              </button>
            </div>
            <div className="col-6 pl-1 pr-0">
              {this.state.hasShipping ? (
                <button
                  onClick={this.handleNext}
                  className="btn-custom-active w-100"
                >
                  다음
                </button>
              ) : (
                <button className="btn-custom-disable w-100">다음</button>
              )}
            </div>
          </div>
        )}

        {this.state.modal && <this.modalComponent1 />}
        {this.state.modal2 && <this.modalComponent2 />}
      </div>
    );
  }

  private modalComponent1 = () => {
    return (
      <div
        className="modal-custom modal-billing modal-full hidden-m"
        style={{ display: "block" }}
      >
        <div className="modal-content-small">
          {this.state.modalAddress ? (
            <>
              <div className="row p-3">
                <div className="col-6 offset-3">
                  <p className="p-16 bold black-2 text-center ">우편번호검색</p>
                </div>
                <div className="col-3 text-right">
                  <img
                    onClick={() => this.setState({ modalAddress: false })}
                    src={imgClose}
                    alt=""
                    className="cursor"
                  />
                </div>
              </div>
              <DaumPostcode
                style={{
                  height: "100%",
                }}
                onComplete={this.handleAddress}
              />
            </>
          ) : (
            <div className="p-3">
              <div className="row">
                <div className="col-1">
                  <img
                    onClick={this.handleModal}
                    src={prev}
                    alt=""
                    className="cursor"
                  />
                </div>
                <div className="col-11">
                  <p className="p-16 bold black-2">배송지 등록</p>
                </div>
              </div>
              <hr className="hr2 mb-4" />

              <p className="p-14 black-2 mb-2 float-left">배송지명</p>
              <p className="p-13 orange mb-2 float-right">*필수입력</p>
              <input
                onChange={this.handleChange}
                value={this.state.tag}
                maxLength={10}
                placeholder={"10자 이내로 입력해주세요"}
                className="form-control form-custom "
                name="tag"
              />
              <p className="placeholder-right p-13 gray-2">
                {this.state.tag.length}/10
              </p>

              <div className="mt-4">
                <p className="p-14 black-2 mb-2">
                  수령인<span className="orange">*</span>
                </p>
                <input
                  onChange={this.handleChange}
                  value={this.state.name}
                  maxLength={10}
                  placeholder={"10자 이내로 입력해주세요"}
                  className="form-control form-custom "
                  name="name"
                />
                <p className="placeholder-right p-13 gray-2">
                  {this.state.name.length}/10
                </p>
              </div>

              <div className="mt-4">
                <p className="p-14 black-2 mb-2">
                  주소<span className="orange">*</span>
                </p>
                <div className="flex">
                  <div className="col-8 pl-0 pr-1">
                    <input
                      value={this.state.zip}
                      placeholder={"10자 이내로 입력해주세요"}
                      className="form-control form-custom "
                      name="zip"
                    />
                  </div>
                  <div className="col-4 pl-0 pr-0">
                    <button
                      onClick={this.handleModalAddress}
                      className="btn-second-sm w-100 h-100"
                    >
                      우편번호찾기
                    </button>
                  </div>
                </div>
              </div>

              <input
                value={this.state.address}
                className="form-control form-custom mt-2"
                name="address"
              />
              <input
                onChange={this.handleChange}
                value={this.state.addressDetail}
                placeholder={"상세 주소를 입력해주세요"}
                className="form-control form-custom mt-2"
                name="addressDetail"
              />

              <p className="p-14 black-2 mt-3 mb-2">
                연락처<span className="orange">*</span>
              </p>
              <input
                onChange={this.handlePhone}
                type="number"
                value={this.state.phone}
                placeholder={"숫자만 입력해주세요"}
                className="form-control form-custom "
                name="phone"
              />

              <div className="mt-3">
                <img
                  onClick={() =>
                    this.setState({ isDefault: !this.state.isDefault })
                  }
                  src={this.state.isDefault ? checked : check}
                  className="cursor mr-2 float-left"
                />
                <p className="gray-3 p-14 cursor">기본 배송지로 설정</p>
              </div>

              <div className="text-center mt-5">
                <button
                  onClick={this.handleSubmit}
                  className="btn-custom-sm-active"
                >
                  저장
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  private modalComponent2 = () => {
    return (
      <>
        <div
          className="hidden-p modal-custom-scroll pb-5"
          style={{ display: "block" }}
        >
          <div className="modal-content-scroll">
            {this.state.modalAddress ? (
              <>
                <div className="row p-3">
                  <div className="col-6 offset-3">
                    <p className="p-16 bold black-2 text-center ">
                      우편번호검색
                    </p>
                  </div>
                  <div className="col-3 text-right">
                    <img
                      onClick={() => this.setState({ modalAddress: false })}
                      src={imgClose}
                      alt=""
                      className="cursor"
                    />
                  </div>
                </div>
                <DaumPostcode
                  style={{ height: "100%", position: "absolute", top: 50 }}
                  onComplete={this.handleAddress}
                />
              </>
            ) : (
              <div className="p-3">
                <div className="row">
                  <div className="col-1">
                    <img
                      onClick={this.handleModal2}
                      src={prev}
                      alt=""
                      className="cursor"
                    />
                  </div>
                  <div className="col-11">
                    <p className="p-16 bold black-2">배송지 등록</p>
                  </div>
                </div>
                <hr className="hr2 mb-4" />

                <p className="p-14 black-2 mb-2 float-left">배송지명</p>
                <p className="p-13 orange mb-2 float-right">*필수입력</p>
                <input
                  onChange={this.handleChange}
                  value={this.state.tag}
                  maxLength={10}
                  placeholder={"10자 이내로 입력해주세요"}
                  className="form-control form-custom "
                  name="tag"
                />
                <p className="placeholder-right p-13 gray-2">
                  {this.state.tag.length}/10
                </p>

                <div className="mt-4">
                  <p className="p-14 black-2 mb-2">
                    수령인<span className="orange">*</span>
                  </p>
                  <input
                    onChange={this.handleChange}
                    value={this.state.name}
                    maxLength={10}
                    placeholder={"10자 이내로 입력해주세요"}
                    className="form-control form-custom "
                    name="name"
                  />
                  <p className="placeholder-right p-13 gray-2">
                    {this.state.name.length}/10
                  </p>
                </div>

                <div className="mt-4">
                  <p className="p-14 black-2 mb-2">
                    주소<span className="orange">*</span>
                  </p>
                  <div className="flex">
                    <div className="col-8 pl-0 pr-1">
                      <input
                        value={this.state.zip}
                        placeholder={"10자 이내로 입력해주세요"}
                        className="form-control form-custom "
                        name="zip"
                      />
                    </div>
                    <div className="col-4 pl-0 pr-0">
                      <button
                        onClick={this.handleModalAddress}
                        className="btn-second-sm w-100 h-100"
                      >
                        우편번호찾기
                      </button>
                    </div>
                  </div>
                </div>

                <input
                  value={this.state.address}
                  className="form-control form-custom mt-2"
                  name="address"
                />
                <input
                  onChange={this.handleChange}
                  value={this.state.addressDetail}
                  placeholder={"상세 주소를 입력해주세요"}
                  className="form-control form-custom mt-2"
                  name="addressDetail"
                />

                <p className="p-14 black-2 mt-3 mb-2">
                  연락처<span className="orange">*</span>
                </p>
                <input
                  type="number"
                  onChange={this.handlePhone}
                  value={this.state.phone}
                  placeholder={"숫자만 입력해주세요"}
                  className="form-control form-custom "
                  name="phone"
                />

                <div className="mt-3">
                  <img
                    onClick={() =>
                      this.setState({ isDefault: !this.state.isDefault })
                    }
                    src={this.state.isDefault ? checked : check}
                    className="cursor mr-2 float-left"
                  />
                  <p className="gray-3 p-14 cursor">기본 배송지로 설정</p>
                </div>
              </div>
            )}
          </div>
        </div>

        <div className="row bottom-box w-100 p-3">
          <button
            onClick={this.handleSubmit}
            className="btn-custom-active w-100"
          >
            저장
          </button>
        </div>
      </>
    );
  };
}

export default AddShipping;
