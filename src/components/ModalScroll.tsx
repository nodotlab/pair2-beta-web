import React from "react";

class Modal extends React.Component<any> {
  public render() {
    return (
      <div
        className="modal-custom-scroll"
        style={{ display: this.props.display, height: this.props.height }}
      >
        {this.props.children}
      </div>
    );
  }
}

export default Modal;
