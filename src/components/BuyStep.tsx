import React from "react";
import { ask } from "../db/Actions";
import AddBilling from "../components/AddBilling";
import AddShipping from "../components/AddShipping";
import imgComplete from "../images/Step_Completed.svg";
import { withRouter } from "react-router-dom";
import { currency } from "../Method";
import bullet from "../images/bullet.svg";

interface BuyStepState {
  screen: number;
  price: number;
  shipping: object;
}

class BuyStep extends React.Component<any, BuyStepState> {
  constructor(props: any) {
    super(props);
    this.state = {
      screen: 2, // default = 1
      price:
        this.props.shippingType === "택배거래"
          ? this.props.price + 4000
          : this.props.price,
      shipping: {},
    };
  }

  public componentDidMount() {}

  private handleBack = (): void => {
    this.setState({ screen: this.state.screen - 1 });
  };

  private handleNext = (): void => {
    this.setState({ screen: this.state.screen + 1 });
    window.scroll(0, 0);
  };

  private handleSubmit = async () => {
    const token: any = localStorage.getItem("pair2token");
    await ask({
      tradeIndex: this.props.index,
      token: token,
      isShipping: this.props.shippingType === "택배거래" ? true : false,
      price: this.props.price,
      size: this.props.size,
      sneakerName: this.props.name,
      brand: this.props.brand,
      status: "거래요청중",
      seller: this.props.seller,
      shipping: this.state.shipping,
    });

    this.props.history.push(`/buycomplete/${this.props.name}`);
  };

  private handleShippingUpdate = (shipping: any) => {
    delete shipping.tag;
    delete shipping.default;
    this.setState({ shipping: shipping });
  };

  public render() {
    return (
      <div className="col-md-4 offset-md-4">
        <div className="buy-step">
          {this.state.screen < 2 ? (
            <div
              className={`box-step ${
                this.state.screen === 1 && "box-step-active"
              }`}
            >
              1
              {this.state.screen === 1 && (
                <p className="step-p">신용카드 등록</p>
              )}
            </div>
          ) : (
            <img src={imgComplete} />
          )}

          <div className="line-buy-step" />

          {this.state.screen < 3 ? (
            <div
              className={`box-step ${
                this.state.screen === 2 && "box-step-active"
              }`}
            >
              2
              {this.state.screen === 2 && (
                <p className="step-p2">배송지 등록</p>
              )}
            </div>
          ) : (
            <img src={imgComplete} />
          )}

          <div className="line-buy-step" />

          <div
            className={`box-step ${
              this.state.screen === 3 && "box-step-active"
            }`}
          >
            3
            {this.state.screen === 3 && <p className="step-p">거래요청 내역</p>}
          </div>
        </div>

        {this.state.screen === 1 && <this.ComponentScreen1 />}
        {this.state.screen === 2 && <this.ComponentScreen2 />}
        {this.state.screen === 3 && <this.ComponentScreen3 />}
      </div>
    );
  }

  private ComponentScreen1 = () => {
    return (
      <AddBilling
        handleBack={this.props.handleToDetail}
        handleNext={this.handleNext}
      />
    );
  };

  private ComponentScreen2 = () => {
    return (
      <AddShipping
        handleNext={this.handleNext}
        handleBack={this.handleBack}
        handleShippingUpdate={this.handleShippingUpdate}
      />
    );
  };

  private ComponentScreen3 = () => {
    return (
      <div>
        <p className="p-14 black-2">거래요청 내역</p>
        <div className="box-pay-info">
          <div className="row">
            <div className="col-6">
              <p className="gray-3 p-14">구매요청 금액</p>
            </div>
            <div className="col-6 text-right">
              <p className="gray-3 p-16">
                {currency(this.props.price)}
                <span className="p-14">원</span>
              </p>
            </div>
          </div>
          <hr className="hr2 mb-1" />

          <div className="row mt-4">
            <div className="col-6">
              <p className="gray-3 p-14 bold">결제예정금액</p>
            </div>
            <div className="col-6 text-right">
              <p className="orange p-18 bold">
                {currency(this.props.price)}
                <span className="p-14">원</span>
              </p>
            </div>
          </div>
        </div>

        <p className="p-13 gray-3 mt-2 hidden-m mb-5">
          <img src={bullet} className="mr-1" />
          거래가 성사되면 판매자의 연락처를 "마이페이지-구매내역"에서 확인할 수
          있으며 판매자 또한 구매자의 연락처를 확인할 수 있습니다.
        </p>

        <div className="row mt-5 hidden-m">
          <div className="col-md-6">
            <button onClick={this.handleBack} className="btn-custom w-100">
              이전
            </button>
          </div>
          <div className="col-md-6">
            <button
              onClick={this.handleSubmit}
              className="btn-custom-active w-100"
            >
              완료
            </button>
          </div>
        </div>

        <p className="p-13 gray-3 mt-2 hidden-p mb-5">
          <img src={bullet} className="mr-1" />
          거래가 성사되면 판매자의 연락처를 "마이페이지-구매내역"에서 확인할 수
          있으며 판매자 또한 구매자의 연락처를 확인할 수 있습니다.
        </p>

        <div className="row bottom-box hidden-p p-3">
          <div className="col-6 pl-0 pr-1">
            <button onClick={this.handleBack} className="btn-custom w-100">
              이전
            </button>
          </div>
          <div className="col-6 pl-1 pr-0">
            {
              <button
                onClick={this.handleSubmit}
                className="btn-custom-active w-100"
              >
                완료
              </button>
            }
          </div>
        </div>
      </div>
    );
  };
}

export default withRouter(BuyStep);
