import React from "react";
import { Link } from "react-router-dom";
import { currency, timeFormat2, timeFormat5, parsePhone } from "../Method";
import {
  tradingDetail,
  askUpdate,
  shippingWhere,
  askDelete,
  buyRefuse,
  cheat,
  shippingCompanyList,
  shipping,
} from "../db/Actions";
import imgComplete from "../images/Step_Completed.svg";
import plus from "../images/Plus.svg";
import equal from "../images/equal.svg";
import imgClose from "../images/Popup_Delete.svg";
import bullet from "../images/bullet.svg";
import check from "../images/Condition_Check.svg";
import bad from "../images/Bad_Normal.svg";
import badActive from "../images/Bad_Active.svg";
import normal from "../images/Normal_Normal.svg";
import normalActive from "../images/Normal_Active.svg";
import good from "../images/Good_Normal.svg";
import goodActive from "../images/Good_Active.svg";
import paycomplete from "../images/domestic.svg";
import { env } from "../reduce/Env";
import imgPrev from "../images/back.svg";

interface TradingDetailState {
  trade: {
    index: number;
    name: string;
    size: string;
    price: number;
    bidPrice: number; // client bid
    bidTimestamp: number;
    imageUrl: string;
    seller: string;
    sellerPhone: string;
    buyer: string;
    buyerPhone: string;
    buyerEmail: string;
    payment: string;
    paymentTimestamp: number;
    isShipping: boolean;
    shippingName: string;
    shippingPhone: string;
    shippingLocation: string;
    shippingRequest: string;
    isCancle: boolean;
    brand: string;
    tradeIndex: string;
  };
  shippingCompanyList: string[];
  codeList: string[];
  progress: number;
  modal: string;
  cancleType: string;
  cancleDesc: string;
  completeComment: string;
  shippingLocation: string;
  shippingCode: string;
  shippingNumber: string;
  cheat: boolean;
}

class TradingDetail extends React.Component<any, TradingDetailState> {
  constructor(props: any) {
    super(props);
    this.state = {
      trade: {
        isShipping: true,
        index: this.props.index,
        name: "",
        price: 0,
        bidPrice: 0,
        size: "",
        bidTimestamp: Date.now(),
        imageUrl: "",
        seller: "",
        sellerPhone: "",
        buyer: "",
        buyerPhone: "",
        buyerEmail: "",
        payment: "BC카드",
        paymentTimestamp: Date.now(),
        shippingName: "",
        shippingPhone: "",
        shippingLocation: "",
        shippingRequest: "",
        isCancle: true, // this state from props
        brand: "",
        tradeIndex: "",
      },
      shippingCompanyList: [],
      codeList: [],
      progress: 1,
      modal: "none",
      cancleType: "",
      cancleDesc: "",
      completeComment: "",
      shippingLocation: "배송중",
      shippingCode: "",
      shippingNumber: "",
      cheat: false,
    };
  }

  public componentDidMount() {
    this.initData();
    // this.isComplete();
    this.initShipping();
    this.initShippingCompanyList();
  }

  private async initShipping() {
    try {
      const result = await shippingWhere(this.props.index);
      // console.log(result);

      this.setState({
        shippingCode: result.result.code,
        shippingNumber: result.result.number,
      });
    } catch (error) {
      this.setState({
        shippingLocation: "운송장번호에러",
      });
    }
  }

  private async initShippingCompanyList() {
    // const result: any = await shippingCompanyList();
    // const company = result["Company"];
    // const arr: string[] = [];
    // const arr2: string[] = [];

    // for (const i in company) {
    //  arr.push(company[i]["Name"]);
    //  arr2.push(company[i]["Code"]);
    // }

    // this.setState({
    //   shippingCompanyList: arr,
    //   codeList: arr2,
    //  });
    const result: any = await shippingCompanyList();
    const company = result["Company"];
    const arr: string[] = [];
    const arr2: string[] = [];
    arr[0] = "택배사 선택";
    arr2[0] = "택배사 선택";

    for (let i in company) {
      arr.push(company[i]["Name"]);
      arr2.push(company[i]["Code"]);
    }

    this.setState({
      shippingCompanyList: arr,
      codeList: arr2,
    });
  }

  private isComplete(): void {
    // if (this.props.complete && this.state.trade.isShipping) {
    //   this.setState({ progress: 5 });
    // }
    // if (this.props.complete && !this.state.trade.isShipping) {
    //   this.setState({ progress: 3 });
    // }
  }

  private async initData() {
    const token: any = localStorage.getItem("pair2token");
    const result = await tradingDetail(token, this.props.index);
    const trade: any = {
      isShipping: result.isShipping,
      index: this.props.index,
      name: result.name,
      price: result.price,
      bidPrice: result.price,
      size: result.size,
      bidTimestamp: result.askIndex,
      imageUrl: result.imageUrls[0],
      seller: result.seller.name,
      sellerPhone: result.seller.phone,
      buyer: result.buyer.name,
      buyerEmail: result.buyer.email,
      buyerPhone: result.buyer.phone,
      payment: "BC카드", // call from buyer index
      paymentTimestamp: Date.now(),
      shippingName: result.shipping.name,
      shippingPhone: result.shipping.phone,
      shippingLocation: result.shipping.address,
      shippingRequest: "요구사항",
      isCancle: false, // this state from props
      brand: result.brand,
      tradeIndex: result.tradeIndex,
    };

    let progress: number = 1;
    if (!result.isShipping) {
      switch (result.status) {
        case "거래거절":
          progress = 5;
          trade.isCancle = true;
          break;
        case "거래요청중":
          progress = 1;
          break;
        case "직거래대기":
          progress = 2;
          break;
        case "거래완료":
          progress = 3;
          break;
      }
    } else {
      switch (result.status) {
        case "거래거절":
          progress = 5;
          trade.isCancle = true;
          break;
        case "거래요청중":
          progress = 1;
          break;
        case "배송준비중":
          progress = 2;
          break;
        case "배송중":
          progress = 3;
          break;
        case "배송완료":
          progress = 4;
          break;
        case "거래완료":
          progress = 5;
          break;
      }
    }

    this.setState({
      trade: trade,
      progress: progress,
    });

    if (progress === 3 && result.isShipping === true) {
      // console.log(`택배사코드: ${this.state.shippingCode} 운송장:  ${this.state.shippingNumber}`);

      const shippingResult = await shipping(
        this.state.shippingCode,
        this.state.shippingNumber
      );
      // console.log(shippingResult);
      if (shippingResult.completeYN === "Y") {
        await askUpdate(this.props.index, "배송완료");
        this.setState({ progress: 4 });
      }
    }

    try {
      const isCheater = await cheat(result.seller.phone);
      this.setState({ cheat: isCheater });
    } catch (error) {}
  }

  private handleConvert = (): void => {
    const trade = this.state.trade;
    trade.isShipping = !trade.isShipping;

    this.setState({ trade: trade });
  };

  private handleCancleScreen = (): void => {
    if (this.props.complete && this.state.trade.isShipping) {
      this.setState({ progress: 5 });
    }
    if (this.props.complete && !this.state.trade.isShipping) {
      this.setState({ progress: 3 });
    }

    const trade = this.state.trade;
    trade.isCancle = true;
    this.setState({ trade: trade });
  };

  private handleCancle = async () => {
    const token: any = localStorage.getItem("pair2token");

    if (window.confirm("구매요청을 취소하시겠습니까?")) {
      await askDelete(token, this.props.index);
      window.location.href = "/mypage/buys";
    } else {
    }
  };

  private handleText = (e: any): void => {
    this.setState({ cancleDesc: e.target.value });
  };

  private handleSubmitCancle = (): void => {
    if (this.state.cancleType === "" || this.state.cancleDesc === "") {
      alert("사유를 선택, 입력해주세요.");
      return;
    }

    const token: any = localStorage.getItem("pair2token");

    buyRefuse({
      index: this.props.index,
      token: token,
      cancleType: this.state.cancleType,
      cancleDesc: this.state.cancleDesc,
    });

    this.setState({ modal: "none" });
    alert(
      "신고가 접수 되었습니다. 확인 후 1~2 영업일(공휴일 제외) 내에 등록된 연락처로 PAIR2 중재팀에서 연락드립니다."
    );
  };

  private handleSubmitBuy = async () => {
    await askUpdate(this.props.index, "거래완료");
    this.setState({ modal: "none" });
    this.initData();
  };

  private shippingCompany(): string {
    let name = "";
    for (const i in this.state.shippingCompanyList) {
      if (this.state.codeList[i] === this.state.shippingCode)
        name = this.state.shippingCompanyList[i];
    }

    return name;
  }

  public render() {
    return (
      <div className="modal-fullscreen-mobile pb-5">
        <div className="hidden-p w-100 p-2">
          <div className="col-12">
            <div className="row">
              <div className="col-6">
                <p className="black-2 p-16 bold"></p>
              </div>
              <div className="col-6 pr-0 text-right">
                <img
                  src={imgClose}
                  onClick={this.props.handleModal}
                  className="cursor"
                />
              </div>
            </div>
          </div>
          <hr className="hr2" />
        </div>

        <div className="container">
          {this.state.trade.isShipping && !this.state.trade.isCancle && (
            <this.tradeShipping />
          )}
          {!this.state.trade.isShipping && !this.state.trade.isCancle && (
            <this.tradeDirect />
          )}
          {this.state.trade.isCancle && <this.tradeCancle />}
        </div>
      </div>
    );
  }

  private tradeDirect = () => {
    return (
      <>
        {/* <p>테스트버튼</p>
      <button onClick={this.handleConvert} className="btn-custom">직거래/택배 전환</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 1 })}>거래요청모드</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 2 })}>직거래대기모드</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 3 })}>거래완료모드</button>
      <button className="btn-custom" onClick={this.handleCancleScreen}>취소화면</button> */}

        <p className="gray-3 p-14 mt-4">
          요청일
          <span className="roboto black-2 bold ml-2 mr-2">
            {timeFormat2(this.state.trade.bidTimestamp)}
          </span>
          <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호{" "}
          {this.state.trade.tradeIndex}
        </p>

        <div className="bg-gray mt-2 pt-3 pb-5">
          <div className="buy-step mt-4 mb-4">
            {this.state.progress < 2 ? (
              <div
                className={`box-step ${
                  this.state.progress === 1 && "box-step-active"
                }`}
              >
                1
                {this.state.progress === 1 ? (
                  <p className="step-p4">거래요청중</p>
                ) : (
                  <p className="step-p4 gray-4 normal">거래요청중</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p4 mt-1">거래요청중</p>
              </div>
            )}
            <div className="line-buy-step" />
            {this.state.progress < 3 ? (
              <div
                className={`box-step ${
                  this.state.progress === 2 && "box-step-active"
                }`}
              >
                2
                {this.state.progress === 2 ? (
                  <>
                    <p className="step-p4">직거래대기</p>
                    <img src={paycomplete} alt="" className="img-paycomplete" />
                  </>
                ) : (
                  <p className="step-p4 gray-4 normal">직거래대기</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p4 mt-1">직거래대기</p>
                <img src={paycomplete} alt="" className="img-paycomplete" />
              </div>
            )}
            <div className="line-buy-step" />
            <div
              className={`box-step ${
                this.state.progress === 3 && "box-step-active"
              }`}
            >
              3
              {this.state.progress === 3 ? (
                <p className="step-p3">거래완료</p>
              ) : (
                <p className="step-p4 gray-4 normal">거래완료</p>
              )}
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-6">
            <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
          </div>
          <div className="col-6 text-right">
            <Link to="/faq">
              <p className="gray-2 p-14 underline mt-4">1:1 문의하기</p>
            </Link>
          </div>
        </div>

        <hr className="hr2 mb-0 mt-2" />
        <div className="row border-bottom mt-0 pt-3 pb-3">
          <div className="col-md-2 col-4 pl-0 pr-0 text-center">
            <div
              className="stuff-info"
              style={{ backgroundImage: `url(${this.state.trade.imageUrl})` }}
            />
          </div>
          <div className="col-md-5 col-8 pl-1">
            <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
            <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
            <p className="orange roboto bold mt-2 p-14">
              {currency(this.state.trade.price)}
              <span className="p-13 normal">원</span>
            </p>
          </div>
          <div className="col-md-2 text-center pt-4 hidden-m">
            {this.state.progress === 1 && (
              <p className="gray-2 p-14">거래요청중</p>
            )}
            {this.state.progress === 2 && (
              <p className="gray-2 p-14">직거래대기</p>
            )}
          </div>
          <div className="offset-4 mt-2 pl-1 hidden-p">
            {this.state.progress === 1 && (
              <p className="gray-3 p-13 bold">거래요청중</p>
            )}
            {this.state.progress === 2 && (
              <p className="gray-3 p-13 bold">직거래대기</p>
            )}
          </div>
          <div className="col-md-3 text-center">
            {this.state.progress === 1 && (
              <button
                onClick={this.handleCancle}
                className="btn-custom-sm mt-3 m-w100"
              >
                거래요청취소
              </button>
            )}
            {this.state.progress === 2 && (
              <div className="row m-mt2">
                <div className="col-md-12 col-6 m-pr1">
                  <button
                    onClick={() => this.setState({ modal: "buyconfirm" })}
                    className="btn-second-sm w-120 m-w100"
                  >
                    구매확정
                  </button>
                </div>
                <div className="col-md-12 col-6 m-pl1">
                  <button
                    onClick={() => this.setState({ modal: "cancle" })}
                    className="btn-custom-sm mt-1 m-mt0 m-w100"
                  >
                    거래파기신고
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>

        <p className="black-2 bold p-16 mt-4">판매자 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom m-noborder">
          <div className="col-md-4 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
            <div className="row">
              <div className="col-6">
                <p className="black-2 p-14 bold">{this.state.trade.seller}</p>
                <p className="gray-3 roboto p-14 mt-1">
                  {this.state.progress === 1 && "010-****-****"}
                  {this.state.progress === 2 &&
                    parsePhone(this.state.trade.sellerPhone)}
                </p>
              </div>
              <div className="col-6 text-right">
                {this.state.progress === 2 && env === "pc" && (
                  <button className="btn-custom-sm-disable btn-xs mt-3">
                    연락하기
                  </button>
                )}
                {this.state.progress === 2 && env === "app" && (
                  <a
                    href={`/sms/${this.state.trade.sellerPhone}`}
                    target="_new"
                  >
                    <button className="btn-custom-sm btn-xs mt-3">
                      연락하기
                    </button>
                  </a>
                )}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">본인인증</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">OK</p>
              </div>
              <div className="col-6">
                {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
              </div>
              <div className="col-6 text-right">
                {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">사기피해이력</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 은행계좌
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
        </div>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-2" />
          {this.state.progress === 1 &&
            "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
          {this.state.progress > 1 &&
            "판매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
        </p>

        <p className="black-2 bold p-16 mt-4">결제 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom m-noborder">
          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pb2">
            <div className="row">
              <div className="col-6">
                <p className="p-14 black-2 mt-1">구매금액</p>
                <p className="p-14 gray-2 mt-2 hidden-p">제품금액</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-18 black-2 bold roboto mr-2">
                  {currency(this.state.trade.bidPrice)}
                  <span className="noto p-14 normal">원</span>
                </p>
                <p className="p-14 gray-2 mt-2 mr-2 hidden-p">
                  {currency(this.state.trade.bidPrice)}
                  <span className="normal">원</span>
                </p>
                <img src={plus} alt="" className="mypage-plus hidden-m" />
              </div>
            </div>
            <div className="text-center hidden-p">
              <img src={plus} alt="" className="mypage-plus m-plus" />
              <hr className="hr4 mb-0" />
            </div>
          </div>

          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <div className="row">
              <div className="col-6">
                <p className="p-14 black-2 mt-1 ml-1">수수료</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-18 black-2 roboto bold mr-2">
                  {0}
                  <span className="noto p-14 normal">원</span>
                </p>
                <img src={equal} alt="" className="mypage-plus hidden-m" />
              </div>
            </div>
            <div className="text-center hidden-p w-100">
              <img src={equal} alt="" className="mypage-plus m-plus" />
              <hr className="hr4 mb-0" />
            </div>
          </div>

          <div className="col-md-4 pt-4 pb-4">
            <div className="row">
              <div className="col-6">
                <p className="p-14 orange bold mt-1 ml-2">결제금액</p>
                <p className="p-13 gray-2 mt-2 hidden-p ml-2">
                  <img src={bullet} alt="" className="mr-2" />
                  결제수단: {this.state.trade.payment}
                </p>
                <p className="p-13 gray-2 mt-2 hidden-p ml-2">
                  <img src={bullet} alt="" className="mr-2" />
                  결제일시: {timeFormat5(this.state.trade.paymentTimestamp)}
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-18 orange bold roboto">
                  {currency(this.state.trade.bidPrice)}
                  <span className="noto p-14 normal">원</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <hr className="hr4 hidden-p mt-1 mb-0" />

        <div className="row mt-0 border-bottom hidden-m">
          <div className="col-md-4 border-right-gray pt-3 pb-4">
            <div className="row ">
              <div className="col-6">
                <p className="p-14 gray-3 mt-2">제품금액</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-3 mt-2 mr-2 roboto">
                  {currency(this.state.trade.price)}
                  <span className="noto p-normal">원</span>
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-4 border-right-gray pt-2 pb-4"></div>
          <div className="col-md-4 pt-3 pb-4">
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-2" />
              결제수단: {this.state.trade.payment}
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-2" />
              결제일시: {timeFormat5(this.state.trade.paymentTimestamp)}
            </p>
          </div>
        </div>

        {this.state.modal === "cancle" && <this.modalCancle />}
        {this.state.modal === "buyconfirm" && <this.buyConfirmModal />}
      </>
    );
  };

  private modalCancle = () => {
    return (
      <div className="modal-custom modal-full" style={{ display: "block" }}>
        <div className="modal-content-small">
          <div className="p-3">
            <div className="row">
              <div className="col-6">
                <p className="p-16 bold black-2">거래파기신고</p>
              </div>
              <div className="col-6 text-right">
                <img
                  onClick={() => this.setState({ modal: "none" })}
                  src={imgClose}
                  alt=""
                  className="cursor"
                />
              </div>
              <div className="col-12">
                <div className="border-bottom-black mt-3" />
              </div>
            </div>

            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-2" />
              상대방의 귀책사유로 인해 일방적인 거래파기가 되었을때 신고를
              해주세요. 사유를 명확히 입력해 주시면 저희가 상황을 파악한 후에
              빠르게 조치해드리도록 하겠습니다.
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-2" />
              추가 문의사항은 고객센터 1:1 문의 또는 070-4256-0099 로
              연락주세요.
            </p>
            {/* <p className="p-13 gray-2 mt-2">
        <img src={bullet} alt="" className="mr-2" />추가 문의사항은 고객센터 1:1 문의 또는 070-4256-0099 로 연락주세요.
      </p> */}

            <p className="p-14 black-2 mt-4">거래취소 사유를 선택해주세요.</p>
            <button
              onClick={() =>
                this.setState({
                  cancleType: "배송지연(결제 이후 +2영업일, 공휴일 제외)",
                })
              }
              className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${
                this.state.cancleType ===
                  "배송지연(결제 이후 +2영업일, 공휴일 제외)" &&
                "btn-active-sm-border"
              }`}
            >
              배송지연(결제 이후 +2영업일, 공휴일 제외)
              {this.state.cancleType ===
                "배송지연(결제 이후 +2영업일, 공휴일 제외)" && (
                <img src={check} alt="" className="float-right mr-3 mt-1" />
              )}
            </button>
            <button
              onClick={() => this.setState({ cancleType: "직거래 노쇼" })}
              className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${
                this.state.cancleType === "직거래 노쇼" &&
                "btn-active-sm-border"
              }`}
            >
              직거래 노쇼
              {this.state.cancleType === "직거래 노쇼" && (
                <img src={check} alt="" className="float-right mr-3 mt-1" />
              )}
            </button>
            <button
              onClick={() => this.setState({ cancleType: "가품 의심" })}
              className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${
                this.state.cancleType === "가품 의심" && "btn-active-sm-border"
              }`}
            >
              가품 의심
              {this.state.cancleType === "가품 의심" && (
                <img src={check} alt="" className="float-right mr-3 mt-1" />
              )}
            </button>
            <button
              onClick={() =>
                this.setState({
                  cancleType:
                    "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)",
                })
              }
              className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${
                this.state.cancleType ===
                  "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)" &&
                "btn-active-sm-border"
              }`}
            >
              제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)
              {this.state.cancleType ===
                "제품 상태 상이함(배송 중 발생한 손상은 해당사항 없음)" && (
                <img src={check} alt="" className="float-right mr-3 mt-1" />
              )}
            </button>
            <button
              onClick={() =>
                this.setState({
                  cancleType: "기타(이 경우 보상을 받을 수 없습니다)",
                })
              }
              className={`btn-custom-sm w-100 cursor pl-3 text-left mt-2 ${
                this.state.cancleType ===
                  "기타(이 경우 보상을 받을 수 없습니다)" &&
                "btn-active-sm-border"
              }`}
            >
              기타(이 경우 보상을 받을 수 없습니다)
              {this.state.cancleType ===
                "기타(이 경우 보상을 받을 수 없습니다)" && (
                <img src={check} alt="" className="float-right mr-3 mt-1" />
              )}
            </button>

            <p className="p-13 gray-2 mt-3">
              <img src={bullet} alt="" className="mr-2" />
              구체적인 사유를 입력해주세요.
            </p>
            <textarea
              rows={4}
              onChange={this.handleText}
              value={this.state.cancleDesc}
              maxLength={50}
              className="form-control textarea-custom mt-1"
              placeholder="제품교환에 대한 기타 특이사항이 있다면 50자 내로 입력해 주세요."
            />
            <p className="text-right p-13 gray-3 mt-1">
              {this.state.cancleDesc.length}/50
            </p>

            <div className="text-center">
              <button
                onClick={this.handleSubmitCancle}
                className="btn-custom-sm-active mt-3"
              >
                신고
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }; // cancle bidding to change

  private buyConfirmModal = () => {
    return (
      <div className="modal-custom modal-full" style={{ display: "block" }}>
        <div className="modal-content-small">
          <div className="p-3">
            <div className="row">
              <div className="col-6">
                <p className="p-16 bold black-2">구매확정</p>
              </div>
              <div className="col-6 text-right">
                <img
                  onClick={() => this.setState({ modal: "none" })}
                  src={imgClose}
                  alt=""
                  className="cursor"
                />
              </div>
              <div className="col-12">
                <div className="border-bottom-black mt-3" />
              </div>
            </div>

            <div className="text-center">
              <p className="p-22 orange mt-5 mb-2">좋은 거래를 하셨나요?</p>
              <p className="p-14 gray-2">
                거래를 평가해주세요. 구매확정 이후에는 이의를
                <br />
                제기할 수 없으며 판매자에게 대금이 전달됩니다.
              </p>
            </div>

            <hr className="hr-2 mt-5" />

            <div className="row">
              <div className="col-3 text-center">
                <div
                  className="confirm-img"
                  style={{
                    backgroundImage: `url(${this.state.trade.imageUrl})`,
                  }}
                />
              </div>
              <div className="col-9">
                <p className="p-14 gray-3">{this.state.trade.brand}</p>
                <p className="p-14 gray-3">{this.state.trade.name}</p>
                <p className="p-14 gray-2 roboto">{this.state.trade.size} mm</p>
              </div>
            </div>

            <div className="container mt-4">
              <div className="row">
                <div
                  onClick={() => this.setState({ completeComment: "만족" })}
                  className={`col-4 text-center border-confirmbox cursor ${
                    this.state.completeComment === "만족" && "confirmbox-active"
                  }`}
                >
                  <img
                    src={
                      this.state.completeComment === "만족" ? goodActive : good
                    }
                    alt=""
                  />
                  <p
                    className={`p-14 black-2 mt-1 ${
                      this.state.completeComment === "만족" && "orange bold"
                    }`}
                  >
                    만족
                  </p>
                </div>
                <div
                  onClick={() => this.setState({ completeComment: "보통" })}
                  className={`col-4 text-center border-confirmbox cursor ${
                    this.state.completeComment === "보통" && "confirmbox-active"
                  }`}
                >
                  <img
                    src={
                      this.state.completeComment === "보통"
                        ? normalActive
                        : normal
                    }
                    alt=""
                  />
                  <p
                    className={`p-14 black-2 mt-1 ${
                      this.state.completeComment === "보통" && "orange bold"
                    }`}
                  >
                    보통
                  </p>
                </div>
                <div
                  onClick={() => this.setState({ completeComment: "불만족" })}
                  className={`col-4 text-center border-confirmbox cursor ${
                    this.state.completeComment === "불만족" &&
                    "confirmbox-active"
                  }`}
                >
                  <img
                    src={
                      this.state.completeComment === "불만족" ? badActive : bad
                    }
                    alt=""
                  />
                  <p
                    className={`p-14 black-2 mt-1 ${
                      this.state.completeComment === "불만족" && "orange bold"
                    }`}
                  >
                    불만족
                  </p>
                </div>
              </div>
            </div>

            <div className="text-center mt-5 mb-3">
              <button
                onClick={() => this.setState({ modal: "none" })}
                className="btn-custom-sm w-160 mr-1"
              >
                취소
              </button>
              <button
                onClick={this.handleSubmitBuy}
                className="btn-custom-sm-active w-160"
              >
                구매확정
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }; // cancle bidding to change

  private tradeShipping = () => {
    return (
      <>
        {/* <p>테스트버튼</p>
      <button onClick={this.handleConvert} className="btn-custom">직거래/택배 전환</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 1 })}>거래요청</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 2 })}>배송준비중</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 3 })}>배송중</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 4 })}>배송완료</button>
      <button className="btn-custom" onClick={() => this.setState({ progress: 5 })}>거래완료</button>
      <button className="btn-custom" onClick={this.handleCancleScreen}>취소화면</button> */}

        <p className="gray-3 p-14">
          요청일
          <span className="roboto black-2 bold ml-2 mr-2">
            {timeFormat2(this.state.trade.bidTimestamp)}
          </span>
          <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호{" "}
          {this.state.trade.tradeIndex}
        </p>

        <div className="bg-gray mt-2 pt-3 pb-5">
          <div className="buy-step mt-4 mb-4">
            {this.state.progress < 2 ? (
              <div
                className={`box-step ${
                  this.state.progress === 1 && "box-step-active"
                }`}
              >
                1
                {this.state.progress === 1 ? (
                  <p className="step-p4">거래요청중</p>
                ) : (
                  <p className="step-p4 gray-4 normal">거래요청중</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p4 mt-1">거래요청중</p>
              </div>
            )}

            <div className="mypagebuy-step" />
            {this.state.progress < 3 ? (
              <div
                className={`box-step ${
                  this.state.progress === 2 && "box-step-active"
                }`}
              >
                2
                {this.state.progress === 2 ? (
                  <>
                    <p className="step-p4">배송준비중</p>
                    <img src={paycomplete} alt="" className="img-paycomplete" />
                  </>
                ) : (
                  <p className="step-p4 gray-4 normal">배송준비중</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p4 mt-1">배송준비중</p>
                <img src={paycomplete} alt="" className="img-paycomplete" />
              </div>
            )}

            <div className="mypagebuy-step" />
            {this.state.progress < 4 ? (
              <div
                className={`box-step ${
                  this.state.progress === 3 && "box-step-active"
                }`}
              >
                3
                {this.state.progress === 3 ? (
                  <p className="step-p5">배송중</p>
                ) : (
                  <p className="step-p5 gray-4 normal">배송중</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p5 mt-1">배송중</p>
              </div>
            )}

            <div className="mypagebuy-step" />
            {this.state.progress < 5 ? (
              <div
                className={`box-step ${
                  this.state.progress === 4 && "box-step-active"
                }`}
              >
                4
                {this.state.progress === 4 ? (
                  <p className="step-p3">배송완료</p>
                ) : (
                  <p className="step-p3 gray-4 normal">배송완료</p>
                )}
              </div>
            ) : (
              <div className="step-img">
                <img src={imgComplete} />
                <p className="step-p3 mt-1">배송완료</p>
              </div>
            )}

            <div className="mypagebuy-step" />
            <div
              className={`box-step ${
                this.state.progress === 5 && "box-step-active"
              }`}
            >
              5
              {this.state.progress === 5 ? (
                <p className="step-p3">거래완료</p>
              ) : (
                <p className="step-p3 gray-4 normal">거래완료</p>
              )}
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-6">
            <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
          </div>
          <div className="col-6 text-right">
            <Link to="/faq">
              <p className="gray-2 p-14 underline mt-4">1:1 문의하기</p>
            </Link>
          </div>
        </div>
        <hr className="hr2 mb-0 mt-2" />

        <div className="row border-bottom mt-0 pt-3 pb-3">
          <div className="col-md-2 col-4 pl-0 pr-0 text-center">
            <div
              className="stuff-info"
              style={{ backgroundImage: `url(${this.state.trade.imageUrl})` }}
            />
          </div>
          <div className="col-md-4 col-8 pl-1">
            <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
            <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
            <p className="orange roboto bold mt-2 p-14">
              {currency(this.state.trade.price)}
              <span className="p-13 normal">원</span>
            </p>
          </div>
          <div className="col-md-3 text-center hidden-m">
            {this.state.progress === 1 && (
              <p className="gray-2 p-14 mt-4">거래요청중</p>
            )}
            {this.state.progress === 2 && (
              <p className="gray-2 p-14 mt-4">배송준비중</p>
            )}
            {this.state.progress === 3 && (
              <a
                target="_new"
                href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${
                  this.state.shippingNumber
                }`}
              >
                <p className="gray-3 p-13 bold">배송중</p>
                <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">
                  {this.shippingCompany()} {this.state.shippingNumber}
                </button>
              </a>
            )}
            {this.state.progress === 4 && (
              <a
                target="_new"
                href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${
                  this.state.shippingNumber
                }`}
              >
                <p className="gray-3 p-13 bold">배송완료</p>
                <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">
                  {this.shippingCompany()} {this.state.shippingNumber}
                </button>
              </a>
            )}
          </div>

          <div className="offset-4 mt-2 pl-1 hidden-p">
            {this.state.progress === 1 && (
              <p className="gray-3 p-13 bold">거래요청중</p>
            )}
            {this.state.progress === 2 && (
              <p className="gray-3 p-13 bold">배송준비중</p>
            )}
            {this.state.progress === 3 && (
              <a
                target="_new"
                href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${
                  this.state.shippingNumber
                }`}
              >
                <p className="gray-3 p-13 bold">배송중</p>
                <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">
                  {this.shippingCompany()} {this.state.shippingNumber}
                </button>
              </a>
            )}
            {this.state.progress === 4 && (
              <a
                target="_new"
                href={`https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=${this.shippingCompany()} ${
                  this.state.shippingNumber
                }`}
              >
                <p className="gray-3 p-13 bold">배송완료</p>
                <button className="btn pt-0 pb-0 pl-0 gray-3 p-13 underline cursor">
                  {this.shippingCompany()} {this.state.shippingNumber}
                </button>
              </a>
            )}
          </div>

          <div className="col-md-3 text-center">
            {this.state.progress === 1 && (
              <button
                onClick={this.handleCancle}
                className="btn-custom-sm mt-3 m-w100"
              >
                거래요청취소
              </button>
            )}
            {this.state.progress === 2 && (
              <button
                onClick={() => this.setState({ modal: "cancle" })}
                className="btn-custom-sm mt-3 m-w100"
              >
                거래파기
              </button>
            )}
            {this.state.progress === 3 && (
              <button
                onClick={() => this.setState({ modal: "cancle" })}
                className="btn-custom-sm mt-3 m-w100"
              >
                거래파기
              </button>
            )}
            {this.state.progress === 4 && (
              <div className="row m-mt2">
                <div className="col-6 col-md-12 m-pr1">
                  <button
                    onClick={() => this.setState({ modal: "buyconfirm" })}
                    className="btn-second-sm w-120 m-w100 "
                  >
                    구매확정
                  </button>
                </div>
                <div className="col-6 col-md-12 m-pl1">
                  <button
                    onClick={() => this.setState({ modal: "cancle" })}
                    className="btn-custom-sm mt-1 m-mt0 m-w100 "
                  >
                    거래파기신고
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>

        <p className="black-2 bold p-16 mt-4">판매자 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom m-noborder">
          <div className="col-md-4 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
            <div className="row">
              <div className="col-6">
                <p className="black-2 p-14 bold">{this.state.trade.seller}</p>
                <p className="gray-3 roboto p-14 mt-1">
                  {this.state.progress === 1 && "010-****-****"}
                  {this.state.progress > 1 &&
                    parsePhone(this.state.trade.sellerPhone)}
                </p>
              </div>
              <div className="col-6 text-right">
                {this.state.progress > 1 && env === "pc" && (
                  <button className="btn-custom-sm-disable btn-xs mt-3">
                    연락하기
                  </button>
                )}
                {this.state.progress > 1 && env === "app" && (
                  <a
                    href={`/sms/${this.state.trade.sellerPhone}`}
                    target="_new"
                  >
                    <button className="btn-custom-sm btn-xs mt-3">
                      연락하기
                    </button>
                  </a>
                )}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">본인인증</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">OK</p>
              </div>
              <div className="col-6">
                {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
              </div>
              <div className="col-6 text-right">
                {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">사기피해이력</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 은행계좌
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
        </div>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-2" />
          {this.state.progress === 1 &&
            "결제 이후에 판매자의 연락처를 확인할 수 있습니다."}
          {this.state.progress > 1 &&
            "판매자의 연락처 정보를 도용할 경우 법적 문제가 발생할 수 있습니다."}
        </p>

        <p className="black-2 bold p-16 mt-4">배송지 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom m-noborder">
          <div className="col-md-7 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <div className="row">
              <div className="col-4">
                <p className="gray-3 p-14 bold">수령인</p>
                <p className="gray-2 p-14 mt-2">연락처</p>
                <p className="gray-2 p-14 h-shipping mt-2">배송지</p>
                <p className="gray-2 p-14">배송요청사항</p>
              </div>
              <div className="col-8 text-right">
                <p className="gray-3 p-14 bold">
                  {this.state.trade.shippingName}
                </p>
                <p className="gray-2 p-14 mt-2">
                  {parsePhone(this.state.trade.shippingPhone)}
                </p>
                <p className="gray-2 p-14 h-shipping mt-2">
                  {this.state.trade.shippingLocation}
                </p>
                <p className="gray-2 p-14">
                  {this.state.trade.shippingRequest}
                </p>
              </div>
            </div>
          </div>

          <div className="col-md-5 pt-4 pb-4 m-pt2 m-pb2">
            <hr className="hr4 hidden-p mt-0" />
            <div className="row">
              <div className="col-4">
                <p className="gray-3 p-14 bold">구매자</p>
                <p className="gray-2 p-14 mt-2">연락처</p>
                <p className="gray-2 p-14 h-shipping mt-2">이메일</p>
              </div>
              <div className="col-8 text-right">
                <p className="gray-3 p-14 bold">{this.state.trade.buyer}</p>
                <p className="gray-2 p-14 mt-2">
                  {parsePhone(this.state.trade.buyerPhone)}
                </p>
                <p className="gray-2 p-14 h-shipping mt-2">
                  {this.state.trade.buyerEmail}
                </p>
              </div>
            </div>
            <hr className="hr4 hidden-p mt-0" />
          </div>
        </div>

        <p className="black-2 bold p-16 mt-4 m-mt0">결제 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom m-noborder">
          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pb2">
            <div className="row">
              <div className="col-6">
                <p className="p-14 black-2 mt-1">구매금액</p>
                <p className="p-14 gray-2 mt-2 hidden-p">제품금액</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-18 black-2 bold roboto mr-2">
                  {currency(this.state.trade.bidPrice)}
                  <span className="noto p-14 normal">원</span>
                </p>
                <p className="p-14 gray-2 mt-2 mr-2 hidden-p">
                  {currency(this.state.trade.bidPrice)}
                  <span className="normal">원</span>
                </p>
                <img src={plus} alt="" className="mypage-plus hidden-m" />
              </div>
            </div>
            <div className="text-center hidden-p">
              <img src={plus} alt="" className="mypage-plus m-plus" />
              <hr className="hr4 mb-0" />
            </div>
          </div>

          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <div className="row">
              <div className="col-6">
                <p className="p-14 black-2 mt-1 ml-1">수수료</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-18 black-2 roboto mr-2">
                  {0}
                  <span className="noto p-14 normal">원</span>
                </p>
                <img src={equal} alt="" className="mypage-plus hidden-m" />
              </div>
            </div>
            <div className="text-center hidden-p w-100">
              <img src={equal} alt="" className="mypage-plus m-plus" />
              <hr className="hr4 mb-0" />
            </div>
          </div>

          <div className="col-md-4 pt-4 pb-4">
            <div className="row">
              <div className="col-6">
                <p className="p-14 orange bold mt-1 ml-2">결제금액</p>
                <p className="p-13 gray-2 mt-2 hidden-p ml-2">
                  <img src={bullet} alt="" className="mr-2" />
                  결제수단: {this.state.trade.payment}
                </p>
                <p className="p-13 gray-2 mt-2 hidden-p ml-2">
                  <img src={bullet} alt="" className="mr-2" />
                  결제일시: {timeFormat5(this.state.trade.paymentTimestamp)}
                </p>
              </div>
              <div className="col-6 text-right">
                {/* <p className="p-18 orange bold roboto">{currency(this.state.trade.bidPrice + 4000)}<span className="noto p-14 normal">원</span></p> */}
                <p className="p-18 orange bold roboto">
                  {currency(this.state.trade.bidPrice)}
                  <span className="noto p-14 normal">원</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <hr className="hr4 hidden-p mt-1 mb-0" />

        <div className="row mt-0 border-bottom hidden-m">
          <div className="col-md-4 border-right-gray pt-3 pb-4">
            <div className="row">
              <div className="col-6">
                <p className="p-14 gray-3 mt-2">제품금액</p>
                <p className="p-14 gray-3 mt-2">배송비</p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 gray-3 mt-2 mr-2 roboto">
                  {currency(this.state.trade.price)}
                  <span className="noto p-normal">원</span>
                </p>
                {/* <p className="p-14 gray-3 mt-2 mr-2 roboto">{currency(4000)}<span className="noto p-normal">원</span></p> */}
              </div>
            </div>
          </div>
          <div className="col-md-4 border-right-gray pt-2 pb-4"></div>
          <div className="col-md-4 pt-3 pb-4">
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-2" />
              결제수단: {this.state.trade.payment}
            </p>
            <p className="p-13 gray-2 mt-2">
              <img src={bullet} alt="" className="mr-2" />
              결제일시: {timeFormat5(this.state.trade.paymentTimestamp)}
            </p>
          </div>
        </div>

        {this.state.modal === "cancle" && <this.modalCancle />}
        {this.state.modal === "buyconfirm" && <this.buyConfirmModal />}
      </>
    );
  };

  private tradeCancle = () => {
    return (
      <>
        <p className="gray-3 p-14">
          요청일
          <span className="roboto black-2 bold ml-2 mr-2">
            {timeFormat2(this.state.trade.bidTimestamp)}
          </span>
          <span className="gray-2 p-12 ml-2 mr-2">|</span> 거래번호{" "}
          {this.state.trade.tradeIndex}
        </p>

        <div className="row">
          <div className="col-6">
            <p className="black-2 bold p-16 mt-4">주문제품 정보</p>
          </div>
          <div className="col-6 text-right">
            <Link to="/faq">
              <p className="gray-2 p-14 underline mt-4">1:1 문의하기</p>
            </Link>
          </div>
        </div>
        <hr className="hr2 mb-0 mt-2" />
        <div className="flex border-bottom mt-0 pt-3 pb-3">
          <div className="col-md-2 col-4 text-center">
            <div
              className="cancle-img"
              style={{ backgroundImage: `url(${this.state.trade.imageUrl})` }}
            />
          </div>
          <div className="col-md-5 col-5 pl-0 pr-0">
            <p className="gray-3 roboto p-14">{this.state.trade.name}</p>
            <p className="gray-2 roboto p-14">{this.state.trade.size} mm</p>
            <p className="orange roboto bold mt-2 p-14">
              {currency(this.state.trade.price)}
              <span className="p-13 normal">원</span>
            </p>
          </div>
          <div className="col-md-5 col-3 text-center">
            <p className="gray-2 p-14 mt-4">거래취소</p>
          </div>
          {/* <div className="col-md-3 text-center">
      </div> */}
        </div>

        <p className="black-2 bold p-16 mt-4">판매자 정보</p>
        <hr className="hr2 mb-0 mt-2" />
        <div className="row mt-0 border-bottom">
          <div className="col-md-4 border-right-gray pt-4 pb-5 m-pt2 m-pb2 m-mt2">
            <div className="row">
              <div className="col-md-6">
                <p className="black-2 p-14 bold">{this.state.trade.seller}</p>
                <p className="gray-3 roboto p-14 mt-1">010-****-****</p>
              </div>
              <div className="col-md-6">
                {this.state.progress === 2 && env === "pc" && (
                  <button className="btn-custom-sm-disable btn-xs mt-3">
                    연락하기
                  </button>
                )}
                {this.state.progress === 2 && env === "app" && (
                  <a
                    href={`/sms/${this.state.trade.sellerPhone}`}
                    target="_new"
                  >
                    <button className="btn-custom-sm btn-xs mt-3">
                      연락하기
                    </button>
                  </a>
                )}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 border-right-gray pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">본인인증</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">OK</p>
              </div>
              <div className="col-6">
                {/* <p className="p-13 gray-2 mt-2"><img src={bullet} alt="" className="mr-2" /> 은행계좌</p> */}
              </div>
              <div className="col-6 text-right">
                {/* <p className="p-14 bold orange-3 mt-2">OK</p> */}
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
          <div className="col-md-4 pt-4 pb-4 m-pt2 m-pb2">
            <p className="black-2 p-14 bold">사기피해이력</p>
            <div className="row">
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 휴대폰
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
              <div className="col-6">
                <p className="p-13 gray-2 mt-2">
                  <img src={bullet} alt="" className="mr-2" /> 은행계좌
                </p>
              </div>
              <div className="col-6 text-right">
                <p className="p-14 bold orange-3 mt-2">
                  {this.state.cheat === true ? "사기이력존재" : "Clean"}
                </p>
              </div>
            </div>
            <hr className="hr4 mb-0 hidden-p" />
          </div>
        </div>
        <p className="p-13 gray-2 mt-2">
          <img src={bullet} alt="" className="mr-2" />
          결제 이후에 판매자의 연락처를 확인할 수 있습니다.
        </p>
      </>
    );
  };
}

export default TradingDetail;
