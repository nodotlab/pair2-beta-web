import React from "react";
import {
  smsSend,
  smsVerify,
  signin,
  getUserInfo,
  eidtUser,
} from "../db/Actions";
import { withRouter } from "react-router-dom";
import bullet from "../images/bullet.svg";
import inputDelete from "../images/Input_Delete_LightBG.svg";
import radio from "../images/Radio_Default.svg";
import radioChecked from "../images/Radio_Checked.svg";
import agreeDisable from "../images/Agree_Disabled.svg";
import agreeActive from "../images/Agree_Active.svg";
import check from "../images/Checkbox_Default.svg";
import checked from "../images/Checkbox_Checked.svg";
import warining from "../images/Input_Warning.svg";
import imgClose from "../images/Popup_Delete.svg";

interface MypageEditState {
  signinPassword: string;
  screen: string;
  name: string;
  phone: string;
  smsText: string;
  smsReady: boolean;
  smsConfirm: boolean;
  password: string;
  passwordConfirm: string;
  email: string;
  nickname: string;
  age: string;
  ageList: string[];
  month: string;
  monthList: string[];
  date: string;
  dateList: string[];
  sex: string;
  size: string;
  sizeList: string[];
  smsAgree: boolean;
  emailAgree: boolean;
  agree1: boolean;
  agree2: boolean;
  agree3: boolean;
  noti: string;
}

class MypageEdit extends React.Component<any, MypageEditState> {
  constructor(props: any) {
    super(props);

    this.state = {
      signinPassword: "",
      screen: "password",
      name: "",
      phone: "",
      smsText: "",
      smsReady: false,
      smsConfirm: true,
      password: "",
      passwordConfirm: "",
      email: "",
      nickname: "",
      age: "",
      ageList: [],
      month: "",
      monthList: [],
      date: "",
      dateList: [],
      sex: "",
      size: "",
      sizeList: [],
      smsAgree: true,
      emailAgree: true,
      agree1: false,
      agree2: false,
      agree3: false,
      noti: "",
    };
  }

  public componentDidMount() {
    this.createList();
  }

  private createList() {
    const tmpAgeList: string[] = [];
    const tmpMonthList: string[] = [];
    const tmpDateList: string[] = [];
    const tempSizeList: string[] = [];

    tmpAgeList.push("10대");
    tmpAgeList.push("20대");
    tmpAgeList.push("30대");
    tmpAgeList.push("40대");
    tmpAgeList.push("50대");
    tmpAgeList.push("60대");
    tmpAgeList.push("70대");
    tmpAgeList.push("80대");
    tmpAgeList.push("90대");

    for (let i = 1; i < 13; ++i) {
      tmpMonthList.push(i.toString());
    }

    for (let i = 1; i < 32; ++i) {
      tmpDateList.push(i.toString());
    }

    for (let i = 220; i < 320; i += 5) {
      tempSizeList.push(i.toString());
    }

    this.setState({
      ageList: tmpAgeList,
      monthList: tmpMonthList,
      dateList: tmpDateList,
      sizeList: tempSizeList,
    });
  }

  private handleText = (e: React.ChangeEvent<any>) => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value } as {
      [i in keyof MypageEditState]: any;
    });
  };

  private handlePhone = (e: React.ChangeEvent<any>): void => {
    this.setState({ [e.target.name]: e.target.value.replace(/\D/, "") } as any);
  };

  private handleInputClear = (formName: string): any => {
    this.setState({ [formName]: "" } as any);
  };

  private handleSmsSend = async () => {
    if (this.state.smsConfirm) {
      this.setState({ smsConfirm: false });
      return;
    }

    const result = await smsSend(this.state.phone);

    if (result === "success") {
      this.setState({
        smsReady: true,
      });
    } else {
      alert("이미 가입 된 번호입니다.");
    }
  };

  private handleSmsConfirm = async () => {
    const result: string = await smsVerify(
      this.state.phone,
      this.state.smsText
    );

    if (result === "success") {
      this.setState({
        smsReady: false,
        smsConfirm: true,
      });
      alert("인증이 완료되었습니다.");
    } else {
      alert("인증번호를 확인해주세요.");
    }
  };

  private handleAgree = (name: string) => {
    if (name === "agree1") this.setState({ agree1: !this.state.agree1 });
    if (name === "agree2") this.setState({ agree2: !this.state.agree2 });
    if (name === "agree3") this.setState({ agree3: !this.state.agree3 });
    if (name === "all")
      this.setState({ agree1: true, agree2: true, agree3: true });
  };

  private agreeAll(): boolean {
    if (this.state.agree1 && this.state.agree2 && this.state.agree3) {
      return true;
    } else {
      return false;
    }
  }

  private verifyPhone = (): boolean => {
    const regex = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/;

    if (regex.test(this.state.phone)) {
      return true;
    } else {
      return false;
    }
  };

  private passwordCheck = (): boolean => {
    if (this.state.signinPassword.length === 0) return false;

    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=_]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.signinPassword)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPassword = (): boolean => {
    const regex = /(?=.*\d{1,20})(?=.*[~`!@#$%\^&*()-+=_]{1,20})(?=.*[a-zA-Z]{1,50}).{8,20}$/;

    if (regex.test(this.state.password)) {
      return true;
    } else {
      return false;
    }
  };

  private verifyPasswordConfirm = (): boolean => {
    if (this.state.password === this.state.passwordConfirm) {
      return true;
    } else {
      return false;
    }
  };

  private verifyEmail = (): boolean => {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(this.state.email.toLowerCase());
  };

  private handleScreen = async (e: any) => {
    e.preventDefault();
    const token = await signin(this.props.userId, this.state.signinPassword);
    if (token === "faild") {
      this.setState({ noti: "비밀번호가 일치하지 않습니다." });
      return;
    } else {
      localStorage.setItem("pair2token", token);
      this.setState({ screen: "form" });
      const result = await getUserInfo(token);
      this.setState({
        name: result.name,
        phone: result.phone,
        nickname: result.nickname,
        email: result.email,
        age: result.age,
        sex: result.sex,
        size: result.size === 0 ? "" : result.size.toString(),
        month: result.birthMonth === 0 ? "" : result.birthMonth.toString(),
        date: result.birthDate === 0 ? "" : result.birthDate.toString(),
        emailAgree: result.emailAgree,
        smsAgree: result.smsAgree,
        agree1: result.agree1,
        agree2: result.agree2,
        agree3: result.agree3,
        noti: "",
      });
    }
  };

  private verifyAll = (): boolean => {
    if (this.state.name.length === 0) return false;
    if (!this.verifyPhone() || this.state.phone.length === 0) return false;
    if (!this.verifyPassword() || this.state.password.length === 0)
      return false;
    if (!this.verifyPasswordConfirm()) return false;
    if (this.state.nickname.length === 0) return false;
    if (!this.verifyEmail() || this.state.email.length === 0) return false;
    if (!this.state.agree1) return false;
    if (!this.state.agree2) return false;

    return true;
  };

  private handleSubmit = async () => {
    if (this.state.name.length === 0) return;
    if (!this.verifyPhone() || this.state.phone.length === 0) return;
    if (!this.verifyPassword() || this.state.password.length === 0) return;
    if (!this.verifyPasswordConfirm()) return;
    if (this.state.nickname.length === 0) return;
    if (!this.verifyEmail() || this.state.email.length === 0) return;
    if (!this.state.agree1) return;
    if (!this.state.agree2) return;
    if (!this.state.smsConfirm) return;

    const token: any = localStorage.getItem("pair2token");

    // const result = await eidtUser(
    //   token,
    //   this.state.name,
    //   this.state.phone,
    //   this.state.password,
    //   this.state.nickname,
    //   this.state.email,
    //   this.state.age,
    //   this.state.month,
    //   this.state.date,
    //   this.state.sex,
    //   this.state.size,
    //   this.state.smsAgree,
    //   this.state.emailAgree,
    //   this.state.smsText,
    //   this.state.agree1,
    //   this.state.agree2,
    //   this.state.agree3
    // );

    const result:any = [];

    if (result === "faild") {
      alert("휴대폰을 인증한 후 5분내로 가입해주세요.");
    } else {
      localStorage.setItem("pair2token", result);
      alert("수정되었습니다.");
      this.setState({
        screen: "password",
        password: "",
        passwordConfirm: "",
        signinPassword: "",
      });

      window.location.reload();
    }
  };

  public render() {
    return (
      <>
        {this.state.screen === "password" && <this.ComponentPassword />}
        {this.state.screen === "form" && <this.ComponentForm />}
        {this.state.screen === "form" && <this.ComponentFormMobile />}
      </>
    );
  }

  private ComponentForm = () => {
    return (
      <div className="hidden-m">
        <p className="text-right mt-4 p-13 red mt-5">*필수입력</p>

        <div className="box-signup mt-3">
          <div className="col-lg-8 offset-lg-2">
            <p className="p-14 black-2 mb-2">
              이름<span className="red">*</span>
            </p>
            <input
              onChange={this.handleText}
              value={this.state.name}
              name="name"
              type="text"
              placeholder="이름을 입력해주세요"
              className={`form-control form-custom`}
            />

            <p className="p-14 black-2 mt-4 mb-2">
              휴대폰 번호<span className="red">*</span>
            </p>

            <div className="flex">
              <div className="col-8 pl-0 pr-1">
                {this.state.smsConfirm ? (
                  <div className="form-disable">{this.state.phone}</div>
                ) : (
                  <input
                    onChange={this.handlePhone}
                    value={this.state.phone}
                    name="phone"
                    type="text"
                    placeholder="숫자만 입력해주세요"
                    className={`form-control form-custom ${
                      !this.verifyPhone() &&
                      this.state.phone.length > 0 &&
                      "form-warning"
                    }`}
                  />
                )}
                {!this.verifyPhone() && this.state.phone.length > 0 && (
                  <img
                    src={inputDelete}
                    onClick={() => this.handleInputClear("phone")}
                    className="placeholder-right cursor"
                  />
                )}
              </div>
              <div className="col-4 pl-1 pr-0">
                {this.verifyPhone() ? (
                  <button
                    onClick={this.handleSmsSend}
                    className={`w-100 h-100 ${
                      this.state.smsConfirm
                        ? "btn-custom-sm-disable"
                        : "btn-second-sm"
                    }`}
                  >
                    {this.state.smsConfirm ? "수정" : "인증번호 받기"}
                  </button>
                ) : (
                  <button className="btn-custom-sm-disable w-100 h-100">
                    인증번호 받기
                  </button>
                )}
              </div>
            </div>
            {!this.verifyPhone() && this.state.phone.length > 0 && (
              <p className="p-13 red mt-1 mb-3">
                <img src={warining} className="mr-2" />
                올바른 휴대폰 번호를 입력해주세요
              </p>
            )}

            {this.state.smsReady && (
              <div>
                <input
                  onChange={this.handleText}
                  value={this.state.smsText}
                  name="smsText"
                  type="text"
                  placeholder="인증번호를 입력해주세요"
                  className="form-control form-custom mt-2"
                />
                {this.state.smsText.length > 0 && (
                  <img
                    src={inputDelete}
                    onClick={() => this.handleInputClear("smsText")}
                    className="placeholder-right cursor"
                  />
                )}

                <div className="mt-2">
                  <p className="p-13 gray-2 float-left">
                    <img src={bullet} className="mr-2" />
                    휴대폰인증은 1일 5회까지 가능합니다.
                  </p>
                  <p
                    onClick={this.handleSmsSend}
                    className="p-13 gray-2 underline cursor float-right"
                  >
                    인증번호 재전송
                  </p>
                </div>
                <button
                  onClick={this.handleSmsConfirm}
                  className={`w-100 mt-4 ${
                    this.state.smsText.length > 0
                      ? "btn-custom-sm-active"
                      : "btn-custom-sm-disable"
                  }`}
                >
                  인증번호 확인
                </button>
              </div>
            )}

            <p className="p-14 black-2 mt-4 mb-2">
              비밀번호<span className="red">*</span>
            </p>
            <input
              onChange={this.handleText}
              value={this.state.password}
              name="password"
              maxLength={20}
              type="password"
              placeholder="8-20자리의 영문, 숫자, 특수문자"
              className={`form-control form-custom ${
                !this.verifyPassword() &&
                this.state.password.length > 0 &&
                "form-warning"
              }`}
            />
            {!this.verifyPassword() && this.state.password.length > 0 && (
              <p className="p-13 red mt-1 mb-3">
                <img src={warining} className="mr-2" />
                8~20자 영문, 숫자, 특수문자를 사용하세요.
              </p>
            )}

            <input
              onChange={this.handleText}
              value={this.state.passwordConfirm}
              maxLength={20}
              name="passwordConfirm"
              type="password"
              placeholder="비밀번호를 재입력해주세요"
              className={`form-control form-custom mt-2 ${
                !this.verifyPasswordConfirm() &&
                this.state.passwordConfirm.length > 0 &&
                "form-warning"
              }`}
            />
            {!this.verifyPasswordConfirm() &&
              this.state.passwordConfirm.length > 0 && (
                <p className="p-13 red mt-1 mb-3">
                  <img src={warining} className="mr-2" />
                  비밀번호가 잘못되었습니다.
                </p>
              )}

            <p className="p-14 black-2 mt-4 mb-2">
              닉네임<span className="red">*</span>
            </p>
            <input
              onChange={this.handleText}
              value={this.state.nickname}
              name="nickname"
              type="text"
              maxLength={19}
              placeholder="닉네임을 입력해주세요"
              className={`form-control form-custom`}
            />
            <p className="p-13 gray-4 placeholder-right">
              {this.state.nickname.length}/20
            </p>

            <p className="p-14 black-2 mt-4 mb-2">
              이메일<span className="red">*</span>
            </p>
            <input
              onChange={this.handleText}
              value={this.state.email}
              name="email"
              type="text"
              placeholder="이메일주소를 입력해주세요"
              className={`form-control form-custom ${
                !this.verifyEmail() &&
                this.state.email.length > 0 &&
                "form-warning"
              }`}
            />
            {this.state.email.length > 0 && (
              <img
                src={inputDelete}
                onClick={() => this.handleInputClear("email")}
                className="placeholder-right cursor"
              />
            )}
            {!this.verifyEmail() && this.state.email.length > 0 && (
              <p className="p-13 red mt-1 mb-2">
                <img src={warining} className="mr-2" />
                이메일 형식이 잘못되었습니다.
              </p>
            )}

            <hr className="br2 mt-5" />

            <div className="row mt-5">
              <div className="col-6 pr-1">
                <p className="p-14 black-2 mb-2">연령대</p>
                <select
                  onChange={this.handleText}
                  value={this.state.age}
                  className="form-control form-select no-radius"
                  name="age"
                >
                  <option value="없음" disabled>
                    연령대 선택
                  </option>
                  {this.state.ageList.map((age: string, i: number) => (
                    <option key={i}>{age}</option>
                  ))}
                </select>
              </div>

              <div className="col-6 pl-1">
                <p className="p-14 black-2 mb-2">생일</p>
                <div className="flex">
                  <select
                    onChange={this.handleText}
                    value={this.state.month}
                    className="form-control form-select mr-1"
                    name="month"
                  >
                    <option value="" disabled>
                      월
                    </option>
                    {this.state.monthList.map((month: string, i: number) => (
                      <option key={i}>{month}</option>
                    ))}
                  </select>
                  <select
                    onChange={this.handleText}
                    value={this.state.date}
                    className="form-control form-select ml-1"
                    name="date"
                  >
                    <option value="" disabled>
                      일
                    </option>
                    {this.state.dateList.map((date: string, i: number) => (
                      <option key={i}>{date}</option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="row mt-4">
              <div className="col-6 pr-1">
                <p className="p-14 black-2 mb-2">성별</p>
                <select
                  onChange={this.handleText}
                  value={this.state.sex}
                  className="form-control form-select no-radius"
                  name="sex"
                >
                  <option value="없음" disabled>
                    성별 선택
                  </option>
                  <option>여자</option>
                  <option>남자</option>
                </select>
              </div>

              <div className="col-6 pl-1">
                <p className="p-14 black-2 mb-2">신발 사이즈</p>
                <select
                  onChange={this.handleText}
                  value={this.state.size}
                  className="form-control form-select mr-1"
                  name="size"
                >
                  <option value="" disabled>
                    사이즈 선택
                  </option>
                  {this.state.sizeList.map((size: string, i: number) => (
                    <option key={i}>{size}</option>
                  ))}
                </select>
              </div>
            </div>

            <p className="black-2 p-14 mt-4">SMS/이메일 수신여부</p>
            <p className="gray-2 p-13 mt-2">
              <img src={bullet} /> PAIR2의 다양한 정보를 받아보시겠습니까?
            </p>

            <div className="bg-gray mt-3 p-4">
              <div className="row">
                <div className="col-4">
                  <p className="black-2 bold p-13">SMS</p>
                  <p className="black-2 bold p-13 mt-3">이메일</p>
                </div>
                <div className="col-4">
                  <p
                    onClick={() => this.setState({ smsAgree: true })}
                    className={`p-14 cursor ${
                      this.state.smsAgree ? "gray-3" : "gray-6"
                    }`}
                  >
                    <img
                      src={this.state.smsAgree ? radioChecked : radio}
                      className="mr-2"
                    />
                    동의함
                  </p>
                  <p
                    onClick={() => this.setState({ emailAgree: true })}
                    className={`p-14 cursor mt-3 ${
                      this.state.emailAgree ? "gray-3" : "gray-6"
                    }`}
                  >
                    <img
                      src={this.state.emailAgree ? radioChecked : radio}
                      className="mr-2"
                    />
                    동의함
                  </p>
                </div>
                <div className="col-4">
                  <p
                    onClick={() => this.setState({ smsAgree: false })}
                    className={`p-14 cursor ${
                      this.state.smsAgree ? "gray-6" : "gray-3"
                    }`}
                  >
                    <img
                      src={this.state.smsAgree ? radio : radioChecked}
                      className="mr-2"
                    />
                    동의안함
                  </p>
                  <p
                    onClick={() => this.setState({ emailAgree: false })}
                    className={`p-14 cursor mt-3 ${
                      this.state.emailAgree ? "gray-6" : "gray-3"
                    }`}
                  >
                    <img
                      src={this.state.emailAgree ? radio : radioChecked}
                      className="mr-2"
                    />
                    동의안함
                  </p>
                </div>
              </div>
            </div>

            <hr className="br2 mt-5" />

            <button
              onClick={() => this.handleAgree("all")}
              className={`w-100 mt-4 ${
                this.agreeAll() ? "btn-second" : "btn-custom-disable"
              }`}
            >
              전체 약관에 동의{" "}
              <img
                src={this.agreeAll() ? agreeActive : agreeDisable}
                className="ml-2"
              />
            </button>

            <div className="mt-3 p-2 w-100">
              <img
                onClick={() => this.handleAgree("agree1")}
                src={this.state.agree1 ? checked : check}
                className="cursor float-left"
              />
              <p
                onClick={() => this.handleAgree("agree1")}
                className="gray-6 p-14 float-left cursor ml-2"
              >
                PAIR2 이용약관 동의 (필수)
              </p>
              <p className="gray-2 p-13 underline cursor float-right">보기</p>
            </div>
            <div className="mt-4 p-2 w-100">
              <img
                onClick={() => this.handleAgree("agree2")}
                src={this.state.agree2 ? checked : check}
                className="cursor float-left"
              />
              <p
                onClick={() => this.handleAgree("agree2")}
                className="gray-6 p-14 float-left cursor ml-2"
              >
                개인정보 수집 및 이용에 대한 안내 (필수)
              </p>
              <p className="gray-2 p-13 underline cursor float-right">보기</p>
            </div>
            <div className="mt-4 p-2 w-100">
              <img
                onClick={() => this.handleAgree("agree3")}
                src={this.state.agree3 ? checked : check}
                className="cursor float-left"
              />
              <p
                onClick={() => this.handleAgree("agree3")}
                className="gray-6 p-14 float-left cursor ml-2"
              >
                위치정보 이용약관 동의 (선택)
              </p>
              <p className="gray-2 p-13 underline cursor float-right">보기</p>
            </div>
          </div>
        </div>

        <div className="mt-6 mb-100 text-center hidden-m">
          <button
            onClick={() => this.setState({ screen: "password" })}
            className="btn-custom mr-2"
          >
            취소
          </button>
          <button
            onClick={this.handleSubmit}
            className={
              this.verifyAll() ? "btn-custom-active" : "btn-custom-disable"
            }
          >
            수정
          </button>
        </div>
        <div className="row bottom-box pt-2 pb-2 hidden-p">
          <div className="col-6 pr-1">
            <button
              onClick={() => this.setState({ screen: "password" })}
              className="btn-custom w-100"
            >
              취소
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className={
                this.verifyAll()
                  ? "btn-custom-active w-100"
                  : "btn-custom-disable w-100"
              }
            >
              수정
            </button>
          </div>
        </div>
      </div>
    );
  };

  private ComponentFormMobile = () => {
    return (
      <>
        <div className="hidden-p modal-fullscreen-mobile pb-5">
          <div className="w-100 pt-2 pb-2">
            <div className="flex">
              <div className="col-6">
                <p className="black-2 p-16 bold"></p>
              </div>
              <div className="col-6 text-right">
                <img
                  src={imgClose}
                  onClick={() => this.setState({ screen: "password" })}
                  className="cursor"
                />
              </div>
            </div>
            <hr className="hr2" />
          </div>

          <div className="box-signup border-0 mt-0 pt-0">
            <p className="text-right mr-2 p-13 red">*필수입력</p>
            <div className="col-lg-8 offset-lg-2">
              <p className="p-14 black-2 mb-2">
                이름<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.name}
                name="name"
                type="text"
                placeholder="이름을 입력해주세요"
                className={`form-control form-custom`}
              />

              <p className="p-14 black-2 mt-4 mb-2">
                휴대폰 번호<span className="red">*</span>
              </p>

              <div className="flex">
                <div className="col-8 pl-0 pr-1">
                  {this.state.smsConfirm ? (
                    <div className="form-disable">{this.state.phone}</div>
                  ) : (
                    <input
                      onChange={this.handlePhone}
                      value={this.state.phone}
                      name="phone"
                      type="text"
                      placeholder="숫자만 입력해주세요"
                      className={`form-control form-custom ${
                        !this.verifyPhone() &&
                        this.state.phone.length > 0 &&
                        "form-warning"
                      }`}
                    />
                  )}
                  {!this.verifyPhone() && this.state.phone.length > 0 && (
                    <img
                      src={inputDelete}
                      onClick={() => this.handleInputClear("phone")}
                      className="placeholder-right cursor"
                    />
                  )}
                </div>
                <div className="col-4 pl-1 pr-0">
                  {this.verifyPhone() ? (
                    <button
                      onClick={this.handleSmsSend}
                      className={`w-100 h-100 ${
                        this.state.smsConfirm
                          ? "btn-custom-sm-disable"
                          : "btn-second-sm"
                      }`}
                    >
                      {this.state.smsConfirm ? "수정" : "인증번호 받기"}
                    </button>
                  ) : (
                    <button className="btn-custom-sm-disable w-100 h-100">
                      인증번호 받기
                    </button>
                  )}
                </div>
              </div>
              {!this.verifyPhone() && this.state.phone.length > 0 && (
                <p className="p-13 red mt-1 mb-3">
                  <img src={warining} className="mr-2" />
                  올바른 휴대폰 번호를 입력해주세요
                </p>
              )}

              {this.state.smsReady && (
                <div>
                  <input
                    onChange={this.handleText}
                    value={this.state.smsText}
                    name="smsText"
                    type="text"
                    placeholder="인증번호를 입력해주세요"
                    className="form-control form-custom mt-2"
                  />
                  {this.state.smsText.length > 0 && (
                    <img
                      src={inputDelete}
                      onClick={() => this.handleInputClear("smsText")}
                      className="placeholder-right cursor"
                    />
                  )}

                  <div className="mt-2">
                    <p className="p-13 gray-2 float-left">
                      <img src={bullet} className="mr-2" />
                      휴대폰인증은 1일 5회까지 가능합니다.
                    </p>
                    <p
                      onClick={this.handleSmsSend}
                      className="p-13 gray-2 underline cursor float-right"
                    >
                      인증번호 재전송
                    </p>
                  </div>
                  <button
                    onClick={this.handleSmsConfirm}
                    className={`w-100 mt-4 ${
                      this.state.smsText.length > 0
                        ? "btn-custom-sm-active"
                        : "btn-custom-sm-disable"
                    }`}
                  >
                    인증번호 확인
                  </button>
                </div>
              )}

              <p className="p-14 black-2 mt-4 mb-2">
                비밀번호<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.password}
                name="password"
                maxLength={20}
                type="password"
                placeholder="8-20자리의 영문, 숫자, 특수문자"
                className={`form-control form-custom ${
                  !this.verifyPassword() &&
                  this.state.password.length > 0 &&
                  "form-warning"
                }`}
              />
              {!this.verifyPassword() && this.state.password.length > 0 && (
                <p className="p-13 red mt-1 mb-3">
                  <img src={warining} className="mr-2" />
                  8~20자 영문, 숫자, 특수문자를 사용하세요.
                </p>
              )}

              <input
                onChange={this.handleText}
                value={this.state.passwordConfirm}
                maxLength={20}
                name="passwordConfirm"
                type="password"
                placeholder="비밀번호를 재입력해주세요"
                className={`form-control form-custom mt-2 ${
                  !this.verifyPasswordConfirm() &&
                  this.state.passwordConfirm.length > 0 &&
                  "form-warning"
                }`}
              />
              {!this.verifyPasswordConfirm() &&
                this.state.passwordConfirm.length > 0 && (
                  <p className="p-13 red mt-1 mb-3">
                    <img src={warining} className="mr-2" />
                    비밀번호가 잘못되었습니다.
                  </p>
                )}

              <p className="p-14 black-2 mt-4 mb-2">
                닉네임<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.nickname}
                name="nickname"
                type="text"
                maxLength={19}
                placeholder="닉네임을 입력해주세요"
                className={`form-control form-custom`}
              />
              <p className="p-13 gray-4 placeholder-right">
                {this.state.nickname.length}/20
              </p>

              <p className="p-14 black-2 mt-4 mb-2">
                이메일<span className="red">*</span>
              </p>
              <input
                onChange={this.handleText}
                value={this.state.email}
                name="email"
                type="text"
                placeholder="이메일주소를 입력해주세요"
                className={`form-control form-custom ${
                  !this.verifyEmail() &&
                  this.state.email.length > 0 &&
                  "form-warning"
                }`}
              />
              {this.state.email.length > 0 && (
                <img
                  src={inputDelete}
                  onClick={() => this.handleInputClear("email")}
                  className="placeholder-right cursor"
                />
              )}
              {!this.verifyEmail() && this.state.email.length > 0 && (
                <p className="p-13 red mt-1 mb-2">
                  <img src={warining} className="mr-2" />
                  이메일 형식이 잘못되었습니다.
                </p>
              )}

              <hr className="br2 mt-5" />

              <div className="row mt-5">
                <div className="col-6 pr-1">
                  <p className="p-14 black-2 mb-2">연령대</p>
                  <select
                    onChange={this.handleText}
                    value={this.state.age}
                    className="form-control form-select no-radius"
                    name="age"
                  >
                    <option value="없음" disabled>
                      연령대 선택
                    </option>
                    {this.state.ageList.map((age: string, i: number) => (
                      <option key={i}>{age}</option>
                    ))}
                  </select>
                </div>

                <div className="col-6 pl-1">
                  <p className="p-14 black-2 mb-2">생일</p>
                  <div className="flex">
                    <select
                      onChange={this.handleText}
                      value={this.state.month}
                      className="form-control form-select mr-1"
                      name="month"
                    >
                      <option value="" disabled>
                        월
                      </option>
                      {this.state.monthList.map((month: string, i: number) => (
                        <option key={i}>{month}</option>
                      ))}
                    </select>
                    <select
                      onChange={this.handleText}
                      value={this.state.date}
                      className="form-control form-select ml-1"
                      name="date"
                    >
                      <option value="" disabled>
                        일
                      </option>
                      {this.state.dateList.map((date: string, i: number) => (
                        <option key={i}>{date}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>

              <div className="row mt-4">
                <div className="col-6 pr-1">
                  <p className="p-14 black-2 mb-2">성별</p>
                  <select
                    onChange={this.handleText}
                    value={this.state.sex}
                    className="form-control form-select no-radius"
                    name="sex"
                  >
                    <option value="없음" disabled>
                      성별 선택
                    </option>
                    <option>여자</option>
                    <option>남자</option>
                  </select>
                </div>

                <div className="col-6 pl-1">
                  <p className="p-14 black-2 mb-2">신발 사이즈</p>
                  <select
                    onChange={this.handleText}
                    value={this.state.size}
                    className="form-control form-select mr-1"
                    name="size"
                  >
                    <option value="" disabled>
                      사이즈 선택
                    </option>
                    {this.state.sizeList.map((size: string, i: number) => (
                      <option key={i}>{size}</option>
                    ))}
                  </select>
                </div>
              </div>

              <p className="black-2 p-14 mt-4">SMS/이메일 수신여부</p>
              <p className="gray-2 p-13 mt-2">
                <img src={bullet} /> PAIR2의 다양한 정보를 받아보시겠습니까?
              </p>

              <div className="bg-gray mt-3 p-4">
                <div className="row">
                  <div className="col-4">
                    <p className="black-2 bold p-13">SMS</p>
                    <p className="black-2 bold p-13 mt-3">이메일</p>
                  </div>
                  <div className="col-4">
                    <p
                      onClick={() => this.setState({ smsAgree: true })}
                      className={`p-14 cursor ${
                        this.state.smsAgree ? "gray-3" : "gray-6"
                      }`}
                    >
                      <img
                        src={this.state.smsAgree ? radioChecked : radio}
                        className="mr-2"
                      />
                      동의함
                    </p>
                    <p
                      onClick={() => this.setState({ emailAgree: true })}
                      className={`p-14 cursor mt-3 ${
                        this.state.emailAgree ? "gray-3" : "gray-6"
                      }`}
                    >
                      <img
                        src={this.state.emailAgree ? radioChecked : radio}
                        className="mr-2"
                      />
                      동의함
                    </p>
                  </div>
                  <div className="col-4">
                    <p
                      onClick={() => this.setState({ smsAgree: false })}
                      className={`p-14 cursor ${
                        this.state.smsAgree ? "gray-6" : "gray-3"
                      }`}
                    >
                      <img
                        src={this.state.smsAgree ? radio : radioChecked}
                        className="mr-2"
                      />
                      동의안함
                    </p>
                    <p
                      onClick={() => this.setState({ emailAgree: false })}
                      className={`p-14 cursor mt-3 ${
                        this.state.emailAgree ? "gray-6" : "gray-3"
                      }`}
                    >
                      <img
                        src={this.state.emailAgree ? radio : radioChecked}
                        className="mr-2"
                      />
                      동의안함
                    </p>
                  </div>
                </div>
              </div>

              <hr className="br2 mt-5" />

              <button
                onClick={() => this.handleAgree("all")}
                className={`w-100 mt-4 ${
                  this.agreeAll() ? "btn-second" : "btn-custom-disable"
                }`}
              >
                전체 약관에 동의{" "}
                <img
                  src={this.agreeAll() ? agreeActive : agreeDisable}
                  className="ml-2"
                />
              </button>

              <div className="mt-3 p-2 w-100">
                <img
                  onClick={() => this.handleAgree("agree1")}
                  src={this.state.agree1 ? checked : check}
                  className="cursor float-left"
                />
                <p
                  onClick={() => this.handleAgree("agree1")}
                  className="gray-6 p-14 float-left cursor ml-2"
                >
                  PAIR2 이용약관 동의 (필수)
                </p>
                <p className="gray-2 p-13 underline cursor float-right">보기</p>
              </div>
              <div className="mt-4 p-2 w-100">
                <img
                  onClick={() => this.handleAgree("agree2")}
                  src={this.state.agree2 ? checked : check}
                  className="cursor float-left"
                />
                <p
                  onClick={() => this.handleAgree("agree2")}
                  className="gray-6 p-14 float-left cursor ml-2"
                >
                  개인정보 수집 및 이용에 대한 안내 (필수)
                </p>
                <p className="gray-2 p-13 underline cursor float-right">보기</p>
              </div>
              <div className="mt-4 p-2 w-100">
                <img
                  onClick={() => this.handleAgree("agree3")}
                  src={this.state.agree3 ? checked : check}
                  className="cursor float-left"
                />
                <p
                  onClick={() => this.handleAgree("agree3")}
                  className="gray-6 p-14 float-left cursor ml-2"
                >
                  위치정보 이용약관 동의 (선택)
                </p>
                <p className="gray-2 p-13 underline cursor float-right">보기</p>
              </div>
            </div>
          </div>
        </div>

        <div className="row bottom-box pt-2 pb-2 hidden-p">
          <div className="col-6 pr-1">
            <button
              onClick={() => this.setState({ screen: "password" })}
              className="btn-custom w-100"
            >
              취소
            </button>
          </div>
          <div className="col-6 pl-1">
            <button
              onClick={this.handleSubmit}
              className={
                this.verifyAll()
                  ? "btn-custom-active w-100"
                  : "btn-custom-disable w-100"
              }
            >
              수정
            </button>
          </div>
        </div>
      </>
    );
  };

  private ComponentPassword = () => {
    return (
      <div className="mt-5 mb-6">
        <p className="black-2 p-22">비밀번호 재확인</p>
        <p className="gray-2 p-13 mt-3">
          <img src={bullet} className="mr-1" alt="" />
          회원님의 소중한 정보보호를 위해 비밀번호를 재확인하고 있습니다.
        </p>
        <p className="gray-2 p-13 mt-2">
          <img src={bullet} className="mr-1" alt="" />
          회원님의 개인정보를 신중히 취급하며, 회원님의 동의 없이는 기재하신
          회원정보를 공개 및 변경하지 않습니다.
        </p>

        <form onSubmit={this.handleScreen}>
          <div className="box-signup mt-5">
            <div className="col-md-8 offset-md-2">
              <p className="black-2 p-14 mb-2">비밀번호</p>
              <input
                onChange={this.handleText}
                value={this.state.signinPassword}
                name="signinPassword"
                maxLength={20}
                className="form-control form-custom"
                type="password"
                placeholder="8-20자리의 영문, 숫자, 특수문자"
              />

              <p className="p-13 red mt-1">{this.state.noti}</p>
            </div>
          </div>
          <div className="text-center mt-5 hidden-m">
            <button onClick={this.handleScreen} className="btn-custom-active">
              확인
            </button>
          </div>
          <div className="text-center mt-5 hidden-p">
            <button
              onClick={this.handleScreen}
              className="btn-custom-sm-active w-120"
            >
              확인
            </button>
          </div>
        </form>
      </div>
    );
  };
}

export default withRouter(MypageEdit);
