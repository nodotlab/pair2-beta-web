import React from "react";
import bullet from "../images/bullet.svg";
import imgMore from "../images/View_More.svg";
import CardState from "../reduce/BillingState";
import { addCard, getCard, deleteCard, paymentUserIndex } from "../db/Actions";

interface AddBillingState {
  hasCard: boolean;
  modalCard: boolean;
  cardState: CardState;
  number1: string;
  number2: string;
  number3: string;
  number4: string;
  expiration: string;
  cvc: string;
  password: string;
  birth: string;
  agree1: boolean;
  agree2: boolean;
  agree3: boolean;
}

class AddBilling extends React.Component<any, AddBillingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      hasCard: false,
      modalCard: false,
      cardState: new CardState("", ""),
      number1: "",
      number2: "",
      number3: "",
      number4: "",
      expiration: "",
      cvc: "",
      password: "",
      birth: "",
      agree1: false,
      agree2: false,
      agree3: false,
    };
  }

  public componentDidMount() {
    this.getPayment();
  }

  private async getPayment() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCard(token);
      if (result === "faild") {
        this.setState({
          hasCard: false,
        });
      } else {
        this.setState({
          cardState: new CardState(result.cardName, "0"),
          hasCard: true,
        });
      }
    } catch (error) {
      this.setState({ hasCard: false });
    }
  }

  private handleDeleteCard = async () => {
    try {
      const token: any = localStorage.getItem("pair2token");
      await deleteCard(token);

      this.setState({
        hasCard: false,
        modalCard: false,
        cardState: new CardState("", ""),
      });

      this.getPayment();
    } catch (error) {}
  };

  private handleBilling = async () => {
    const token: any = localStorage.getItem("pair2token");
    const userIndex = await paymentUserIndex(token);
    const metchant = Date.now().toString();
    const { IMP }: any = window;
    IMP.init("imp36196053");

    const data = {
      pg: "dinal",
      pay_method: "card",
      merchant_uid: metchant,
      customer_uid: userIndex,
      name: "카드등록",
      amount: 0,
      buyer_email: "",
      buyer_name: "",
      buyer_tel: "",
      buyer_addr: "",
      buyer_postcode: "",
    };

    IMP.request_pay(data, async (res: any) => {
      if (res.success) {
        await addCard({
          token: token,
          merchant_uid: res.merchant_uid,
          card_name: res.card_name,
        });
        this.getPayment();
      } else {
        alert("다시 시도해주세요.");
      }
    });
  };

  public render() {
    return (
      <>
        <div className="mt-5 mb-6">
          <p className="gray-2 p-13">
            <img src={bullet} className="mr-1" alt="" />
            투명한 거래를 위해 본인명의의 신용/체크카드와 계좌번호를
            등록해주세요.
          </p>
          <p className="gray-2 p-13 mt-2">
            <img src={bullet} className="mr-1" alt="" />
            신용/체크카드와 환급계좌는 각 하나씩만 등록 가능합니다.
          </p>

          <div className="box-card w-100">
            {this.state.hasCard ? (
              <div className="box-card-info">
                <p className="p-16 bold gray-3 float-right mr-5 mt-4">
                  {this.state.cardState.cardName}
                </p>
                <p className="p-18 bold black-2 text-center mt-100 boxcard-margin">
                  {/* {this.state.cardState.number1.substr(0, 4)}-{this.state.cardState.number1.substr(4, 2)}**-****-***{this.state.cardState.number1.substr(15, 1)} */}
                </p>
                <button
                  onClick={this.handleDeleteCard}
                  className="btn-custom-xs mt-2 mr-1"
                >
                  삭제
                </button>
                {/* <button onClick={this.handleModalCard} className="btn-second-xs mt-2">수정</button> */}
              </div>
            ) : (
              <div className="box-card-in">
                <p className="black-2 p-14 mb-2">카드 등록하기</p>
                <img
                  onClick={this.handleBilling}
                  src={imgMore}
                  alt=""
                  className="cursor plus-mobile"
                />
              </div>
            )}
          </div>
        </div>

        <div className="row mt-5 hidden-m">
          <div className="col-md-6">
            <button
              onClick={this.props.handleBack}
              className="btn-custom w-100"
            >
              이전
            </button>
          </div>
          <div className="col-md-6">
            {this.state.hasCard ? (
              <button
                onClick={this.props.handleNext}
                className="btn-custom-active w-100"
              >
                다음
              </button>
            ) : (
              <button className="btn-custom-disable w-100">다음</button>
            )}
          </div>
        </div>

        {!this.state.modalCard && (
          <div className="row bottom-box hidden-p p-3">
            <div className="col-6 pl-0 pr-1">
              <button
                onClick={this.props.handleBack}
                className="btn-custom w-100"
              >
                이전
              </button>
            </div>
            <div className="col-6 pl-1 pr-0">
              {this.state.hasCard ? (
                <button
                  onClick={this.props.handleNext}
                  className="btn-custom-active w-100"
                >
                  다음
                </button>
              ) : (
                <button className="btn-custom-disable w-100">다음</button>
              )}
            </div>
          </div>
        )}
      </>
    );
  }
}

export default AddBilling;
