import React from 'react';
import * as firebase from 'firebase/app';

const firebaseConfig = {
    apiKey: "AIzaSyAH04jkePExu2LPulQDRha1kPnTiL40dWg",
    authDomain: "pair2-27129.firebaseapp.com",
    projectId: "pair2-27129",
    storageBucket: "pair2-27129.appspot.com",
    messagingSenderId: "244150093619",
    appId: "1:244150093619:web:9cc0b801523c221adca046",
    measurementId: "G-FWZ73SQRJ0"
  };

  export default firebase.initializeApp(firebaseConfig);