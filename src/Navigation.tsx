import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ScrollToTop from "./components/ScrolltoTop";
import TopNavigation from "./TopNavigation";
import Release from "./pages/Release_vTab";
// import Release from "./pages/Release";
import Game from "./pages/Game"
import GameDetail from "./pages/GameDetail"
import ReleaseDetail from "./pages/ReleaseDetail";
import ReleaseToday from "./pages/ReleaseToday";
import Faq from "./pages/Faq";
import MyPage from "./pages/MyPageByEmail";
import Signup from "./pages/SignupScreen";
import Signin from "./pages/SigninScreen";
import FindPassword from "./pages/FindPassword";
import Notice from "./pages/Notice";
import Tos from "./pages/Tos";
import About from "./pages/About";
import Footer from "./components/Footer";

class Navigation extends React.Component<any> {
  public render() {
    return (
      <Router>
        <ScrollToTop />
        <Route
          path="/:id"
          render={({ match }) => <TopNavigation match={match} />}
        />
        {/* <Route path="/service/v01" exact component={TopNavigation} /> */}
        <Route path="/" exact component={TopNavigation} />
        {/* <Route path="/search/:search" exact component={SearchRedirect} />
        <Route path="/search/" exact component={SearchRedirect} />
        <Route path="/searchresult/:search" exact component={SearchResult} />
        <Route path="/searchresult/" exact component={SearchResult} /> */}
        {/* <Route exact path="/release/" component={Release} /> */}
        <Route exact path="/shoegame/" component={Game} />
        <Route exact path="/shoegame/:id/" component={GameDetail} />
        <Route exact path="/upcoming/" component={Release} />
        <Route exact path="/launched/" component={Release} />
        <Route exact path="/release/:id/" component={ReleaseDetail} />
        <Route exact path="/today/" component={ReleaseToday} />
        <Route exact path="/support/" component={Faq} />
        {/* <Route path="/sneakers" component={Sneakers} />
        <Route exact path="/sneaker/:id" component={SneakerDetail} />
        <Route exact path="/buy/:id/:size" component={BuyList} />
        <Route exact path="/buy/:id/:size/:sale" component={BuyList} />
        <Route exact path="/buycomplete" component={BuyComplete} />
        <Route exact path="/buycomplete/:sneakername" component={BuyComplete} />
        <Route exact path="/askcomplete" component={AskComplete} />
        <Route exact path="/sell/:id/:size" component={Sell} /> */}
        <Route exact path="/mypage/:screen" component={MyPage} />
        <Route exact path="/mypage/" component={MyPage} />
        <Route exact path="/login/" component={Signin} />
        <Route exact path="/login/:page/:id" component={Signin} />
        <Route exact path="/signup/" component={Signup} />
        <Route exact path="/findpassword/" component={FindPassword} />
        <Route exact path="/notice/" component={Notice} />
        <Route exact path="/tos/:page" component={Tos} />
        <Route exact path="/about/" component={About} />
        {/* <Route exact path="/payment/" component={Payment} /> */}
        <Route exact path="/sms/:id" component={MyPage} />
        <Route path="/:id" render={({ match }) => <Footer match={match} />} />
      </Router>
    );
  }
}

export default Navigation;
