import React from "react";
import Navigation from "./Navigation";
import { Provider, ContextInterface } from "./store";
import { User } from "./reduce/User";
import firebaseInit from './firebaseInit';

class App extends React.Component<any, ContextInterface> {
  constructor(props: any) {
    super(props);
    this.state = {
      user: new User(),
      searchText: "",
      providerSearchText: this._providerSearchText,
    };
  }

  public componentDidMount() {}

  public _providerSearchText = (_searchText: string) => {
    this.setState({ searchText: _searchText });
  };

  public render() {
    return (
      <Provider value={this.state}>
        <Navigation />
      </Provider>
    );
  }
}

export default App;
