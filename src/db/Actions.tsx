import axios from "axios";
import { restfulUrl } from "../reduce/Urls";

export const search = async (search: string) => {
  const result = await axios.get(
    `${restfulUrl}/search/sneakers?search=${search}`,
  );
  return result.data.result;
};

export const sneakers = async (
  sort: string,
  filter: object,
  length: number,
) => {
  const result = await axios.get(
    `${restfulUrl}/data/sneakers?sort=${sort}&filter=${JSON.stringify(
      filter,
    )}&length=${length}`,
  );
  return result.data.result;
};

export const release = async () => {
  const result = await axios.get(`${restfulUrl}/data/release`);
  return result.data.result;
};

export const releaseToday = async () => {
  const result = await axios.get(`${restfulUrl}/data/release/today`);
  return result.data.result;
};

export const releaseDetail = async (index: number) => {
  const result = await axios.get(
    `${restfulUrl}/data/release/detail?index=${index}`,
  );
  return result.data.result;
};

export const getStore = async (index: number) => {
  const result = await axios.get(`${restfulUrl}/data/store?index=${index}`);
  return result.data.result;
};

export const faq = async () => {
  const result = await axios.get(`${restfulUrl}/data/faq`);
  return result.data.result;
};

export const getMain = async () => {
  const result = await axios.get(`${restfulUrl}/data/main`);
  return result.data.result;
};

export const getSneakerName = async (name: string) => {
  const result = await axios.get(
    `${restfulUrl}/data/sneaker/name?name=${name}`,
  );
  return result.data;
};

export const getSneakerIndex = async (index: string) => {
  const result = await axios.get(
    `${restfulUrl}/data/sneaker/index?index=${index}`,
  );
  return result.data;
};

export const filters = async () => {
  const result = await axios.get(`${restfulUrl}/data/filters`);
  return result.data.result;
};

export const putLike = async (body: any) => {
  const result = await axios.post(`${restfulUrl}/verify/putlike`, body);
  return result.data.result;
};

export const putLikeTrade = async (body: any) => {
  const result = await axios.post(`${restfulUrl}/verify/putliketrade`, body);
  return result.data.result;
};

export const existLike = async (token: string, name: string) => {
  const result = await axios.get(
    `${restfulUrl}/verify/existlike?token=${token}&name=${name}`,
  );
  return result.data.result;
}; // get my likes

export const existLikeTrade = async (token: string, tradeIndex: string) => {
  const result = await axios.get(
    `${restfulUrl}/verify/existliketrade?token=${token}&tradeindex=${tradeIndex}`,
  );
  return result.data.result;
};

export const likes = async (token: string, count: number) => {
  const result = await axios.get(
    `${restfulUrl}/verify/likes?token=${token}&count=${count}`,
  );
  return result.data.result;
}; // get my likes

export const likesTrade = async (token: string, count: number) => {
  const result = await axios.get(
    `${restfulUrl}/verify/likestrade?token=${token}&count=${count}`,
  );
  return result.data.result;
};

export const deleteLike = async (token: string, index: number) => {
  const result = await axios.get(
    `${restfulUrl}/verify/deletelike?token=${token}&index=${index}`,
  );
  return result.data.result;
};

export const deleteLikeTrade = async (token: string, index: number) => {
  const result = await axios.get(
    `${restfulUrl}/verify/deleteliketrade?token=${token}&index=${index}`,
  );
  return result.data.result;
};

export const smsSend = async (phone: string) => {
  const result = await axios.get(`${restfulUrl}/auth/smssend?phone=${phone}`);
  return result.data.result;
};

export const smsVerify = async (phone: string, code: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/smsverify?phone=${phone}&code=${code}`,
  );
  return result.data.result;
};

export const signin = async (phone: string, password: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/signin?phone=${phone}&password=${password}`,
  );
  return result.data.result;
};

export const signinByEmail = async (email: string, password: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/signin?email=${email}&password=${password}`,
  );
  return result.data.result;
};

export const signup = async (
  name: string,
  phone: string,
  password: string,
  nickname: string,
  email: string,
  age: string,
  birthMonth: string,
  birthDate: string,
  sex: string,
  size: string,
  smsAgree: boolean,
  emailAgree: boolean,
  code: string,
  agree1: boolean,
  agree2: boolean,
  agree3: boolean,
) => {
  const result = await axios.post(`${restfulUrl}/auth/signup`, {
    name: name,
    phone: phone,
    password: password,
    nickname: nickname,
    email: email,
    age: age,
    birthMonth: parseInt(birthMonth),
    birthDate: parseInt(birthDate),
    sex: sex,
    size: parseInt(size),
    smsAgree: smsAgree,
    emailAgree: emailAgree,
    code: code,
    agree1: agree1,
    agree2: agree2,
    agree3: agree3,
  });

  return result.data.result;
};

export const signupByEmail = async (
  password: string,
  nickname: string,
  email: string,
  agree1: boolean,
  agree2: boolean
) => {
  const result = await axios.post(`${restfulUrl}/auth/signupByEmail`, {
    password: password,
    nickname: nickname,
    email: email,
    smsAgree: false,
    emailAgree: false,
    agree1: agree1,
    agree2: agree2,
    agree3: false,
  });

  return result.data.result;
};

export const smsSendFind = async (phone: string, name: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/smssendfind?phone=${phone}&name=${name}`,
  );
  return result.data.result;
};

export const smsVerifyFind = async (phone: string, code: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/smsverifyfind?phone=${phone}&code=${code}`,
  );
  return result.data.result;
};

export const editPassword = async (
  phone: string,
  code: string,
  password: string,
) => {
  const result = await axios.get(
    `${restfulUrl}/auth/editpassword?phone=${phone}&code=${code}&password=${password}`,
  );
  return result.data.result;
};

export const getCurrentUser = async (token: string) => {
  const result = await axios.get(`${restfulUrl}/verify/mypage?token=${token}`);
  return result.data.result;
};

export const getUserVerify = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/userverify?token=${token}`,
  );
  return result.data.result;
};

export const userVerifyupdate = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/auth/verifyupdate?token=${token}`,
  );
  return result.data.result;
};

export const getUserInfo = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/verify/userinfo?token=${token}`,
  );
  return result.data.result;
};

export const eidtUser = async (
  token: string,
  password: string,
  nickname: string,
  email: string,
  agree1: boolean,
  agree2: boolean,
) => {
  const result = await axios.post(`${restfulUrl}/auth/edituserByEmail`, {
    token: token,
    password: password,
    nickname: nickname,
    email: email,
    agree1: agree1,
    agree2: agree2,
  });

  return result.data.result;
};

export const getShippings = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/verify/shippings?token=${token}`,
  );
  return result.data.result;
};

export const putShippings = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/verify/editshippings`, data);
  return result.data.result;
}; // new recurrenting

// export const shippingCompanyList = async () => {
//   const result = await axios.get("https://info.sweettracker.co.kr/api/v1/companylist?t_key=plGP8DagkGlOmydpqvVPYw");
//   return result.data;
// }; // new recurrenting
export const shippingCompanyList = async () => {
  const company = {
    Company: [
      { Name: "건영택배", Code: "18" },
      { Name: "경동택배", Code: "23" },
      { Name: "홈픽택배", Code: "54" },
      { Name: "굿투럭", Code: "40" },
      { Name: "농협택배", Code: "53" },
      { Name: "대신택배", Code: "22" },
      { Name: "로젠택배", Code: "06" },
      { Name: "롯데택배", Code: "08" },
      { Name: "세방", Code: "52" },
      { Name: "애니트랙", Code: "43" },
      { Name: "우체국택배", Code: "01" },
      { Name: "일양로지스", Code: "11" },
      { Name: "천일택배", Code: "17" },
      { Name: "한덱스", Code: "20" },
      { Name: "한의사랑택배", Code: "16" },
      { Name: "한진택배", Code: "05" },
      { Name: "합동택배", Code: "32" },
      { Name: "호남택배", Code: "45" },
      { Name: "CJ대한통운", Code: "04" },
      { Name: "CU편의점택배", Code: "46" },
      { Name: "CVSnet 편의점택배", Code: "24" },
      { Name: "KGB택배", Code: "56" },
      { Name: "KGL네트웍스", Code: "30" },
      { Name: "SLX", Code: "44" },
      { Name: "하이택배", Code: "58" },
      { Name: "FLF퍼레버택배", Code: "64" },
    ],
  };

  return company;
};

export const shippingCode = async (
  index: number,
  code: string,
  num: string,
  request: string,
) => {
  const result = await axios.get(
    `${restfulUrl}/shipping/update?index=${index}&code=${code}&num=${num}&request=${request}`,
  );
  return result.data;
}; // put shipping company code and number

export const shippingWhere = async (index: number) => {
  const result = await axios.get(`${restfulUrl}/shipping/where?index=${index}`);
  return result.data;
}; // put shipping company code and number

export const getBank = async (token: string) => {
  const result = await axios.get(`${restfulUrl}/verify/bank?token=${token}`);
  return result.data.result;
};

export const putBank = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/verify/editbank`, data);
  return result.data.result;
};

export const addCard = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/payment/addcard`, data);
  return result.data.result;
};

export const getCard = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/payment/cardname?token=${token}`,
  );
  return result.data.result;
};

export const deleteCard = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/payment/deletecard?token=${token}`,
  );
  return result.data.result;
};

export const payment = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/payment/gateway`, data);
  return result.data.result;
};

export const tradeUpload = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/trade/upload`, data);
  return result.data.result;
};

export const tradeImageUpload = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/trade/imageupload`, data);
  return result.data.result;
};

export const tradeList = async (index: number) => {
  const result = await axios.get(
    `${restfulUrl}/trade/list?index=${index}&limit=1000`,
  );
  return result.data.result;
};

export const tradeEdit = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/trade/edit`, data);
  return result.data.result;
};

export const tradeDetail = async (index: number) => {
  const result = await axios.get(`${restfulUrl}/trade/detail?index=${index}`);
  return result.data.result;
};

export const tradeBlock = async (index: number, trade: number) => {
  const result = await axios.get(
    `${restfulUrl}/trade/block?index=${index}&trade=${trade}`,
  );
  // console.log(result);
  return result.data.result;
};

export const tradeUpdate = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/trade/update`, data);
  // console.log(result);
  return result.data.result;
};

export const ask = async (data: any) => {
  const result = await axios.post(`${restfulUrl}/ask/put`, data);
  return result.data.result;
};

export const tradingBuyer = async (token: string) => {
  const result = await axios.get(`${restfulUrl}/trading/buyer?token=${token}`);
  return result.data.result;
};

export const tradingSeller = async (token: string) => {
  const result = await axios.get(`${restfulUrl}/trading/seller?token=${token}`);
  return result.data.result;
};

export const tradingDetail = async (token: string, index: number) => {
  const result = await axios.get(
    `${restfulUrl}/trading/detail?token=${token}&index=${index}`,
  );
  // console.log(result);
  return result.data.result;
};

export const askSeller = async (token: string) => {
  const result = await axios.get(`${restfulUrl}/ask/seller?token=${token}`);
  return result.data.result;
};

export const askExist = async (index: string) => {
  const result = await axios.get(`${restfulUrl}/ask/exist?index=${index}`);
  return result.data.result;
};

export const askBuyerInfo = async (index: string) => {
  const result = await axios.get(`${restfulUrl}/ask/buyerinfo?index=${index}`);
  return result.data.result;
};

export const askRefuse = async (index: string) => {
  const result = await axios.get(`${restfulUrl}/ask/refuse?index=${index}`);
  return result.data.result;
};

export const askAccept = async (token: string, index: string) => {
  const result = await axios.get(
    `${restfulUrl}/ask/accept?token=${token}&index=${index}`,
  );
  return result.data.result;
};

export const askUpdate = async (index: string, status: string) => {
  const result = await axios.get(
    `${restfulUrl}/ask/update?index=${index}&status=${status}`,
  );
  return result.data.result;
};

export const askDelete = async (token: string, index: string) => {
  const result = await axios.get(
    `${restfulUrl}/ask/delete?token=${token}&index=${index}`,
  );
  return result.data.result;
};

// new method billings
export const paymentUserIndex = async (token: string) => {
  const result = await axios.get(
    `${restfulUrl}/payment/userindex?token=${token}`,
  );
  return result.data.result;
};

// export const sneakerPrice = async (index: number) => {
//   const result = await axios.get(`${restfulUrl}/trade/price?index=${index}`);
//   return result.data.result;
// };

export const sneakerPricelist = async (index: number) => {
  const result = await axios.get(
    `${restfulUrl}/trade/pricelist?index=${index}`,
  );
  return result.data.result;
}; // sneaker detail price

export const buyRefuse = async (body: any) => {
  const result = await axios.post(`${restfulUrl}/trade/refuse`, body);
  return result.data.result;
};

export const questionSend = async (body: any) => {
  const result = await axios.post(`${restfulUrl}/question/send`, body);
  return result.data.result;
};

export const cheat = async (phone: string) => {
  const result = await axios.get(`${restfulUrl}/data/cheat?phone=${phone}`);
  return result.data.result;
};

export const cheatRecurrent = async (phone: string) => {
  const result = await axios.get(
    `${restfulUrl}/data/cheatrecurrent?phone=${phone}`,
  );
  return result.data.result;
};

export const cheatIndex = async (index: number) => {
  const result = await axios.get(
    `${restfulUrl}/data/cheatindex?index=${index}`,
  );
  return result.data.result;
};

export const shipping = async (code: string, num: string) => {
  const result = await axios.get(
    `https://info.sweettracker.co.kr/api/v1/trackingInfo?t_key=rsYzbGyzOfc1fg4kYJi6Bg&t_code=${code}&t_invoice=${num}`,
  );
  return result.data;
};

export const mainProduct = async () => {
  const result = await axios.get(`${restfulUrl}/data/mainproduct`);
  return result.data.result;
};

export const getCommision = async () => {
  const result: any = await axios.get(`${restfulUrl}/data/commision`);
  return result.data.result;
};
