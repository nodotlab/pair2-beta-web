export interface User {
  index: number; // uid -> timestamp
  name: string;
  phone: number;
  hash: string;
  salt: string;
  nickname: string;
  email: string;
  age: string;
  month: string;
  date: string;
  agree1: boolean;
  agree2: boolean;
  verifyPhone: boolean;
  verifyBank: boolean;
}

export interface Filter {
  index: number // uid timestamp
  brand: string // brand name
  otherBrand: boolean // other brand or not
}

export interface Sneaker {
  index: number; // index(uid) timestamp
  brand: string; // sub index 
  name: string; // sub index
  nameKr: string; // sub index
  releaseDate: number; // timestamp
  releasePrice: number;
  serial: string;
  thumbUrl: string;
  imageUrl1: string;
  imageUrl2: string;
  imageUrl3: string;
  imageUrl4: string;
  imageUrl5: string;  
  videoUrl: string;
} // Sneaker information

