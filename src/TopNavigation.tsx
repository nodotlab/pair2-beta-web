import React from "react";
import { Link, withRouter } from "react-router-dom";
import { getCurrentUser } from "./db/Actions";
import { parsePhone } from "./Method";
import Footer from "./components/Footer";
// import Main from "./pages/Main";
import Main from "./pages/ReleaseToday";
import imgLogo from "./images/pair2_web_logo.png";
import imgMy from "./images/GNB_My.svg";
import imgMenu from "./images/GNB_Menu.svg";
import imgSearch from "./images/GNB_Search.svg";
import imgSearch2 from "./images/Search.svg";
import imgClose from "./images/GNB_Close.svg";
import imgInputClose from "./images/Input_Delete_DarkBG.svg";
import imgPrev from "./images/back.svg";
import imgMenu1 from "./images/Menu_LuckyDraw.svg";
import imgMenu2 from "./images/Menu_Shop.svg";
import imgMenu3 from "./images/Menu_Calendar.svg";
import imgMenu4 from "./images/Menu_About.svg";
import imgMenu5 from "./images/Menu_Setting.svg";

interface TopNavigationState {
  modalSearch: boolean;
  search: string;
  menuOpen: boolean;
  nickname: string;
  phone: string;
}

class TopNavigation extends React.Component<any, TopNavigationState> {
  constructor(props: any) {
    super(props);
    this.state = {
      modalSearch: false,
      search: "",
      menuOpen: false,
      nickname: "",
      phone: "",
    };
  }

  public componentDidUpdate() {
    this.searchInput.focus();
  }

  public searchInput: any = null;

  private handleModalSearch = (): void => {
    this.setState({ modalSearch: !this.state.modalSearch });
  };

  private handleText = (e: any) => this.setState({ search: e.target.value });

  private handleClear = () => this.setState({ search: "" });

  private handleSubmit = (e: any): void => {
    e.preventDefault();
    this.setState({ modalSearch: !this.state.modalSearch });
    return this.props.history.push(`/search/${this.state.search}`);
  };

  private async getUser() {
    try {
      const token: any = localStorage.getItem("pair2token");
      const result = await getCurrentUser(token);

      this.setState({
        phone: result.phone,
        nickname: result.nickname,
      });
    } catch (error) {
      localStorage.setItem("pair2signin", "false");
      localStorage.setItem("pair2token", "");
      localStorage.setItem("pair2nickname", "");
    }
  }

  private handleMenu = (): void => {
    this.getUser();
    this.setState({ menuOpen: !this.state.menuOpen });
  };

  private handleSecondMenu = (): void => {};

  private isSecondMenu(): boolean {
    switch (this.props.match.params.id) {
      case undefined:
        return false;
      default:
        return true;
    }
  }

  private menuTitle(): string {
    switch (this.props.match.params.id) {
      case "buy":
        return "구매";
      case "sell":
        return "판매";
      case "sneakers":
        return "SNEAKERS";
      case "today":
        return "TODAY";
      case "release":
        return "LAUNCH CALENDAR";
      case "upcoming":
        return "UPCOMING";
      case "launched":
        return "LAUNCHED";
      case "about":
        return "ABOUT";
      case "sneakers":
        return "SNEAKERS";
      case "buycomplete":
        return "구매완료";
      case "faq":
        return "고객센터";
      case "login":
        return "로그인";
      case "signup":
        return "회원가입";
      case "findpassword":
        return "비밀번호 찾기";
      case "notice":
        return "공지사항";
      default:
        return "";
    }
  }

  private historyBack = () => {
    const lastPath: string = this.props.history.location.pathname.split("/")[
      this.props.history.location.pathname.split("/").length - 1
    ];
    let loginWithBuyAndSell: boolean = false;
    if (
      this.props.history.location.pathname.split("/")[2] === "buy" ||
      this.props.history.location.pathname.split("/")[2] === "sell"
    ) {
      loginWithBuyAndSell = true;
    }

    switch (this.props.match.params.id) {
      case "buycomplete":
        return this.props.history.push(`/sneaker/${lastPath}`);
      case "login":
        if (loginWithBuyAndSell)
          return this.props.history.push(`/sneaker/${lastPath}`);
      default:
        return this.props.history.goBack();
    }
  };

  private handleToMypage = (): void => {
    this.props.history.push("/mypage");
    this.handleMenu();
  };

  public render() {
    return (
      <div>
        <this.componentHeader />
        <this.componentModal />
      </div>
    );
  }

  private componentHeader = () => {
    return (
      <div>
        <nav className={`navbar fixed-top navbar-expand-lg navbar-dark`}>
          <Link
            onClick={this.handleMenu}
            to="/"
            className="navbar-brand hidden-m"
          >
            <img src={imgLogo} alt="" />
          </Link>
          <div className="hidden-p">
            {!this.isSecondMenu() && (
              <Link onClick={this.handleMenu} to="/" className="navbar-brand">
                <img src={imgLogo} alt="" />
              </Link>
            )}
            {this.state.menuOpen && this.isSecondMenu() && (
              <Link onClick={this.handleMenu} to="/" className="navbar-brand">
                <img src={imgLogo} alt="" />
              </Link>
            )}
            {this.isSecondMenu() && !this.state.menuOpen && (
              <div
                onClick={this.historyBack}
                className="navbar-brand navbar-title"
              >
                <img src={imgPrev} className="navbar-prev" alt="" />
                {this.menuTitle()}
              </div>
            )}
          </div>
          <ul className="navbar-nav ml-auto mr-5 hidden-m">
            {/* <li
              className={`nav-item ${
                this.props.match.params.id === "sneakers" && "nav-active"
              } ${this.props.match.url === "/sneaker" && "nav-active"}`}
            >
              <Link to="/sneakers" className="nav-link" href="#">
                Sneakers
              </Link>
            </li> */}
            <li
              className={`nav-item ${
                (this.props.match.params.id === "today" || this.props.match.params.id === undefined) && "nav-active"
              }`}
            >
              <Link to="/today/" className="nav-link" href="#">
                Today 
              </Link>
            </li>
            <li
              className={`nav-item ${
                this.props.match.params.id === "upcoming" && "nav-active"
              }`}
            >
              <Link to="/upcoming/" className="nav-link" href="#">
                Upcoming
              </Link>
            </li>
            <li
              className={`nav-item ${
                this.props.match.params.id === "launched" && "nav-active"
              }`}
            >
              <Link to="/launched/" className="nav-link" href="#">
                Launched
              </Link>
            </li>
            <li
              className={`nav-item ${
                this.props.match.params.id === "about" && "nav-active"
              }`}
            >
              <Link to="/about/" className="nav-link" href="#">
                About
              </Link>
            </li>
     
          </ul>

          <div className="nav-right-box">
            <Link
              to={
                localStorage.getItem("pair2signin") === "true"
                  ? "/mypage/"
                  : "/login/"
              }
            >
              <div className="nav-right border-right-white hidden-m cursor">
                <img src={imgMy} />
              </div>
            </Link>
            {/* <div
              onClick={this.handleModalSearch}
              className={`nav-right d-inline-block cursor`}
            >
              {this.state.modalSearch ? (
                <img src={imgClose} />
              ) : (
                <img src={imgSearch} />
              )}
            </div> */}
            {!this.state.modalSearch && (
              <div
                onClick={this.handleMenu}
                className={`nav-right border-left-white hidden-p`}
              >
                {this.state.menuOpen ? (
                  <img src={imgClose} />
                ) : (
                  <img src={imgMenu} />
                )}
              </div>
            )}
          </div>
        </nav>

        {this.state.menuOpen && <this.componentMobileMenu />}

        {!this.props.match.params.id && (
          <>
            <Main />
            <Footer main={true} />
          </>
        )}
      </div>
    );
  };

  private componentModal = () => {
    return (
      <div
        className="modal modal-search"
        style={{ display: this.state.modalSearch ? "block" : "none" }}
      >
        <div className="modal-search-content">
          <div className="container">
            <div className="col-lg-6 offset-lg-3">
              <form onSubmit={this.handleSubmit}>
                <input
                  ref={(ref) => (this.searchInput = ref)}
                  onChange={this.handleText}
                  name="search"
                  value={this.state.search}
                  className="form-control form-search"
                  placeholder="검색어를 입력하세요"
                />
                {this.state.search.length > 0 && (
                  <img
                    onClick={this.handleClear}
                    src={imgInputClose}
                    className="placeholder-right placeholder-right-search-close cursor"
                  />
                )}
                <img
                  onClick={this.handleSubmit}
                  src={imgSearch2}
                  className="placeholder-right placeholder-right-search cursor"
                />
              </form>
            </div>
          </div>

          <div className="box-search-bottom">
            <p className="searchslide">
              <span className="mr-8">
                ENTER THE SHOE GAME&nbsp;&nbsp;&nbsp;ENTER THE SHOE
                GAME&nbsp;&nbsp;&nbsp;ENTER THE SHOE GAME&nbsp;&nbsp;&nbsp;ENTER
                THE SHOE GAME&nbsp;&nbsp;&nbsp;ENTER THE SHOE
                GAME&nbsp;&nbsp;&nbsp;ENTER THE SHOE GAME&nbsp;&nbsp;&nbsp;ENTER
                THE SHOE GAME&nbsp;&nbsp;&nbsp;ENTER THE SHOE
                GAME&nbsp;&nbsp;&nbsp;
              </span>
            </p>
          </div>
        </div>
      </div>
    );
  };

  private componentMobileMenu = () => {
    return (
      <div className="menu-mobile hidden-p">
        <div className="menu-mobile-top">
          {localStorage.getItem("pair2signin") === "true" ? (
            <div onClick={this.handleToMypage} className="container">
              <p className="white p-14">{this.state.nickname}</p>
              <p className="white roboto bold p-14">
                {parsePhone(this.state.phone)}
              </p>
            </div>
          ) : (
            <div className="container">
              <div className="flex">
                <div className="col-7 pl-0 pr-0">
                  <p className="white p-14">
                    PAIR2 서비스를 이용하시려면
                    <br />
                    로그인이 필요합니다.
                  </p>
                </div>
                <div className="col-3 text-center">
                  <Link onClick={this.handleMenu} to="/login">
                    <button className="btn-border mt-2">로그인</button>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="menu-mobile-li">
          <Link onClick={this.handleMenu} to="/today/">
            <p className="black-1 p-16">
              <span className="mt-1">TODAY - 오늘의 발매</span>
            </p>
          </Link>
        </div>
        <div className="menu-mobile-li">
          <Link onClick={this.handleMenu} to="/upcoming/">
            <p className="black-1 p-16">
              <span className="mt-1">UPCOMING - 발매 예정</span>
            </p>
          </Link>
        </div>
        <div className="menu-mobile-li">
          <Link onClick={this.handleMenu} to="/launched/">
            <p className="black-1 p-16">
              <span className="mt-1">LAUNCHED - 발매 완료</span>
            </p>
          </Link>
        </div>
        <div className="menu-mobile-li">
          <Link onClick={this.handleMenu} to="/about/">
            <p className="black-1 p-16">
              <span className="mt-1">ABOUT - 소개</span>
            </p>
          </Link>
        </div>
      </div>
    );
  };

  private componentMobileSecondMenu = () => {
    return (
      <div className="menu-mobile">
        <div className="menu-mobile-top">
          {localStorage.getItem("pair2signin") === "true" ? (
            <div className="container">
              <p className="white p-14">{this.state.nickname}</p>
              <p className="white roboto bold p-14">
                {parsePhone(this.state.phone)}
              </p>
            </div>
          ) : (
            <div className="container">
              <div className="flex">
                <div className="col-7 pl-0 pr-0">
                  <p className="white p-14">
                    PAIR2 서비스를 이용하시려면
                    <br />
                    로그인이 필요합니다.
                  </p>
                </div>
                <div className="col-3 text-center">
                  <Link to="/login/">
                    <button className="btn-border mt-2">로그인</button>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="menu-mobile-li">
          <p className="black-1 roboto p-16">
            <img src={imgMenu1} alt="" className="mr-2" />
            <span className="mt-1">Game</span>
          </p>
        </div>
        <div className="menu-mobile-li">
          <p className="black-1 roboto p-16">
            <img src={imgMenu2} alt="" className="mr-2" />
            <span className="mt-1">Shop</span>
          </p>
        </div>
        <div className="menu-mobile-li">
          <p className="black-1 roboto p-16">
            <img src={imgMenu3} alt="" className="mr-2" />
            <span className="mt-1">Launch Calendar</span>
          </p>
        </div>
        <div className="menu-mobile-li">
          <p className="black-1 roboto p-16">
            <img src={imgMenu4} alt="" className="mr-2" />
            <span className="mt-1">About</span>
          </p>
        </div>
        <div className="menu-mobile-li mt-2">
          <p className="black-1 roboto p-16">
            <img src={imgMenu5} alt="" className="mr-2" />
            <span className="mt-1">Setting</span>
          </p>
        </div>
      </div>
    );
  };
}

export default withRouter(TopNavigation);
