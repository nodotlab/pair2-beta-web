import * as React from "react";
import { User } from "../reduce/User";

export interface ContextInterface {
  user: User;
  searchText: string;
  providerSearchText: any;
}

export const ctxt: any = React.createContext<ContextInterface | null>(null);
export const Provider = ctxt.Provider;
export const Consumer = ctxt.Consumer;