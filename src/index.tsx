import React from "react";
import { render, hydrate } from "react-dom";
import App from "./App";
import "react-app-polyfill/ie9";
import "./styles/index.css";
import "./styles/auth.css";
import "./styles/main.css";
import "./styles/buy.css";
import "./styles/faq.css";
import "./styles/game.css";
import "./styles/gamedetail.css";
import "./styles/release.css";
import "./styles/sneakerdetail.css";
import "./styles/sneakers.css";
import "./styles/search.css";
import "./styles/notice.css";
import "./styles/about.css";
import "./styles/mypage.css";
import "./styles/mglobal.css";
import "./styles/mpages.css";
import * as serviceWorker from "./serviceWorker";

const rootElement = document.getElementById("root")!;

if (rootElement.hasChildNodes()) {
    hydrate(<App />, rootElement);
  } else {
    render(<App />, rootElement);
  }

serviceWorker.unregister(); // If you want your app to work offline and load faster unregister change to register;
